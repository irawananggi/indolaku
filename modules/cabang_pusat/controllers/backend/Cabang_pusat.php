<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Cabang Pusat Controller
*| --------------------------------------------------------------------------
*| Cabang Pusat site
*|
*/
class Cabang_pusat extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_cabang_pusat');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Cabang Pusats
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('cabang_pusat_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['cabang_pusats'] = $this->model_cabang_pusat->get($filter, $field, $this->limit_page, $offset);
		$this->data['cabang_pusat_counts'] = $this->model_cabang_pusat->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/cabang_pusat/index/',
			'total_rows'   => $this->model_cabang_pusat->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Cabang Pusat List');
		$this->render('backend/standart/administrator/cabang_pusat/cabang_pusat_list', $this->data);
	}
	
	/**
	* Add new cabang_pusats
	*
	*/
	public function add()
	{
		$this->is_allowed('cabang_pusat_add');

		$this->template->title('Cabang Pusat New');
		$this->render('backend/standart/administrator/cabang_pusat/cabang_pusat_add', $this->data);
	}

	/**
	* Add New Cabang Pusats
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('cabang_pusat_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('cabang_pusat_ket', 'Keterangan', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'cabang_pusat_ket' => $this->input->post('cabang_pusat_ket'),
			];

			
			$save_cabang_pusat = $this->model_cabang_pusat->store($save_data);
            

			if ($save_cabang_pusat) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_cabang_pusat;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/cabang_pusat/edit/' . $save_cabang_pusat, 'Edit Cabang Pusat'),
						anchor('administrator/cabang_pusat', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/cabang_pusat/edit/' . $save_cabang_pusat, 'Edit Cabang Pusat')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/cabang_pusat');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/cabang_pusat');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Cabang Pusats
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('cabang_pusat_update');

		$this->data['cabang_pusat'] = $this->model_cabang_pusat->find($id);

		$this->template->title('Cabang Pusat Update');
		$this->render('backend/standart/administrator/cabang_pusat/cabang_pusat_update', $this->data);
	}

	/**
	* Update Cabang Pusats
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('cabang_pusat_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('cabang_pusat_ket', 'Keterangan', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'cabang_pusat_ket' => $this->input->post('cabang_pusat_ket'),
			];

			
			$save_cabang_pusat = $this->model_cabang_pusat->change($id, $save_data);

			if ($save_cabang_pusat) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/cabang_pusat', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/cabang_pusat');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/cabang_pusat');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Cabang Pusats
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('cabang_pusat_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'cabang_pusat'), 'success');
        } else {
            set_message(cclang('error_delete', 'cabang_pusat'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Cabang Pusats
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('cabang_pusat_view');

		$this->data['cabang_pusat'] = $this->model_cabang_pusat->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Cabang Pusat Detail');
		$this->render('backend/standart/administrator/cabang_pusat/cabang_pusat_view', $this->data);
	}
	
	/**
	* delete Cabang Pusats
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$cabang_pusat = $this->model_cabang_pusat->find($id);

		
		
		return $this->model_cabang_pusat->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('cabang_pusat_export');

		$this->model_cabang_pusat->export('cabang_pusat', 'cabang_pusat');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('cabang_pusat_export');

		$this->model_cabang_pusat->pdf('cabang_pusat', 'cabang_pusat');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('cabang_pusat_export');

		$table = $title = 'cabang_pusat';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_cabang_pusat->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file cabang_pusat.php */
/* Location: ./application/controllers/administrator/Cabang Pusat.php */