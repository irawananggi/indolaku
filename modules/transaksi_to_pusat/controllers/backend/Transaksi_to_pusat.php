<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Transaksi To Pusat Controller
*| --------------------------------------------------------------------------
*| Transaksi To Pusat site
*|
*/
class Transaksi_to_pusat extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_transaksi_to_pusat');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Transaksi To Pusats
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('transaksi_to_pusat_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['transaksi_to_pusats'] = $this->model_transaksi_to_pusat->get($filter, $field, $this->limit_page, $offset);
		$this->data['transaksi_to_pusat_counts'] = $this->model_transaksi_to_pusat->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/transaksi_to_pusat/index/',
			'total_rows'   => $this->model_transaksi_to_pusat->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Transaksi To Ke Pusat List');
		$this->render('backend/standart/administrator/transaksi_to_pusat/transaksi_to_pusat_list', $this->data);
	}
	
	/**
	* Add new transaksi_to_pusats
	*
	*/
	public function add()
	{
		$this->is_allowed('transaksi_to_pusat_add');

		$this->template->title('Transaksi To Ke Pusat New');
		$this->render('backend/standart/administrator/transaksi_to_pusat/transaksi_to_pusat_add', $this->data);
	}

	/**
	* Add New Transaksi To Pusats
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('transaksi_to_pusat_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('transaksi_to_id', 'TO', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_cabang_id', 'Cabang', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_note', 'Catatan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_longitude', 'Longitude', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_latitude', 'Latitude', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_total', 'Total', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_status', 'Status', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'transaksi_to_id' => $this->input->post('transaksi_to_id'),
				'transaksi_cabang_id' => $this->input->post('transaksi_cabang_id'),
				'transaksi_note' => $this->input->post('transaksi_note'),
				'transaksi_longitude' => $this->input->post('transaksi_longitude'),
				'transaksi_latitude' => $this->input->post('transaksi_latitude'),
				'transaksi_total' => $this->input->post('transaksi_total'),
				'transaksi_status' => $this->input->post('transaksi_status'),
				'transaksi_ongkir' => $this->input->post('transaksi_ongkir'),
				'transaksi_tanggal' => $this->input->post('transaksi_tanggal'),
			];

			
			$save_transaksi_to_pusat = $this->model_transaksi_to_pusat->store($save_data);
            

			if ($save_transaksi_to_pusat) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_transaksi_to_pusat;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/transaksi_to_pusat/edit/' . $save_transaksi_to_pusat, 'Edit Transaksi To Pusat'),
						anchor('administrator/transaksi_to_pusat', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/transaksi_to_pusat/edit/' . $save_transaksi_to_pusat, 'Edit Transaksi To Pusat')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/transaksi_to_pusat');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/transaksi_to_pusat');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Transaksi To Pusats
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('transaksi_to_pusat_update');

		$this->data['transaksi_to_pusat'] = $this->model_transaksi_to_pusat->find($id);

		$this->template->title('Transaksi To Ke Pusat Update');
		$this->render('backend/standart/administrator/transaksi_to_pusat/transaksi_to_pusat_update', $this->data);
	}

	/**
	* Update Transaksi To Pusats
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('transaksi_to_pusat_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('transaksi_to_id', 'TO', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_cabang_id', 'Cabang', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_note', 'Catatan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_longitude', 'Longitude', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_latitude', 'Latitude', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_total', 'Total', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_status', 'Status', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'transaksi_to_id' => $this->input->post('transaksi_to_id'),
				'transaksi_cabang_id' => $this->input->post('transaksi_cabang_id'),
				'transaksi_note' => $this->input->post('transaksi_note'),
				'transaksi_longitude' => $this->input->post('transaksi_longitude'),
				'transaksi_latitude' => $this->input->post('transaksi_latitude'),
				'transaksi_total' => $this->input->post('transaksi_total'),
				'transaksi_status' => $this->input->post('transaksi_status'),
				'transaksi_ongkir' => $this->input->post('transaksi_ongkir'),
				'transaksi_tanggal' => $this->input->post('transaksi_tanggal'),
			];

			
			$save_transaksi_to_pusat = $this->model_transaksi_to_pusat->change($id, $save_data);

			if ($save_transaksi_to_pusat) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/transaksi_to_pusat', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/transaksi_to_pusat');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/transaksi_to_pusat');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Transaksi To Pusats
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('transaksi_to_pusat_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'transaksi_to_pusat'), 'success');
        } else {
            set_message(cclang('error_delete', 'transaksi_to_pusat'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Transaksi To Pusats
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('transaksi_to_pusat_view');

		$this->data['transaksi_to_pusat'] = $this->model_transaksi_to_pusat->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Transaksi To Ke Pusat Detail');
		$this->render('backend/standart/administrator/transaksi_to_pusat/transaksi_to_pusat_view', $this->data);
	}
	
	
	public function view_produk($id)
	{

		$this->is_allowed('transaksi_to_pusat_view_produk');

		$this->data['transaksi_to_pusat'] = $this->model_transaksi_to_pusat->withquery("select * from transaksi_detail td, produk p where td.transaksi_jenis = 3 and td.transaksi_id = '".$id ."' and p.produk_id = td.transaksi_detail_product_id","result");

		$this->template->title('Transaksi TO Ke Pusat Detail');
		$this->render('backend/standart/administrator/transaksi_to_pusat/transaksi_to_pusat_view_produk', $this->data);
	}
	/**
	* delete Transaksi To Pusats
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$transaksi_to_pusat = $this->model_transaksi_to_pusat->find($id);

		
		
		return $this->model_transaksi_to_pusat->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('transaksi_to_pusat_export');

		$this->model_transaksi_to_pusat->export('transaksi_to_pusat', 'transaksi_to_pusat');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('transaksi_to_pusat_export');

		$this->model_transaksi_to_pusat->pdf('transaksi_to_pusat', 'transaksi_to_pusat');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('transaksi_to_pusat_export');

		$table = $title = 'transaksi_to_pusat';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_transaksi_to_pusat->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file transaksi_to_pusat.php */
/* Location: ./application/controllers/administrator/Transaksi To Pusat.php */