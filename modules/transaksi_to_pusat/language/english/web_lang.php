<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['transaksi_to_pusat'] = 'Transaksi To Ke Pusat';
$lang['transaksi_to_pusat_id'] = 'Transaksi To Pusat Id';
$lang['transaksi_to_id'] = 'TO';
$lang['transaksi_cabang_id'] = 'Cabang';
$lang['transaksi_note'] = 'Catatan';
$lang['transaksi_longitude'] = 'Longitude';
$lang['transaksi_latitude'] = 'Latitude';
$lang['transaksi_total'] = 'Total';
$lang['transaksi_status'] = 'Status';
$lang['transaksi_ongkir'] = 'Ongkir';
$lang['transaksi_tanggal'] = 'Tanggal';
