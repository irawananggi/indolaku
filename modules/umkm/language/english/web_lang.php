<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['umkm'] = 'Umkm';
$lang['umkm_id'] = 'Umkm Id';
$lang['umkm_nama'] = 'Nama';
$lang['umkm_email'] = 'Email';
$lang['umkm_password'] = 'Password';
$lang['umkm_telp'] = 'Telp';
$lang['umkm_province'] = 'Provinsi';
$lang['umkm_kota'] = 'Kota/Kab';
$lang['umkm_kec'] = 'Kecamatan';
$lang['umkm_kel'] = 'Kelurahan';
$lang['umkm_alamat'] = 'Alamat';
$lang['umkm_foto'] = 'Foto';
$lang['umkm_status'] = 'Status';
$lang['umkm_latitude'] = 'Latitude';
$lang['umkm_longitude'] = 'Longitude';
