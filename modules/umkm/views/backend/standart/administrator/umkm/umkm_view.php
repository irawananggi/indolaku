
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
//This page is a result of an autogenerated content made by running test.html with firefox.
function domo(){
 
   // Binding keys
   $('*').bind('keydown', 'Ctrl+e', function assets() {
      $('#btn_edit').trigger('click');
       return false;
   });

   $('*').bind('keydown', 'Ctrl+x', function assets() {
      $('#btn_back').trigger('click');
       return false;
   });
    
}


jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
      Umkm      <small><?= cclang('detail', ['Umkm']); ?> </small>
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class=""><a  href="<?= site_url('administrator/umkm'); ?>">Umkm</a></li>
      <li class="active"><?= cclang('detail'); ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
     
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">

               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                    
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/view.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Umkm</h3>
                     <h5 class="widget-user-desc">Detail Umkm</h5>
                     <hr>
                  </div>

                 
                  <div class="form-horizontal" name="form_umkm" id="form_umkm" >
                   
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Umkm Id </label>

                        <div class="col-sm-8">
                           <?= _ent($umkm->umkm_id); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Nama </label>

                        <div class="col-sm-8">
                           <?= _ent($umkm->umkm_nama); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Email </label>

                        <div class="col-sm-8">
                           <?= _ent($umkm->umkm_email); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Password </label>

                        <div class="col-sm-8">
                           <?= _ent($umkm->umkm_password); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Telp </label>

                        <div class="col-sm-8">
                           <?= _ent($umkm->umkm_telp); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Provinsi </label>

                        <div class="col-sm-8">
                           <?= _ent($umkm->reg_provinces_name); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Kota/Kab </label>

                        <div class="col-sm-8">
                           <?= _ent($umkm->reg_regencies_name); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Kecamatan </label>

                        <div class="col-sm-8">
                           <?= _ent($umkm->reg_districts_name); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Kelurahan </label>

                        <div class="col-sm-8">
                           <?= _ent($umkm->reg_villages_name); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Alamat </label>

                        <div class="col-sm-8">
                           <?= _ent($umkm->umkm_alamat); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label"> Foto </label>
                        <div class="col-sm-8">
                             <?php if (is_image($umkm->umkm_foto)): ?>
                              <a class="fancybox" rel="group" href="<?= BASE_URL . 'uploads/umkm/' . $umkm->umkm_foto; ?>">
                                <img src="<?= BASE_URL . 'uploads/umkm/' . $umkm->umkm_foto; ?>" class="image-responsive" alt="image umkm" title="umkm_foto umkm" width="40px">
                              </a>
                              <?php else: ?>
                              <label>
                                <a href="<?= BASE_URL . 'administrator/file/download/umkm/' . $umkm->umkm_foto; ?>">
                                 <img src="<?= get_icon_file($umkm->umkm_foto); ?>" class="image-responsive" alt="image umkm" title="umkm_foto <?= $umkm->umkm_foto; ?>" width="40px"> 
                               <?= $umkm->umkm_foto ?>
                               </a>
                               </label>
                              <?php endif; ?>
                        </div>
                    </div>
                                       
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Status </label>

                        <div class="col-sm-8">
                           <?= _ent($umkm->umkm_status); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Latitude </label>

                        <div class="col-sm-8">
                           <?= _ent($umkm->umkm_latitude); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">Longitude </label>

                        <div class="col-sm-8">
                           <?= _ent($umkm->umkm_longitude); ?>
                        </div>
                    </div>
                                        
                    <br>
                    <br>

                    <div class="view-nav">
                        <?php is_allowed('umkm_update', function() use ($umkm){?>
                        <a class="btn btn-flat btn-info btn_edit btn_action" id="btn_edit" data-stype='back' title="edit umkm (Ctrl+e)" href="<?= site_url('administrator/umkm/edit/'.$umkm->umkm_id); ?>"><i class="fa fa-edit" ></i> <?= cclang('update', ['Umkm']); ?> </a>
                        <?php }) ?>
                        <a class="btn btn-flat btn-default btn_action" id="btn_back" title="back (Ctrl+x)" href="<?= site_url('administrator/umkm/'); ?>"><i class="fa fa-undo" ></i> <?= cclang('go_list_button', ['Umkm']); ?></a>
                     </div>
                    
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->

      </div>
   </div>
</section>
<!-- /.content -->

<script>
    $(document).ready(function(){
      $('#btn_back').click(function(){
        swal({
            title: "<?= cclang('are_you_sure'); ?>",
            text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/umkm';
            }
          });
    
        return false;
      }); /*end btn cancel*/

      $('.btn_edit').click(function(){
         window.location.href = BASE_URL + 'administrator/umkm/edit/' + <?php echo $this->uri->segment(4)?>;
      });
}); /*end doc ready*/
</script>