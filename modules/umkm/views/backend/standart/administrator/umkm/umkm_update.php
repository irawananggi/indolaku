

<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Umkm        <small>Edit Umkm</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/umkm'); ?>">Umkm</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Umkm</h3>
                            <h5 class="widget-user-desc">Edit Umkm</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/umkm/edit_save/'.$this->uri->segment(4)), [
                            'name'    => 'form_umkm', 
                            'class'   => 'form-horizontal form-step', 
                            'id'      => 'form_umkm', 
                            'method'  => 'POST'
                            ]); ?>
                         
                                        
                                                <div class="form-group ">
                            <label for="umkm_survey" class="col-sm-2 control-label">Status 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="umkm_survey" id="umkm_survey" data-placeholder="Select Survey Status" >
                                    <option value=""></option>
                                    <option <?= $umkm->umkm_survey == "1" ? 'selected' :''; ?> value="1">Sudah</option>
                                    <option <?= $umkm->umkm_survey == "0" ? 'selected' :''; ?> value="2">Belum</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                              
                                                
                                                 <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                 <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script> 
	var firstload = 1;
	function pilihprovinsi(){
		var skota = '<?php echo $umkm->umkm_kota; ?>';
		//alert(skota);
		
		 $.ajax({
			type: "GET",
			data: {provinsi_id : $('#umkm_provinsi option:selected').val(), kota_id: skota, first: firstload },
			url: BASE_URL+ "administrator/umkm/getkota1/",
			success: function(data) {
				$('#umkm_kota').html(data);
				if(firstload){
					pilihkota();
				}else{
					$('#umkm_kecamatan').html("<option value=''>Pilih Kecamatan</option>");
					$('#umkm_kelurahan').html("<option value=''>Pilih Kelurahan</option>");
				}
				//alert(data);
			}
		});
	}
	function pilihkota(){
			var skecamatan = '<?php echo $umkm->umkm_kec; ?>';
			//alert(skecamatan);
			
			 $.ajax({
				type: "GET",
				data: {kota_id : $('#umkm_kota option:selected').val(), kecamatan_id: skecamatan, first: firstload},
				url: BASE_URL+ "administrator/umkm/getkecamatan1/",
				success: function(data) {
					//alert(firstload);
					$('#umkm_kecamatan').html(data);
					if(firstload){
						pilihkecamatan();
						firstload = 0;
					}else{
						$('#umkm_kelurahan').html("<option value=''>Pilih Kelurahan</option>");
					}
					//alert(data);
				}
			});
	}
	function pilihkecamatan(){
			var skelurahan = '<?php echo $umkm->umkm_kel; ?>';
			//alert(skelurahan);
			
			 $.ajax({
				type: "GET",
				data: {kecamatan_id : $('#umkm_kecamatan option:selected').val(), kelurahan_id: skelurahan, first: firstload},
				url: BASE_URL+ "administrator/umkm/getkelurahan1/",
				success: function(data) {
					$('#umkm_kelurahan').html(data);
					//alert(data);
				}
			});
	}
    $(document).ready(function(){
       
      pilihprovinsi();
      
             
      $('#btn_cancel').click(function(){
        swal({
            title: "Are you sure?",
            text: "the data that you have created will be in the exhaust!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/umkm';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_umkm = $('#form_umkm');
        var data_post = form_umkm.serializeArray();
        var save_type = $(this).attr('data-stype');
        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: form_umkm.attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          $('form').find('.form-group').removeClass('has-error');
          $('form').find('.error-input').remove();
          $('.steps li').removeClass('error');
          if(res.success) {
            var id = $('#umkm_image_galery').find('li').attr('qq-file-id');
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            $('.data_file_uuid').val('');
    
          } else {
            if (res.errors) {
               parseErrorField(res.errors);
            }
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
                     var params = {};
       params[csrf] = token;

       $('#umkm_umkm_foto_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/umkm/upload_umkm_foto_file',
              params : params
          },
          deleteFile: {
              enabled: true, // defaults to false
              endpoint: BASE_URL + '/administrator/umkm/delete_umkm_foto_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/umkm/get_umkm_foto_file/<?= $umkm->umkm_id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["*"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#umkm_umkm_foto_galery').fineUploader('getUuid', id);
                   $('#umkm_umkm_foto_uuid').val(uuid);
                   $('#umkm_umkm_foto_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#umkm_umkm_foto_uuid').val();
                  $.get(BASE_URL + '/administrator/umkm/delete_umkm_foto_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#umkm_umkm_foto_uuid').val('');
                  $('#umkm_umkm_foto_name').val('');
                }
              }
          }
      }); /*end umkm_foto galey*/
              
       
       

      async function chain(){
      }
       
      chain();


    
    
    }); /*end doc ready*/
</script>