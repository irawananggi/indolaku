<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Umkm Controller
*| --------------------------------------------------------------------------
*| Umkm site
*|
*/
class Umkm extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_umkm');
		$this->lang->load('web_lang', $this->current_lang);
	}

	function getkota($id)
    {
		$data = $this->model_umkm->withquery("select * from reg_regencies where province_id = '".$id."' order by name asc","result");
		echo "<option value=''>Pilih Kota</option>";
		foreach($data as $datalist){
			echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			
		}
    }
	function getkecamatan($id)
    {
		$data = $this->model_umkm->withquery("select * from reg_districts where regency_id  = '".$id."' order by name asc","result");
		echo "<option value=''>Pilih Kecamatan</option>";
		foreach($data as $datalist){
			echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			
		}
    }
	
	function getkelurahan($id)
    {
		$data = $this->model_umkm->withquery("select * from reg_villages where district_id  = '".$id."' order by name asc","result");
		echo "<option value=''>Pilih Kelurahan</option>";
		foreach($data as $datalist){
			echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			
		}
    }
	function getkota1()
    {
		
		$data = $this->model_umkm->withquery("select * from reg_regencies where province_id = '".$_GET['provinsi_id']."' order by name asc","result");
		echo "<option value=''>Pilih Kota</option>";
		foreach($data as $datalist){
			if($datalist->id == $_GET['kota_id'] && $_GET['first'] == 1){
				echo "<option value='".$datalist->id."' selected>".$datalist->name."</option>";
			}else{
				echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			}
			
		}
    }
	function getkecamatan1()
    {
		$data = $this->model_umkm->withquery("select * from reg_districts where regency_id  = '".$_GET['kota_id']."' order by name asc","result");
		echo "<option value=''>Pilih Kecamatan</option>";
		foreach($data as $datalist){
			if($datalist->id == $_GET['kecamatan_id']  && $_GET['first'] == 1 ){
				echo "<option value='".$datalist->id."' selected>".$datalist->name."</option>";
			}else{
				echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			}
			
		}
    }
	
	function getkelurahan1()
    {
		$data = $this->model_umkm->withquery("select * from reg_villages where district_id  = '".$_GET['kecamatan_id']."' order by name asc","result");
		echo "<option value=''>Pilih Kelurahan</option>";
		foreach($data as $datalist){
			
			if($datalist->id == $_GET['kelurahan_id']  && $_GET['first'] == 1){
				echo "<option value='".$datalist->id."' selected>".$datalist->name."</option>";
			}else{
				echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			}
		}
    }
	/**
	* show all Umkms
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('umkm_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['umkms'] = $this->model_umkm->get($filter, $field, $this->limit_page, $offset);
		$this->data['umkm_counts'] = $this->model_umkm->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/umkm/index/',
			'total_rows'   => $this->model_umkm->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Umkm List');
		$this->render('backend/standart/administrator/umkm/umkm_list', $this->data);
	}
	
	/**
	* Add new umkms
	*
	*/
	public function add()
	{
		$this->is_allowed('umkm_add');

		$this->template->title('Umkm New');
		$this->render('backend/standart/administrator/umkm/umkm_add', $this->data);
	}

	/**
	* Add New Umkms
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('umkm_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('umkm_nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('umkm_email', 'Email', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('umkm_password', 'Password', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('umkm_telp', 'Telp', 'trim|required|max_length[15]');
		$this->form_validation->set_rules('umkm_province', 'Provinsi', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('umkm_kota', 'Kota/Kab', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('umkm_kec', 'Kecamatan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('umkm_kel', 'Kelurahan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('umkm_alamat', 'Alamat', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('umkm_status', 'Status', 'trim|required');
		$this->form_validation->set_rules('umkm_latitude', 'Latitude', 'trim|max_length[255]');
		$this->form_validation->set_rules('umkm_longitude', 'Longitude', 'trim|max_length[255]');
		

		if ($this->form_validation->run()) {
			$umkm_umkm_foto_uuid = $this->input->post('umkm_umkm_foto_uuid');
			$umkm_umkm_foto_name = $this->input->post('umkm_umkm_foto_name');
		
			$save_data = [
				'umkm_nama' => $this->input->post('umkm_nama'),
				'umkm_email' => $this->input->post('umkm_email'),
				'umkm_password' => md5($this->input->post('umkm_password')),
				'umkm_telp' => $this->input->post('umkm_telp'),
				'umkm_province' => $this->input->post('umkm_province'),
				'umkm_kota' => $this->input->post('umkm_kota'),
				'umkm_kec' => $this->input->post('umkm_kec'),
				'umkm_kel' => $this->input->post('umkm_kel'),
				'umkm_alamat' => $this->input->post('umkm_alamat'),
				'umkm_status' => $this->input->post('umkm_status'),
				'umkm_latitude' => $this->input->post('umkm_latitude'),
				'umkm_longitude' => $this->input->post('umkm_longitude'),
				'umkm_token' => md5($this->input->post('umkm_email')."".$this->input->post('umkm_password')."".time()),
			];

			if (!is_dir(FCPATH . '/uploads/umkm/')) {
				mkdir(FCPATH . '/uploads/umkm/');
			}

			if (!empty($umkm_umkm_foto_name)) {
				$umkm_umkm_foto_name_copy = date('YmdHis') . '-' . $umkm_umkm_foto_name;

				rename(FCPATH . 'uploads/tmp/' . $umkm_umkm_foto_uuid . '/' . $umkm_umkm_foto_name, 
						FCPATH . 'uploads/umkm/' . $umkm_umkm_foto_name_copy);

				if (!is_file(FCPATH . '/uploads/umkm/' . $umkm_umkm_foto_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['umkm_foto'] = $umkm_umkm_foto_name_copy;
			}
		
			
			$save_umkm = $this->model_umkm->store($save_data);
            

			if ($save_umkm) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_umkm;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/umkm/edit/' . $save_umkm, 'Edit Umkm'),
						anchor('administrator/umkm', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/umkm/edit/' . $save_umkm, 'Edit Umkm')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/umkm');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/umkm');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Umkms
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('umkm_update');

		$this->data['umkm'] = $this->model_umkm->find($id);

		$this->template->title('Umkm Update');
		$this->render('backend/standart/administrator/umkm/umkm_update', $this->data);
	}

	/**
	* Update Umkms
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('umkm_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('umkm_survey', 'Survey', 'trim|required');
		
		if ($this->form_validation->run()) {
			$umkm_umkm_foto_uuid = $this->input->post('umkm_umkm_foto_uuid');
			$umkm_umkm_foto_name = $this->input->post('umkm_umkm_foto_name');
		
			$save_data = [
					'umkm_survey' => $this->input->post('umkm_survey')
				];
			
			$save_umkm = $this->model_umkm->change($id, $save_data);

			if ($save_umkm) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/umkm', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/umkm');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/umkm');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Umkms
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('umkm_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'umkm'), 'success');
        } else {
            set_message(cclang('error_delete', 'umkm'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Umkms
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('umkm_view');

		$this->data['umkm'] = $this->model_umkm->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Umkm Detail');
		$this->render('backend/standart/administrator/umkm/umkm_view', $this->data);
	}
	
	/**
	* delete Umkms
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$umkm = $this->model_umkm->find($id);

		if (!empty($umkm->umkm_foto)) {
			$path = FCPATH . '/uploads/umkm/' . $umkm->umkm_foto;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_umkm->remove($id);
	}
	
	/**
	* Upload Image Umkm	* 
	* @return JSON
	*/
	public function upload_umkm_foto_file()
	{
		if (!$this->is_allowed('umkm_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'umkm',
		]);
	}

	/**
	* Delete Image Umkm	* 
	* @return JSON
	*/
	public function delete_umkm_foto_file($uuid)
	{
		if (!$this->is_allowed('umkm_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'umkm_foto', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'umkm',
            'primary_key'       => 'umkm_id',
            'upload_path'       => 'uploads/umkm/'
        ]);
	}

	/**
	* Get Image Umkm	* 
	* @return JSON
	*/
	public function get_umkm_foto_file($id)
	{
		if (!$this->is_allowed('umkm_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$umkm = $this->model_umkm->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'umkm_foto', 
            'table_name'        => 'umkm',
            'primary_key'       => 'umkm_id',
            'upload_path'       => 'uploads/umkm/',
            'delete_endpoint'   => 'administrator/umkm/delete_umkm_foto_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('umkm_export');

		$this->model_umkm->export('umkm', 'umkm');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('umkm_export');

		$this->model_umkm->pdf('umkm', 'umkm');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('umkm_export');

		$table = $title = 'umkm';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_umkm->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file umkm.php */
/* Location: ./application/controllers/administrator/Umkm.php */