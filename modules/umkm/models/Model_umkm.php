<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_umkm extends MY_Model {

    private $primary_key    = 'umkm_id';
    private $table_name     = 'umkm';
    private $field_search   = ['umkm_nama', 'umkm_email', 'umkm_password', 'umkm_telp', 'umkm_province', 'umkm_kota', 'umkm_kec', 'umkm_kel', 'umkm_alamat', 'umkm_foto', 'umkm_status', 'umkm_latitude', 'umkm_longitude'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

	
	public function withquery($query1,$result)
	  {
		$query = $this->db->query($query1);
		if ($result=='result') {
		  return $query->result();
		}else {
			return $query->row();
		}
	}
    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "umkm.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "umkm.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "umkm.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "umkm.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "umkm.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "umkm.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('umkm.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('reg_provinces', 'reg_provinces.id = umkm.umkm_province', 'LEFT');
        $this->db->join('reg_regencies', 'reg_regencies.id = umkm.umkm_kota', 'LEFT');
        $this->db->join('reg_districts', 'reg_districts.id = umkm.umkm_kec', 'LEFT');
        $this->db->join('reg_villages', 'reg_villages.id = umkm.umkm_kel', 'LEFT');
        
        $this->db->select('umkm.*,reg_provinces.name as reg_provinces_name,reg_regencies.name as reg_regencies_name,reg_districts.name as reg_districts_name,reg_villages.name as reg_villages_name');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_umkm.php */
/* Location: ./application/models/Model_umkm.php */