<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_produk_per_cabang extends MY_Model {

    private $primary_key    = 'produk_per_cabang_id';
    private $table_name     = 'produk_per_cabang';
    private $field_search   = ['produk_per_cabang_produk_id', 'produk_per_cabang_cabang_id', 'produk_per_cabang_stok'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

	public function withquery($query1,$result)
	  {
		$query = $this->db->query($query1);
		if ($result=='result') {
		  return $query->result();
		}else {
			return $query->row();
		}
	}
    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "produk_per_cabang.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "produk_per_cabang.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "produk_per_cabang.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "produk_per_cabang.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "produk_per_cabang.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "produk_per_cabang.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('produk_per_cabang.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('produk', 'produk.produk_id = produk_per_cabang.produk_per_cabang_produk_id', 'LEFT');
        $this->db->join('cabang', 'cabang.cabang_id = produk_per_cabang.produk_per_cabang_cabang_id', 'LEFT');
        
        $this->db->select('produk_per_cabang.*,produk.produk_nama as produk_produk_nama,cabang.cabang_nama as cabang_cabang_nama');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_produk_per_cabang.php */
/* Location: ./application/models/Model_produk_per_cabang.php */