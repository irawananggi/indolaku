<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['produk_per_cabang'] = 'Produk Per Cabang';
$lang['produk_per_cabang_id'] = 'Produk Per Cabang Id';
$lang['produk_per_cabang_produk_id'] = 'Produk';
$lang['produk_per_cabang_cabang_id'] = 'Cabang';
$lang['produk_per_cabang_stok'] = 'Stok Dicabang';
