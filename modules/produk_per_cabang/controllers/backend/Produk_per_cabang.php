<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Produk Per Cabang Controller
*| --------------------------------------------------------------------------
*| Produk Per Cabang site
*|
*/
class Produk_per_cabang extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_produk_per_cabang');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Produk Per Cabangs
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('produk_per_cabang_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['produk_per_cabangs'] = $this->model_produk_per_cabang->get($filter, $field, $this->limit_page, $offset);
		$this->data['produk_per_cabang_counts'] = $this->model_produk_per_cabang->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/produk_per_cabang/index/',
			'total_rows'   => $this->model_produk_per_cabang->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Produk Per Cabang List');
		$this->render('backend/standart/administrator/produk_per_cabang/produk_per_cabang_list', $this->data);
	}
	
	/**
	* Add new produk_per_cabangs
	*
	*/
	public function add()
	{
		$this->is_allowed('produk_per_cabang_add');

		$this->template->title('Produk Per Cabang New');
		$this->render('backend/standart/administrator/produk_per_cabang/produk_per_cabang_add', $this->data);
	}

	/**
	* Add New Produk Per Cabangs
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('produk_per_cabang_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('produk_per_cabang_produk_id', 'Produk', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('produk_per_cabang_cabang_id', 'Cabang', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('produk_per_cabang_stok', 'Stok Dicabang', 'trim|required|max_length[11]');
		
		
		$cekdata = $this->model_produk_per_cabang->withquery("select * from produk_per_cabang where produk_per_cabang_produk_id = '".$this->input->post('produk_per_cabang_produk_id')."' and produk_per_cabang_cabang_id = '".$this->input->post('produk_per_cabang_cabang_id')."'", "result");
		if(count($cekdata) == 0){
			if ($this->form_validation->run()) {
			
				$save_data = [
					'produk_per_cabang_produk_id' => $this->input->post('produk_per_cabang_produk_id'),
					'produk_per_cabang_cabang_id' => $this->input->post('produk_per_cabang_cabang_id'),
					'produk_per_cabang_stok' => $this->input->post('produk_per_cabang_stok'),
				];

				
				$save_produk_per_cabang = $this->model_produk_per_cabang->store($save_data);
				

					
					if ($save_produk_per_cabang) {
						if ($this->input->post('save_type') == 'stay') {
							$this->data['success'] = true;
							$this->data['id'] 	   = $save_produk_per_cabang;
							$this->data['message'] = cclang('success_save_data_stay', [
								anchor('administrator/produk_per_cabang/edit/' . $save_produk_per_cabang, 'Edit Produk Per Cabang'),
								anchor('administrator/produk_per_cabang', ' Go back to list')
							]);
						} else {
							set_message(
								cclang('success_save_data_redirect', [
								anchor('administrator/produk_per_cabang/edit/' . $save_produk_per_cabang, 'Edit Produk Per Cabang')
							]), 'success');

							$this->data['success'] = true;
							$this->data['redirect'] = base_url('administrator/produk_per_cabang');
						}
					} else {
						if ($this->input->post('save_type') == 'stay') {
							$this->data['success'] = false;
							$this->data['message'] = cclang('data_not_change');
						} else {
							$this->data['success'] = false;
							$this->data['message'] = cclang('data_not_change');
							$this->data['redirect'] = base_url('administrator/produk_per_cabang');
						}
					}
				

			} else {
				$this->data['success'] = false;
				$this->data['message'] = 'Opss validation failed';
				$this->data['errors'] = $this->form_validation->error_array();
			}
		}else{
				
			$this->data['success'] = false;
			$this->data['message'] = 'Data Sudah ada';
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Produk Per Cabangs
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('produk_per_cabang_update');

		$this->data['produk_per_cabang'] = $this->model_produk_per_cabang->find($id);

		$this->template->title('Produk Per Cabang Update');
		$this->render('backend/standart/administrator/produk_per_cabang/produk_per_cabang_update', $this->data);
	}

	/**
	* Update Produk Per Cabangs
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('produk_per_cabang_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('produk_per_cabang_produk_id', 'Produk', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('produk_per_cabang_cabang_id', 'Cabang', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('produk_per_cabang_stok', 'Stok Dicabang', 'trim|required|max_length[11]');
		
		$cekdata = $this->model_produk_per_cabang->withquery("select * from produk_per_cabang where produk_per_cabang_produk_id = '".$this->input->post('produk_per_cabang_produk_id')."' and produk_per_cabang_cabang_id = '".$this->input->post('produk_per_cabang_cabang_id')."'", "result");
		if(count($cekdata) == 0){
			if ($this->form_validation->run()) {
			
				$save_data = [
					'produk_per_cabang_produk_id' => $this->input->post('produk_per_cabang_produk_id'),
					'produk_per_cabang_cabang_id' => $this->input->post('produk_per_cabang_cabang_id'),
					'produk_per_cabang_stok' => $this->input->post('produk_per_cabang_stok'),
				];

				
				$save_produk_per_cabang = $this->model_produk_per_cabang->change($id, $save_data);

				if ($save_produk_per_cabang) {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = true;
						$this->data['id'] 	   = $id;
						$this->data['message'] = cclang('success_update_data_stay', [
							anchor('administrator/produk_per_cabang', ' Go back to list')
						]);
					} else {
						set_message(
							cclang('success_update_data_redirect', [
						]), 'success');

						$this->data['success'] = true;
						$this->data['redirect'] = base_url('administrator/produk_per_cabang');
					}
				} else {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = false;
						$this->data['message'] = cclang('data_not_change');
					} else {
						$this->data['success'] = false;
						$this->data['message'] = cclang('data_not_change');
						$this->data['redirect'] = base_url('administrator/produk_per_cabang');
					}
				}
			} else {
				$this->data['success'] = false;
				$this->data['message'] = 'Opss validation failed';
				$this->data['errors'] = $this->form_validation->error_array();
			}
		}else{

			$cekstokprodukcabang = $this->mymodel->withquery("select * from produk_per_cabang where produk_per_cabang_id = '".$id."'","row");
			
			if($cekstokprodukcabang->produk_per_cabang_stok != $this->input->post('produk_per_cabang_stok')){
				$save_data = [
					'produk_per_cabang_stok' => $this->input->post('produk_per_cabang_stok'),
				];

				
				$save_produk_per_cabang = $this->model_produk_per_cabang->change($id, $save_data);
				set_message(
							cclang('success_update_data_redirect', [
						]), 'success');

						$this->data['success'] = true;
						$this->data['redirect'] = base_url('administrator/produk_per_cabang');
			}else{

				$this->data['success'] = false;
				$this->data['message'] = 'Data Sudah ada';
			}
			
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Produk Per Cabangs
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('produk_per_cabang_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'produk_per_cabang'), 'success');
        } else {
            set_message(cclang('error_delete', 'produk_per_cabang'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Produk Per Cabangs
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('produk_per_cabang_view');

		$this->data['produk_per_cabang'] = $this->model_produk_per_cabang->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Produk Per Cabang Detail');
		$this->render('backend/standart/administrator/produk_per_cabang/produk_per_cabang_view', $this->data);
	}
	
	/**
	* delete Produk Per Cabangs
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$produk_per_cabang = $this->model_produk_per_cabang->find($id);

		
		
		return $this->model_produk_per_cabang->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('produk_per_cabang_export');

		$this->model_produk_per_cabang->export('produk_per_cabang', 'produk_per_cabang');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('produk_per_cabang_export');

		$this->model_produk_per_cabang->pdf('produk_per_cabang', 'produk_per_cabang');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('produk_per_cabang_export');

		$table = $title = 'produk_per_cabang';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_produk_per_cabang->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file produk_per_cabang.php */
/* Location: ./application/controllers/administrator/Produk Per Cabang.php */