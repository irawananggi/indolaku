<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define( 'API_ACCESS_KEY', 'AAAAWyu_Puo:APA91bEbUprYzMYA4yV_RVzdsdQozDOEG1KcG-R1IVBvYn13UraC0Weu20vE37c-e1BDME9P-u9ap-70OERDxg7gzrVSGmWq__RRAnefzz02z-WyA92veVFm_Uj6GrNr236MvldG7wIe' );


/**
*| --------------------------------------------------------------------------
*| Customer Controller
*| --------------------------------------------------------------------------
*| Customer site
*|
*/
class Customer extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_customer');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Customers
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('customer_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['customers'] = $this->model_customer->get($filter, $field, $this->limit_page, $offset);
		$this->data['customer_counts'] = $this->model_customer->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/customer/index/',
			'total_rows'   => $this->model_customer->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Customer List');
		$this->render('backend/standart/administrator/customer/customer_list', $this->data);
	}
	
	/**
	* Add new customers
	*
	*/
	public function add()
	{
		$this->is_allowed('customer_add');

		$this->template->title('Customer New');
		$this->render('backend/standart/administrator/customer/customer_add', $this->data);
	}

	/**
	* Add New Customers
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('customer_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('customer_nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('customer_telp', 'Telp', 'trim|required|max_length[15]');
		$this->form_validation->set_rules('customer_latitude', 'Latitude', 'trim|max_length[255]');
		$this->form_validation->set_rules('customer_longitude', 'Longitude', 'trim|max_length[255]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'customer_nama' => $this->input->post('customer_nama'),
				'customer_telp' => $this->input->post('customer_telp'),
				'customer_latitude' => $this->input->post('customer_latitude'),
				'customer_longitude' => $this->input->post('customer_longitude'),
				'customer_token' => md5($this->input->post('customer_nama')."".$this->input->post('customer_telp')."".time()),
			];

			
			$save_customer = $this->model_customer->store($save_data);
            

			if ($save_customer) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_customer;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/customer/edit/' . $save_customer, 'Edit Customer'),
						anchor('administrator/customer', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/customer/edit/' . $save_customer, 'Edit Customer')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/customer');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/customer');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Customers
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('customer_update');

		$this->data['customer'] = $this->model_customer->find($id);

		$this->template->title('Customer Update');
		$this->render('backend/standart/administrator/customer/customer_update', $this->data);
	}

	/**
	* Update Customers
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('customer_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('customer_nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('customer_telp', 'Telp', 'trim|required|max_length[15]');
		$this->form_validation->set_rules('customer_latitude', 'Latitude', 'trim|max_length[255]');
		$this->form_validation->set_rules('customer_longitude', 'Longitude', 'trim|max_length[255]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'customer_nama' => $this->input->post('customer_nama'),
				'customer_telp' => $this->input->post('customer_telp'),
				'customer_latitude' => $this->input->post('customer_latitude'),
				'customer_longitude' => $this->input->post('customer_longitude'),
				'customer_survey' => $this->input->post('customer_survey')
			];

			
			$save_customer = $this->model_customer->change($id, $save_data);
			
			if($this->input->post('customer_survey') == 1){
	    		//send notif ke customer
	    		$getcustomer = $this->mymodel->getbywhere('customer',"customer_id",$id,'row');
	    		
	    		$title = 'Akun anda telah berhasil di survey';
	    		$desc = 'Akun anda '. $getcustomer->customer_nama;
	    		$id_fcm = $getcustomer->customer_fcm_id;
	    		$data = array(
	    			'title' => "Akun anda telah berhasil di survey", 
	    			'message' => "Akun anda " .$getcustomer->customer_nama . " ",
	    			'content' => array(
	    				"customer_id " => $getcustomer->customer_id , 
	    				"nama_cutomer" => $getcustomer->customer_nama
	    			)
	    		);
	    
	    		$Msg = array(
	                'body' => $desc,
	                'title' => $title
	            );
	    
	            $fcmFields = array(
	    	      'to' => $id_fcm,
	    	      'notification' => $Msg,
	    	      'data'=>$data
	    	    );
	    	    $headers = array(
	    	      'Authorization: key=' . API_ACCESS_KEY,
	    	      'Content-Type: application/json'
	    	    );
	    	    $ch = curl_init();
	    	    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
	    	    curl_setopt( $ch,CURLOPT_POST, true );
	    	    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	    	    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
	    	    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	    	    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
	    	    $result = curl_exec($ch );
	    	    curl_close( $ch );
	    
	    	    $cek_respon = explode(',',$result);
	    	    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
	            //echo $result."\n\n";
			}


			if ($save_customer) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/customer', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/customer');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/customer');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Customers
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('customer_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'customer'), 'success');
        } else {
            set_message(cclang('error_delete', 'customer'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Customers
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('customer_view');

		$this->data['customer'] = $this->model_customer->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Customer Detail');
		$this->render('backend/standart/administrator/customer/customer_view', $this->data);
	}
	
	/**
	* delete Customers
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$customer = $this->model_customer->find($id);

		
		
		return $this->model_customer->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('customer_export');

		$this->model_customer->export('customer', 'customer');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('customer_export');

		$this->model_customer->pdf('customer', 'customer');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('customer_export');

		$table = $title = 'customer';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_customer->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file customer.php */
/* Location: ./application/controllers/administrator/Customer.php */