<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['customer'] = 'Customer';
$lang['customer_id'] = 'Customer Id';
$lang['customer_nama'] = 'Nama';
$lang['customer_telp'] = 'Telp';
$lang['customer_latitude'] = 'Latitude';
$lang['customer_longitude'] = 'Longitude';
$lang['customer_token'] = 'Token';
$lang['customer_otp'] = 'OTP';
