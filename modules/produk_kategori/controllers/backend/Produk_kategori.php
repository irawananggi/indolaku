<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Produk Kategori Controller
*| --------------------------------------------------------------------------
*| Produk Kategori site
*|
*/
class Produk_kategori extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_produk_kategori');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Produk Kategoris
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('produk_kategori_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['produk_kategoris'] = $this->model_produk_kategori->get($filter, $field, $this->limit_page, $offset);
		$this->data['produk_kategori_counts'] = $this->model_produk_kategori->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/produk_kategori/index/',
			'total_rows'   => $this->model_produk_kategori->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Kategori List');
		$this->render('backend/standart/administrator/produk_kategori/produk_kategori_list', $this->data);
	}
	
	/**
	* Add new produk_kategoris
	*
	*/
	public function add()
	{
		$this->is_allowed('produk_kategori_add');

		$this->template->title('Kategori New');
		$this->render('backend/standart/administrator/produk_kategori/produk_kategori_add', $this->data);
	}

	/**
	* Add New Produk Kategoris
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('produk_kategori_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('produk_kategori_nama', 'Nama Kategori', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('produk_kategori_produk_kategori_icon_name', 'Icon', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
			$produk_kategori_produk_kategori_icon_uuid = $this->input->post('produk_kategori_produk_kategori_icon_uuid');
			$produk_kategori_produk_kategori_icon_name = $this->input->post('produk_kategori_produk_kategori_icon_name');
		
			$save_data = [
				'produk_kategori_nama' => $this->input->post('produk_kategori_nama'),
			];

			if (!is_dir(FCPATH . '/uploads/produk_kategori/')) {
				mkdir(FCPATH . '/uploads/produk_kategori/');
			}

			if (!empty($produk_kategori_produk_kategori_icon_name)) {
				$produk_kategori_produk_kategori_icon_name_copy = date('YmdHis') . '-' . $produk_kategori_produk_kategori_icon_name;

				rename(FCPATH . 'uploads/tmp/' . $produk_kategori_produk_kategori_icon_uuid . '/' . $produk_kategori_produk_kategori_icon_name, 
						FCPATH . 'uploads/produk_kategori/' . $produk_kategori_produk_kategori_icon_name_copy);

				if (!is_file(FCPATH . '/uploads/produk_kategori/' . $produk_kategori_produk_kategori_icon_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['produk_kategori_icon'] = $produk_kategori_produk_kategori_icon_name_copy;
			}
		
			
			$save_produk_kategori = $this->model_produk_kategori->store($save_data);
            

			if ($save_produk_kategori) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_produk_kategori;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/produk_kategori/edit/' . $save_produk_kategori, 'Edit Produk Kategori'),
						anchor('administrator/produk_kategori', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/produk_kategori/edit/' . $save_produk_kategori, 'Edit Produk Kategori')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/produk_kategori');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/produk_kategori');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Produk Kategoris
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('produk_kategori_update');

		$this->data['produk_kategori'] = $this->model_produk_kategori->find($id);

		$this->template->title('Kategori Update');
		$this->render('backend/standart/administrator/produk_kategori/produk_kategori_update', $this->data);
	}

	/**
	* Update Produk Kategoris
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('produk_kategori_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('produk_kategori_nama', 'Nama Kategori', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('produk_kategori_produk_kategori_icon_name', 'Icon', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
			$produk_kategori_produk_kategori_icon_uuid = $this->input->post('produk_kategori_produk_kategori_icon_uuid');
			$produk_kategori_produk_kategori_icon_name = $this->input->post('produk_kategori_produk_kategori_icon_name');
		
			$save_data = [
				'produk_kategori_nama' => $this->input->post('produk_kategori_nama'),
			];

			if (!is_dir(FCPATH . '/uploads/produk_kategori/')) {
				mkdir(FCPATH . '/uploads/produk_kategori/');
			}

			if (!empty($produk_kategori_produk_kategori_icon_uuid)) {
				$produk_kategori_produk_kategori_icon_name_copy = date('YmdHis') . '-' . $produk_kategori_produk_kategori_icon_name;

				rename(FCPATH . 'uploads/tmp/' . $produk_kategori_produk_kategori_icon_uuid . '/' . $produk_kategori_produk_kategori_icon_name, 
						FCPATH . 'uploads/produk_kategori/' . $produk_kategori_produk_kategori_icon_name_copy);

				if (!is_file(FCPATH . '/uploads/produk_kategori/' . $produk_kategori_produk_kategori_icon_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['produk_kategori_icon'] = $produk_kategori_produk_kategori_icon_name_copy;
			}
		
			
			$save_produk_kategori = $this->model_produk_kategori->change($id, $save_data);

			if ($save_produk_kategori) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/produk_kategori', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/produk_kategori');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/produk_kategori');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Produk Kategoris
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('produk_kategori_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'produk_kategori'), 'success');
        } else {
            set_message(cclang('error_delete', 'produk_kategori'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Produk Kategoris
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('produk_kategori_view');

		$this->data['produk_kategori'] = $this->model_produk_kategori->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Kategori Detail');
		$this->render('backend/standart/administrator/produk_kategori/produk_kategori_view', $this->data);
	}
	
	/**
	* delete Produk Kategoris
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$produk_kategori = $this->model_produk_kategori->find($id);

		if (!empty($produk_kategori->produk_kategori_icon)) {
			$path = FCPATH . '/uploads/produk_kategori/' . $produk_kategori->produk_kategori_icon;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_produk_kategori->remove($id);
	}
	
	/**
	* Upload Image Produk Kategori	* 
	* @return JSON
	*/
	public function upload_produk_kategori_icon_file()
	{
		if (!$this->is_allowed('produk_kategori_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'produk_kategori',
			'allowed_types' => 'jpg|jpeg|png',
		]);
	}

	/**
	* Delete Image Produk Kategori	* 
	* @return JSON
	*/
	public function delete_produk_kategori_icon_file($uuid)
	{
		if (!$this->is_allowed('produk_kategori_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'produk_kategori_icon', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'produk_kategori',
            'primary_key'       => 'produk_kategori_id',
            'upload_path'       => 'uploads/produk_kategori/'
        ]);
	}

	/**
	* Get Image Produk Kategori	* 
	* @return JSON
	*/
	public function get_produk_kategori_icon_file($id)
	{
		if (!$this->is_allowed('produk_kategori_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$produk_kategori = $this->model_produk_kategori->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'produk_kategori_icon', 
            'table_name'        => 'produk_kategori',
            'primary_key'       => 'produk_kategori_id',
            'upload_path'       => 'uploads/produk_kategori/',
            'delete_endpoint'   => 'administrator/produk_kategori/delete_produk_kategori_icon_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('produk_kategori_export');

		$this->model_produk_kategori->export('produk_kategori', 'produk_kategori');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('produk_kategori_export');

		$this->model_produk_kategori->pdf('produk_kategori', 'produk_kategori');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('produk_kategori_export');

		$table = $title = 'produk_kategori';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_produk_kategori->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file produk_kategori.php */
/* Location: ./application/controllers/administrator/Produk Kategori.php */