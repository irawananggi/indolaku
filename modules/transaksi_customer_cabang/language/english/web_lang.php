<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['transaksi_customer_cabang'] = 'Transaksi Customer Cabang';
$lang['transaksi_customer_cabang_id'] = 'Transaksi Customer Cabang Id';
$lang['transaksi_customer_id'] = 'Customer';
$lang['transaksi_cabang_id'] = 'Cabang';
$lang['transaksi_note'] = 'Catatan';
$lang['transaksi_kurir'] = 'Kurir';
$lang['transaksi_longitude'] = 'Longitude';
$lang['transaksi_latitude'] = 'Latitude';
$lang['transaksi_alamat'] = 'Alamat';
$lang['transaksi_total'] = 'Total';
$lang['transaksi_status'] = 'Status';
$lang['transaksi_ongkir'] = 'Ongkir';
$lang['transaksi_tanggal'] = 'Tanggal';
$lang['transaksi_bukti_trf'] = 'Bukti Transfer';
