<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Transaksi Customer Cabang Controller
*| --------------------------------------------------------------------------
*| Transaksi Customer Cabang site
*|
*/
class Transaksi_customer_cabang extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_transaksi_customer_cabang');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Transaksi Customer Cabangs
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('transaksi_customer_cabang_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['transaksi_customer_cabangs'] = $this->model_transaksi_customer_cabang->get($filter, $field, $this->limit_page, $offset);
		$this->data['transaksi_customer_cabang_counts'] = $this->model_transaksi_customer_cabang->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/transaksi_customer_cabang/index/',
			'total_rows'   => $this->model_transaksi_customer_cabang->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Transaksi Customer Cabang List');
		$this->render('backend/standart/administrator/transaksi_customer_cabang/transaksi_customer_cabang_list', $this->data);
	}
	
	/**
	* Add new transaksi_customer_cabangs
	*
	*/
	public function add()
	{
		$this->is_allowed('transaksi_customer_cabang_add');

		$this->template->title('Transaksi Customer Cabang New');
		$this->render('backend/standart/administrator/transaksi_customer_cabang/transaksi_customer_cabang_add', $this->data);
	}

	/**
	* Add New Transaksi Customer Cabangs
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('transaksi_customer_cabang_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('transaksi_customer_id', 'Customer', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_cabang_id', 'Cabang', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_note', 'Catatan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_kurir', 'Kurir', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_longitude', 'Longitude', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_latitude', 'Latitude', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_total', 'Total', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_status', 'Status', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
			$transaksi_customer_cabang_transaksi_bukti_trf_uuid = $this->input->post('transaksi_customer_cabang_transaksi_bukti_trf_uuid');
			$transaksi_customer_cabang_transaksi_bukti_trf_name = $this->input->post('transaksi_customer_cabang_transaksi_bukti_trf_name');
		
			$save_data = [
				'transaksi_customer_id' => $this->input->post('transaksi_customer_id'),
				'transaksi_cabang_id' => $this->input->post('transaksi_cabang_id'),
				'transaksi_note' => $this->input->post('transaksi_note'),
				'transaksi_kurir' => $this->input->post('transaksi_kurir'),
				'transaksi_longitude' => $this->input->post('transaksi_longitude'),
				'transaksi_latitude' => $this->input->post('transaksi_latitude'),
				'transaksi_alamat' => $this->input->post('transaksi_alamat'),
				'transaksi_total' => $this->input->post('transaksi_total'),
				'transaksi_status' => $this->input->post('transaksi_status'),
				'transaksi_ongkir' => $this->input->post('transaksi_ongkir'),
				'transaksi_tanggal' => $this->input->post('transaksi_tanggal'),
			];

			if (!is_dir(FCPATH . '/uploads/transaksi_customer_cabang/')) {
				mkdir(FCPATH . '/uploads/transaksi_customer_cabang/');
			}

			if (!empty($transaksi_customer_cabang_transaksi_bukti_trf_name)) {
				$transaksi_customer_cabang_transaksi_bukti_trf_name_copy = date('YmdHis') . '-' . $transaksi_customer_cabang_transaksi_bukti_trf_name;

				rename(FCPATH . 'uploads/tmp/' . $transaksi_customer_cabang_transaksi_bukti_trf_uuid . '/' . $transaksi_customer_cabang_transaksi_bukti_trf_name, 
						FCPATH . 'uploads/transaksi_customer_cabang/' . $transaksi_customer_cabang_transaksi_bukti_trf_name_copy);

				if (!is_file(FCPATH . '/uploads/transaksi_customer_cabang/' . $transaksi_customer_cabang_transaksi_bukti_trf_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['transaksi_bukti_trf'] = $transaksi_customer_cabang_transaksi_bukti_trf_name_copy;
			}
		
			
			$save_transaksi_customer_cabang = $this->model_transaksi_customer_cabang->store($save_data);
            

			if ($save_transaksi_customer_cabang) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_transaksi_customer_cabang;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/transaksi_customer_cabang/edit/' . $save_transaksi_customer_cabang, 'Edit Transaksi Customer Cabang'),
						anchor('administrator/transaksi_customer_cabang', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/transaksi_customer_cabang/edit/' . $save_transaksi_customer_cabang, 'Edit Transaksi Customer Cabang')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/transaksi_customer_cabang');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/transaksi_customer_cabang');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Transaksi Customer Cabangs
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('transaksi_customer_cabang_update');

		$this->data['transaksi_customer_cabang'] = $this->model_transaksi_customer_cabang->find($id);

		$this->template->title('Transaksi Customer Cabang Update');
		$this->render('backend/standart/administrator/transaksi_customer_cabang/transaksi_customer_cabang_update', $this->data);
	}

	/**
	* Update Transaksi Customer Cabangs
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('transaksi_customer_cabang_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('transaksi_status', 'Status', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
			
			$save_data = [
				'transaksi_status' => $this->input->post('transaksi_status')
			];

		
			
			$save_transaksi_customer_cabang = $this->model_transaksi_customer_cabang->change($id, $save_data);

			if ($save_transaksi_customer_cabang) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/transaksi_customer_cabang', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/transaksi_customer_cabang');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/transaksi_customer_cabang');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Transaksi Customer Cabangs
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('transaksi_customer_cabang_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'transaksi_customer_cabang'), 'success');
        } else {
            set_message(cclang('error_delete', 'transaksi_customer_cabang'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Transaksi Customer Cabangs
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('transaksi_customer_cabang_view');

		$this->data['transaksi_customer_cabang'] = $this->model_transaksi_customer_cabang->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Transaksi Customer Cabang Detail');
		$this->render('backend/standart/administrator/transaksi_customer_cabang/transaksi_customer_cabang_view', $this->data);
	}
	
	
	public function view_produk($id)
	{

		$this->is_allowed('transaksi_customer_cabang_view_produk');

		$this->data['transaksi_customer_cabang'] = $this->model_transaksi_customer_cabang->withquery("select * from transaksi_detail td, produk p where td.transaksi_jenis = 2 and td.transaksi_id = '".$id."' and p.produk_id = td.transaksi_detail_product_id","result");

		$this->template->title('Transaksi TO Ke Pusat Detail');
		$this->render('backend/standart/administrator/transaksi_customer_cabang/transaksi_customer_cabang_view_produk', $this->data);
	}
	/**
	* delete Transaksi Customer Cabangs
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$transaksi_customer_cabang = $this->model_transaksi_customer_cabang->find($id);

		if (!empty($transaksi_customer_cabang->transaksi_bukti_trf)) {
			$path = FCPATH . '/uploads/transaksi_customer_cabang/' . $transaksi_customer_cabang->transaksi_bukti_trf;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_transaksi_customer_cabang->remove($id);
	}
	
	/**
	* Upload Image Transaksi Customer Cabang	* 
	* @return JSON
	*/
	public function upload_transaksi_bukti_trf_file()
	{
		if (!$this->is_allowed('transaksi_customer_cabang_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'transaksi_customer_cabang',
		]);
	}

	/**
	* Delete Image Transaksi Customer Cabang	* 
	* @return JSON
	*/
	public function delete_transaksi_bukti_trf_file($uuid)
	{
		if (!$this->is_allowed('transaksi_customer_cabang_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'transaksi_bukti_trf', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'transaksi_customer_cabang',
            'primary_key'       => 'transaksi_customer_cabang_id',
            'upload_path'       => 'uploads/transaksi_customer_cabang/'
        ]);
	}

	/**
	* Get Image Transaksi Customer Cabang	* 
	* @return JSON
	*/
	public function get_transaksi_bukti_trf_file($id)
	{
		if (!$this->is_allowed('transaksi_customer_cabang_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$transaksi_customer_cabang = $this->model_transaksi_customer_cabang->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'transaksi_bukti_trf', 
            'table_name'        => 'transaksi_customer_cabang',
            'primary_key'       => 'transaksi_customer_cabang_id',
            'upload_path'       => 'uploads/transaksi_customer_cabang/',
            'delete_endpoint'   => 'administrator/transaksi_customer_cabang/delete_transaksi_bukti_trf_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('transaksi_customer_cabang_export');

		$this->model_transaksi_customer_cabang->export('transaksi_customer_cabang', 'transaksi_customer_cabang');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('transaksi_customer_cabang_export');

		$this->model_transaksi_customer_cabang->pdf('transaksi_customer_cabang', 'transaksi_customer_cabang');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('transaksi_customer_cabang_export');

		$table = $title = 'transaksi_customer_cabang';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_transaksi_customer_cabang->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file transaksi_customer_cabang.php */
/* Location: ./application/controllers/administrator/Transaksi Customer Cabang.php */