<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_transaksi_customer_cabang extends MY_Model {

    private $primary_key    = 'transaksi_customer_cabang_id';
    private $table_name     = 'transaksi_customer_cabang';
    private $field_search   = ['transaksi_customer_id', 'transaksi_cabang_id', 'transaksi_note', 'transaksi_kurir', 'transaksi_longitude', 'transaksi_latitude', 'transaksi_alamat', 'transaksi_total', 'transaksi_status', 'transaksi_ongkir', 'transaksi_tanggal', 'transaksi_bukti_trf'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

  public function withquery($query1,$result)
  {
    $query = $this->db->query($query1);
    if ($result=='result') {
      return $query->result();
    }else {
        return $query->row();
    }
  }
    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "transaksi_customer_cabang.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "transaksi_customer_cabang.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "transaksi_customer_cabang.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "transaksi_customer_cabang.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "transaksi_customer_cabang.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "transaksi_customer_cabang.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('transaksi_customer_cabang.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('customer', 'customer.customer_id = transaksi_customer_cabang.transaksi_customer_id', 'LEFT');
        $this->db->join('cabang', 'cabang.cabang_id = transaksi_customer_cabang.transaksi_cabang_id', 'LEFT');
        $this->db->join('kurir', 'kurir.kurir_id = transaksi_customer_cabang.transaksi_kurir', 'LEFT');
        $this->db->join('transaksi_status', 'transaksi_status.transaksi_status_id = transaksi_customer_cabang.transaksi_status', 'LEFT');
        
        $this->db->select('transaksi_customer_cabang.*,customer.customer_nama as customer_customer_nama,cabang.cabang_nama as cabang_cabang_nama,kurir.kurir_nama as kurir_kurir_nama,transaksi_status.transaksi_status_keterangan as transaksi_status_transaksi_status_keterangan');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_transaksi_customer_cabang.php */
/* Location: ./application/models/Model_transaksi_customer_cabang.php */