<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['transaksi_canvas_cabang'] = 'Transaksi Canvas Cabang';
$lang['transaksi_canvas_cabang_id'] = 'Transaksi Canvas Cabang Id';
$lang['transaksi_canvas_id'] = 'Canvas';
$lang['transaksi_cabang_id'] = 'Cabang';
$lang['transaksi_total'] = 'Total';
$lang['transaksi_status'] = 'Status';
$lang['transaksi_tanggal'] = ' Tanggal';
$lang['transaksi_note'] = 'Catatan';
