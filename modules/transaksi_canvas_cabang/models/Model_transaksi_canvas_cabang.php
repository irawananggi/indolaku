<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_transaksi_canvas_cabang extends MY_Model {

    private $primary_key    = 'transaksi_canvas_cabang_id';
    private $table_name     = 'transaksi_canvas_cabang';
    private $field_search   = ['transaksi_canvas_id', 'transaksi_cabang_id', 'transaksi_total', 'transaksi_status_canvascabang', 'transaksi_tanggal', 'transaksi_note'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "transaksi_canvas_cabang.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "transaksi_canvas_cabang.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "transaksi_canvas_cabang.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "transaksi_canvas_cabang.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "transaksi_canvas_cabang.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "transaksi_canvas_cabang.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('transaksi_canvas_cabang.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('kanvas', 'kanvas.kanvas_id = transaksi_canvas_cabang.transaksi_canvas_id', 'LEFT');
        $this->db->join('cabang', 'cabang.cabang_id = transaksi_canvas_cabang.transaksi_cabang_id', 'LEFT');
        $this->db->join('transaksi_status_canvascabang', 'transaksi_status_canvascabang.transaksi_status_canvascabang_id = transaksi_canvas_cabang.transaksi_status_canvascabang', 'LEFT');
        
        $this->db->select('transaksi_canvas_cabang.*,kanvas.kanvas_nama as kanvas_kanvas_nama,cabang.cabang_nama as cabang_cabang_nama,transaksi_status_canvascabang.transaksi_status_canvascabang_keterangan as transaksi_status_canvascabang_transaksi_status_canvascabang_keterangan');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_transaksi_canvas_cabang.php */
/* Location: ./application/models/Model_transaksi_canvas_cabang.php */