<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Transaksi Canvas Cabang Controller
*| --------------------------------------------------------------------------
*| Transaksi Canvas Cabang site
*|
*/
class Transaksi_canvas_cabang extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_transaksi_canvas_cabang');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Transaksi Canvas Cabangs
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('transaksi_canvas_cabang_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['transaksi_canvas_cabangs'] = $this->model_transaksi_canvas_cabang->get($filter, $field, $this->limit_page, $offset);
		$this->data['transaksi_canvas_cabang_counts'] = $this->model_transaksi_canvas_cabang->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/transaksi_canvas_cabang/index/',
			'total_rows'   => $this->model_transaksi_canvas_cabang->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Transaksi Canvas Cabang List');
		$this->render('backend/standart/administrator/transaksi_canvas_cabang/transaksi_canvas_cabang_list', $this->data);
	}
	
	/**
	* Add new transaksi_canvas_cabangs
	*
	*/
	public function add()
	{
		$this->is_allowed('transaksi_canvas_cabang_add');

		$this->template->title('Transaksi Canvas Cabang New');
		$this->render('backend/standart/administrator/transaksi_canvas_cabang/transaksi_canvas_cabang_add', $this->data);
	}

	/**
	* Add New Transaksi Canvas Cabangs
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('transaksi_canvas_cabang_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('transaksi_canvas_id', 'Canvas', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_cabang_id', 'Cabang', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_tanggal', ' Tanggal', 'trim|required');
		$this->form_validation->set_rules('transaksi_note', 'Catatan', 'trim|required|max_length[255]');
		$no_tracking = 'KTC-'.date('dmYHi').''.$this->input->post('transaksi_canvas_id');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'transaksi_canvas_id' => $this->input->post('transaksi_canvas_id'),
				'transaksi_cabang_id' => $this->input->post('transaksi_cabang_id'),
				'transaksi_status_canvascabang' => '1',
				'transaksi_no_tracking' => $no_tracking,
				'transaksi_tanggal' => $this->input->post('transaksi_tanggal'),
				'transaksi_note' => $this->input->post('transaksi_note'),
			];

			
			$save_transaksi_canvas_cabang = $this->model_transaksi_canvas_cabang->store($save_data);
			if ($save_transaksi_canvas_cabang) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_transaksi_canvas_cabang;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/transaksi_canvas_cabang/edit/' . $save_transaksi_canvas_cabang, 'Edit Transaksi Canvas Cabang'),
						anchor('administrator/transaksi_canvas_cabang', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/transaksi_canvas_cabang/edit/' . $save_transaksi_canvas_cabang, 'Edit Transaksi Canvas Cabang')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/transaksi_canvas_cabang');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/transaksi_canvas_cabang');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Transaksi Canvas Cabangs
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('transaksi_canvas_cabang_update');

		$this->data['transaksi_canvas_cabang'] = $this->model_transaksi_canvas_cabang->find($id);

		$this->template->title('Transaksi Canvas Cabang Update');
		$this->render('backend/standart/administrator/transaksi_canvas_cabang/transaksi_canvas_cabang_update', $this->data);
	}

	/**
	* Update Transaksi Canvas Cabangs
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('transaksi_canvas_cabang_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('transaksi_canvas_id', 'Canvas', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_cabang_id', 'Cabang', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_status_canvascabang', 'Status', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_tanggal', ' Tanggal', 'trim|required');
		$this->form_validation->set_rules('transaksi_note', 'Catatan', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
			$status = $this->input->post('transaksi_status_canvascabang');
			if($status == '5'){

				$cek_produk = $this->mymodel->withquery("select * from produk_canvas_cabang where transaksi_canvas_cabangid = '".$id."'","result");
				foreach ($cek_produk as $key => $value) {
					//update menambah stok kanvas
					$cek_produk_canvas = $this->mymodel->withquery("select * from produk_per_canvas where produk_per_canvas_produkid = '".$value->produk_canvas_cabang_produk_id."' and produk_per_canvas_canvasid='".$value->produk_canvas_cabang_canvas_id."'","row");
					$dataProdCan = array(
					   "produk_per_canvas_stok" => $cek_produk_canvas->produk_per_canvas_stok + $value->produk_canvas_cabang_qty
					);
					$up = $this->mymodel->update('produk_per_canvas',$dataProdCan,"produk_per_canvas_produkid='".$value->produk_canvas_cabang_produk_id."' AND produk_per_canvas_canvasid='".$value->produk_canvas_cabang_canvas_id."'");

					//update mengurangi stok kanvas di cabang
					$cek_produk_cabang = $this->mymodel->withquery("select * from produk_per_cabang where produk_per_cabang_produk_id = '".$value->produk_canvas_cabang_produk_id."' and produk_per_cabang_cabang_id='".$value->produk_canvas_cabang_cabang_id."'","row");
					$dataProdCab = array(
					   "produk_per_cabang_stok_canvas" => $value->produk_per_cabang_stok_canvas - $cek_produk_cabang->produk_canvas_cabang_qty
					);
					$up = $this->mymodel->update('produk_per_cabang',$dataProdCab,"produk_per_cabang_produk_id='".$value->produk_canvas_cabang_produk_id."' AND produk_per_cabang_cabang_id='".$value->produk_canvas_cabang_cabang_id."'");
				}
			}
			/*die();*/

			$save_data = [
				'transaksi_canvas_id' => $this->input->post('transaksi_canvas_id'),
				'transaksi_cabang_id' => $this->input->post('transaksi_cabang_id'),
				'transaksi_status_canvascabang' => $this->input->post('transaksi_status_canvascabang'),
				'transaksi_tanggal' => $this->input->post('transaksi_tanggal'),
				'transaksi_note' => $this->input->post('transaksi_note'),
			];

			
			$save_transaksi_canvas_cabang = $this->model_transaksi_canvas_cabang->change($id, $save_data);

			if ($save_transaksi_canvas_cabang) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/transaksi_canvas_cabang', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/transaksi_canvas_cabang');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/transaksi_canvas_cabang');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Transaksi Canvas Cabangs
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('transaksi_canvas_cabang_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'transaksi_canvas_cabang'), 'success');
        } else {
            set_message(cclang('error_delete', 'transaksi_canvas_cabang'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Transaksi Canvas Cabangs
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('transaksi_canvas_cabang_view');

		$this->data['transaksi_canvas_cabang'] = $this->model_transaksi_canvas_cabang->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Transaksi Canvas Cabang Detail');
		$this->render('backend/standart/administrator/transaksi_canvas_cabang/transaksi_canvas_cabang_view', $this->data);
	}
	
	/**
	* delete Transaksi Canvas Cabangs
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$transaksi_canvas_cabang = $this->model_transaksi_canvas_cabang->find($id);

		
		
		return $this->model_transaksi_canvas_cabang->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('transaksi_canvas_cabang_export');

		$this->model_transaksi_canvas_cabang->export('transaksi_canvas_cabang', 'transaksi_canvas_cabang');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('transaksi_canvas_cabang_export');

		$this->model_transaksi_canvas_cabang->pdf('transaksi_canvas_cabang', 'transaksi_canvas_cabang');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('transaksi_canvas_cabang_export');

		$table = $title = 'transaksi_canvas_cabang';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_transaksi_canvas_cabang->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

		/**
	* Update view Produk Canvas Cabangs
	*
	* @var $id String
	*/
	public function add_produk($id)
	{
		$p = un_de($id);
		$no_track = $p['no_track'];
		$this->is_allowed('transaksi_canvas_cabang_add_produk');
		$cek_id_trans = $this->mymodel->getbywhere('transaksi_canvas_cabang',"transaksi_no_tracking",$no_track,'row');

		$cek_data_produk  =  $this->mymodel->withquery("select * from produk_canvas_cabang where 
				   transaksi_canvas_cabangid = '".$cek_id_trans->transaksi_canvas_cabang_id."'","result");
        
        if(count($cek_data_produk) == 0){
        	$data_x = [];
	        foreach ($cek_data_produk as $key1 => $value1) {
	          $data_x[] = $value1->produk_canvas_cabang_produk_id;
	        }
	        $idProduk = implode(', ',$data_x);

			$this->data['produk'] =  $this->mymodel->withquery("select * from produk where produk_id","result");

        }else{
        	$data_x = [];
	        foreach ($cek_data_produk as $key1 => $value1) {
	          $data_x[] = $value1->produk_canvas_cabang_produk_id;
	        }
	        $idProduk = implode(', ',$data_x);

			$this->data['produk'] =  $this->mymodel->withquery("select * from produk where produk_id NOT IN ($idProduk)","result");
        }
		
			/*$this->data['produk'] = $this->mymodel->withquery("select * from produk where 
					   transaksi_jenis = '".$this->input->post('jenis')."' and
					   transaksi_id = '".$this->input->post('id_transaksi')."'","result");*/
		$this->data['produk_canvas_cabang'] = $this->mymodel->getbywhere('produk_canvas_cabang',"transaksi_canvas_cabangid",$cek_id_trans->transaksi_canvas_cabang_id,'result');
		$this->data['canvas'] =  $this->mymodel->getbywhere('kanvas',"kanvas_id",$cek_id_trans->transaksi_canvas_id,'row');
		$this->data['cabang'] =  $this->mymodel->getbywhere('cabang',"cabang_id",$cek_id_trans->transaksi_cabang_id,'row');
		$this->data['transaksi_canvas_cabang_id'] =  $cek_id_trans->transaksi_canvas_cabang_id;
		$this->data['id_trans'] =  $id;
		$this->template->title('Produk Canvas Di Cabang Update');
		$this->render('backend/standart/administrator/transaksi_canvas_cabang/transaksi_canvas_cabang_add_produk', $this->data);
	}

	public function save_add_produk()
	{
		if (!$this->is_allowed('transaksi_canvas_cabang_add_produk', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('produk_id', 'Produk', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('qty', 'Qty', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'transaksi_canvas_cabangid' => $this->input->post('transaksi_canvas_cabang_id'),
				'transaksi_canvas_cabang_produk_id' => $this->input->post('produk_id'),
				'transaksi_canvas_cabang_canvas_id' => $this->input->post('kanvas_id'),
				'transaksi_canvas_cabang_cabang_id' => $this->input->post('cabang_id'),
				'transaksi_canvas_cabang_qty' => $this->input->post('qty'),
			];

			
			$save_transaksi_canvas_cabang = $this->model_transaksi_canvas_cabang->store($save_data);
			$no_track = $this->input->post('id_trans');
			if ($save_transaksi_canvas_cabang) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $no_track;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/transaksi_canvas_cabang/add_produk/' . $no_track, 'Add Produk Canvas Cabang'),
						anchor('administrator/transaksi_canvas_cabang', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/transaksi_canvas_cabang/add_produk/' . $no_track, 'Add Produk Canvas Cabang')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/transaksi_canvas_cabang/add_produk/'.$no_track);
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/transaksi_canvas_cabang');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}

}


/* End of file transaksi_canvas_cabang.php */
/* Location: ./application/controllers/administrator/Transaksi Canvas Cabang.php */