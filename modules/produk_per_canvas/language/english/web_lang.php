<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['produk_per_canvas'] = 'Produk Per Canvas';
$lang['produk_per_canvasid'] = 'Produk Per Canvasid';
$lang['produk_per_canvas_produkid'] = 'Produk';
$lang['produk_per_canvas_canvasid'] = 'Kanvaser';
$lang['produk_per_canvas_stok'] = 'Stok';
