<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Produk Per Canvas Controller
*| --------------------------------------------------------------------------
*| Produk Per Canvas site
*|
*/
class Produk_per_canvas extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_produk_per_canvas');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Produk Per Canvass
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('produk_per_canvas_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['produk_per_canvass'] = $this->model_produk_per_canvas->get($filter, $field, $this->limit_page, $offset);
		$this->data['produk_per_canvas_counts'] = $this->model_produk_per_canvas->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/produk_per_canvas/index/',
			'total_rows'   => $this->model_produk_per_canvas->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Produk Per Canvas List');
		$this->render('backend/standart/administrator/produk_per_canvas/produk_per_canvas_list', $this->data);
	}
	
	/**
	* Add new produk_per_canvass
	*
	*/
	public function add()
	{
		$this->is_allowed('produk_per_canvas_add');

		$this->template->title('Produk Per Canvas New');
		$this->render('backend/standart/administrator/produk_per_canvas/produk_per_canvas_add', $this->data);
	}

	/**
	* Add New Produk Per Canvass
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('produk_per_canvas_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('produk_per_canvas_produkid', 'Produk', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('produk_per_canvas_canvasid', 'Kanvaser', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('produk_per_canvas_stok', 'Stok', 'trim|required|max_length[11]');
		

		$cekdata = $this->model_produk_per_canvas->withquery("select * from produk_per_canvas where produk_per_canvas_produkid = '".$this->input->post('produk_per_canvas_produkid')."' and produk_per_canvas_canvasid = '".$this->input->post('produk_per_canvas_canvasid')."'", "result");
		if(count($cekdata) == 0){
			if ($this->form_validation->run()) {
			
				$save_data = [
					'produk_per_canvas_produkid' => $this->input->post('produk_per_canvas_produkid'),
					'produk_per_canvas_canvasid' => $this->input->post('produk_per_canvas_canvasid'),
					'produk_per_canvas_stok' => $this->input->post('produk_per_canvas_stok'),
				];

				
				$save_produk_per_canvas = $this->model_produk_per_canvas->store($save_data);
				

				if ($save_produk_per_canvas) {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = true;
						$this->data['id'] 	   = $save_produk_per_canvas;
						$this->data['message'] = cclang('success_save_data_stay', [
							anchor('administrator/produk_per_canvas/edit/' . $save_produk_per_canvas, 'Edit Produk Per Canvas'),
							anchor('administrator/produk_per_canvas', ' Go back to list')
						]);
					} else {
						set_message(
							cclang('success_save_data_redirect', [
							anchor('administrator/produk_per_canvas/edit/' . $save_produk_per_canvas, 'Edit Produk Per Canvas')
						]), 'success');

						$this->data['success'] = true;
						$this->data['redirect'] = base_url('administrator/produk_per_canvas');
					}
				} else {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = false;
						$this->data['message'] = cclang('data_not_change');
					} else {
						$this->data['success'] = false;
						$this->data['message'] = cclang('data_not_change');
						$this->data['redirect'] = base_url('administrator/produk_per_canvas');
					}
				}

			} else {
				$this->data['success'] = false;
				$this->data['message'] = 'Opss validation failed';
				$this->data['errors'] = $this->form_validation->error_array();
			}
	    }else{
				
			$this->data['success'] = false;
			$this->data['message'] = 'Data Sudah ada';
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Produk Per Canvass
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('produk_per_canvas_update');

		$this->data['produk_per_canvas'] = $this->model_produk_per_canvas->find($id);

		$this->template->title('Produk Per Canvas Update');
		$this->render('backend/standart/administrator/produk_per_canvas/produk_per_canvas_update', $this->data);
	}

	/**
	* Update Produk Per Canvass
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('produk_per_canvas_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('produk_per_canvas_produkid', 'Produk', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('produk_per_canvas_canvasid', 'Kanvaser', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('produk_per_canvas_stok', 'Stok', 'trim|required|max_length[11]');
		

		$cekdata = $this->model_produk_per_canvas->withquery("select * from produk_per_canvas where produk_per_canvas_produkid = '".$this->input->post('produk_per_canvas_produkid')."' and produk_per_canvas_canvasid = '".$this->input->post('produk_per_canvas_canvasid')."'", "result");
		if(count($cekdata) == 0){
			if ($this->form_validation->run()) {
			
				$save_data = [
					'produk_per_canvas_produkid' => $this->input->post('produk_per_canvas_produkid'),
					'produk_per_canvas_canvasid' => $this->input->post('produk_per_canvas_canvasid'),
					'produk_per_canvas_stok' => $this->input->post('produk_per_canvas_stok'),
				];

				
				$save_produk_per_canvas = $this->model_produk_per_canvas->change($id, $save_data);

				if ($save_produk_per_canvas) {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = true;
						$this->data['id'] 	   = $id;
						$this->data['message'] = cclang('success_update_data_stay', [
							anchor('administrator/produk_per_canvas', ' Go back to list')
						]);
					} else {
						set_message(
							cclang('success_update_data_redirect', [
						]), 'success');

						$this->data['success'] = true;
						$this->data['redirect'] = base_url('administrator/produk_per_canvas');
					}
				} else {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = false;
						$this->data['message'] = cclang('data_not_change');
					} else {
						$this->data['success'] = false;
						$this->data['message'] = cclang('data_not_change');
						$this->data['redirect'] = base_url('administrator/produk_per_canvas');
					}
				}
			} else {
				$this->data['success'] = false;
				$this->data['message'] = 'Opss validation failed';
				$this->data['errors'] = $this->form_validation->error_array();
			}
		}else{
			$cekstokprodukcanvas = $this->mymodel->withquery("select * from produk_per_canvas where produk_per_canvasid = 26","row");
			if($cekstokprodukcanvas->produk_per_canvas_stok != $this->input->post('produk_per_canvas_stok')){
				$save_data = [
					'produk_per_canvas_stok' => $this->input->post('produk_per_canvas_stok'),
				];

				
				$save_produk_per_canvas = $this->model_produk_per_canvas->change($id, $save_data);
				set_message(
							cclang('success_update_data_redirect', [
						]), 'success');

						$this->data['success'] = true;
						$this->data['redirect'] = base_url('administrator/produk_per_canvas');
			}else{

				$this->data['success'] = false;
				$this->data['message'] = 'Data Sudah ada';
			}

		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Produk Per Canvass
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('produk_per_canvas_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'produk_per_canvas'), 'success');
        } else {
            set_message(cclang('error_delete', 'produk_per_canvas'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Produk Per Canvass
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('produk_per_canvas_view');

		$this->data['produk_per_canvas'] = $this->model_produk_per_canvas->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Produk Per Canvas Detail');
		$this->render('backend/standart/administrator/produk_per_canvas/produk_per_canvas_view', $this->data);
	}
	
	/**
	* delete Produk Per Canvass
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$produk_per_canvas = $this->model_produk_per_canvas->find($id);

		
		
		return $this->model_produk_per_canvas->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('produk_per_canvas_export');

		$this->model_produk_per_canvas->export('produk_per_canvas', 'produk_per_canvas');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('produk_per_canvas_export');

		$this->model_produk_per_canvas->pdf('produk_per_canvas', 'produk_per_canvas');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('produk_per_canvas_export');

		$table = $title = 'produk_per_canvas';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_produk_per_canvas->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file produk_per_canvas.php */
/* Location: ./application/controllers/administrator/Produk Per Canvas.php */