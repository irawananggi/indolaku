<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['transaksi_umkm_pusat'] = 'Transaksi Umkm Ke Pusat';
$lang['transaksi_umkm_pusat_id'] = 'Transaksi Umkm Pusat Id';
$lang['transaksi_umkm_id'] = 'UMKM';
$lang['transaksi_cabang_id'] = 'Cabang';
$lang['transaksi_note'] = 'Catatan';
$lang['transaksi_longitude'] = 'Longitude';
$lang['transaksi_latitude'] = 'Latitude';
$lang['transaksi_kurir'] = 'Kurir';
$lang['transaksi_total'] = 'Total';
$lang['transaksi_status'] = 'Status';
$lang['transaksi_ongkir'] = 'Ongkir';
$lang['transaksi_tanggal'] = 'Tanggal';
