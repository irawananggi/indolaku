<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Transaksi Umkm Pusat Controller
*| --------------------------------------------------------------------------
*| Transaksi Umkm Pusat site
*|
*/
class Transaksi_umkm_pusat extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_transaksi_umkm_pusat');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Transaksi Umkm Pusats
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('transaksi_umkm_pusat_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['transaksi_umkm_pusats'] = $this->model_transaksi_umkm_pusat->get($filter, $field, $this->limit_page, $offset);
		$this->data['transaksi_umkm_pusat_counts'] = $this->model_transaksi_umkm_pusat->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/transaksi_umkm_pusat/index/',
			'total_rows'   => $this->model_transaksi_umkm_pusat->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Transaksi Umkm Ke Pusat List');
		$this->render('backend/standart/administrator/transaksi_umkm_pusat/transaksi_umkm_pusat_list', $this->data);
	}
	
	/**
	* Add new transaksi_umkm_pusats
	*
	*/
	public function add()
	{
		$this->is_allowed('transaksi_umkm_pusat_add');

		$this->template->title('Transaksi Umkm Ke Pusat New');
		$this->render('backend/standart/administrator/transaksi_umkm_pusat/transaksi_umkm_pusat_add', $this->data);
	}

	/**
	* Add New Transaksi Umkm Pusats
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('transaksi_umkm_pusat_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('transaksi_umkm_id', 'UMKM', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_cabang_id', 'Cabang', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_note', 'Catatan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_longitude', 'Longitude', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_latitude', 'Latitude', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_kurir', 'Kurir', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_total', 'Total', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_status', 'Status', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'transaksi_umkm_id' => $this->input->post('transaksi_umkm_id'),
				'transaksi_cabang_id' => $this->input->post('transaksi_cabang_id'),
				'transaksi_note' => $this->input->post('transaksi_note'),
				'transaksi_longitude' => $this->input->post('transaksi_longitude'),
				'transaksi_latitude' => $this->input->post('transaksi_latitude'),
				'transaksi_kurir' => $this->input->post('transaksi_kurir'),
				'transaksi_total' => $this->input->post('transaksi_total'),
				'transaksi_status' => $this->input->post('transaksi_status'),
				'transaksi_ongkir' => $this->input->post('transaksi_ongkir'),
				'transaksi_tanggal' => $this->input->post('transaksi_tanggal'),
			];

			
			$save_transaksi_umkm_pusat = $this->model_transaksi_umkm_pusat->store($save_data);
            

			if ($save_transaksi_umkm_pusat) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_transaksi_umkm_pusat;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/transaksi_umkm_pusat/edit/' . $save_transaksi_umkm_pusat, 'Edit Transaksi Umkm Pusat'),
						anchor('administrator/transaksi_umkm_pusat', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/transaksi_umkm_pusat/edit/' . $save_transaksi_umkm_pusat, 'Edit Transaksi Umkm Pusat')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/transaksi_umkm_pusat');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/transaksi_umkm_pusat');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Transaksi Umkm Pusats
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('transaksi_umkm_pusat_update');

		$this->data['transaksi_umkm_pusat'] = $this->model_transaksi_umkm_pusat->find($id);

		$this->template->title('Transaksi Umkm Ke Pusat Update');
		$this->render('backend/standart/administrator/transaksi_umkm_pusat/transaksi_umkm_pusat_update', $this->data);
	}

	/**
	* Update Transaksi Umkm Pusats
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('transaksi_umkm_pusat_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('transaksi_umkm_id', 'UMKM', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_cabang_id', 'Cabang', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_note', 'Catatan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_longitude', 'Longitude', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_latitude', 'Latitude', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_kurir', 'Kurir', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_total', 'Total', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_status', 'Status', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'transaksi_umkm_id' => $this->input->post('transaksi_umkm_id'),
				'transaksi_cabang_id' => $this->input->post('transaksi_cabang_id'),
				'transaksi_note' => $this->input->post('transaksi_note'),
				'transaksi_longitude' => $this->input->post('transaksi_longitude'),
				'transaksi_latitude' => $this->input->post('transaksi_latitude'),
				'transaksi_kurir' => $this->input->post('transaksi_kurir'),
				'transaksi_total' => $this->input->post('transaksi_total'),
				'transaksi_status' => $this->input->post('transaksi_status'),
				'transaksi_ongkir' => $this->input->post('transaksi_ongkir'),
				'transaksi_tanggal' => $this->input->post('transaksi_tanggal'),
			];

			
			$save_transaksi_umkm_pusat = $this->model_transaksi_umkm_pusat->change($id, $save_data);

			if ($save_transaksi_umkm_pusat) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/transaksi_umkm_pusat', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/transaksi_umkm_pusat');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/transaksi_umkm_pusat');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Transaksi Umkm Pusats
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('transaksi_umkm_pusat_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'transaksi_umkm_pusat'), 'success');
        } else {
            set_message(cclang('error_delete', 'transaksi_umkm_pusat'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Transaksi Umkm Pusats
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('transaksi_umkm_pusat_view');

		$this->data['transaksi_umkm_pusat'] = $this->model_transaksi_umkm_pusat->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Transaksi Umkm Ke Pusat Detail');
		$this->render('backend/standart/administrator/transaksi_umkm_pusat/transaksi_umkm_pusat_view', $this->data);
	}
	
	public function view_produk($id)
	{

		$this->is_allowed('transaksi_umkm_pusat_view_produk');

		$this->data['transaksi_umkm_pusat'] = $this->model_transaksi_umkm_pusat->withquery("select * from transaksi_detail td, produk p where td.transaksi_jenis = 4 and td.transaksi_id = '".$id ."' and p.produk_id = td.transaksi_detail_product_id","result");

		$this->template->title('Transaksi Umkm Ke Pusat Detail');
		$this->render('backend/standart/administrator/transaksi_umkm_pusat/transaksi_umkm_pusat_view_produk', $this->data);
	}
	
	/**
	* delete Transaksi Umkm Pusats
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$transaksi_umkm_pusat = $this->model_transaksi_umkm_pusat->find($id);

		
		
		return $this->model_transaksi_umkm_pusat->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('transaksi_umkm_pusat_export');

		$this->model_transaksi_umkm_pusat->export('transaksi_umkm_pusat', 'transaksi_umkm_pusat');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('transaksi_umkm_pusat_export');

		$this->model_transaksi_umkm_pusat->pdf('transaksi_umkm_pusat', 'transaksi_umkm_pusat');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('transaksi_umkm_pusat_export');

		$table = $title = 'transaksi_umkm_pusat';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_transaksi_umkm_pusat->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file transaksi_umkm_pusat.php */
/* Location: ./application/controllers/administrator/Transaksi Umkm Pusat.php */