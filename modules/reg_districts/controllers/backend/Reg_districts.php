<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Reg Districts Controller
*| --------------------------------------------------------------------------
*| Reg Districts site
*|
*/
class Reg_districts extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_reg_districts');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Reg Districtss
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('reg_districts_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['reg_districtss'] = $this->model_reg_districts->get($filter, $field, $this->limit_page, $offset);
		$this->data['reg_districts_counts'] = $this->model_reg_districts->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/reg_districts/index/',
			'total_rows'   => $this->model_reg_districts->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Reg Districts List');
		$this->render('backend/standart/administrator/reg_districts/reg_districts_list', $this->data);
	}
	
	/**
	* Add new reg_districtss
	*
	*/
	public function add()
	{
		$this->is_allowed('reg_districts_add');

		$this->template->title('Reg Districts New');
		$this->render('backend/standart/administrator/reg_districts/reg_districts_add', $this->data);
	}

	/**
	* Add New Reg Districtss
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('reg_districts_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('regency_id', 'Regency Id', 'trim|required|max_length[5]');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'regency_id' => $this->input->post('regency_id'),
				'name' => $this->input->post('name'),
			];

			
			$save_reg_districts = $this->model_reg_districts->store($save_data);
            

			if ($save_reg_districts) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_reg_districts;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/reg_districts/edit/' . $save_reg_districts, 'Edit Reg Districts'),
						anchor('administrator/reg_districts', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/reg_districts/edit/' . $save_reg_districts, 'Edit Reg Districts')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/reg_districts');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/reg_districts');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Reg Districtss
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('reg_districts_update');

		$this->data['reg_districts'] = $this->model_reg_districts->find($id);

		$this->template->title('Reg Districts Update');
		$this->render('backend/standart/administrator/reg_districts/reg_districts_update', $this->data);
	}

	/**
	* Update Reg Districtss
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('reg_districts_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('regency_id', 'Regency Id', 'trim|required|max_length[5]');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'regency_id' => $this->input->post('regency_id'),
				'name' => $this->input->post('name'),
			];

			
			$save_reg_districts = $this->model_reg_districts->change($id, $save_data);

			if ($save_reg_districts) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/reg_districts', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/reg_districts');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/reg_districts');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Reg Districtss
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('reg_districts_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'reg_districts'), 'success');
        } else {
            set_message(cclang('error_delete', 'reg_districts'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Reg Districtss
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('reg_districts_view');

		$this->data['reg_districts'] = $this->model_reg_districts->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Reg Districts Detail');
		$this->render('backend/standart/administrator/reg_districts/reg_districts_view', $this->data);
	}
	
	/**
	* delete Reg Districtss
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$reg_districts = $this->model_reg_districts->find($id);

		
		
		return $this->model_reg_districts->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('reg_districts_export');

		$this->model_reg_districts->export('reg_districts', 'reg_districts');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('reg_districts_export');

		$this->model_reg_districts->pdf('reg_districts', 'reg_districts');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('reg_districts_export');

		$table = $title = 'reg_districts';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_reg_districts->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file reg_districts.php */
/* Location: ./application/controllers/administrator/Reg Districts.php */