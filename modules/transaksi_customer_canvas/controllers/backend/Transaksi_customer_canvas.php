<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Transaksi Customer Canvas Controller
*| --------------------------------------------------------------------------
*| Transaksi Customer Canvas site
*|
*/
class Transaksi_customer_canvas extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_transaksi_customer_canvas');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Transaksi Customer Canvass
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('transaksi_customer_canvas_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['transaksi_customer_canvass'] = $this->model_transaksi_customer_canvas->get($filter, $field, $this->limit_page, $offset);
		$this->data['transaksi_customer_canvas_counts'] = $this->model_transaksi_customer_canvas->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/transaksi_customer_canvas/index/',
			'total_rows'   => $this->model_transaksi_customer_canvas->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Transaksi Customer Canvas List');
		$this->render('backend/standart/administrator/transaksi_customer_canvas/transaksi_customer_canvas_list', $this->data);
	}
	
	/**
	* Add new transaksi_customer_canvass
	*
	*/
	public function add()
	{
		$this->is_allowed('transaksi_customer_canvas_add');

		$this->template->title('Transaksi Customer Canvas New');
		$this->render('backend/standart/administrator/transaksi_customer_canvas/transaksi_customer_canvas_add', $this->data);
	}

	/**
	* Add New Transaksi Customer Canvass
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('transaksi_customer_canvas_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('transaksi_customer_id', 'Customer', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_canvas_id', 'Canvas', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_note', 'Catatan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_total', 'Total', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_longitude', 'Longitude', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_latitude', 'Latitude', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('transaksi_status', 'Status', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
			$transaksi_customer_canvas_transaksi_bukti_trf_uuid = $this->input->post('transaksi_customer_canvas_transaksi_bukti_trf_uuid');
			$transaksi_customer_canvas_transaksi_bukti_trf_name = $this->input->post('transaksi_customer_canvas_transaksi_bukti_trf_name');
		
			$save_data = [
				'transaksi_customer_id' => $this->input->post('transaksi_customer_id'),
				'transaksi_canvas_id' => $this->input->post('transaksi_canvas_id'),
				'transaksi_note' => $this->input->post('transaksi_note'),
				'transaksi_total' => $this->input->post('transaksi_total'),
				'transaksi_longitude' => $this->input->post('transaksi_longitude'),
				'transaksi_latitude' => $this->input->post('transaksi_latitude'),
				'transaksi_alamat' => $this->input->post('transaksi_alamat'),
				'transaksi_status' => $this->input->post('transaksi_status'),
				'transaksi_ongkir' => $this->input->post('transaksi_ongkir'),
				'transaksi_tanggal' => $this->input->post('transaksi_tanggal'),
			];

			if (!is_dir(FCPATH . '/uploads/transaksi_customer_canvas/')) {
				mkdir(FCPATH . '/uploads/transaksi_customer_canvas/');
			}

			if (!empty($transaksi_customer_canvas_transaksi_bukti_trf_name)) {
				$transaksi_customer_canvas_transaksi_bukti_trf_name_copy = date('YmdHis') . '-' . $transaksi_customer_canvas_transaksi_bukti_trf_name;

				rename(FCPATH . 'uploads/tmp/' . $transaksi_customer_canvas_transaksi_bukti_trf_uuid . '/' . $transaksi_customer_canvas_transaksi_bukti_trf_name, 
						FCPATH . 'uploads/transaksi_customer_canvas/' . $transaksi_customer_canvas_transaksi_bukti_trf_name_copy);

				if (!is_file(FCPATH . '/uploads/transaksi_customer_canvas/' . $transaksi_customer_canvas_transaksi_bukti_trf_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['transaksi_bukti_trf'] = $transaksi_customer_canvas_transaksi_bukti_trf_name_copy;
			}
		
			
			$save_transaksi_customer_canvas = $this->model_transaksi_customer_canvas->store($save_data);
            

			if ($save_transaksi_customer_canvas) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_transaksi_customer_canvas;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/transaksi_customer_canvas/edit/' . $save_transaksi_customer_canvas, 'Edit Transaksi Customer Canvas'),
						anchor('administrator/transaksi_customer_canvas', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/transaksi_customer_canvas/edit/' . $save_transaksi_customer_canvas, 'Edit Transaksi Customer Canvas')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/transaksi_customer_canvas');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/transaksi_customer_canvas');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Transaksi Customer Canvass
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('transaksi_customer_canvas_update');

		$this->data['transaksi_customer_canvas'] = $this->model_transaksi_customer_canvas->find($id);

		$this->template->title('Transaksi Customer Canvas Update');
		$this->render('backend/standart/administrator/transaksi_customer_canvas/transaksi_customer_canvas_update', $this->data);
	}

	/**
	* Update Transaksi Customer Canvass
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('transaksi_customer_canvas_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('transaksi_status', 'Status', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
			
			$save_data = [
				'transaksi_status' => $this->input->post('transaksi_status')
			];

			
			
			$save_transaksi_customer_canvas = $this->model_transaksi_customer_canvas->change($id, $save_data);

			if ($save_transaksi_customer_canvas) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/transaksi_customer_canvas', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/transaksi_customer_canvas');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/transaksi_customer_canvas');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Transaksi Customer Canvass
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('transaksi_customer_canvas_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'transaksi_customer_canvas'), 'success');
        } else {
            set_message(cclang('error_delete', 'transaksi_customer_canvas'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Transaksi Customer Canvass
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('transaksi_customer_canvas_view');

		$this->data['transaksi_customer_canvas'] = $this->model_transaksi_customer_canvas->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Transaksi Customer Canvas Detail');
		$this->render('backend/standart/administrator/transaksi_customer_canvas/transaksi_customer_canvas_view', $this->data);
	}
	
	public function view_produk($id)
	{

		$this->is_allowed('transaksi_customer_canvas_view_produk');

		$this->data['transaksi_customer_canvas'] = $this->model_transaksi_customer_canvas->withquery("select * from transaksi_detail td, produk p where td.transaksi_jenis = 1 and td.transaksi_id = '".$id."' and p.produk_id = td.transaksi_detail_product_id","result");

		$this->template->title('Transaksi TO Ke Pusat Detail');
		$this->render('backend/standart/administrator/transaksi_customer_canvas/transaksi_customer_canvas_view_produk', $this->data);
	}
	/**
	* delete Transaksi Customer Canvass
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$transaksi_customer_canvas = $this->model_transaksi_customer_canvas->find($id);

		if (!empty($transaksi_customer_canvas->transaksi_bukti_trf)) {
			$path = FCPATH . '/uploads/transaksi_customer_canvas/' . $transaksi_customer_canvas->transaksi_bukti_trf;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_transaksi_customer_canvas->remove($id);
	}
	
	/**
	* Upload Image Transaksi Customer Canvas	* 
	* @return JSON
	*/
	public function upload_transaksi_bukti_trf_file()
	{
		if (!$this->is_allowed('transaksi_customer_canvas_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'transaksi_customer_canvas',
		]);
	}

	/**
	* Delete Image Transaksi Customer Canvas	* 
	* @return JSON
	*/
	public function delete_transaksi_bukti_trf_file($uuid)
	{
		if (!$this->is_allowed('transaksi_customer_canvas_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'transaksi_bukti_trf', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'transaksi_customer_canvas',
            'primary_key'       => 'transaksi_customer_canvas_id',
            'upload_path'       => 'uploads/transaksi_customer_canvas/'
        ]);
	}

	/**
	* Get Image Transaksi Customer Canvas	* 
	* @return JSON
	*/
	public function get_transaksi_bukti_trf_file($id)
	{
		if (!$this->is_allowed('transaksi_customer_canvas_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$transaksi_customer_canvas = $this->model_transaksi_customer_canvas->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'transaksi_bukti_trf', 
            'table_name'        => 'transaksi_customer_canvas',
            'primary_key'       => 'transaksi_customer_canvas_id',
            'upload_path'       => 'uploads/transaksi_customer_canvas/',
            'delete_endpoint'   => 'administrator/transaksi_customer_canvas/delete_transaksi_bukti_trf_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('transaksi_customer_canvas_export');

		$this->model_transaksi_customer_canvas->export('transaksi_customer_canvas', 'transaksi_customer_canvas');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('transaksi_customer_canvas_export');

		$this->model_transaksi_customer_canvas->pdf('transaksi_customer_canvas', 'transaksi_customer_canvas');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('transaksi_customer_canvas_export');

		$table = $title = 'transaksi_customer_canvas';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_transaksi_customer_canvas->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file transaksi_customer_canvas.php */
/* Location: ./application/controllers/administrator/Transaksi Customer Canvas.php */