
<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Transaksi Customer Canvas        <small><?= cclang('new', ['Transaksi Customer Canvas']); ?> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/transaksi_customer_canvas'); ?>">Transaksi Customer Canvas</a></li>
        <li class="active"><?= cclang('new'); ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Transaksi Customer Canvas</h3>
                            <h5 class="widget-user-desc"><?= cclang('new', ['Transaksi Customer Canvas']); ?></h5>
                            <hr>
                        </div>
                        <?= form_open('', [
                            'name'    => 'form_transaksi_customer_canvas', 
                            'class'   => 'form-horizontal form-step', 
                            'id'      => 'form_transaksi_customer_canvas', 
                            'enctype' => 'multipart/form-data', 
                            'method'  => 'POST'
                            ]); ?>
                         
                                                <div class="form-group ">
                            <label for="transaksi_customer_id" class="col-sm-2 control-label">Customer 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="transaksi_customer_id" id="transaksi_customer_id" data-placeholder="Select Customer" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('customer') as $row): ?>
                                    <option value="<?= $row->customer_id ?>"><?= $row->customer_nama; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Transaksi Customer Id</b> Max Length : 11.</small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="transaksi_canvas_id" class="col-sm-2 control-label">Canvas 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="transaksi_canvas_id" id="transaksi_canvas_id" data-placeholder="Select Canvas" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('kanvas') as $row): ?>
                                    <option value="<?= $row->kanvas_id ?>"><?= $row->kanvas_nama; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Transaksi Canvas Id</b> Max Length : 11.</small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="transaksi_note" class="col-sm-2 control-label">Catatan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="transaksi_note" id="transaksi_note" placeholder="Catatan" value="<?= set_value('transaksi_note'); ?>">
                                <small class="info help-block">
                                <b>Input Transaksi Note</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="transaksi_total" class="col-sm-2 control-label">Total 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="transaksi_total" id="transaksi_total" placeholder="Total" value="<?= set_value('transaksi_total'); ?>">
                                <small class="info help-block">
                                <b>Input Transaksi Total</b> Max Length : 11.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="transaksi_longitude" class="col-sm-2 control-label">Longitude 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="transaksi_longitude" id="transaksi_longitude" placeholder="Longitude" value="<?= set_value('transaksi_longitude'); ?>">
                                <small class="info help-block">
                                <b>Input Transaksi Longitude</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="transaksi_latitude" class="col-sm-2 control-label">Latitude 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="transaksi_latitude" id="transaksi_latitude" placeholder="Latitude" value="<?= set_value('transaksi_latitude'); ?>">
                                <small class="info help-block">
                                <b>Input Transaksi Latitude</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="transaksi_alamat" class="col-sm-2 control-label">Alamat 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="transaksi_alamat" id="transaksi_alamat" placeholder="Alamat" value="<?= set_value('transaksi_alamat'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="transaksi_status" class="col-sm-2 control-label">Status 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="transaksi_status" id="transaksi_status" data-placeholder="Select Status" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('transaksi_status') as $row): ?>
                                    <option value="<?= $row->transaksi_status_id ?>"><?= $row->transaksi_status_keterangan; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Transaksi Status</b> Max Length : 11.</small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="transaksi_ongkir" class="col-sm-2 control-label">Ongkir 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="transaksi_ongkir" id="transaksi_ongkir" placeholder="Ongkir" value="<?= set_value('transaksi_ongkir'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="transaksi_tanggal" class="col-sm-2 control-label">Tanggal 
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datepicker" name="transaksi_tanggal"  placeholder="Tanggal" id="transaksi_tanggal">
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="transaksi_bukti_trf" class="col-sm-2 control-label">Bukti Transfer 
                            </label>
                            <div class="col-sm-8">
                                <div id="transaksi_customer_canvas_transaksi_bukti_trf_galery"></div>
                                <input class="data_file" name="transaksi_customer_canvas_transaksi_bukti_trf_uuid" id="transaksi_customer_canvas_transaksi_bukti_trf_uuid" type="hidden" value="<?= set_value('transaksi_customer_canvas_transaksi_bukti_trf_uuid'); ?>">
                                <input class="data_file" name="transaksi_customer_canvas_transaksi_bukti_trf_name" id="transaksi_customer_canvas_transaksi_bukti_trf_name" type="hidden" value="<?= set_value('transaksi_customer_canvas_transaksi_bukti_trf_name'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                
                        
                                                <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                           <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                 <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){

                          
      $('#btn_cancel').click(function(){
        swal({
            title: "<?= cclang('are_you_sure'); ?>",
            text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/transaksi_customer_canvas';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_transaksi_customer_canvas = $('#form_transaksi_customer_canvas');
        var data_post = form_transaksi_customer_canvas.serializeArray();
        var save_type = $(this).attr('data-stype');

        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: BASE_URL + '/administrator/transaksi_customer_canvas/add_save',
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          $('form').find('.form-group').removeClass('has-error');
          $('.steps li').removeClass('error');
          $('form').find('.error-input').remove();
          if(res.success) {
            var id_transaksi_bukti_trf = $('#transaksi_customer_canvas_transaksi_bukti_trf_galery').find('li').attr('qq-file-id');
            
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            resetForm();
            if (typeof id_transaksi_bukti_trf !== 'undefined') {
                    $('#transaksi_customer_canvas_transaksi_bukti_trf_galery').fineUploader('deleteFile', id_transaksi_bukti_trf);
                }
            $('.chosen option').prop('selected', false).trigger('chosen:updated');
                
          } else {
            if (res.errors) {
                
                $.each(res.errors, function(index, val) {
                    $('form #'+index).parents('.form-group').addClass('has-error');
                    $('form #'+index).parents('.form-group').find('small').prepend(`
                      <div class="error-input">`+val+`</div>
                      `);
                });
                $('.steps li').removeClass('error');
                $('.content section').each(function(index, el) {
                    if ($(this).find('.has-error').length) {
                        $('.steps li:eq('+index+')').addClass('error').find('a').trigger('click');
                    }
                });
            }
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
              var params = {};
       params[csrf] = token;

       $('#transaksi_customer_canvas_transaksi_bukti_trf_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/transaksi_customer_canvas/upload_transaksi_bukti_trf_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + '/administrator/transaksi_customer_canvas/delete_transaksi_bukti_trf_file',
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
          multiple : false,
          validation: {
              allowedExtensions: ["*"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#transaksi_customer_canvas_transaksi_bukti_trf_galery').fineUploader('getUuid', id);
                   $('#transaksi_customer_canvas_transaksi_bukti_trf_uuid').val(uuid);
                   $('#transaksi_customer_canvas_transaksi_bukti_trf_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#transaksi_customer_canvas_transaksi_bukti_trf_uuid').val();
                  $.get(BASE_URL + '/administrator/transaksi_customer_canvas/delete_transaksi_bukti_trf_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#transaksi_customer_canvas_transaksi_bukti_trf_uuid').val('');
                  $('#transaksi_customer_canvas_transaksi_bukti_trf_name').val('');
                }
              }
          }
      }); /*end transaksi_bukti_trf galery*/
              
 
       

      
    
    
    }); /*end doc ready*/
</script>