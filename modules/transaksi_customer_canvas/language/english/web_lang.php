<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['transaksi_customer_canvas'] = 'Transaksi Customer Canvas';
$lang['transaksi_customer_canvas_id'] = 'Transaksi Customer Canvas Id';
$lang['transaksi_customer_id'] = 'Customer';
$lang['transaksi_canvas_id'] = 'Canvas';
$lang['transaksi_note'] = 'Catatan';
$lang['transaksi_total'] = 'Total';
$lang['transaksi_longitude'] = 'Longitude';
$lang['transaksi_latitude'] = 'Latitude';
$lang['transaksi_alamat'] = 'Alamat';
$lang['transaksi_status'] = 'Status';
$lang['transaksi_ongkir'] = 'Ongkir';
$lang['transaksi_tanggal'] = 'Tanggal';
$lang['transaksi_bukti_trf'] = 'Bukti Transfer';
