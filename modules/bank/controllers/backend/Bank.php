<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Bank Controller
*| --------------------------------------------------------------------------
*| Bank site
*|
*/
class Bank extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_bank');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Banks
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('bank_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['banks'] = $this->model_bank->get($filter, $field, $this->limit_page, $offset);
		$this->data['bank_counts'] = $this->model_bank->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/bank/index/',
			'total_rows'   => $this->model_bank->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Bank List');
		$this->render('backend/standart/administrator/bank/bank_list', $this->data);
	}
	
	/**
	* Add new banks
	*
	*/
	public function add()
	{
		$this->is_allowed('bank_add');

		$this->template->title('Bank New');
		$this->render('backend/standart/administrator/bank/bank_add', $this->data);
	}

	/**
	* Add New Banks
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('bank_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('bank_norek', 'Norek', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('bank_an', 'Atas Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('bank_jenis', 'Jenis', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'bank_norek' => $this->input->post('bank_norek'),
				'bank_an' => $this->input->post('bank_an'),
				'bank_jenis' => $this->input->post('bank_jenis'),
			];

			
			$save_bank = $this->model_bank->store($save_data);
            

			if ($save_bank) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_bank;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/bank/edit/' . $save_bank, 'Edit Bank'),
						anchor('administrator/bank', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/bank/edit/' . $save_bank, 'Edit Bank')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/bank');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/bank');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Banks
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('bank_update');

		$this->data['bank'] = $this->model_bank->find($id);

		$this->template->title('Bank Update');
		$this->render('backend/standart/administrator/bank/bank_update', $this->data);
	}

	/**
	* Update Banks
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('bank_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('bank_norek', 'Norek', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('bank_an', 'Atas Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('bank_jenis', 'Jenis', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'bank_norek' => $this->input->post('bank_norek'),
				'bank_an' => $this->input->post('bank_an'),
				'bank_jenis' => $this->input->post('bank_jenis'),
			];

			
			$save_bank = $this->model_bank->change($id, $save_data);

			if ($save_bank) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/bank', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/bank');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/bank');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Banks
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('bank_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'bank'), 'success');
        } else {
            set_message(cclang('error_delete', 'bank'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Banks
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('bank_view');

		$this->data['bank'] = $this->model_bank->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Bank Detail');
		$this->render('backend/standart/administrator/bank/bank_view', $this->data);
	}
	
	/**
	* delete Banks
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$bank = $this->model_bank->find($id);

		
		
		return $this->model_bank->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('bank_export');

		$this->model_bank->export('bank', 'bank');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('bank_export');

		$this->model_bank->pdf('bank', 'bank');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('bank_export');

		$table = $title = 'bank';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_bank->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}
	public function aktif($id = null)
	{
		$data = array( 
			"bank_status" =>2
		);
		$sim = $this->mymodel->update('bank',$data,'bank_status',1);

		$data2 = array( 
			"bank_status" =>1
		);
		$sim2 = $this->mymodel->update('bank',$data2,'bank_id',$id);

		if ($sim OR $sim2) {
            set_message(cclang('Status Aktif berhasil disimpan', 'bank'), 'success');
        } else {
            set_message(cclang('gagal di simpan', 'bank'), 'error');
        }

		redirect_back();
	}
	
}


/* End of file bank.php */
/* Location: ./application/controllers/administrator/Bank.php */