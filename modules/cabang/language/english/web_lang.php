<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['cabang'] = 'Cabang';
$lang['cabang_id'] = 'Cabang Id';
$lang['cabang_nama'] = 'Nama';
$lang['cabang_username'] = 'Username';
$lang['cabang_password'] = 'Password';
$lang['cabang_alamat'] = 'Alamat';
$lang['cabang_telp'] = 'Telp';
$lang['cabang_email'] = 'Email';
$lang['cabang_status'] = 'Status';
$lang['cabang_pusat'] = 'Pusat';
$lang['cabang_pj'] = 'PJ';
$lang['cabang_pj_telp'] = 'PJ Telp';
$lang['cabang_npwp'] = 'NPWP';
$lang['cabang_longitude'] = 'Longitude';
$lang['cabang_latitude'] = 'Latitude';
