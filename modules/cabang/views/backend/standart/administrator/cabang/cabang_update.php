

<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Cabang        <small>Edit Cabang</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/cabang'); ?>">Cabang</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Cabang</h3>
                            <h5 class="widget-user-desc">Edit Cabang</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/cabang/edit_save/'.$this->uri->segment(4)), [
                            'name'    => 'form_cabang', 
                            'class'   => 'form-horizontal form-step', 
                            'id'      => 'form_cabang', 
                            'method'  => 'POST'
                            ]); ?>
                         
                                                <div class="form-group ">
                            <label for="cabang_nama" class="col-sm-2 control-label">Nama 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="cabang_nama" id="cabang_nama" placeholder="Nama" value="<?= set_value('cabang_nama', $cabang->cabang_nama); ?>">
                                <small class="info help-block">
                                <b>Input Cabang Nama</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="cabang_username" class="col-sm-2 control-label">Username 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="cabang_username" id="cabang_username" placeholder="Username" value="<?= set_value('cabang_username', $cabang->cabang_username); ?>">
                                <small class="info help-block">
                                <b>Input Cabang Username</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="cabang_password" class="col-sm-2 control-label">Password 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                              <div class="input-group col-md-8 input-password">
                              <input type="password" class="form-control password" name="cabang_password" id="cabang_password" placeholder="Password" value="">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-flat show-password"><i class="fa fa-eye eye"></i></button>
                                </span>
                              </div>
                            <small class="info help-block">
                            <b>Input Cabang Password</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="cabang_alamat" class="col-sm-2 control-label">Alamat 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="cabang_alamat" id="cabang_alamat" placeholder="Alamat" value="<?= set_value('cabang_alamat', $cabang->cabang_alamat); ?>">
                                <small class="info help-block">
                                <b>Input Cabang Alamat</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="cabang_telp" class="col-sm-2 control-label">Telp 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="cabang_telp" id="cabang_telp" placeholder="Telp" value="<?= set_value('cabang_telp', $cabang->cabang_telp); ?>" min="1" onkeypress="return event.charCode >= 48 && event.charCode <=57">
                                <small class="info help-block">
                                <b>Input Cabang Telp</b> Max Length : 15.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="cabang_email" class="col-sm-2 control-label">Email 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="cabang_email" id="cabang_email" placeholder="Email" value="<?= set_value('cabang_email', $cabang->cabang_email); ?>">
                                <small class="info help-block">
                                <b>Input Cabang Email</b> Max Length : 100.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="cabang_status" class="col-sm-2 control-label">Status 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="cabang_status" id="cabang_status" data-placeholder="Select Status" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('cabang_status') as $row): ?>
                                    <option <?=  $row->cabang_status_id ==  $cabang->cabang_status ? 'selected' : ''; ?> value="<?= $row->cabang_status_id ?>"><?= $row->cabang_status_ket; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Cabang Status</b> Max Length : 11.</small>
                            </div>
                        </div>


                                                 
                                                <div class="form-group ">
                            <label for="cabang_pusat" class="col-sm-2 control-label">Pusat 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="cabang_pusat" id="cabang_pusat" data-placeholder="Select Pusat" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('cabang_pusat') as $row): ?>
                                    <option <?=  $row->cabang_pusat_id ==  $cabang->cabang_pusat ? 'selected' : ''; ?> value="<?= $row->cabang_pusat_id ?>"><?= $row->cabang_pusat_ket; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Cabang Pusat</b> Max Length : 11.</small>
                            </div>
                        </div>


                                                 
                                                <div class="form-group ">
                            <label for="cabang_pj" class="col-sm-2 control-label">PJ 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="cabang_pj" id="cabang_pj" placeholder="PJ" value="<?= set_value('cabang_pj', $cabang->cabang_pj); ?>">
                                <small class="info help-block">
                                <b>Input Cabang Pj</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="cabang_pj_telp" class="col-sm-2 control-label">PJ Telp 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="cabang_pj_telp" id="cabang_pj_telp" placeholder="PJ Telp" value="<?= set_value('cabang_pj_telp', $cabang->cabang_pj_telp); ?>" min="1" onkeypress="return event.charCode >= 48 && event.charCode <=57">
                                <small class="info help-block">
                                <b>Input Cabang Pj Telp</b> Max Length : 15.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="cabang_npwp" class="col-sm-2 control-label">NPWP 
                            </label>
                            <div class="col-sm-8">
                                <div id="cabang_cabang_npwp_galery"></div>
                                <input class="data_file data_file_uuid" name="cabang_cabang_npwp_uuid" id="cabang_cabang_npwp_uuid" type="hidden" value="<?= set_value('cabang_cabang_npwp_uuid'); ?>">
                                <input class="data_file" name="cabang_cabang_npwp_name" id="cabang_cabang_npwp_name" type="hidden" value="<?= set_value('cabang_cabang_npwp_name', $cabang->cabang_npwp); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                  
                                                <div class="form-group ">
                            <label for="cabang_longitude" class="col-sm-2 control-label">Longitude 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="cabang_longitude" id="cabang_longitude" placeholder="Longitude" value="<?= set_value('cabang_longitude', $cabang->cabang_longitude); ?>">
                                <small class="info help-block">
                                <b>Input Cabang Longitude</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="cabang_latitude" class="col-sm-2 control-label">Latitude 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="cabang_latitude" id="cabang_latitude" placeholder="Latitude" value="<?= set_value('cabang_latitude', $cabang->cabang_latitude); ?>">
                                <small class="info help-block">
                                <b>Input Cabang Latitude</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                
                                                 <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                 <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){
       
      
             
      $('#btn_cancel').click(function(){
        swal({
            title: "Are you sure?",
            text: "the data that you have created will be in the exhaust!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/cabang';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_cabang = $('#form_cabang');
        var data_post = form_cabang.serializeArray();
        var save_type = $(this).attr('data-stype');
        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: form_cabang.attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          $('form').find('.form-group').removeClass('has-error');
          $('form').find('.error-input').remove();
          $('.steps li').removeClass('error');
          if(res.success) {
            var id = $('#cabang_image_galery').find('li').attr('qq-file-id');
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            $('.data_file_uuid').val('');
    
          } else {
            if (res.errors) {
               parseErrorField(res.errors);
            }
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
                     var params = {};
       params[csrf] = token;

       $('#cabang_cabang_npwp_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/cabang/upload_cabang_npwp_file',
              params : params
          },
          deleteFile: {
              enabled: true, // defaults to false
              endpoint: BASE_URL + '/administrator/cabang/delete_cabang_npwp_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/cabang/get_cabang_npwp_file/<?= $cabang->cabang_id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["*"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#cabang_cabang_npwp_galery').fineUploader('getUuid', id);
                   $('#cabang_cabang_npwp_uuid').val(uuid);
                   $('#cabang_cabang_npwp_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#cabang_cabang_npwp_uuid').val();
                  $.get(BASE_URL + '/administrator/cabang/delete_cabang_npwp_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#cabang_cabang_npwp_uuid').val('');
                  $('#cabang_cabang_npwp_name').val('');
                }
              }
          }
      }); /*end cabang_npwp galey*/
              
       
       

      async function chain(){
      }
       
      chain();


    
    
    }); /*end doc ready*/
</script>