<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Cabang Controller
*| --------------------------------------------------------------------------
*| Cabang site
*|
*/
class Cabang extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_cabang');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Cabangs
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('cabang_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['cabangs'] = $this->model_cabang->get($filter, $field, $this->limit_page, $offset);
		$this->data['cabang_counts'] = $this->model_cabang->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/cabang/index/',
			'total_rows'   => $this->model_cabang->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Cabang List');
		$this->render('backend/standart/administrator/cabang/cabang_list', $this->data);
	}
	
	/**
	* Add new cabangs
	*
	*/
	public function add()
	{
		$this->is_allowed('cabang_add');

		$this->template->title('Cabang New');
		$this->render('backend/standart/administrator/cabang/cabang_add', $this->data);
	}

	/**
	* Add New Cabangs
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('cabang_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('cabang_nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('cabang_username', 'Username', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('cabang_password', 'Password', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('cabang_alamat', 'Alamat', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('cabang_telp', 'Telp', 'trim|required|max_length[15]');
		$this->form_validation->set_rules('cabang_email', 'Email', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('cabang_status', 'Status', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('cabang_pusat', 'Pusat', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('cabang_pj', 'PJ', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('cabang_pj_telp', 'PJ Telp', 'trim|required|max_length[15]');
		//$this->form_validation->set_rules('cabang_cabang_npwp_name', 'NPWP', 'trim|required');
		$this->form_validation->set_rules('cabang_longitude', 'Longitude', 'trim|max_length[255]');
		$this->form_validation->set_rules('cabang_latitude', 'Latitude', 'trim|max_length[255]');
		
		$kalimat = $this->input->post('cabang_email');
		$cekdata = $this->model_cabang->withquery("select * from cabang where cabang_longitude = '".$this->input->post('cabang_longitude')."' and cabang_latitude = '".$this->input->post('cabang_latitude')."'", "result");
		if(count($cekdata) == 0){
	        if(strpos($kalimat, '.') AND strpos($kalimat, '.')) {
				if ($this->form_validation->run()) {
					$cabang_cabang_npwp_uuid = $this->input->post('cabang_cabang_npwp_uuid');
					$cabang_cabang_npwp_name = $this->input->post('cabang_cabang_npwp_name');
				
					$save_data = [
						'cabang_nama' => $this->input->post('cabang_nama'),
						'cabang_username' => $this->input->post('cabang_username'),
						'cabang_password' => md5($this->input->post('cabang_password')),
						'cabang_alamat' => $this->input->post('cabang_alamat'),
						'cabang_telp' => $this->input->post('cabang_telp'),
						'cabang_email' => $this->input->post('cabang_email'),
						'cabang_status' => $this->input->post('cabang_status'),
						'cabang_pusat' => $this->input->post('cabang_pusat'),
						'cabang_pj' => $this->input->post('cabang_pj'),
						'cabang_pj_telp' => $this->input->post('cabang_pj_telp'),
						'cabang_longitude' => $this->input->post('cabang_longitude'),
						'cabang_latitude' => $this->input->post('cabang_latitude'),
						'cabang_token' => md5($this->input->post('cabang_email')."".$this->input->post('cabang_password')."".time()),
					];

					if (!is_dir(FCPATH . '/uploads/cabang/')) {
						mkdir(FCPATH . '/uploads/cabang/');
					}

					if (!empty($cabang_cabang_npwp_name)) {
						$cabang_cabang_npwp_name_copy = date('YmdHis') . '-' . $cabang_cabang_npwp_name;

						rename(FCPATH . 'uploads/tmp/' . $cabang_cabang_npwp_uuid . '/' . $cabang_cabang_npwp_name, 
								FCPATH . 'uploads/cabang/' . $cabang_cabang_npwp_name_copy);

						if (!is_file(FCPATH . '/uploads/cabang/' . $cabang_cabang_npwp_name_copy)) {
							echo json_encode([
								'success' => false,
								'message' => 'Error uploading file'
								]);
							exit;
						}

						$save_data['cabang_npwp'] = $cabang_cabang_npwp_name_copy;
					}
				
					
					$save_cabang = $this->model_cabang->store($save_data);
		            

					if ($save_cabang) {
						if ($this->input->post('save_type') == 'stay') {
							$this->data['success'] = true;
							$this->data['id'] 	   = $save_cabang;
							$this->data['message'] = cclang('success_save_data_stay', [
								anchor('administrator/cabang/edit/' . $save_cabang, 'Edit Cabang'),
								anchor('administrator/cabang', ' Go back to list')
							]);
						} else {
							set_message(
								cclang('success_save_data_redirect', [
								anchor('administrator/cabang/edit/' . $save_cabang, 'Edit Cabang')
							]), 'success');

		            		$this->data['success'] = true;
							$this->data['redirect'] = base_url('administrator/cabang');
						}
					} else {
						if ($this->input->post('save_type') == 'stay') {
							$this->data['success'] = false;
							$this->data['message'] = cclang('data_not_change');
						} else {
		            		$this->data['success'] = false;
		            		$this->data['message'] = cclang('data_not_change');
							$this->data['redirect'] = base_url('administrator/cabang');
						}
					}

				} else {
					$this->data['success'] = false;
					$this->data['message'] = 'Opss validation failed';
					$this->data['errors'] = $this->form_validation->error_array();
				}
			}else {
	        	$this->data['success'] = false;
	        	$this->data['message'] = 'Email tidak sesuai format';
	        	$this->data['errors'] = $this->form_validation->error_array();
	        }
	    }else{
			$this->data['success'] = false;
			$this->data['message'] = 'Latitude dan Longitude Sudah ada!!!';
		}
		echo json_encode($this->data);
	}
	
		/**
	* Update view Cabangs
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('cabang_update');

		$this->data['cabang'] = $this->model_cabang->find($id);

		$this->template->title('Cabang Update');
		$this->render('backend/standart/administrator/cabang/cabang_update', $this->data);
	}

	/**
	* Update Cabangs
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('cabang_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('cabang_nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('cabang_username', 'Username', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('cabang_alamat', 'Alamat', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('cabang_telp', 'Telp', 'trim|required|max_length[15]');
		$this->form_validation->set_rules('cabang_email', 'Email', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('cabang_status', 'Status', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('cabang_pusat', 'Pusat', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('cabang_pj', 'PJ', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('cabang_pj_telp', 'PJ Telp', 'trim|required|max_length[15]');
		//$this->form_validation->set_rules('cabang_cabang_npwp_name', 'NPWP', 'trim|required');
		$this->form_validation->set_rules('cabang_longitude', 'Longitude', 'trim|max_length[255]');
		$this->form_validation->set_rules('cabang_latitude', 'Latitude', 'trim|max_length[255]');
		$kalimat = $this->input->post('cabang_email');
		$cekdata = $this->model_cabang->withquery("select * from cabang where cabang_longitude = '".$this->input->post('cabang_longitude')."' and cabang_latitude = '".$this->input->post('cabang_latitude')."'", "result");
		if(count($cekdata) == 0){
	        if(strpos($kalimat, '.') AND strpos($kalimat, '.')) {
				if ($this->form_validation->run()) {
					$cabang_cabang_npwp_uuid = $this->input->post('cabang_cabang_npwp_uuid');
					$cabang_cabang_npwp_name = $this->input->post('cabang_cabang_npwp_name');
				
					$save_data = [];
					
					if(empty($this->input->post('cabang_password'))){
						
						$save_data = [
							'cabang_nama' => $this->input->post('cabang_nama'),
							'cabang_username' => $this->input->post('cabang_username'),
							'cabang_alamat' => $this->input->post('cabang_alamat'),
							'cabang_telp' => $this->input->post('cabang_telp'),
							'cabang_email' => $this->input->post('cabang_email'),
							'cabang_status' => $this->input->post('cabang_status'),
							'cabang_pusat' => $this->input->post('cabang_pusat'),
							'cabang_pj' => $this->input->post('cabang_pj'),
							'cabang_pj_telp' => $this->input->post('cabang_pj_telp'),
							'cabang_longitude' => $this->input->post('cabang_longitude'),
							'cabang_latitude' => $this->input->post('cabang_latitude'),
						];
					}else{
						$save_data = [
							'cabang_nama' => $this->input->post('cabang_nama'),
							'cabang_username' => $this->input->post('cabang_username'),
							'cabang_password' => md5($this->input->post('cabang_password')),
							'cabang_alamat' => $this->input->post('cabang_alamat'),
							'cabang_telp' => $this->input->post('cabang_telp'),
							'cabang_email' => $this->input->post('cabang_email'),
							'cabang_status' => $this->input->post('cabang_status'),
							'cabang_pusat' => $this->input->post('cabang_pusat'),
							'cabang_pj' => $this->input->post('cabang_pj'),
							'cabang_pj_telp' => $this->input->post('cabang_pj_telp'),
							'cabang_longitude' => $this->input->post('cabang_longitude'),
							'cabang_latitude' => $this->input->post('cabang_latitude'),
						];
					}

					if (!is_dir(FCPATH . '/uploads/cabang/')) {
						mkdir(FCPATH . '/uploads/cabang/');
					}

					if (!empty($cabang_cabang_npwp_uuid)) {
						$cabang_cabang_npwp_name_copy = date('YmdHis') . '-' . $cabang_cabang_npwp_name;

						rename(FCPATH . 'uploads/tmp/' . $cabang_cabang_npwp_uuid . '/' . $cabang_cabang_npwp_name, 
								FCPATH . 'uploads/cabang/' . $cabang_cabang_npwp_name_copy);

						if (!is_file(FCPATH . '/uploads/cabang/' . $cabang_cabang_npwp_name_copy)) {
							echo json_encode([
								'success' => false,
								'message' => 'Error uploading file'
								]);
							exit;
						}

						$save_data['cabang_npwp'] = $cabang_cabang_npwp_name_copy;
					}
				
					
					$save_cabang = $this->model_cabang->change($id, $save_data);

					if ($save_cabang) {
						if ($this->input->post('save_type') == 'stay') {
							$this->data['success'] = true;
							$this->data['id'] 	   = $id;
							$this->data['message'] = cclang('success_update_data_stay', [
								anchor('administrator/cabang', ' Go back to list')
							]);
						} else {
							set_message(
								cclang('success_update_data_redirect', [
							]), 'success');

		            		$this->data['success'] = true;
							$this->data['redirect'] = base_url('administrator/cabang');
						}
					} else {
						if ($this->input->post('save_type') == 'stay') {
							$this->data['success'] = false;
							$this->data['message'] = cclang('data_not_change');
						} else {
		            		$this->data['success'] = false;
		            		$this->data['message'] = cclang('data_not_change');
							$this->data['redirect'] = base_url('administrator/cabang');
						}
					}
				} else {
					$this->data['success'] = false;
					$this->data['message'] = 'Opss validation failed';
					$this->data['errors'] = $this->form_validation->error_array();
				}
			}else {
	        	$this->data['success'] = false;
	        	$this->data['message'] = 'Email tidak sesuai format';
	        	$this->data['errors'] = $this->form_validation->error_array();
	        }
	    }else{
			$this->data['success'] = false;
			$this->data['message'] = 'Latitude dan Longitude Sudah ada!!!';
		}
		echo json_encode($this->data);
	}
	
	/**
	* delete Cabangs
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('cabang_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'cabang'), 'success');
        } else {
            set_message(cclang('error_delete', 'cabang'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Cabangs
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('cabang_view');

		$this->data['cabang'] = $this->model_cabang->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Cabang Detail');
		$this->render('backend/standart/administrator/cabang/cabang_view', $this->data);
	}
	
	/**
	* delete Cabangs
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$cabang = $this->model_cabang->find($id);

		if (!empty($cabang->cabang_npwp)) {
			$path = FCPATH . '/uploads/cabang/' . $cabang->cabang_npwp;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_cabang->remove($id);
	}
	
	/**
	* Upload Image Cabang	* 
	* @return JSON
	*/
	public function upload_cabang_npwp_file()
	{
		if (!$this->is_allowed('cabang_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'cabang',
		]);
	}

	/**
	* Delete Image Cabang	* 
	* @return JSON
	*/
	public function delete_cabang_npwp_file($uuid)
	{
		if (!$this->is_allowed('cabang_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'cabang_npwp', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'cabang',
            'primary_key'       => 'cabang_id',
            'upload_path'       => 'uploads/cabang/'
        ]);
	}

	/**
	* Get Image Cabang	* 
	* @return JSON
	*/
	public function get_cabang_npwp_file($id)
	{
		if (!$this->is_allowed('cabang_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$cabang = $this->model_cabang->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'cabang_npwp', 
            'table_name'        => 'cabang',
            'primary_key'       => 'cabang_id',
            'upload_path'       => 'uploads/cabang/',
            'delete_endpoint'   => 'administrator/cabang/delete_cabang_npwp_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('cabang_export');

		$this->model_cabang->export('cabang', 'cabang');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('cabang_export');

		$this->model_cabang->pdf('cabang', 'cabang');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('cabang_export');

		$table = $title = 'cabang';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_cabang->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file cabang.php */
/* Location: ./application/controllers/administrator/Cabang.php */