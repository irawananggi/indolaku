<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_cabang extends MY_Model {

    private $primary_key    = 'cabang_id';
    private $table_name     = 'cabang';
    private $field_search   = ['cabang_nama', 'cabang_username', 'cabang_password', 'cabang_alamat', 'cabang_telp', 'cabang_email', 'cabang_status', 'cabang_pusat', 'cabang_pj', 'cabang_pj_telp', 'cabang_npwp', 'cabang_longitude', 'cabang_latitude'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function withquery($query1,$result)
      {
        $query = $this->db->query($query1);
        if ($result=='result') {
          return $query->result();
        }else {
            return $query->row();
        }
    }
    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "cabang.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "cabang.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "cabang.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "cabang.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "cabang.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "cabang.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('cabang.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('cabang_status', 'cabang_status.cabang_status_id = cabang.cabang_status', 'LEFT');
        $this->db->join('cabang_pusat', 'cabang_pusat.cabang_pusat_id = cabang.cabang_pusat', 'LEFT');
        
        $this->db->select('cabang.*,cabang_status.cabang_status_ket as cabang_status_cabang_status_ket,cabang_pusat.cabang_pusat_ket as cabang_pusat_cabang_pusat_ket');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_cabang.php */
/* Location: ./application/models/Model_cabang.php */