

<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Pelanggang        <small>Edit Pelanggang</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/pelanggang'); ?>">Pelanggang</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Pelanggang</h3>
                            <h5 class="widget-user-desc">Edit Pelanggang</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/pelanggang/edit_save/'.$this->uri->segment(4)), [
                            'name'    => 'form_pelanggang', 
                            'class'   => 'form-horizontal form-step', 
                            'id'      => 'form_pelanggang', 
                            'method'  => 'POST'
                            ]); ?>
                         
                                                <div class="form-group ">
                            <label for="pelanggang_nama" class="col-sm-2 control-label">Nama 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="pelanggang_nama" id="pelanggang_nama" placeholder="Nama" value="<?= set_value('pelanggang_nama', $pelanggang->pelanggang_nama); ?>">
                                <small class="info help-block">
                                <b>Input Pelanggang Nama</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="pelanggang_email" class="col-sm-2 control-label">Email 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" name="pelanggang_email" id="pelanggang_email" placeholder="Email" value="<?= set_value('pelanggang_email', $pelanggang->pelanggang_email); ?>">
                                <small class="info help-block">
                                <b>Input Pelanggang Email</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="pelanggang_password" class="col-sm-2 control-label">Password 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                              <div class="input-group col-md-8 input-password">
                              <input type="password" class="form-control password" name="pelanggang_password" id="pelanggang_password" placeholder="Password" value="">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-flat show-password"><i class="fa fa-eye eye"></i></button>
                                </span>
                              </div>
                            <small class="info help-block">
                            <b>Input Pelanggang Password</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="pelanggang_telp" class="col-sm-2 control-label">Telp 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="pelanggang_telp" id="pelanggang_telp" placeholder="Telp" value="<?= set_value('pelanggang_telp', $pelanggang->pelanggang_telp); ?>" min="1" onkeypress="return event.charCode >= 48 && event.charCode <=57">
                                <small class="info help-block">
                                <b>Input Pelanggang Telp</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="pelanggang_npwp" class="col-sm-2 control-label">NPWP 
                            </label>
                            <div class="col-sm-8">
                                <div id="pelanggang_pelanggang_npwp_galery"></div>
                                <input class="data_file data_file_uuid" name="pelanggang_pelanggang_npwp_uuid" id="pelanggang_pelanggang_npwp_uuid" type="hidden" value="<?= set_value('pelanggang_pelanggang_npwp_uuid'); ?>">
                                <input class="data_file" name="pelanggang_pelanggang_npwp_name" id="pelanggang_pelanggang_npwp_name" type="hidden" value="<?= set_value('pelanggang_pelanggang_npwp_name', $pelanggang->pelanggang_npwp); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                  
                                                
                                                 <div class="form-group ">
                            <label for="pelanggang_provinsi" class="col-sm-2 control-label">Provinsi 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen" onchange="pilihprovinsi()" name="pelanggang_provinsi" id="pelanggang_provinsi" data-placeholder="Select Provinsi" >
                                    <option value="">Pilih Provinsi</option>
                                    <?php foreach (db_get_all_data('reg_provinces') as $row): ?>
                                    <option <?=  $row->id ==  $pelanggang->pelanggang_provinsi ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->name; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input pelanggang Provinsi</b> Max Length : 11.</small>
                            </div>
                        </div>


                                                 
                                                <div class="form-group ">
                            <label for="pelanggang_kota" class="col-sm-2 control-label">Kota 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen" onchange="pilihkota()" name="pelanggang_kota" id="pelanggang_kota" data-placeholder="Select Kota" >
                                    <option value="">Pilih Kota</option>
                                </select>
                                <small class="info help-block">
                                <b>Input pelanggang Kota</b> Max Length : 11.</small>
                            </div>
                        </div>


                         <div class="form-group ">
                            <label for="pelanggang_kec" class="col-sm-2 control-label">Kecamatan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen" onchange="pilihkecamatan()" name="pelanggang_kec" id="pelanggang_kec" data-placeholder="Select Kecamatan" >
                                    <option value="">Pilih Kecamatan</option>
                                </select>
                                <small class="info help-block">
                                <b>Input pelanggang Kecamatan</b> Max Length : 11.</small>
                            </div>
                        </div>


                                                 
                                                <div class="form-group ">
                            <label for="pelanggang_kelurahan" class="col-sm-2 control-label">Kelurahan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen" name="pelanggang_kel" id="pelanggang_kelurahan" data-placeholder="Select Kelurahan" >
                                    <option value="">Pilih Kelurahan</option>
                                </select>
                                <small class="info help-block">
                                <b>Input pelanggang Kelurahan</b> Max Length : 11.</small>
                            </div>
                        </div>


                                                 
                                                <div class="form-group ">
                            <label for="pelanggang_alamat" class="col-sm-2 control-label">Alamat 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="pelanggang_alamat" id="pelanggang_alamat" placeholder="Alamat" value="<?= set_value('pelanggang_alamat', $pelanggang->pelanggang_alamat); ?>">
                                <small class="info help-block">
                                <b>Input Pelanggang Alamat</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <!-- <div class="form-group ">
                            <label for="pelanggang_cabang" class="col-sm-2 control-label">Cabang 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="pelanggang_cabang" id="pelanggang_cabang" data-placeholder="Select Cabang" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('cabang') as $row): ?>
                                    <option <?=  $row->cabang_id ==  $pelanggang->pelanggang_cabang ? 'selected' : ''; ?> value="<?= $row->cabang_id ?>"><?= $row->cabang_nama; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Pelanggang Cabang</b> Max Length : 11.</small>
                            </div>
                        </div> -->


                                                 
                                                <div class="form-group ">
                            <label for="pelanggang_foto" class="col-sm-2 control-label">Foto 
                            </label>
                            <div class="col-sm-8">
                                <div id="pelanggang_pelanggang_foto_galery"></div>
                                <input class="data_file data_file_uuid" name="pelanggang_pelanggang_foto_uuid" id="pelanggang_pelanggang_foto_uuid" type="hidden" value="<?= set_value('pelanggang_pelanggang_foto_uuid'); ?>">
                                <input class="data_file" name="pelanggang_pelanggang_foto_name" id="pelanggang_pelanggang_foto_name" type="hidden" value="<?= set_value('pelanggang_pelanggang_foto_name', $pelanggang->pelanggang_foto); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                  
                                                <div class="form-group ">
                            <label for="pelanggang_status" class="col-sm-2 control-label">Status 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="pelanggang_status" id="pelanggang_status" data-placeholder="Select Status" >
                                    <option value=""></option>
                                    <option <?= $pelanggang->pelanggang_status == "1" ? 'selected' :''; ?> value="1">Iya</option>
                                    <option <?= $pelanggang->pelanggang_status == "2" ? 'selected' :''; ?> value="2">Tidak</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="pelanggang_longitude" class="col-sm-2 control-label">Longitude 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="pelanggang_longitude" id="pelanggang_longitude" placeholder="Longitude" value="<?= set_value('pelanggang_longitude', $pelanggang->pelanggang_longitude); ?>">
                                <small class="info help-block">
                                <b>Input Pelanggang Longitude</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="pelanggang_latitude" class="col-sm-2 control-label">Latitude 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="pelanggang_latitude" id="pelanggang_latitude" placeholder="Latitude" value="<?= set_value('pelanggang_latitude', $pelanggang->pelanggang_latitude); ?>">
                                <small class="info help-block">
                                <b>Input Pelanggang Latitude</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                
                                                 <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                 <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    var firstload = 1;
	function pilihprovinsi(){
		var skota = '<?php echo $pelanggang->pelanggang_kota; ?>';
		//alert(skota);
		
		 $.ajax({
			type: "GET",
			data: {provinsi_id : $('#pelanggang_provinsi option:selected').val(), kota_id: skota, first: firstload },
			url: BASE_URL+ "administrator/pelanggang/getkota1/",
			success: function(data) {
				$('#pelanggang_kota').html(data);
				if(firstload){
					pilihkota();
				}else{
					$('#pelanggang_kec').html("<option value=''>Pilih Kecamatan</option>");
					$('#pelanggang_kelurahan').html("<option value=''>Pilih Kelurahan</option>");
				}
				//alert(data);
			}
		});
	}
	function pilihkota(){
			var skecamatan = '<?php echo $pelanggang->pelanggang_kec; ?>';
			//alert(skecamatan);
			
			 $.ajax({
				type: "GET",
				data: {kota_id : $('#pelanggang_kota option:selected').val(), kecamatan_id: skecamatan, first: firstload},
				url: BASE_URL+ "administrator/pelanggang/getkecamatan1/",
				success: function(data) {
					//alert(firstload);
					$('#pelanggang_kec').html(data);
					if(firstload){
						pilihkecamatan();
						firstload = 0;
					}else{
						$('#pelanggang_kelurahan').html("<option value=''>Pilih Kelurahan</option>");
					}
					//alert(data);
				}
			});
	}
	function pilihkecamatan(){
			var skelurahan = '<?php echo $pelanggang->pelanggang_kel; ?>';
			//alert(skelurahan);
			
			 $.ajax({
				type: "GET",
				data: {kecamatan_id : $('#pelanggang_kec option:selected').val(), kelurahan_id: skelurahan, first: firstload},
				url: BASE_URL+ "administrator/pelanggang/getkelurahan1/",
				success: function(data) {
					$('#pelanggang_kelurahan').html(data);
					//alert(data);
				}
			});
	}
    $(document).ready(function(){
       
      pilihprovinsi();
       
      
             
      $('#btn_cancel').click(function(){
        swal({
            title: "Are you sure?",
            text: "the data that you have created will be in the exhaust!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/pelanggang';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_pelanggang = $('#form_pelanggang');
        var data_post = form_pelanggang.serializeArray();
        var save_type = $(this).attr('data-stype');
        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: form_pelanggang.attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          $('form').find('.form-group').removeClass('has-error');
          $('form').find('.error-input').remove();
          $('.steps li').removeClass('error');
          if(res.success) {
            var id = $('#pelanggang_image_galery').find('li').attr('qq-file-id');
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            $('.data_file_uuid').val('');
    
          } else {
            if (res.errors) {
               parseErrorField(res.errors);
            }
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
                     var params = {};
       params[csrf] = token;

       $('#pelanggang_pelanggang_npwp_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/pelanggang/upload_pelanggang_npwp_file',
              params : params
          },
          deleteFile: {
              enabled: true, // defaults to false
              endpoint: BASE_URL + '/administrator/pelanggang/delete_pelanggang_npwp_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/pelanggang/get_pelanggang_npwp_file/<?= $pelanggang->pelanggang_id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["*"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#pelanggang_pelanggang_npwp_galery').fineUploader('getUuid', id);
                   $('#pelanggang_pelanggang_npwp_uuid').val(uuid);
                   $('#pelanggang_pelanggang_npwp_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#pelanggang_pelanggang_npwp_uuid').val();
                  $.get(BASE_URL + '/administrator/pelanggang/delete_pelanggang_npwp_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#pelanggang_pelanggang_npwp_uuid').val('');
                  $('#pelanggang_pelanggang_npwp_name').val('');
                }
              }
          }
      }); /*end pelanggang_npwp galey*/
                            var params = {};
       params[csrf] = token;

       $('#pelanggang_pelanggang_foto_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/pelanggang/upload_pelanggang_foto_file',
              params : params
          },
          deleteFile: {
              enabled: true, // defaults to false
              endpoint: BASE_URL + '/administrator/pelanggang/delete_pelanggang_foto_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/pelanggang/get_pelanggang_foto_file/<?= $pelanggang->pelanggang_id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["*"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#pelanggang_pelanggang_foto_galery').fineUploader('getUuid', id);
                   $('#pelanggang_pelanggang_foto_uuid').val(uuid);
                   $('#pelanggang_pelanggang_foto_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#pelanggang_pelanggang_foto_uuid').val();
                  $.get(BASE_URL + '/administrator/pelanggang/delete_pelanggang_foto_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#pelanggang_pelanggang_foto_uuid').val('');
                  $('#pelanggang_pelanggang_foto_name').val('');
                }
              }
          }
      }); /*end pelanggang_foto galey*/
              
       
       

      async function chain(){
      }
       
      chain();


    
    
    }); /*end doc ready*/
</script>