<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Pelanggang Controller
*| --------------------------------------------------------------------------
*| Pelanggang site
*|
*/
class Pelanggang extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_pelanggang');
		$this->lang->load('web_lang', $this->current_lang);
	}
	function getkota($id)
    {
		$data = $this->model_pelanggang->withquery("select * from reg_regencies where province_id = '".$id."' order by name asc","result");
		echo "<option value=''>Pilih Kota</option>";
		foreach($data as $datalist){
			echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			
		}
    }
	function getkecamatan($id)
    {
		$data = $this->model_pelanggang->withquery("select * from reg_districts where regency_id  = '".$id."' order by name asc","result");
		echo "<option value=''>Pilih Kecamatan</option>";
		foreach($data as $datalist){
			echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			
		}
    }
	
	function getkelurahan($id)
    {
		$data = $this->model_pelanggang->withquery("select * from reg_villages where district_id  = '".$id."' order by name asc","result");
		echo "<option value=''>Pilih Kelurahan</option>";
		foreach($data as $datalist){
			echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			
		}
    }
	function getkota1()
    {
		
		$data = $this->model_pelanggang->withquery("select * from reg_regencies where province_id = '".$_GET['provinsi_id']."' order by name asc","result");
		echo "<option value=''>Pilih Kota</option>";
		foreach($data as $datalist){
			if($datalist->id == $_GET['kota_id'] && $_GET['first'] == 1){
				echo "<option value='".$datalist->id."' selected>".$datalist->name."</option>";
			}else{
				echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			}
			
		}
    }
	function getkecamatan1()
    {
		$data = $this->model_pelanggang->withquery("select * from reg_districts where regency_id  = '".$_GET['kota_id']."' order by name asc","result");
		echo "<option value=''>Pilih Kecamatan</option>";
		foreach($data as $datalist){
			if($datalist->id == $_GET['kecamatan_id']  && $_GET['first'] == 1 ){
				echo "<option value='".$datalist->id."' selected>".$datalist->name."</option>";
			}else{
				echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			}
			
		}
    }
	
	function getkelurahan1()
    {
		$data = $this->model_pelanggang->withquery("select * from reg_villages where district_id  = '".$_GET['kecamatan_id']."' order by name asc","result");
		echo "<option value=''>Pilih Kelurahan</option>";
		foreach($data as $datalist){
			
			if($datalist->id == $_GET['kelurahan_id']  && $_GET['first'] == 1){
				echo "<option value='".$datalist->id."' selected>".$datalist->name."</option>";
			}else{
				echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			}
		}
    }
	/**
	* show all Pelanggangs
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('pelanggang_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['pelanggangs'] = $this->model_pelanggang->get($filter, $field, $this->limit_page, $offset);
		$this->data['pelanggang_counts'] = $this->model_pelanggang->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/pelanggang/index/',
			'total_rows'   => $this->model_pelanggang->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Pelanggang List');
		$this->render('backend/standart/administrator/pelanggang/pelanggang_list', $this->data);
	}
	
	/**
	* Add new pelanggangs
	*
	*/
	public function add()
	{
		$this->is_allowed('pelanggang_add');

		$this->template->title('Pelanggang New');
		$this->render('backend/standart/administrator/pelanggang/pelanggang_add', $this->data);
	}

	/**
	* Add New Pelanggangs
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('pelanggang_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		$this->form_validation->set_rules('pelanggang_nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('pelanggang_email', 'Email', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('pelanggang_password', 'Password', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('pelanggang_telp', 'Telp', 'trim|required|max_length[255]');
		//$this->form_validation->set_rules('pelanggang_pelanggang_npwp_name', 'NPWP', 'trim|required');
		$this->form_validation->set_rules('pelanggang_provinsi', 'Provinsi', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('pelanggang_kota', 'Kota', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('pelanggang_kec', 'Kecamatan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('pelanggang_kel', 'Kelurahan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('pelanggang_alamat', 'Alamat', 'trim|required|max_length[255]');
		/*$this->form_validation->set_rules('pelanggang_cabang', 'Cabang', 'trim|required|max_length[11]');*/
		$this->form_validation->set_rules('pelanggang_status', 'Status', 'trim|required');
		$this->form_validation->set_rules('pelanggang_longitude', 'Longitude', 'trim|max_length[255]');
		$this->form_validation->set_rules('pelanggang_latitude', 'Latitude', 'trim|max_length[255]');
		
		$kalimat = $this->input->post('pelanggang_email');
		
		if(strpos($kalimat, '.') AND strpos($kalimat, '.')) {
			
			if ($this->form_validation->run()) {
				$pelanggang_pelanggang_npwp_uuid = $this->input->post('pelanggang_pelanggang_npwp_uuid');
				$pelanggang_pelanggang_npwp_name = $this->input->post('pelanggang_pelanggang_npwp_name');
				$pelanggang_pelanggang_foto_uuid = $this->input->post('pelanggang_pelanggang_foto_uuid');
				$pelanggang_pelanggang_foto_name = $this->input->post('pelanggang_pelanggang_foto_name');
			
				$save_data = [
					'pelanggang_nama' => $this->input->post('pelanggang_nama'),
					'pelanggang_email' => $this->input->post('pelanggang_email'),
					'pelanggang_password' => md5($this->input->post('pelanggang_password')),
					'pelanggang_telp' => $this->input->post('pelanggang_telp'),
					'pelanggang_provinsi' => $this->input->post('pelanggang_provinsi'),
					'pelanggang_kota' => $this->input->post('pelanggang_kota'),
					'pelanggang_kec' => $this->input->post('pelanggang_kec'),
					'pelanggang_kel' => $this->input->post('pelanggang_kel'),
					'pelanggang_alamat' => $this->input->post('pelanggang_alamat'),
					//'pelanggang_cabang' => $this->input->post('pelanggang_cabang'),
					'pelanggang_status' => $this->input->post('pelanggang_status'),
					'pelanggang_longitude' => $this->input->post('pelanggang_longitude'),
					'pelanggang_latitude' => $this->input->post('pelanggang_latitude'),
					'pelanggang_token' => md5($this->input->post('pelanggang_email')."".$this->input->post('pelanggang_password')."".time()),
				];

				if (!is_dir(FCPATH . '/uploads/pelanggang/')) {
					mkdir(FCPATH . '/uploads/pelanggang/');
				}

				if (!empty($pelanggang_pelanggang_npwp_name)) {
					$pelanggang_pelanggang_npwp_name_copy = date('YmdHis') . '-' . $pelanggang_pelanggang_npwp_name;

					rename(FCPATH . 'uploads/tmp/' . $pelanggang_pelanggang_npwp_uuid . '/' . $pelanggang_pelanggang_npwp_name, 
							FCPATH . 'uploads/pelanggang/' . $pelanggang_pelanggang_npwp_name_copy);

					if (!is_file(FCPATH . '/uploads/pelanggang/' . $pelanggang_pelanggang_npwp_name_copy)) {
						echo json_encode([
							'success' => false,
							'message' => 'Error uploading file'
							]);
						exit;
					}

					$save_data['pelanggang_npwp'] = $pelanggang_pelanggang_npwp_name_copy;
				}
			
				if (!empty($pelanggang_pelanggang_foto_name)) {
					$pelanggang_pelanggang_foto_name_copy = date('YmdHis') . '-' . $pelanggang_pelanggang_foto_name;

					rename(FCPATH . 'uploads/tmp/' . $pelanggang_pelanggang_foto_uuid . '/' . $pelanggang_pelanggang_foto_name, 
							FCPATH . 'uploads/pelanggang/' . $pelanggang_pelanggang_foto_name_copy);

					if (!is_file(FCPATH . '/uploads/pelanggang/' . $pelanggang_pelanggang_foto_name_copy)) {
						echo json_encode([
							'success' => false,
							'message' => 'Error uploading file'
							]);
						exit;
					}

					$save_data['pelanggang_foto'] = $pelanggang_pelanggang_foto_name_copy;
				}
			
				
				$save_pelanggang = $this->model_pelanggang->store($save_data);
				if ($save_pelanggang) {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = true;
						$this->data['id'] 	   = $save_pelanggang;
						$this->data['message'] = cclang('success_save_data_stay', [
							anchor('administrator/pelanggang/edit/' . $save_pelanggang, 'Edit Pelanggang'),
							anchor('administrator/pelanggang', ' Go back to list')
						]);
					} else {
						set_message(
							cclang('success_save_data_redirect', [
							anchor('administrator/pelanggang/edit/' . $save_pelanggang, 'Edit Pelanggang')
						]), 'success');

	            		$this->data['success'] = true;
						$this->data['redirect'] = base_url('administrator/pelanggang');
					}
				} else {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = false;
						$this->data['message'] = cclang('data_not_change');
					} else {
	            		$this->data['success'] = false;
	            		$this->data['message'] = cclang('data_not_change');
						$this->data['redirect'] = base_url('administrator/pelanggang');
					}
				}

			} else {
				$this->data['success'] = false;
				$this->data['message'] = 'Opss validation failed';
				$this->data['errors'] = $this->form_validation->error_array();
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Email tidak sesuai format';
			$this->data['errors'] = $this->form_validation->error_array();
		}
	
		echo json_encode($this->data);
	}
	
		/**
	* Update view Pelanggangs
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('pelanggang_update');

		$this->data['pelanggang'] = $this->model_pelanggang->find($id);

		$this->template->title('Pelanggang Update');
		$this->render('backend/standart/administrator/pelanggang/pelanggang_update', $this->data);
	}

	/**
	* Update Pelanggangs
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('pelanggang_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('pelanggang_nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('pelanggang_email', 'Email', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('pelanggang_telp', 'Telp', 'trim|required|max_length[255]');
		//$this->form_validation->set_rules('pelanggang_pelanggang_npwp_name', 'NPWP', 'trim|required');
		$this->form_validation->set_rules('pelanggang_provinsi', 'Provinsi', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('pelanggang_kota', 'Kota', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('pelanggang_kec', 'Kecamatan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('pelanggang_kel', 'Kelurahan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('pelanggang_alamat', 'Alamat', 'trim|required|max_length[255]');
		/*$this->form_validation->set_rules('pelanggang_cabang', 'Cabang', 'trim|required|max_length[11]');*/
		$this->form_validation->set_rules('pelanggang_status', 'Status', 'trim|required');
		$this->form_validation->set_rules('pelanggang_longitude', 'Longitude', 'trim|max_length[255]');
		$this->form_validation->set_rules('pelanggang_latitude', 'Latitude', 'trim|max_length[255]');

		$kalimat = $this->input->post('pelanggang_email');
		
		if(strpos($kalimat, '.') AND strpos($kalimat, '.')) {
          
			if ($this->form_validation->run()) {
				$pelanggang_pelanggang_npwp_uuid = $this->input->post('pelanggang_pelanggang_npwp_uuid');
				$pelanggang_pelanggang_npwp_name = $this->input->post('pelanggang_pelanggang_npwp_name');
				$pelanggang_pelanggang_foto_uuid = $this->input->post('pelanggang_pelanggang_foto_uuid');
				$pelanggang_pelanggang_foto_name = $this->input->post('pelanggang_pelanggang_foto_name');
				$save_data = [];
				if(empty($this->input->post('pelanggang_password'))){
					$save_data = [
						'pelanggang_nama' => $this->input->post('pelanggang_nama'),
						'pelanggang_email' => $this->input->post('pelanggang_email'),
						'pelanggang_telp' => $this->input->post('pelanggang_telp'),
						'pelanggang_provinsi' => $this->input->post('pelanggang_provinsi'),
						'pelanggang_kota' => $this->input->post('pelanggang_kota'),
						'pelanggang_kec' => $this->input->post('pelanggang_kec'),
						'pelanggang_kel' => $this->input->post('pelanggang_kel'),
						'pelanggang_alamat' => $this->input->post('pelanggang_alamat'),
						//'pelanggang_cabang' => $this->input->post('pelanggang_cabang'),
						'pelanggang_status' => $this->input->post('pelanggang_status'),
						'pelanggang_longitude' => $this->input->post('pelanggang_longitude'),
						'pelanggang_latitude' => $this->input->post('pelanggang_latitude'),
					];
				}else{
					
					$save_data = [
						'pelanggang_nama' => $this->input->post('pelanggang_nama'),
						'pelanggang_email' => $this->input->post('pelanggang_email'),
						'pelanggang_password' => md5($this->input->post('pelanggang_password')),
						'pelanggang_telp' => $this->input->post('pelanggang_telp'),
						'pelanggang_provinsi' => $this->input->post('pelanggang_provinsi'),
						'pelanggang_kota' => $this->input->post('pelanggang_kota'),
						'pelanggang_kec' => $this->input->post('pelanggang_kec'),
						'pelanggang_kel' => $this->input->post('pelanggang_kel'),
						'pelanggang_alamat' => $this->input->post('pelanggang_alamat'),
						//'pelanggang_cabang' => $this->input->post('pelanggang_cabang'),
						'pelanggang_status' => $this->input->post('pelanggang_status'),
						'pelanggang_longitude' => $this->input->post('pelanggang_longitude'),
						'pelanggang_latitude' => $this->input->post('pelanggang_latitude'),
					];
				}

				if (!is_dir(FCPATH . '/uploads/pelanggang/')) {
					mkdir(FCPATH . '/uploads/pelanggang/');
				}

				if (!empty($pelanggang_pelanggang_npwp_uuid)) {
					$pelanggang_pelanggang_npwp_name_copy = date('YmdHis') . '-' . $pelanggang_pelanggang_npwp_name;

					rename(FCPATH . 'uploads/tmp/' . $pelanggang_pelanggang_npwp_uuid . '/' . $pelanggang_pelanggang_npwp_name, 
							FCPATH . 'uploads/pelanggang/' . $pelanggang_pelanggang_npwp_name_copy);

					if (!is_file(FCPATH . '/uploads/pelanggang/' . $pelanggang_pelanggang_npwp_name_copy)) {
						echo json_encode([
							'success' => false,
							'message' => 'Error uploading file'
							]);
						exit;
					}

					$save_data['pelanggang_npwp'] = $pelanggang_pelanggang_npwp_name_copy;
				}
			
				if (!empty($pelanggang_pelanggang_foto_uuid)) {
					$pelanggang_pelanggang_foto_name_copy = date('YmdHis') . '-' . $pelanggang_pelanggang_foto_name;

					rename(FCPATH . 'uploads/tmp/' . $pelanggang_pelanggang_foto_uuid . '/' . $pelanggang_pelanggang_foto_name, 
							FCPATH . 'uploads/pelanggang/' . $pelanggang_pelanggang_foto_name_copy);

					if (!is_file(FCPATH . '/uploads/pelanggang/' . $pelanggang_pelanggang_foto_name_copy)) {
						echo json_encode([
							'success' => false,
							'message' => 'Error uploading file'
							]);
						exit;
					}

					$save_data['pelanggang_foto'] = $pelanggang_pelanggang_foto_name_copy;
				}
			
				
				$save_pelanggang = $this->model_pelanggang->change($id, $save_data);

				if ($save_pelanggang) {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = true;
						$this->data['id'] 	   = $id;
						$this->data['message'] = cclang('success_update_data_stay', [
							anchor('administrator/pelanggang', ' Go back to list')
						]);
					} else {
						set_message(
							cclang('success_update_data_redirect', [
						]), 'success');

	            		$this->data['success'] = true;
						$this->data['redirect'] = base_url('administrator/pelanggang');
					}
				} else {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = false;
						$this->data['message'] = cclang('data_not_change');
					} else {
	            		$this->data['success'] = false;
	            		$this->data['message'] = cclang('data_not_change');
						$this->data['redirect'] = base_url('administrator/pelanggang');
					}
				}
			} else {
				$this->data['success'] = false;
				$this->data['message'] = 'Opss validation failed';
				$this->data['errors'] = $this->form_validation->error_array();
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Email tidak sesuai format';
			$this->data['errors'] = $this->form_validation->error_array();
		}
		echo json_encode($this->data);
	}
	
	/**
	* delete Pelanggangs
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('pelanggang_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'pelanggang'), 'success');
        } else {
            set_message(cclang('error_delete', 'pelanggang'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Pelanggangs
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('pelanggang_view');

		$this->data['pelanggang'] = $this->model_pelanggang->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Pelanggang Detail');
		$this->render('backend/standart/administrator/pelanggang/pelanggang_view', $this->data);
	}
	
	/**
	* delete Pelanggangs
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$pelanggang = $this->model_pelanggang->find($id);

		if (!empty($pelanggang->pelanggang_npwp)) {
			$path = FCPATH . '/uploads/pelanggang/' . $pelanggang->pelanggang_npwp;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($pelanggang->pelanggang_foto)) {
			$path = FCPATH . '/uploads/pelanggang/' . $pelanggang->pelanggang_foto;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_pelanggang->remove($id);
	}
	
	/**
	* Upload Image Pelanggang	* 
	* @return JSON
	*/
	public function upload_pelanggang_npwp_file()
	{
		if (!$this->is_allowed('pelanggang_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'pelanggang',
		]);
	}

	/**
	* Delete Image Pelanggang	* 
	* @return JSON
	*/
	public function delete_pelanggang_npwp_file($uuid)
	{
		if (!$this->is_allowed('pelanggang_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'pelanggang_npwp', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'pelanggang',
            'primary_key'       => 'pelanggang_id',
            'upload_path'       => 'uploads/pelanggang/'
        ]);
	}

	/**
	* Get Image Pelanggang	* 
	* @return JSON
	*/
	public function get_pelanggang_npwp_file($id)
	{
		if (!$this->is_allowed('pelanggang_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$pelanggang = $this->model_pelanggang->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'pelanggang_npwp', 
            'table_name'        => 'pelanggang',
            'primary_key'       => 'pelanggang_id',
            'upload_path'       => 'uploads/pelanggang/',
            'delete_endpoint'   => 'administrator/pelanggang/delete_pelanggang_npwp_file'
        ]);
	}
	
	/**
	* Upload Image Pelanggang	* 
	* @return JSON
	*/
	public function upload_pelanggang_foto_file()
	{
		if (!$this->is_allowed('pelanggang_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'pelanggang',
		]);
	}

	/**
	* Delete Image Pelanggang	* 
	* @return JSON
	*/
	public function delete_pelanggang_foto_file($uuid)
	{
		if (!$this->is_allowed('pelanggang_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'pelanggang_foto', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'pelanggang',
            'primary_key'       => 'pelanggang_id',
            'upload_path'       => 'uploads/pelanggang/'
        ]);
	}

	/**
	* Get Image Pelanggang	* 
	* @return JSON
	*/
	public function get_pelanggang_foto_file($id)
	{
		if (!$this->is_allowed('pelanggang_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$pelanggang = $this->model_pelanggang->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'pelanggang_foto', 
            'table_name'        => 'pelanggang',
            'primary_key'       => 'pelanggang_id',
            'upload_path'       => 'uploads/pelanggang/',
            'delete_endpoint'   => 'administrator/pelanggang/delete_pelanggang_foto_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('pelanggang_export');

		$this->model_pelanggang->export('pelanggang', 'pelanggang');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('pelanggang_export');

		$this->model_pelanggang->pdf('pelanggang', 'pelanggang');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('pelanggang_export');

		$table = $title = 'pelanggang';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_pelanggang->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file pelanggang.php */
/* Location: ./application/controllers/administrator/Pelanggang.php */