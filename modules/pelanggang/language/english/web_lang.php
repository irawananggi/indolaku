<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['pelanggang'] = 'Pelanggang';
$lang['pelanggang_id'] = 'Pelanggang Id';
$lang['pelanggang_nama'] = 'Nama';
$lang['pelanggang_email'] = 'Email';
$lang['pelanggang_password'] = 'Password';
$lang['pelanggang_telp'] = 'Telp';
$lang['pelanggang_npwp'] = 'NPWP';
$lang['pelanggang_provinsi'] = 'Provinsi';
$lang['pelanggang_kota'] = 'Kota';
$lang['pelanggang_kec'] = 'Kecamatan';
$lang['pelanggang_kel'] = 'Kelurahan';
$lang['pelanggang_alamat'] = 'Alamat';
$lang['pelanggang_cabang'] = 'Cabang';
$lang['pelanggang_foto'] = 'Foto';
$lang['pelanggang_status'] = 'Status';
$lang['pelanggang_longitude'] = 'Longitude';
$lang['pelanggang_latitude'] = 'Latitude';
