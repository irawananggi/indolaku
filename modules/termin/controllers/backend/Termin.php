<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Termin Controller
*| --------------------------------------------------------------------------
*| Termin site
*|
*/
class Termin extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_termin');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Termins
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('termin_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['termins'] = $this->model_termin->get($filter, $field, $this->limit_page, $offset);
		$this->data['termin_counts'] = $this->model_termin->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/termin/index/',
			'total_rows'   => $this->model_termin->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Termin List');
		$this->render('backend/standart/administrator/termin/termin_list', $this->data);
	}
	
	/**
	* Add new termins
	*
	*/
	public function add()
	{
		$this->is_allowed('termin_add');

		$this->template->title('Termin New');
		$this->render('backend/standart/administrator/termin/termin_add', $this->data);
	}

	/**
	* Add New Termins
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('termin_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('termin_keterangan', 'Keterangan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('termin_persen', 'Persentase', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'termin_keterangan' => $this->input->post('termin_keterangan'),
				'termin_persen' => $this->input->post('termin_persen'),
			];

			
			$save_termin = $this->model_termin->store($save_data);
            

			if ($save_termin) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_termin;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/termin/edit/' . $save_termin, 'Edit Termin'),
						anchor('administrator/termin', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/termin/edit/' . $save_termin, 'Edit Termin')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/termin');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/termin');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Termins
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('termin_update');

		$this->data['termin'] = $this->model_termin->find($id);

		$this->template->title('Termin Update');
		$this->render('backend/standart/administrator/termin/termin_update', $this->data);
	}

	/**
	* Update Termins
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('termin_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('termin_keterangan', 'Keterangan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('termin_persen', 'Persentase', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'termin_keterangan' => $this->input->post('termin_keterangan'),
				'termin_persen' => $this->input->post('termin_persen'),
			];

			
			$save_termin = $this->model_termin->change($id, $save_data);

			if ($save_termin) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/termin', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/termin');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/termin');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Termins
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('termin_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'termin'), 'success');
        } else {
            set_message(cclang('error_delete', 'termin'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Termins
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('termin_view');

		$this->data['termin'] = $this->model_termin->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Termin Detail');
		$this->render('backend/standart/administrator/termin/termin_view', $this->data);
	}
	
	/**
	* delete Termins
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$termin = $this->model_termin->find($id);

		
		
		return $this->model_termin->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('termin_export');

		$this->model_termin->export('termin', 'termin');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('termin_export');

		$this->model_termin->pdf('termin', 'termin');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('termin_export');

		$table = $title = 'termin';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_termin->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file termin.php */
/* Location: ./application/controllers/administrator/Termin.php */