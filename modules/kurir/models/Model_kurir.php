<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_kurir extends MY_Model {

    private $primary_key    = 'kurir_id';
    private $table_name     = 'kurir';
    private $field_search   = ['kurir_nama', 'kurir_email', 'kurir_password', 'kurir_nik', 'kurir_npwp', 'kurir_provinsi', 'kurir_kota', 'kurir_kecamatan', 'kurir_kelurahan', 'kurir_alamat', 'kurir_cabang', 'kurir_foto', 'kurir_status', 'kurir_longitude', 'kurir_latitude'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

	
	public function withquery($query1,$result)
	  {
		$query = $this->db->query($query1);
		if ($result=='result') {
		  return $query->result();
		}else {
			return $query->row();
		}
	}
    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "kurir.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "kurir.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "kurir.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "kurir.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "kurir.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "kurir.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('kurir.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('reg_provinces', 'reg_provinces.id = kurir.kurir_provinsi', 'LEFT');
        $this->db->join('reg_regencies', 'reg_regencies.id = kurir.kurir_kota', 'LEFT');
        $this->db->join('reg_districts', 'reg_districts.id = kurir.kurir_kecamatan', 'LEFT');
        $this->db->join('reg_villages', 'reg_villages.id = kurir.kurir_kelurahan', 'LEFT');
        $this->db->join('cabang', 'cabang.cabang_id = kurir.kurir_cabang', 'LEFT');
        $this->db->join('cabang_status', 'cabang_status.cabang_status_id = kurir.kurir_status', 'LEFT');
        
        $this->db->select('kurir.*,reg_provinces.name as reg_provinces_name,reg_regencies.name as reg_regencies_name,reg_districts.name as reg_districts_name,reg_villages.name as reg_villages_name,cabang.cabang_nama as cabang_cabang_nama,cabang_status.cabang_status_ket as cabang_status_cabang_status_ket');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_kurir.php */
/* Location: ./application/models/Model_kurir.php */