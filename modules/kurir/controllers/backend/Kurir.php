<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Kurir Controller
*| --------------------------------------------------------------------------
*| Kurir site
*|
*/
class Kurir extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_kurir');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Kurirs
	*
	* @var $offset String
	*/
	function getkota($id)
    {
		$data = $this->model_kurir->withquery("select * from reg_regencies where province_id = '".$id."' order by name asc","result");
		echo "<option value=''>Pilih Kota</option>";
		foreach($data as $datalist){
			echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			
		}
    }
	function getkecamatan($id)
    {
		$data = $this->model_kurir->withquery("select * from reg_districts where regency_id  = '".$id."' order by name asc","result");
		echo "<option value=''>Pilih Kecamatan</option>";
		foreach($data as $datalist){
			echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			
		}
    }
	
	function getkelurahan($id)
    {
		$data = $this->model_kurir->withquery("select * from reg_villages where district_id  = '".$id."' order by name asc","result");
		echo "<option value=''>Pilih Kelurahan</option>";
		foreach($data as $datalist){
			echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			
		}
    }
	function getkota1()
    {
		
		$data = $this->model_kurir->withquery("select * from reg_regencies where province_id = '".$_GET['provinsi_id']."' order by name asc","result");
		echo "<option value=''>Pilih Kota</option>";
		foreach($data as $datalist){
			if($datalist->id == $_GET['kota_id'] && $_GET['first'] == 1){
				echo "<option value='".$datalist->id."' selected>".$datalist->name."</option>";
			}else{
				echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			}
			
		}
    }
	function getkecamatan1()
    {
		$data = $this->model_kurir->withquery("select * from reg_districts where regency_id  = '".$_GET['kota_id']."' order by name asc","result");
		echo "<option value=''>Pilih Kecamatan</option>";
		foreach($data as $datalist){
			if($datalist->id == $_GET['kecamatan_id']  && $_GET['first'] == 1 ){
				echo "<option value='".$datalist->id."' selected>".$datalist->name."</option>";
			}else{
				echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			}
			
		}
    }
	
	function getkelurahan1()
    {
		$data = $this->model_kurir->withquery("select * from reg_villages where district_id  = '".$_GET['kecamatan_id']."' order by name asc","result");
		echo "<option value=''>Pilih Kelurahan</option>";
		foreach($data as $datalist){
			
			if($datalist->id == $_GET['kelurahan_id']  && $_GET['first'] == 1){
				echo "<option value='".$datalist->id."' selected>".$datalist->name."</option>";
			}else{
				echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			}
		}
    }
	public function index($offset = 0)
	{
		$this->is_allowed('kurir_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['kurirs'] = $this->model_kurir->get($filter, $field, $this->limit_page, $offset);
		$this->data['kurir_counts'] = $this->model_kurir->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/kurir/index/',
			'total_rows'   => $this->model_kurir->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Kurir List');
		$this->render('backend/standart/administrator/kurir/kurir_list', $this->data);
	}
	
	/**
	* Add new kurirs
	*
	*/
	public function add()
	{
		$this->is_allowed('kurir_add');

		$this->template->title('Kurir New');
		$this->render('backend/standart/administrator/kurir/kurir_add', $this->data);
	}

	/**
	* Add New Kurirs
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('kurir_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('kurir_nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kurir_email', 'Email', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kurir_password', 'Password', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kurir_nik', 'NIK', 'trim|required|max_length[255]');
		//$this->form_validation->set_rules('kurir_kurir_npwp_name', 'NPWP', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kurir_provinsi', 'Provinsi', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kurir_kota', 'Kota', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kurir_kecamatan', 'Kecamatan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kurir_kelurahan', 'Kelurahan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kurir_alamat', 'Alamat', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kurir_cabang', 'Cabang', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kurir_kurir_foto_name', 'Foto', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kurir_status', 'Status', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kurir_longitude', 'Longitude', 'trim|max_length[255]');
		$this->form_validation->set_rules('kurir_latitude', 'Latitude', 'trim|max_length[255]');
		
		$kalimat = $this->input->post('kurir_email');
		
		if(strpos($kalimat, '.') AND strpos($kalimat, '.')) {
			
			if ($this->form_validation->run()) {
				$kurir_kurir_npwp_uuid = $this->input->post('kurir_kurir_npwp_uuid');
				$kurir_kurir_npwp_name = $this->input->post('kurir_kurir_npwp_name');
				$kurir_kurir_foto_uuid = $this->input->post('kurir_kurir_foto_uuid');
				$kurir_kurir_foto_name = $this->input->post('kurir_kurir_foto_name');
			
				$save_data = [
					'kurir_nama' => $this->input->post('kurir_nama'),
					'kurir_email' => $this->input->post('kurir_email'),
					'kurir_password' => md5($this->input->post('kurir_password')),
					'kurir_nik' => $this->input->post('kurir_nik'),
					'kurir_provinsi' => $this->input->post('kurir_provinsi'),
					'kurir_kota' => $this->input->post('kurir_kota'),
					'kurir_kecamatan' => $this->input->post('kurir_kecamatan'),
					'kurir_kelurahan' => $this->input->post('kurir_kelurahan'),
					'kurir_alamat' => $this->input->post('kurir_alamat'),
					'kurir_cabang' => $this->input->post('kurir_cabang'),
					'kurir_status' => $this->input->post('kurir_status'),
					'kurir_longitude' => $this->input->post('kurir_longitude'),
					'kurir_latitude' => $this->input->post('kurir_latitude'),
					'kurir_token' => md5($this->input->post('kurir_email')."".$this->input->post('kurir_password')."".time()),
				];

				if (!is_dir(FCPATH . '/uploads/kurir/')) {
					mkdir(FCPATH . '/uploads/kurir/');
				}

				if (!empty($kurir_kurir_npwp_name)) {
					$kurir_kurir_npwp_name_copy = date('YmdHis') . '-' . $kurir_kurir_npwp_name;

					rename(FCPATH . 'uploads/tmp/' . $kurir_kurir_npwp_uuid . '/' . $kurir_kurir_npwp_name, 
							FCPATH . 'uploads/kurir/' . $kurir_kurir_npwp_name_copy);

					if (!is_file(FCPATH . '/uploads/kurir/' . $kurir_kurir_npwp_name_copy)) {
						echo json_encode([
							'success' => false,
							'message' => 'Error uploading file'
							]);
						exit;
					}

					$save_data['kurir_npwp'] = $kurir_kurir_npwp_name_copy;
				}
			
				if (!empty($kurir_kurir_foto_name)) {
					$kurir_kurir_foto_name_copy = date('YmdHis') . '-' . $kurir_kurir_foto_name;

					rename(FCPATH . 'uploads/tmp/' . $kurir_kurir_foto_uuid . '/' . $kurir_kurir_foto_name, 
							FCPATH . 'uploads/kurir/' . $kurir_kurir_foto_name_copy);

					if (!is_file(FCPATH . '/uploads/kurir/' . $kurir_kurir_foto_name_copy)) {
						echo json_encode([
							'success' => false,
							'message' => 'Error uploading file'
							]);
						exit;
					}

					$save_data['kurir_foto'] = $kurir_kurir_foto_name_copy;
				}
			
				
				$save_kurir = $this->model_kurir->store($save_data);
	            

				if ($save_kurir) {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = true;
						$this->data['id'] 	   = $save_kurir;
						$this->data['message'] = cclang('success_save_data_stay', [
							anchor('administrator/kurir/edit/' . $save_kurir, 'Edit Kurir'),
							anchor('administrator/kurir', ' Go back to list')
						]);
					} else {
						set_message(
							cclang('success_save_data_redirect', [
							anchor('administrator/kurir/edit/' . $save_kurir, 'Edit Kurir')
						]), 'success');

	            		$this->data['success'] = true;
						$this->data['redirect'] = base_url('administrator/kurir');
					}
				} else {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = false;
						$this->data['message'] = cclang('data_not_change');
					} else {
	            		$this->data['success'] = false;
	            		$this->data['message'] = cclang('data_not_change');
						$this->data['redirect'] = base_url('administrator/kurir');
					}
				}

			} else {
				$this->data['success'] = false;
				$this->data['message'] = 'Opss validation failed';
				$this->data['errors'] = $this->form_validation->error_array();
			}
		
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Email tidak sesuai format';
			$this->data['errors'] = $this->form_validation->error_array();
		}
		echo json_encode($this->data);
	}
	
		/**
	* Update view Kurirs
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('kurir_update');

		$this->data['kurir'] = $this->model_kurir->find($id);

		$this->template->title('Kurir Update');
		$this->render('backend/standart/administrator/kurir/kurir_update', $this->data);
	}

	/**
	* Update Kurirs
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('kurir_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('kurir_nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kurir_email', 'Email', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kurir_nik', 'NIK', 'trim|required|max_length[255]');
		//$this->form_validation->set_rules('kurir_kurir_npwp_name', 'NPWP', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kurir_provinsi', 'Provinsi', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kurir_kota', 'Kota', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kurir_kecamatan', 'Kecamatan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kurir_kelurahan', 'Kelurahan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kurir_alamat', 'Alamat', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kurir_cabang', 'Cabang', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kurir_kurir_foto_name', 'Foto', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kurir_status', 'Status', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kurir_longitude', 'Longitude', 'trim|max_length[255]');
		$this->form_validation->set_rules('kurir_latitude', 'Latitude', 'trim|max_length[255]');
		$kalimat = $this->input->post('kurir_email');
		
		if(strpos($kalimat, '.') AND strpos($kalimat, '.')) {
			if ($this->form_validation->run()) {
				$kurir_kurir_npwp_uuid = $this->input->post('kurir_kurir_npwp_uuid');
				$kurir_kurir_npwp_name = $this->input->post('kurir_kurir_npwp_name');
				$kurir_kurir_foto_uuid = $this->input->post('kurir_kurir_foto_uuid');
				$kurir_kurir_foto_name = $this->input->post('kurir_kurir_foto_name');
				$save_data = [];
				if(empty($this->input->post('kurir_password'))){
					$save_data = [
						'kurir_nama' => $this->input->post('kurir_nama'),
						'kurir_email' => $this->input->post('kurir_email'),
						'kurir_nik' => $this->input->post('kurir_nik'),
						'kurir_provinsi' => $this->input->post('kurir_provinsi'),
						'kurir_kota' => $this->input->post('kurir_kota'),
						'kurir_kecamatan' => $this->input->post('kurir_kecamatan'),
						'kurir_kelurahan' => $this->input->post('kurir_kelurahan'),
						'kurir_alamat' => $this->input->post('kurir_alamat'),
						'kurir_cabang' => $this->input->post('kurir_cabang'),
						'kurir_status' => $this->input->post('kurir_status'),
						'kurir_longitude' => $this->input->post('kurir_longitude'),
						'kurir_latitude' => $this->input->post('kurir_latitude'),
					];
				}else{
					$save_data = [
						'kurir_nama' => $this->input->post('kurir_nama'),
						'kurir_email' => $this->input->post('kurir_email'),
						'kurir_password' => md5($this->input->post('kurir_password')),
						'kurir_nik' => $this->input->post('kurir_nik'),
						'kurir_provinsi' => $this->input->post('kurir_provinsi'),
						'kurir_kota' => $this->input->post('kurir_kota'),
						'kurir_kecamatan' => $this->input->post('kurir_kecamatan'),
						'kurir_kelurahan' => $this->input->post('kurir_kelurahan'),
						'kurir_alamat' => $this->input->post('kurir_alamat'),
						'kurir_cabang' => $this->input->post('kurir_cabang'),
						'kurir_status' => $this->input->post('kurir_status'),
						'kurir_longitude' => $this->input->post('kurir_longitude'),
						'kurir_latitude' => $this->input->post('kurir_latitude'),
					];
				}
				

				if (!is_dir(FCPATH . '/uploads/kurir/')) {
					mkdir(FCPATH . '/uploads/kurir/');
				}

				if (!empty($kurir_kurir_npwp_uuid)) {
					$kurir_kurir_npwp_name_copy = date('YmdHis') . '-' . $kurir_kurir_npwp_name;

					rename(FCPATH . 'uploads/tmp/' . $kurir_kurir_npwp_uuid . '/' . $kurir_kurir_npwp_name, 
							FCPATH . 'uploads/kurir/' . $kurir_kurir_npwp_name_copy);

					if (!is_file(FCPATH . '/uploads/kurir/' . $kurir_kurir_npwp_name_copy)) {
						echo json_encode([
							'success' => false,
							'message' => 'Error uploading file'
							]);
						exit;
					}

					$save_data['kurir_npwp'] = $kurir_kurir_npwp_name_copy;
				}
			
				if (!empty($kurir_kurir_foto_uuid)) {
					$kurir_kurir_foto_name_copy = date('YmdHis') . '-' . $kurir_kurir_foto_name;

					rename(FCPATH . 'uploads/tmp/' . $kurir_kurir_foto_uuid . '/' . $kurir_kurir_foto_name, 
							FCPATH . 'uploads/kurir/' . $kurir_kurir_foto_name_copy);

					if (!is_file(FCPATH . '/uploads/kurir/' . $kurir_kurir_foto_name_copy)) {
						echo json_encode([
							'success' => false,
							'message' => 'Error uploading file'
							]);
						exit;
					}

					$save_data['kurir_foto'] = $kurir_kurir_foto_name_copy;
				}
			
				
				$save_kurir = $this->model_kurir->change($id, $save_data);

				if ($save_kurir) {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = true;
						$this->data['id'] 	   = $id;
						$this->data['message'] = cclang('success_update_data_stay', [
							anchor('administrator/kurir', ' Go back to list')
						]);
					} else {
						set_message(
							cclang('success_update_data_redirect', [
						]), 'success');

	            		$this->data['success'] = true;
						$this->data['redirect'] = base_url('administrator/kurir');
					}
				} else {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = false;
						$this->data['message'] = cclang('data_not_change');
					} else {
	            		$this->data['success'] = false;
	            		$this->data['message'] = cclang('data_not_change');
						$this->data['redirect'] = base_url('administrator/kurir');
					}
				}
			} else {
				$this->data['success'] = false;
				$this->data['message'] = 'Opss validation failed';
				$this->data['errors'] = $this->form_validation->error_array();
			}
		
		
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Email tidak sesuai format';
			$this->data['errors'] = $this->form_validation->error_array();
		}
		echo json_encode($this->data);
	}
	
	/**
	* delete Kurirs
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('kurir_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'kurir'), 'success');
        } else {
            set_message(cclang('error_delete', 'kurir'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Kurirs
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('kurir_view');

		$this->data['kurir'] = $this->model_kurir->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Kurir Detail');
		$this->render('backend/standart/administrator/kurir/kurir_view', $this->data);
	}
	
	/**
	* delete Kurirs
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$kurir = $this->model_kurir->find($id);

		if (!empty($kurir->kurir_npwp)) {
			$path = FCPATH . '/uploads/kurir/' . $kurir->kurir_npwp;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($kurir->kurir_foto)) {
			$path = FCPATH . '/uploads/kurir/' . $kurir->kurir_foto;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_kurir->remove($id);
	}
	
	/**
	* Upload Image Kurir	* 
	* @return JSON
	*/
	public function upload_kurir_npwp_file()
	{
		if (!$this->is_allowed('kurir_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'kurir',
		]);
	}

	/**
	* Delete Image Kurir	* 
	* @return JSON
	*/
	public function delete_kurir_npwp_file($uuid)
	{
		if (!$this->is_allowed('kurir_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'kurir_npwp', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'kurir',
            'primary_key'       => 'kurir_id',
            'upload_path'       => 'uploads/kurir/'
        ]);
	}

	/**
	* Get Image Kurir	* 
	* @return JSON
	*/
	public function get_kurir_npwp_file($id)
	{
		if (!$this->is_allowed('kurir_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$kurir = $this->model_kurir->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'kurir_npwp', 
            'table_name'        => 'kurir',
            'primary_key'       => 'kurir_id',
            'upload_path'       => 'uploads/kurir/',
            'delete_endpoint'   => 'administrator/kurir/delete_kurir_npwp_file'
        ]);
	}
	
	/**
	* Upload Image Kurir	* 
	* @return JSON
	*/
	public function upload_kurir_foto_file()
	{
		if (!$this->is_allowed('kurir_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'kurir',
		]);
	}

	/**
	* Delete Image Kurir	* 
	* @return JSON
	*/
	public function delete_kurir_foto_file($uuid)
	{
		if (!$this->is_allowed('kurir_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'kurir_foto', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'kurir',
            'primary_key'       => 'kurir_id',
            'upload_path'       => 'uploads/kurir/'
        ]);
	}

	/**
	* Get Image Kurir	* 
	* @return JSON
	*/
	public function get_kurir_foto_file($id)
	{
		if (!$this->is_allowed('kurir_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$kurir = $this->model_kurir->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'kurir_foto', 
            'table_name'        => 'kurir',
            'primary_key'       => 'kurir_id',
            'upload_path'       => 'uploads/kurir/',
            'delete_endpoint'   => 'administrator/kurir/delete_kurir_foto_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('kurir_export');

		$this->model_kurir->export('kurir', 'kurir');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('kurir_export');

		$this->model_kurir->pdf('kurir', 'kurir');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('kurir_export');

		$table = $title = 'kurir';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_kurir->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file kurir.php */
/* Location: ./application/controllers/administrator/Kurir.php */