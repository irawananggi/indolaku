

<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Kurir        <small>Edit Kurir</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/kurir'); ?>">Kurir</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Kurir</h3>
                            <h5 class="widget-user-desc">Edit Kurir</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/kurir/edit_save/'.$this->uri->segment(4)), [
                            'name'    => 'form_kurir', 
                            'class'   => 'form-horizontal form-step', 
                            'id'      => 'form_kurir', 
                            'method'  => 'POST'
                            ]); ?>
                         
                                                <div class="form-group ">
                            <label for="kurir_nama" class="col-sm-2 control-label">Nama 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kurir_nama" id="kurir_nama" placeholder="Nama" value="<?= set_value('kurir_nama', $kurir->kurir_nama); ?>">
                                <small class="info help-block">
                                <b>Input Kurir Nama</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kurir_email" class="col-sm-2 control-label">Email 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kurir_email" id="kurir_email" placeholder="Email" value="<?= set_value('kurir_email', $kurir->kurir_email); ?>">
                                <small class="info help-block">
                                <b>Input Kurir Email</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kurir_password" class="col-sm-2 control-label">Password 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                              <div class="input-group col-md-8 input-password">
                              <input type="password" class="form-control password" name="kurir_password" id="kurir_password" placeholder="Password" value="">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-flat show-password"><i class="fa fa-eye eye"></i></button>
                                </span>
                              </div>
                            <small class="info help-block">
                            <b>Input Kurir Password</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kurir_nik" class="col-sm-2 control-label">NIK 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kurir_nik" id="kurir_nik" placeholder="NIK" value="<?= set_value('kurir_nik', $kurir->kurir_nik); ?>" min="1" onkeypress="return event.charCode >= 48 && event.charCode <=57">
                                <small class="info help-block">
                                <b>Input Kurir Nik</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kurir_npwp" class="col-sm-2 control-label">NPWP 
                            </label>
                            <div class="col-sm-8">
                                <div id="kurir_kurir_npwp_galery"></div>
                                <input class="data_file data_file_uuid" name="kurir_kurir_npwp_uuid" id="kurir_kurir_npwp_uuid" type="hidden" value="<?= set_value('kurir_kurir_npwp_uuid'); ?>">
                                <input class="data_file" name="kurir_kurir_npwp_name" id="kurir_kurir_npwp_name" type="hidden" value="<?= set_value('kurir_kurir_npwp_name', $kurir->kurir_npwp); ?>">
                                <small class="info help-block">
                                <b>Input Kurir Npwp</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                  
                                                <div class="form-group ">
                            <label for="kurir_provinsi" class="col-sm-2 control-label">Provinsi 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen" onchange="pilihprovinsi()" name="kurir_provinsi" id="kurir_provinsi" data-placeholder="Select Provinsi" >
                                    <option value="">Pilih Provinsi</option>
                                    <?php foreach (db_get_all_data('reg_provinces') as $row): ?>
                                    <option <?=  $row->id ==  $kurir->kurir_provinsi ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->name; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Kurir Provinsi</b> Max Length : 11.</small>
                            </div>
                        </div>


                                                 
                                                <div class="form-group ">
                            <label for="kurir_kota" class="col-sm-2 control-label">Kota 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen" onchange="pilihkota()" name="kurir_kota" id="kurir_kota" data-placeholder="Select Kota" >
                                    <option value="">Pilih Kota</option>
                                </select>
                                <small class="info help-block">
                                <b>Input Kurir Kota</b> Max Length : 11.</small>
                            </div>
                        </div>


                         <div class="form-group ">
                            <label for="kurir_kecamatan" class="col-sm-2 control-label">Kecamatan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen" onchange="pilihkecamatan()" name="kurir_kecamatan" id="kurir_kecamatan" data-placeholder="Select Kecamatan" >
                                    <option value="">Pilih Kecamatan</option>
                                </select>
                                <small class="info help-block">
                                <b>Input Kurir Kecamatan</b> Max Length : 11.</small>
                            </div>
                        </div>


                                                 
                                                <div class="form-group ">
                            <label for="kurir_kelurahan" class="col-sm-2 control-label">Kelurahan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen" name="kurir_kelurahan" id="kurir_kelurahan" data-placeholder="Select Kelurahan" >
                                    <option value="">Pilih Kelurahan</option>
                                </select>
                                <small class="info help-block">
                                <b>Input Kurir Kelurahan</b> Max Length : 11.</small>
                            </div>
                        </div>


                                                 
                                                <div class="form-group ">
                            <label for="kurir_alamat" class="col-sm-2 control-label">Alamat 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kurir_alamat" id="kurir_alamat" placeholder="Alamat" value="<?= set_value('kurir_alamat', $kurir->kurir_alamat); ?>">
                                <small class="info help-block">
                                <b>Input Kurir Alamat</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kurir_cabang" class="col-sm-2 control-label">Cabang 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="kurir_cabang" id="kurir_cabang" data-placeholder="Select Cabang" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('cabang') as $row): ?>
                                    <option <?=  $row->cabang_id ==  $kurir->kurir_cabang ? 'selected' : ''; ?> value="<?= $row->cabang_id ?>"><?= $row->cabang_nama; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Kurir Cabang</b> Max Length : 11.</small>
                            </div>
                        </div>


                                                 
                                                <div class="form-group ">
                            <label for="kurir_foto" class="col-sm-2 control-label">Foto 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="kurir_kurir_foto_galery"></div>
                                <input class="data_file data_file_uuid" name="kurir_kurir_foto_uuid" id="kurir_kurir_foto_uuid" type="hidden" value="<?= set_value('kurir_kurir_foto_uuid'); ?>">
                                <input class="data_file" name="kurir_kurir_foto_name" id="kurir_kurir_foto_name" type="hidden" value="<?= set_value('kurir_kurir_foto_name', $kurir->kurir_foto); ?>">
                                <small class="info help-block">
                                <b>Input Kurir Foto</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                  
                                                <div class="form-group ">
                            <label for="kurir_status" class="col-sm-2 control-label">Status 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="kurir_status" id="kurir_status" data-placeholder="Select Status" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('cabang_status') as $row): ?>
                                    <option <?=  $row->cabang_status_id ==  $kurir->kurir_status ? 'selected' : ''; ?> value="<?= $row->cabang_status_id ?>"><?= $row->cabang_status_ket; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Kurir Status</b> Max Length : 11.</small>
                            </div>
                        </div>


                                                 
                                                <div class="form-group ">
                            <label for="kurir_longitude" class="col-sm-2 control-label">Longitude 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kurir_longitude" id="kurir_longitude" placeholder="Longitude" value="<?= set_value('kurir_longitude', $kurir->kurir_longitude); ?>">
                                <small class="info help-block">
                                <b>Input Kurir Longitude</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kurir_latitude" class="col-sm-2 control-label">Latitude 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kurir_latitude" id="kurir_latitude" placeholder="Latitude" value="<?= set_value('kurir_latitude', $kurir->kurir_latitude); ?>">
                                <small class="info help-block">
                                <b>Input Kurir Latitude</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                
                                                 <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                 <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>

	var firstload = 1;
	function pilihprovinsi(){
		var skota = '<?php echo $kurir->kurir_kota; ?>';
		//alert(skota);
		
		 $.ajax({
			type: "GET",
			data: {provinsi_id : $('#kurir_provinsi option:selected').val(), kota_id: skota, first: firstload },
			url: BASE_URL+ "administrator/kurir/getkota1/",
			success: function(data) {
				$('#kurir_kota').html(data);
				if(firstload){
					pilihkota();
				}else{
					$('#kurir_kecamatan').html("<option value=''>Pilih Kecamatan</option>");
					$('#kurir_kelurahan').html("<option value=''>Pilih Kelurahan</option>");
				}
				//alert(data);
			}
		});
	}
	function pilihkota(){
			var skecamatan = '<?php echo $kurir->kurir_kecamatan; ?>';
			//alert(skecamatan);
			
			 $.ajax({
				type: "GET",
				data: {kota_id : $('#kurir_kota option:selected').val(), kecamatan_id: skecamatan, first: firstload},
				url: BASE_URL+ "administrator/kurir/getkecamatan1/",
				success: function(data) {
					//alert(firstload);
					$('#kurir_kecamatan').html(data);
					if(firstload){
						pilihkecamatan();
						firstload = 0;
					}else{
						$('#kurir_kelurahan').html("<option value=''>Pilih Kelurahan</option>");
					}
					//alert(data);
				}
			});
	}
	function pilihkecamatan(){
			var skelurahan = '<?php echo $kurir->kurir_kelurahan; ?>';
			//alert(skelurahan);
			
			 $.ajax({
				type: "GET",
				data: {kecamatan_id : $('#kurir_kecamatan option:selected').val(), kelurahan_id: skelurahan, first: firstload},
				url: BASE_URL+ "administrator/kurir/getkelurahan1/",
				success: function(data) {
					$('#kurir_kelurahan').html(data);
					//alert(data);
				}
			});
	}
    $(document).ready(function(){
       
      pilihprovinsi();
	  
      $('#btn_cancel').click(function(){
        swal({
            title: "Are you sure?",
            text: "the data that you have created will be in the exhaust!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/kurir';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_kurir = $('#form_kurir');
        var data_post = form_kurir.serializeArray();
        var save_type = $(this).attr('data-stype');
        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: form_kurir.attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          $('form').find('.form-group').removeClass('has-error');
          $('form').find('.error-input').remove();
          $('.steps li').removeClass('error');
          if(res.success) {
            var id = $('#kurir_image_galery').find('li').attr('qq-file-id');
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            $('.data_file_uuid').val('');
    
          } else {
            if (res.errors) {
               parseErrorField(res.errors);
            }
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
                     var params = {};
       params[csrf] = token;

       $('#kurir_kurir_npwp_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/kurir/upload_kurir_npwp_file',
              params : params
          },
          deleteFile: {
              enabled: true, // defaults to false
              endpoint: BASE_URL + '/administrator/kurir/delete_kurir_npwp_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/kurir/get_kurir_npwp_file/<?= $kurir->kurir_id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["*"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#kurir_kurir_npwp_galery').fineUploader('getUuid', id);
                   $('#kurir_kurir_npwp_uuid').val(uuid);
                   $('#kurir_kurir_npwp_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#kurir_kurir_npwp_uuid').val();
                  $.get(BASE_URL + '/administrator/kurir/delete_kurir_npwp_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#kurir_kurir_npwp_uuid').val('');
                  $('#kurir_kurir_npwp_name').val('');
                }
              }
          }
      }); /*end kurir_npwp galey*/
                            var params = {};
       params[csrf] = token;

       $('#kurir_kurir_foto_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/kurir/upload_kurir_foto_file',
              params : params
          },
          deleteFile: {
              enabled: true, // defaults to false
              endpoint: BASE_URL + '/administrator/kurir/delete_kurir_foto_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/kurir/get_kurir_foto_file/<?= $kurir->kurir_id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["*"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#kurir_kurir_foto_galery').fineUploader('getUuid', id);
                   $('#kurir_kurir_foto_uuid').val(uuid);
                   $('#kurir_kurir_foto_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#kurir_kurir_foto_uuid').val();
                  $.get(BASE_URL + '/administrator/kurir/delete_kurir_foto_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#kurir_kurir_foto_uuid').val('');
                  $('#kurir_kurir_foto_name').val('');
                }
              }
          }
      }); /*end kurir_foto galey*/
              
       
       

      async function chain(){
      }
       
      chain();


    
    
    }); /*end doc ready*/
</script>