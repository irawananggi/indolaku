<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['kurir'] = 'Kurir';
$lang['kurir_id'] = 'Kurir Id';
$lang['kurir_nama'] = 'Nama';
$lang['kurir_email'] = 'Email';
$lang['kurir_password'] = 'Password';
$lang['kurir_nik'] = 'NIK';
$lang['kurir_npwp'] = 'NPWP';
$lang['kurir_provinsi'] = 'Provinsi';
$lang['kurir_kota'] = 'Kota';
$lang['kurir_kecamatan'] = 'Kecamatan';
$lang['kurir_kelurahan'] = 'Kelurahan';
$lang['kurir_alamat'] = 'Alamat';
$lang['kurir_cabang'] = 'Cabang';
$lang['kurir_foto'] = 'Foto';
$lang['kurir_status'] = 'Status';
$lang['kurir_longitude'] = 'Longitude';
$lang['kurir_latitude'] = 'Latitude';
