<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Reg Provinces Controller
*| --------------------------------------------------------------------------
*| Reg Provinces site
*|
*/
class Reg_provinces extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_reg_provinces');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Reg Provincess
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('reg_provinces_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['reg_provincess'] = $this->model_reg_provinces->get($filter, $field, $this->limit_page, $offset);
		$this->data['reg_provinces_counts'] = $this->model_reg_provinces->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/reg_provinces/index/',
			'total_rows'   => $this->model_reg_provinces->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Reg Provinces List');
		$this->render('backend/standart/administrator/reg_provinces/reg_provinces_list', $this->data);
	}
	
	/**
	* Add new reg_provincess
	*
	*/
	public function add()
	{
		$this->is_allowed('reg_provinces_add');

		$this->template->title('Reg Provinces New');
		$this->render('backend/standart/administrator/reg_provinces/reg_provinces_add', $this->data);
	}

	/**
	* Add New Reg Provincess
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('reg_provinces_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'name' => $this->input->post('name'),
			];

			
			$save_reg_provinces = $this->model_reg_provinces->store($save_data);
            

			if ($save_reg_provinces) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_reg_provinces;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/reg_provinces/edit/' . $save_reg_provinces, 'Edit Reg Provinces'),
						anchor('administrator/reg_provinces', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/reg_provinces/edit/' . $save_reg_provinces, 'Edit Reg Provinces')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/reg_provinces');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/reg_provinces');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Reg Provincess
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('reg_provinces_update');

		$this->data['reg_provinces'] = $this->model_reg_provinces->find($id);

		$this->template->title('Reg Provinces Update');
		$this->render('backend/standart/administrator/reg_provinces/reg_provinces_update', $this->data);
	}

	/**
	* Update Reg Provincess
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('reg_provinces_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'name' => $this->input->post('name'),
			];

			
			$save_reg_provinces = $this->model_reg_provinces->change($id, $save_data);

			if ($save_reg_provinces) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/reg_provinces', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/reg_provinces');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/reg_provinces');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Reg Provincess
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('reg_provinces_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'reg_provinces'), 'success');
        } else {
            set_message(cclang('error_delete', 'reg_provinces'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Reg Provincess
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('reg_provinces_view');

		$this->data['reg_provinces'] = $this->model_reg_provinces->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Reg Provinces Detail');
		$this->render('backend/standart/administrator/reg_provinces/reg_provinces_view', $this->data);
	}
	
	/**
	* delete Reg Provincess
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$reg_provinces = $this->model_reg_provinces->find($id);

		
		
		return $this->model_reg_provinces->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('reg_provinces_export');

		$this->model_reg_provinces->export('reg_provinces', 'reg_provinces');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('reg_provinces_export');

		$this->model_reg_provinces->pdf('reg_provinces', 'reg_provinces');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('reg_provinces_export');

		$table = $title = 'reg_provinces';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_reg_provinces->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file reg_provinces.php */
/* Location: ./application/controllers/administrator/Reg Provinces.php */