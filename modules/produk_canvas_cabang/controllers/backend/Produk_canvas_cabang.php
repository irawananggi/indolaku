<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Produk Canvas Cabang Controller
*| --------------------------------------------------------------------------
*| Produk Canvas Cabang site
*|
*/
class Produk_canvas_cabang extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_produk_canvas_cabang');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Produk Canvas Cabangs
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('produk_canvas_cabang_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['produk_canvas_cabangs'] = $this->model_produk_canvas_cabang->get($filter, $field, $this->limit_page, $offset);
		$this->data['produk_canvas_cabang_counts'] = $this->model_produk_canvas_cabang->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/produk_canvas_cabang/index/',
			'total_rows'   => $this->model_produk_canvas_cabang->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Produk Canvas Di Cabang List');
		$this->render('backend/standart/administrator/produk_canvas_cabang/produk_canvas_cabang_list', $this->data);
	}
	
	/**
	* Add new produk_canvas_cabangs
	*
	*/
	public function add()
	{
		$this->is_allowed('produk_canvas_cabang_add');

		$this->template->title('Produk Canvas Di Cabang New');
		$this->render('backend/standart/administrator/produk_canvas_cabang/produk_canvas_cabang_add', $this->data);
	}

	/**
	* Add New Produk Canvas Cabangs
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('produk_canvas_cabang_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('transaksi_canvas_cabangid', 'Transaksi Canvas Cabangid', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('produk_canvas_cabang_produk_id', 'Produk', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('produk_canvas_cabang_canvas_id', 'Kanvas', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('produk_canvas_cabang_cabang_id', 'Cabang', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('produk_canvas_cabang_qty', 'Qty', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'transaksi_canvas_cabangid' => $this->input->post('transaksi_canvas_cabangid'),
				'produk_canvas_cabang_produk_id' => $this->input->post('produk_canvas_cabang_produk_id'),
				'produk_canvas_cabang_canvas_id' => $this->input->post('produk_canvas_cabang_canvas_id'),
				'produk_canvas_cabang_cabang_id' => $this->input->post('produk_canvas_cabang_cabang_id'),
				'produk_canvas_cabang_qty' => $this->input->post('produk_canvas_cabang_qty'),
			];

			
			$save_produk_canvas_cabang = $this->model_produk_canvas_cabang->store($save_data);
            

			if ($save_produk_canvas_cabang) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_produk_canvas_cabang;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/produk_canvas_cabang/edit/' . $save_produk_canvas_cabang, 'Edit Produk Canvas Cabang'),
						anchor('administrator/produk_canvas_cabang', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/produk_canvas_cabang/edit/' . $save_produk_canvas_cabang, 'Edit Produk Canvas Cabang')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/produk_canvas_cabang');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/produk_canvas_cabang');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Produk Canvas Cabangs
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('produk_canvas_cabang_update');

		$this->data['produk_canvas_cabang'] = $this->model_produk_canvas_cabang->find($id);

		$this->template->title('Produk Canvas Di Cabang Update');
		$this->render('backend/standart/administrator/produk_canvas_cabang/produk_canvas_cabang_update', $this->data);
	}

	/**
	* Update Produk Canvas Cabangs
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('produk_canvas_cabang_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('transaksi_canvas_cabangid', 'Transaksi Canvas Cabangid', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('produk_canvas_cabang_produk_id', 'Produk', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('produk_canvas_cabang_canvas_id', 'Kanvas', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('produk_canvas_cabang_cabang_id', 'Cabang', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('produk_canvas_cabang_qty', 'Qty', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'transaksi_canvas_cabangid' => $this->input->post('transaksi_canvas_cabangid'),
				'produk_canvas_cabang_produk_id' => $this->input->post('produk_canvas_cabang_produk_id'),
				'produk_canvas_cabang_canvas_id' => $this->input->post('produk_canvas_cabang_canvas_id'),
				'produk_canvas_cabang_cabang_id' => $this->input->post('produk_canvas_cabang_cabang_id'),
				'produk_canvas_cabang_qty' => $this->input->post('produk_canvas_cabang_qty'),
			];

			
			$save_produk_canvas_cabang = $this->model_produk_canvas_cabang->change($id, $save_data);

			if ($save_produk_canvas_cabang) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/produk_canvas_cabang', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/produk_canvas_cabang');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/produk_canvas_cabang');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Produk Canvas Cabangs
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('produk_canvas_cabang_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {

			$cek_produk_canvas_cabang = $this->mymodel->withquery("select * from produk_canvas_cabang where produk_canvas_cabang_id = '".$id."'","row");
			if(!empty($cek_produk_canvas_cabang)){
				$dataProd = array(
				   "produk_per_cabang_stok_canvas" =>0
				);
				$up = $this->mymodel->update('produk_per_cabang',$dataProd,"produk_per_cabang_produk_id='".$cek_produk_canvas_cabang->produk_canvas_cabang_produk_id."' AND produk_per_cabang_cabang_id='".$cek_produk_canvas_cabang->produk_canvas_cabang_cabang_id."'");
			}

			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'produk_canvas_cabang'), 'success');
        } else {
            set_message(cclang('error_delete', 'produk_canvas_cabang'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Produk Canvas Cabangs
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('produk_canvas_cabang_view');

		$this->data['produk_canvas_cabang'] = $this->model_produk_canvas_cabang->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Produk Canvas Di Cabang Detail');
		$this->render('backend/standart/administrator/produk_canvas_cabang/produk_canvas_cabang_view', $this->data);
	}
	
	/**
	* delete Produk Canvas Cabangs
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$produk_canvas_cabang = $this->model_produk_canvas_cabang->find($id);

		
		
		return $this->model_produk_canvas_cabang->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('produk_canvas_cabang_export');

		$this->model_produk_canvas_cabang->export('produk_canvas_cabang', 'produk_canvas_cabang');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('produk_canvas_cabang_export');

		$this->model_produk_canvas_cabang->pdf('produk_canvas_cabang', 'produk_canvas_cabang');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('produk_canvas_cabang_export');

		$table = $title = 'produk_canvas_cabang';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_produk_canvas_cabang->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

		/**
	* Update view Produk Canvas Cabangs
	*
	* @var $id String
	*/
	public function add_produk($id)
	{
		$p = un_de($id);
		$no_track = $p['no_track'];
		$this->is_allowed('produk_canvas_cabang_add_produk');
		$cek_id_trans = $this->mymodel->getbywhere('transaksi_canvas_cabang',"transaksi_no_tracking",$no_track,'row');

		$cek_data_produk  =  $this->mymodel->withquery("select * from produk_canvas_cabang where 
				   transaksi_canvas_cabangid = '".$cek_id_trans->transaksi_canvas_cabang_id."'","result");
		$data_x = '';
        foreach ($cek_data_produk as $key1 => $value1) {
          $data_x  .= $value1->produk_canvas_cabang_produk_id;
        }
		// cek($data_x);
		// die();
			/*$this->data['produk'] = $this->mymodel->withquery("select * from produk where 
					   transaksi_jenis = '".$this->input->post('jenis')."' and
					   transaksi_id = '".$this->input->post('id_transaksi')."'","result");*/
		$this->data['produk_canvas_cabang'] = $this->mymodel->getbywhere('produk_canvas_cabang',"transaksi_canvas_cabangid",$cek_id_trans->transaksi_canvas_cabang_id,'result');
		$this->data['canvas'] =  $this->mymodel->getbywhere('kanvas',"kanvas_id",$cek_id_trans->transaksi_canvas_id,'row');
		$this->data['cabang'] =  $this->mymodel->getbywhere('cabang',"cabang_id",$cek_id_trans->transaksi_cabang_id,'row');
		$this->data['produk'] =  $this->mymodel->withquery("select * from produk ","result");
		$this->data['transaksi_canvas_cabang_id'] =  $cek_id_trans->transaksi_canvas_cabang_id;
		$this->data['id_trans'] =  $id;
		$this->template->title('Produk Canvas Di Cabang Update');
		$this->render('backend/standart/administrator/produk_canvas_cabang/produk_canvas_cabang_add_produk', $this->data);
	}

	public function save_add_produk()
	{
		if (!$this->is_allowed('produk_canvas_cabang_add_produk', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('produk_id', 'Produk', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('qty', 'Qty', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {

			$cek_produk_cabang = $this->mymodel->withquery("select * from produk_per_cabang where produk_per_cabang_produk_id = '".$this->input->post('produk_id')."' and produk_per_cabang_cabang_id='".$this->input->post('cabang_id')."'","row");
			if(!empty($cek_produk_cabang)){
				$dataProd = array(
				   "produk_per_cabang_stok_canvas" => $this->input->post('qty')+$cek_produk_cabang->produk_per_cabang_stok_canvas
				);
				$up = $this->mymodel->update('produk_per_cabang',$dataProd,"produk_per_cabang_produk_id='".$this->input->post('produk_id')."' AND produk_per_cabang_cabang_id='".$this->input->post('cabang_id')."'");
			}

			$cek_produk_canvas = $this->mymodel->withquery("select * from produk_per_canvas where produk_per_canvas_produkid = '".$this->input->post('produk_id')."' and produk_per_canvas_canvasid='".$this->input->post('kanvas_id')."'","row");

			if(empty($cek_produk_canvas)){
				$dataProd = array(
				   "produk_per_canvas_produkid" =>$this->input->post('produk_id'),
				   "produk_per_canvas_canvasid" =>$this->input->post('kanvas_id'),
				);
				$this->mymodel->insert('produk_per_canvas',$dataProd);
			}
			$save_data = [
				'transaksi_canvas_cabangid' => $this->input->post('transaksi_canvas_cabang_id'),
				'produk_canvas_cabang_produk_id' => $this->input->post('produk_id'),
				'produk_canvas_cabang_canvas_id' => $this->input->post('kanvas_id'),
				'produk_canvas_cabang_cabang_id' => $this->input->post('cabang_id'),
				'produk_canvas_cabang_qty' => $this->input->post('qty'),
			];

			
			$save_produk_canvas_cabang = $this->model_produk_canvas_cabang->store($save_data);
			$no_track = $this->input->post('id_trans');
			if ($save_produk_canvas_cabang) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $no_track;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/transaksi_canvas_cabang/add_produk/' . $no_track, 'Add Produk Canvas Cabang'),
						anchor('administrator/transaksi_canvas_cabang', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/transaksi_canvas_cabang/add_produk/' . $no_track, 'Add Produk Canvas Cabang')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/transaksi_canvas_cabang/add_produk/'.$no_track);
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/transaksi_canvas_cabang');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
}


/* End of file produk_canvas_cabang.php */
/* Location: ./application/controllers/administrator/Produk Canvas Cabang.php */