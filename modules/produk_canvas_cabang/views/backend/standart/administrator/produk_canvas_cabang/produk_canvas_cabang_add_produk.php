

<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Produk Canvas Di Cabang        <small>Edit Produk Canvas Di Cabang</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/produk_canvas_cabang'); ?>">Produk Canvas Di Cabang</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Produk Canvas Di Cabang</h3>
                            <h5 class="widget-user-desc">Edit Produk Canvas Di Cabang</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/produk_canvas_cabang/save_add_produk/'.$this->uri->segment(4)), [
                            'name'    => 'form_produk_canvas_cabang', 
                            'class'   => 'form-horizontal form-step', 
                            'id'      => 'form_produk_canvas_cabang', 
                            'method'  => 'POST'
                            ]); ?>
                          
                          <div class="form-group ">
                            <div class="col-sm-6">

                              <input type="hidden" class="form-control" name="transaksi_canvas_cabang_id" id="transaksi_canvas_cabang_id" value="<?php echo $transaksi_canvas_cabang_id; ?>">
                              <input type="hidden" class="form-control" name="id_trans" id="id_trans" value="<?php echo $id_trans; ?>">
                              <div class="form-group ">
                              <label for="kanvas_id" class="col-sm-2 control-label">Kanvas 
                              <i class="required">*</i>
                              </label>
                              <div class="col-sm-8">
                              <input type="hidden" class="form-control" name="kanvas_id" id="kanvas_id" placeholder="Id Kanvas" value="<?php echo $canvas->kanvas_id; ?>">
                              <input type="input" class="form-control" name="kanvas_nama" id="kanvas_nama" placeholder="Kanvas" value="<?php echo $canvas->kanvas_nama; ?>" disabled>
                              <small class="info help-block">
                              </div>
                              </div>

                              <div class="form-group ">
                              <label for="cabang_id" class="col-sm-2 control-label">Cabang 
                              <i class="required">*</i>
                              </label>
                              <div class="col-sm-8">
                              <input type="hidden" class="form-control" name="cabang_id" id="cabang_id" placeholder="id Kanvas" value="<?php echo $cabang->cabang_id; ?>">
                              <input type="input" class="form-control" name="cabang_nama" id="cabang_nama" placeholder="Nama Cabang" value="<?= set_value('cabang_nama', $cabang->cabang_nama); ?>" disabled>
                              <small class="info help-block">
                              </div>
                              </div>
                            </div>
                          </div>
                            <div class="col-sm-6">
                                                   <div class="form-group ">
                                <label for="produk_id" class="col-sm-2 control-label">Produk 
                                <i class="required">*</i>
                                </label>
                                <div class="col-sm-8">
                                    <select  class="form-control chosen chosen-select-deselect" name="produk_id" id="produk_id" data-placeholder="Select Produk" >
                                        <option value=""></option>
                                        <?php foreach ($produk as $row): ?>
                                        <option value="<?= $row->produk_id ?>"><?= $row->produk_nama; ?></option>
                                        <?php endforeach; ?>  
                                    </select>
                                    <small class="info help-block">
                                </div>
                            </div>
                          </div>
                          <div class="col-sm-2">
                                                 
                                                      <div class="form-group ">
                                  <label for="qty" class="col-sm-2 control-label">Qty 
                                  <i class="required">*</i>
                                  </label>
                                  <div class="col-sm-8">
                                      <input type="number" class="form-control" name="qty" id="qty" placeholder="2" value="<?= set_value('qty', $produk_canvas_cabang->qty); ?>">
                                      <small class="info help-block">
                                  </div>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="Simpan data produk">
                            <i class="ion ion-ios-list-outline" ></i> Save
                            </a>
                          </div>
                                                
                                                 <!-- <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div> -->
                                                 <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->


            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <div class="table-responsive"> 
                    <table class="table table-bordered table-striped dataTable">
                      <thead>
                      <tr class="">
                        <th>NO
                        </th>
                        <th> Produk</th>
                        <th> Qty</th>
                        <th>Action</th>
                      </tr>
                      </thead>
                      <tbody id="tbody_produk_canvas_cabang">
                        <?php $no = 1; foreach($produk_canvas_cabang as $produk_canvas_cabang): 
                        $data_prod =  $this->mymodel->getbywhere('produk',"produk_id",$produk_canvas_cabang->produk_canvas_cabang_produk_id,'row');
                        ?>
                        <tr>
                          <td><?php echo $no;?>
                          </td>
                          <td><?php echo $data_prod->produk_nama;?>
                          </td>
                          <td><?php echo $produk_canvas_cabang->produk_canvas_cabang_qty;?>
                          </td>       
                          <td>
                              <a href="javascript:void(0);" data-href="<?= site_url('administrator/produk_canvas_cabang/delete/' . $produk_canvas_cabang->produk_canvas_cabang_id); ?>" class="label-default remove-data"><i class="fa fa-close"></i> <?= cclang('remove_button'); ?></a>
                          </td>                 
                          </tr>
                          <?php $no++; endforeach; ?>
                          <?php if ($produk_canvas_cabang == 0) :?>
                        <tr>
                          <td colspan="100">
                          Produk tidak ditemukan
                          </td>
                        </tr>
                        <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){
       
      
   
    $('.remove-data').click(function(){

      var url = $(this).attr('data-href');

      swal({
          title: "<?= cclang('are_you_sure'); ?>",
          text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "<?= cclang('yes_delete_it'); ?>",
          cancelButtonText: "<?= cclang('no_cancel_plx'); ?>",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            document.location.href = url;            
          }
        });

      return false;
    });

             
      $('#btn_cancel').click(function(){
        swal({
            title: "Are you sure?",
            text: "the data that you have created will be in the exhaust!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/produk_canvas_cabang';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_produk_canvas_cabang = $('#form_produk_canvas_cabang');
        var data_post = form_produk_canvas_cabang.serializeArray();
        var save_type = $(this).attr('data-stype');
        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: form_produk_canvas_cabang.attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          $('form').find('.form-group').removeClass('has-error');
          $('form').find('.error-input').remove();
          $('.steps li').removeClass('error');
          if(res.success) {
            var id = $('#produk_canvas_cabang_image_galery').find('li').attr('qq-file-id');
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            $('.data_file_uuid').val('');
    
          } else {
            if (res.errors) {
               parseErrorField(res.errors);
            }
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
       
       
       

      async function chain(){
      }
       
      chain();


    
    
    }); /*end doc ready*/
</script>