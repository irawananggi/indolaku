<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['produk_canvas_cabang'] = 'Produk Canvas di Cabang';
$lang['produk_canvas_cabang_id'] = 'Produk Canvas Cabang Id';
$lang['transaksi_canvas_cabangid'] = 'Transaksi Canvas Cabangid';
$lang['produk_canvas_cabang_produk_id'] = 'Produk';
$lang['produk_canvas_cabang_canvas_id'] = 'Kanvas';
$lang['produk_canvas_cabang_cabang_id'] = 'Cabang';
$lang['produk_canvas_cabang_qty'] = 'Qty';
