<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Alasanbatal Controller
*| --------------------------------------------------------------------------
*| Alasanbatal site
*|
*/
class Alasanbatal extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_alasanbatal');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Alasanbatals
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('alasanbatal_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['alasanbatals'] = $this->model_alasanbatal->get($filter, $field, $this->limit_page, $offset);
		$this->data['alasanbatal_counts'] = $this->model_alasanbatal->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/alasanbatal/index/',
			'total_rows'   => $this->model_alasanbatal->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Alasan Batal List');
		$this->render('backend/standart/administrator/alasanbatal/alasanbatal_list', $this->data);
	}
	
	/**
	* Add new alasanbatals
	*
	*/
	public function add()
	{
		$this->is_allowed('alasanbatal_add');

		$this->template->title('Alasan Batal New');
		$this->render('backend/standart/administrator/alasanbatal/alasanbatal_add', $this->data);
	}

	/**
	* Add New Alasanbatals
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('alasanbatal_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('alasanbatal_keterangan', 'Keterangan', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'alasanbatal_keterangan' => $this->input->post('alasanbatal_keterangan'),
			];

			
			$save_alasanbatal = $this->model_alasanbatal->store($save_data);
            

			if ($save_alasanbatal) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_alasanbatal;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/alasanbatal/edit/' . $save_alasanbatal, 'Edit Alasanbatal'),
						anchor('administrator/alasanbatal', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/alasanbatal/edit/' . $save_alasanbatal, 'Edit Alasanbatal')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/alasanbatal');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/alasanbatal');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Alasanbatals
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('alasanbatal_update');

		$this->data['alasanbatal'] = $this->model_alasanbatal->find($id);

		$this->template->title('Alasan Batal Update');
		$this->render('backend/standart/administrator/alasanbatal/alasanbatal_update', $this->data);
	}

	/**
	* Update Alasanbatals
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('alasanbatal_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('alasanbatal_keterangan', 'Keterangan', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'alasanbatal_keterangan' => $this->input->post('alasanbatal_keterangan'),
			];

			
			$save_alasanbatal = $this->model_alasanbatal->change($id, $save_data);

			if ($save_alasanbatal) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/alasanbatal', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/alasanbatal');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/alasanbatal');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Alasanbatals
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('alasanbatal_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'alasanbatal'), 'success');
        } else {
            set_message(cclang('error_delete', 'alasanbatal'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Alasanbatals
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('alasanbatal_view');

		$this->data['alasanbatal'] = $this->model_alasanbatal->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Alasan Batal Detail');
		$this->render('backend/standart/administrator/alasanbatal/alasanbatal_view', $this->data);
	}
	
	/**
	* delete Alasanbatals
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$alasanbatal = $this->model_alasanbatal->find($id);

		
		
		return $this->model_alasanbatal->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('alasanbatal_export');

		$this->model_alasanbatal->export('alasanbatal', 'alasanbatal');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('alasanbatal_export');

		$this->model_alasanbatal->pdf('alasanbatal', 'alasanbatal');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('alasanbatal_export');

		$table = $title = 'alasanbatal';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_alasanbatal->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file alasanbatal.php */
/* Location: ./application/controllers/administrator/Alasanbatal.php */