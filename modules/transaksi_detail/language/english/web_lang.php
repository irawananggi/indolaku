<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['transaksi_detail'] = 'Transaksi Detail';
$lang['transaksi_detail_id'] = 'Transaksi Detail Id';
$lang['transaksi_detail_product_id'] = 'Produk';
$lang['transaksi_detail_harga'] = 'Harga';
$lang['transaksi_detail_qty'] = 'QTY';
$lang['transaksi_detail_total'] = 'Total';
$lang['transaksi_jenis'] = 'Jenis';
$lang['transaksi_id'] = 'Trans ID';
