<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Transaksi Detail Controller
*| --------------------------------------------------------------------------
*| Transaksi Detail site
*|
*/
class Transaksi_detail extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_transaksi_detail');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Transaksi Details
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('transaksi_detail_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['transaksi_details'] = $this->model_transaksi_detail->get($filter, $field, $this->limit_page, $offset);
		$this->data['transaksi_detail_counts'] = $this->model_transaksi_detail->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/transaksi_detail/index/',
			'total_rows'   => $this->model_transaksi_detail->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Transaksi Detail List');
		$this->render('backend/standart/administrator/transaksi_detail/transaksi_detail_list', $this->data);
	}
	
	/**
	* Add new transaksi_details
	*
	*/
	public function add()
	{
		$this->is_allowed('transaksi_detail_add');

		$this->template->title('Transaksi Detail New');
		$this->render('backend/standart/administrator/transaksi_detail/transaksi_detail_add', $this->data);
	}

	/**
	* Add New Transaksi Details
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('transaksi_detail_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('transaksi_detail_product_id', 'Produk', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_detail_harga', 'Harga', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_detail_qty', 'QTY', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_detail_total', 'Total', 'trim|required|max_length[12]');
		$this->form_validation->set_rules('transaksi_jenis', 'Jenis', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_id', 'Trans ID', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'transaksi_detail_product_id' => $this->input->post('transaksi_detail_product_id'),
				'transaksi_detail_harga' => $this->input->post('transaksi_detail_harga'),
				'transaksi_detail_qty' => $this->input->post('transaksi_detail_qty'),
				'transaksi_detail_total' => $this->input->post('transaksi_detail_total'),
				'transaksi_jenis' => $this->input->post('transaksi_jenis'),
				'transaksi_id' => $this->input->post('transaksi_id'),
			];

			
			$save_transaksi_detail = $this->model_transaksi_detail->store($save_data);
            

			if ($save_transaksi_detail) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_transaksi_detail;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/transaksi_detail/edit/' . $save_transaksi_detail, 'Edit Transaksi Detail'),
						anchor('administrator/transaksi_detail', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/transaksi_detail/edit/' . $save_transaksi_detail, 'Edit Transaksi Detail')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/transaksi_detail');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/transaksi_detail');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Transaksi Details
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('transaksi_detail_update');

		$this->data['transaksi_detail'] = $this->model_transaksi_detail->find($id);

		$this->template->title('Transaksi Detail Update');
		$this->render('backend/standart/administrator/transaksi_detail/transaksi_detail_update', $this->data);
	}

	/**
	* Update Transaksi Details
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('transaksi_detail_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('transaksi_detail_product_id', 'Produk', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_detail_harga', 'Harga', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_detail_qty', 'QTY', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_detail_total', 'Total', 'trim|required|max_length[12]');
		$this->form_validation->set_rules('transaksi_jenis', 'Jenis', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('transaksi_id', 'Trans ID', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'transaksi_detail_product_id' => $this->input->post('transaksi_detail_product_id'),
				'transaksi_detail_harga' => $this->input->post('transaksi_detail_harga'),
				'transaksi_detail_qty' => $this->input->post('transaksi_detail_qty'),
				'transaksi_detail_total' => $this->input->post('transaksi_detail_total'),
				'transaksi_jenis' => $this->input->post('transaksi_jenis'),
				'transaksi_id' => $this->input->post('transaksi_id'),
			];

			
			$save_transaksi_detail = $this->model_transaksi_detail->change($id, $save_data);

			if ($save_transaksi_detail) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/transaksi_detail', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/transaksi_detail');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/transaksi_detail');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Transaksi Details
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('transaksi_detail_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'transaksi_detail'), 'success');
        } else {
            set_message(cclang('error_delete', 'transaksi_detail'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Transaksi Details
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('transaksi_detail_view');

		$this->data['transaksi_detail'] = $this->model_transaksi_detail->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Transaksi Detail Detail');
		$this->render('backend/standart/administrator/transaksi_detail/transaksi_detail_view', $this->data);
	}
	
	/**
	* delete Transaksi Details
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$transaksi_detail = $this->model_transaksi_detail->find($id);

		
		
		return $this->model_transaksi_detail->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('transaksi_detail_export');

		$this->model_transaksi_detail->export('transaksi_detail', 'transaksi_detail');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('transaksi_detail_export');

		$this->model_transaksi_detail->pdf('transaksi_detail', 'transaksi_detail');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('transaksi_detail_export');

		$table = $title = 'transaksi_detail';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_transaksi_detail->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file transaksi_detail.php */
/* Location: ./application/controllers/administrator/Transaksi Detail.php */