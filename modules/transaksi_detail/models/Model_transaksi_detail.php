<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_transaksi_detail extends MY_Model {

    private $primary_key    = 'transaksi_detail_id';
    private $table_name     = 'transaksi_detail';
    private $field_search   = ['transaksi_detail_product_id', 'transaksi_detail_harga', 'transaksi_detail_qty', 'transaksi_detail_total', 'transaksi_jenis', 'transaksi_id'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "transaksi_detail.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "transaksi_detail.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "transaksi_detail.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "transaksi_detail.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "transaksi_detail.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "transaksi_detail.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('transaksi_detail.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('produk', 'produk.produk_id = transaksi_detail.transaksi_detail_product_id', 'LEFT');
        $this->db->join('transaksi_jenis', 'transaksi_jenis.transaksi_jenis_id = transaksi_detail.transaksi_jenis', 'LEFT');
        
        $this->db->select('transaksi_detail.*,produk.produk_nama as produk_produk_nama,transaksi_jenis.transaksi_jenis_keterangan as transaksi_jenis_transaksi_jenis_keterangan');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_transaksi_detail.php */
/* Location: ./application/models/Model_transaksi_detail.php */