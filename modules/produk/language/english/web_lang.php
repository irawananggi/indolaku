<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['produk'] = 'Produk';
$lang['produk_id'] = 'Produk Id';
$lang['produk_nama'] = 'Nama';
$lang['produk_kategori'] = 'Kategori';
$lang['produk_foto'] = 'Foto';
$lang['produk_harga'] = 'Harga';
$lang['produk_harga_promo'] = 'Harga Promo';
