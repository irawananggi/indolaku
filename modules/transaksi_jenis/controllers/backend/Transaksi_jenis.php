<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Transaksi Jenis Controller
*| --------------------------------------------------------------------------
*| Transaksi Jenis site
*|
*/
class Transaksi_jenis extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_transaksi_jenis');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Transaksi Jeniss
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('transaksi_jenis_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['transaksi_jeniss'] = $this->model_transaksi_jenis->get($filter, $field, $this->limit_page, $offset);
		$this->data['transaksi_jenis_counts'] = $this->model_transaksi_jenis->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/transaksi_jenis/index/',
			'total_rows'   => $this->model_transaksi_jenis->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Transaksi Jenis List');
		$this->render('backend/standart/administrator/transaksi_jenis/transaksi_jenis_list', $this->data);
	}
	
	/**
	* Add new transaksi_jeniss
	*
	*/
	public function add()
	{
		$this->is_allowed('transaksi_jenis_add');

		$this->template->title('Transaksi Jenis New');
		$this->render('backend/standart/administrator/transaksi_jenis/transaksi_jenis_add', $this->data);
	}

	/**
	* Add New Transaksi Jeniss
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('transaksi_jenis_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('transaksi_jenis_keterangan', 'Keterangan', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'transaksi_jenis_keterangan' => $this->input->post('transaksi_jenis_keterangan'),
			];

			
			$save_transaksi_jenis = $this->model_transaksi_jenis->store($save_data);
            

			if ($save_transaksi_jenis) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_transaksi_jenis;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/transaksi_jenis/edit/' . $save_transaksi_jenis, 'Edit Transaksi Jenis'),
						anchor('administrator/transaksi_jenis', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/transaksi_jenis/edit/' . $save_transaksi_jenis, 'Edit Transaksi Jenis')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/transaksi_jenis');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/transaksi_jenis');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Transaksi Jeniss
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('transaksi_jenis_update');

		$this->data['transaksi_jenis'] = $this->model_transaksi_jenis->find($id);

		$this->template->title('Transaksi Jenis Update');
		$this->render('backend/standart/administrator/transaksi_jenis/transaksi_jenis_update', $this->data);
	}

	/**
	* Update Transaksi Jeniss
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('transaksi_jenis_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('transaksi_jenis_keterangan', 'Keterangan', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'transaksi_jenis_keterangan' => $this->input->post('transaksi_jenis_keterangan'),
			];

			
			$save_transaksi_jenis = $this->model_transaksi_jenis->change($id, $save_data);

			if ($save_transaksi_jenis) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/transaksi_jenis', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/transaksi_jenis');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/transaksi_jenis');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Transaksi Jeniss
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('transaksi_jenis_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'transaksi_jenis'), 'success');
        } else {
            set_message(cclang('error_delete', 'transaksi_jenis'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Transaksi Jeniss
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('transaksi_jenis_view');

		$this->data['transaksi_jenis'] = $this->model_transaksi_jenis->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Transaksi Jenis Detail');
		$this->render('backend/standart/administrator/transaksi_jenis/transaksi_jenis_view', $this->data);
	}
	
	/**
	* delete Transaksi Jeniss
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$transaksi_jenis = $this->model_transaksi_jenis->find($id);

		
		
		return $this->model_transaksi_jenis->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('transaksi_jenis_export');

		$this->model_transaksi_jenis->export('transaksi_jenis', 'transaksi_jenis');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('transaksi_jenis_export');

		$this->model_transaksi_jenis->pdf('transaksi_jenis', 'transaksi_jenis');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('transaksi_jenis_export');

		$table = $title = 'transaksi_jenis';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_transaksi_jenis->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file transaksi_jenis.php */
/* Location: ./application/controllers/administrator/Transaksi Jenis.php */