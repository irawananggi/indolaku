<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_kanvas extends MY_Model {

    private $primary_key    = 'kanvas_id';
    private $table_name     = 'kanvas';
    private $field_search   = ['kanvas_nama', 'kanvas_jenis_kelamin', 'kanvas_tgl_lahir', 'kanvas_nik', 'kanvas_npwp', 'kanvas_telp', 'kanvas_email', 'kanvas_password', 'kanvas_provinsi', 'kanvas_kota', 'kanvas_kec', 'kanvas_kelurahan', 'kanvas_rt', 'kanvas_rw', 'kanvas_alamat', 'kanvas_cabang', 'kanvas_foto', 'kanvas_status', 'kanvas_longitude', 'kanvas_latitude'];

    public function __construct()
    {
        $config = array(
            'primary_key'   => $this->primary_key,
            'table_name'    => $this->table_name,
            'field_search'  => $this->field_search,
         );

        parent::__construct($config);
    }

	
	public function withquery($query1,$result)
	  {
		$query = $this->db->query($query1);
		if ($result=='result') {
		  return $query->result();
		}else {
			return $query->row();
		}
	}
    public function count_all($q = null, $field = null)
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "kanvas.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "kanvas.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "kanvas.".$field . " LIKE '%" . $q . "%' )";
        }

        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $query = $this->db->get($this->table_name);

        return $query->num_rows();
    }

    public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
    {
        $iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
        $field = $this->scurity($field);

        if (empty($field)) {
            foreach ($this->field_search as $field) {
                if ($iterasi == 1) {
                    $where .= "kanvas.".$field . " LIKE '%" . $q . "%' ";
                } else {
                    $where .= "OR " . "kanvas.".$field . " LIKE '%" . $q . "%' ";
                }
                $iterasi++;
            }

            $where = '('.$where.')';
        } else {
            $where .= "(" . "kanvas.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
            $this->db->select($select_field);
        }
        
        $this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
                $this->db->order_by('kanvas.'.$this->primary_key, "DESC");
                $query = $this->db->get($this->table_name);

        return $query->result();
    }

    public function join_avaiable() {
        $this->db->join('reg_provinces', 'reg_provinces.id = kanvas.kanvas_provinsi', 'LEFT');
        $this->db->join('reg_regencies', 'reg_regencies.id = kanvas.kanvas_kota', 'LEFT');
        $this->db->join('reg_districts', 'reg_districts.id = kanvas.kanvas_kec', 'LEFT');
        $this->db->join('reg_villages', 'reg_villages.id = kanvas.kanvas_kelurahan', 'LEFT');
        $this->db->join('cabang', 'cabang.cabang_id = kanvas.kanvas_cabang', 'LEFT');
        
        $this->db->select('kanvas.*,reg_provinces.name as reg_provinces_name,reg_regencies.name as reg_regencies_name,reg_districts.name as reg_districts_name,reg_villages.name as reg_villages_name,cabang.cabang_nama as cabang_cabang_nama');


        return $this;
    }

    public function filter_avaiable() {

        if (!$this->aauth->is_admin()) {
            }

        return $this;
    }

}

/* End of file Model_kanvas.php */
/* Location: ./application/models/Model_kanvas.php */