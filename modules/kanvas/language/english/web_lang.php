<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['kanvas'] = 'Kanvas';
$lang['kanvas_id'] = 'Kanvas Id';
$lang['kanvas_nama'] = 'Nama';
$lang['kanvas_jenis_kelamin'] = 'Jenis Kelamin';
$lang['kanvas_tgl_lahir'] = 'Tanggal Lahir';
$lang['kanvas_nik'] = 'NIK';
$lang['kanvas_npwp'] = 'NPWP';
$lang['kanvas_telp'] = 'Telp';
$lang['kanvas_email'] = 'Email';
$lang['kanvas_password'] = 'Password';
$lang['kanvas_provinsi'] = 'Provinsi';
$lang['kanvas_kota'] = 'Kota/Kab';
$lang['kanvas_kec'] = 'Kecamatan';
$lang['kanvas_kelurahan'] = 'Kelurahan';
$lang['kanvas_rt'] = 'RT';
$lang['kanvas_rw'] = 'RW';
$lang['kanvas_alamat'] = 'Alamat';
$lang['kanvas_cabang'] = 'Cabang';
$lang['kanvas_foto'] = 'Foto';
$lang['kanvas_status'] = 'Status';
$lang['kanvas_longitude'] = 'Longitude';
$lang['kanvas_latitude'] = 'Latitude';
