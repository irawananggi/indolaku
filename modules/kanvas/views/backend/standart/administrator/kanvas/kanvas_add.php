
<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Kanvas        <small><?= cclang('new', ['Kanvas']); ?> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/kanvas'); ?>">Kanvas</a></li>
        <li class="active"><?= cclang('new'); ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Kanvas</h3>
                            <h5 class="widget-user-desc"><?= cclang('new', ['Kanvas']); ?></h5>
                            <hr>
                        </div>
                        <?= form_open('', [
                            'name'    => 'form_kanvas', 
                            'class'   => 'form-horizontal form-step', 
                            'id'      => 'form_kanvas', 
                            'enctype' => 'multipart/form-data', 
                            'method'  => 'POST'
                            ]); ?>
                         
                                                <div class="form-group ">
                            <label for="kanvas_nama" class="col-sm-2 control-label">Nama 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_nama" id="kanvas_nama" placeholder="Nama" value="<?= set_value('kanvas_nama'); ?>">
                                <small class="info help-block">
                                <b>Input Kanvas Nama</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_jenis_kelamin" class="col-sm-2 control-label">Jenis Kelamin 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="kanvas_jenis_kelamin" id="kanvas_jenis_kelamin" data-placeholder="Select Jenis Kelamin" >
                                    <option value=""></option>
                                    <option value="1">Laki Laki</option>
                                    <option value="2">Perempuan</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_tgl_lahir" class="col-sm-2 control-label">Tanggal Lahir 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datepicker" name="kanvas_tgl_lahir"  placeholder="Tanggal Lahir" id="kanvas_tgl_lahir">
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_nik" class="col-sm-2 control-label">NIK 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_nik" id="kanvas_nik" placeholder="NIK" value="<?= set_value('kanvas_nik'); ?>" min="1" onkeypress="return event.charCode >= 48 && event.charCode <=57">
                                <small class="info help-block">
                                <b>Input Kanvas Nik</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_npwp" class="col-sm-2 control-label">NPWP 
                            </label>
                            <div class="col-sm-8">
                                <div id="kanvas_kanvas_npwp_galery"></div>
                                <input class="data_file" name="kanvas_kanvas_npwp_uuid" id="kanvas_kanvas_npwp_uuid" type="hidden" value="<?= set_value('kanvas_kanvas_npwp_uuid'); ?>">
                                <input class="data_file" name="kanvas_kanvas_npwp_name" id="kanvas_kanvas_npwp_name" type="hidden" value="<?= set_value('kanvas_kanvas_npwp_name'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_telp" class="col-sm-2 control-label">Telp 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_telp" id="kanvas_telp" placeholder="Telp" value="<?= set_value('kanvas_telp'); ?>" min="1" onkeypress="return event.charCode >= 48 && event.charCode <=57">
                                <small class="info help-block">
                                <b>Input Kanvas Telp</b> Max Length : 15.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_email" class="col-sm-2 control-label">Email 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_email" id="kanvas_email" placeholder="Email" value="<?= set_value('kanvas_email'); ?>">
                                <small class="info help-block">
                                <b>Input Kanvas Email</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_password" class="col-sm-2 control-label">Password 
                            </label>
                            <div class="col-sm-6">
                              <div class="input-group col-md-8 input-password">
                              <input type="password" class="form-control password" name="kanvas_password" id="kanvas_password" placeholder="Password" value="">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-flat show-password"><i class="fa fa-eye eye"></i></button>
                                </span>
                              </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                 
                              <div class="form-group ">
                            <label for="kanvas_provinsi" class="col-sm-2 control-label">Provinsi 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen" onchange="pilihprovinsi()" name="kanvas_provinsi" id="kanvas_provinsi" data-placeholder="Select Provinsi" >
                                    <option value="">Pilih Provinsi</option>
                                    <?php foreach (db_get_all_data('reg_provinces') as $row): ?>
                                    <option value="<?= $row->id ?>"><?= $row->name; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Kanvas Provinsi</b> Max Length : 11.</small>
                            </div>
                        </div>

                                                 
                        <div class="form-group ">
                            <label for="kanvas_kota" class="col-sm-2 control-label">Kota 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen" name="kanvas_kota"  onchange="pilihkota()" id="kanvas_kota" data-placeholder="Select Kota" >
                                   <option value="">Pilih Kota</option>
                                </select>
                                <small class="info help-block">
                                <b>Input Kanvas Kota</b> Max Length : 11.</small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="kanvas_kec" class="col-sm-2 control-label">Kecamatan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen" name="kanvas_kec"   onchange="pilihkecamatan()" id="kanvas_kec" data-placeholder="Select Kecamatan" >
                                   <option value="">Pilih Kecamatan</option>
                                </select>
                                <small class="info help-block">
                                <b>Input Kanvas Kecamatan</b> Max Length : 11.</small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="kanvas_kelurahan" class="col-sm-2 control-label">Kelurahan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen"  onchange="pilihkelurahan()" name="kanvas_kelurahan" id="kanvas_kelurahan" data-placeholder="Select Kelurahan" >
                                    <option value="">Pilih Kelurahan</option>
                                </select>
                                <small class="info help-block">
                                <b>Input Kanvas Kelurahan</b> Max Length : 11.</small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="kanvas_rt" class="col-sm-2 control-label">RT 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_rt" id="kanvas_rt" placeholder="RT" value="<?= set_value('kanvas_rt'); ?>">
                                <small class="info help-block">
                                <b>Input Kanvas Rt</b> Max Length : 11.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_rw" class="col-sm-2 control-label">RW 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_rw" id="kanvas_rw" placeholder="RW" value="<?= set_value('kanvas_rw'); ?>">
                                <small class="info help-block">
                                <b>Input Kanvas Rw</b> Max Length : 11.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_alamat" class="col-sm-2 control-label">Alamat 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_alamat" id="kanvas_alamat" placeholder="Alamat" value="<?= set_value('kanvas_alamat'); ?>">
                                <small class="info help-block">
                                <b>Input Kanvas Alamat</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_cabang" class="col-sm-2 control-label">Cabang 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="kanvas_cabang" id="kanvas_cabang" data-placeholder="Select Cabang" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('cabang') as $row): ?>
                                    <option value="<?= $row->cabang_id ?>"><?= $row->cabang_nama; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Kanvas Cabang</b> Max Length : 11.</small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="kanvas_foto" class="col-sm-2 control-label">Foto 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="kanvas_kanvas_foto_galery"></div>
                                <input class="data_file" name="kanvas_kanvas_foto_uuid" id="kanvas_kanvas_foto_uuid" type="hidden" value="<?= set_value('kanvas_kanvas_foto_uuid'); ?>">
                                <input class="data_file" name="kanvas_kanvas_foto_name" id="kanvas_kanvas_foto_name" type="hidden" value="<?= set_value('kanvas_kanvas_foto_name'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_status" class="col-sm-2 control-label">Status 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="kanvas_status" id="kanvas_status" data-placeholder="Select Status" >
                                    <option value=""></option>
                                    <option value="1">Iya</option>
                                    <option value="2">Tidak</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_longitude" class="col-sm-2 control-label">Longitude 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_longitude" id="kanvas_longitude" placeholder="Longitude" value="<?= set_value('kanvas_longitude'); ?>">
                                <small class="info help-block">
                                <b>Input Kanvas Longitude</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_latitude" class="col-sm-2 control-label">Latitude 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_latitude" id="kanvas_latitude" placeholder="Latitude" value="<?= set_value('kanvas_latitude'); ?>">
                                <small class="info help-block">
                                <b>Input Kanvas Latitude</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                
                        
                                                <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                           <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                 <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
	function pilihprovinsi(){
		console.log($('#kanvas_provinsi option:selected').val());
		 $.ajax({
			type: "GET",
			url: BASE_URL+ "administrator/kanvas/getkota/"+$('#kanvas_provinsi option:selected').val(),
			success: function(data) {
				$('#kanvas_kota').html(data);
				$('#kanvas_kec').html("<option value=''>Pilih Kecamatan</option>");
				$('#kanvas_kelurahan').html("<option value=''>Pilih Kelurahan</option>");
				//alert(data);
			}
		});
	}
	function pilihkota(){
		if($('#kanvas_kota option:selected').val() != ""){
			 $.ajax({
				type: "GET",
				url: BASE_URL+ "administrator/kanvas/getkecamatan/"+$('#kanvas_kota option:selected').val(),
				success: function(data) {
					$('#kanvas_kec').html(data);
					$('#kanvas_kelurahan').html("<option value=''>Pilih Kelurahan</option>");
					//alert(data);
				}
			});
		}
	}
	function pilihkecamatan(){
		if($('#kanvas_kec option:selected').val() != ""){
			 $.ajax({
				type: "GET",
				url: BASE_URL+ "administrator/kanvas/getkelurahan/"+$('#kanvas_kec option:selected').val(),
				success: function(data) {
					$('#kanvas_kelurahan').html(data);
					//alert(data);
				}
			});
		}
	}
    $(document).ready(function(){

                          
      $('#btn_cancel').click(function(){
        swal({
            title: "<?= cclang('are_you_sure'); ?>",
            text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/kanvas';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_kanvas = $('#form_kanvas');
        var data_post = form_kanvas.serializeArray();
        var save_type = $(this).attr('data-stype');

        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: BASE_URL + '/administrator/kanvas/add_save',
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          $('form').find('.form-group').removeClass('has-error');
          $('.steps li').removeClass('error');
          $('form').find('.error-input').remove();
          if(res.success) {
            var id_kanvas_npwp = $('#kanvas_kanvas_npwp_galery').find('li').attr('qq-file-id');
            var id_kanvas_foto = $('#kanvas_kanvas_foto_galery').find('li').attr('qq-file-id');
            
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            resetForm();
            if (typeof id_kanvas_npwp !== 'undefined') {
                    $('#kanvas_kanvas_npwp_galery').fineUploader('deleteFile', id_kanvas_npwp);
                }
            if (typeof id_kanvas_foto !== 'undefined') {
                    $('#kanvas_kanvas_foto_galery').fineUploader('deleteFile', id_kanvas_foto);
                }
            $('.chosen option').prop('selected', false).trigger('chosen:updated');
                
          } else {
            if (res.errors) {
                
                $.each(res.errors, function(index, val) {
                    $('form #'+index).parents('.form-group').addClass('has-error');
                    $('form #'+index).parents('.form-group').find('small').prepend(`
                      <div class="error-input">`+val+`</div>
                      `);
                });
                $('.steps li').removeClass('error');
                $('.content section').each(function(index, el) {
                    if ($(this).find('.has-error').length) {
                        $('.steps li:eq('+index+')').addClass('error').find('a').trigger('click');
                    }
                });
            }
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
              var params = {};
       params[csrf] = token;

       $('#kanvas_kanvas_npwp_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/kanvas/upload_kanvas_npwp_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + '/administrator/kanvas/delete_kanvas_npwp_file',
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
          multiple : false,
          validation: {
              allowedExtensions: ["*"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#kanvas_kanvas_npwp_galery').fineUploader('getUuid', id);
                   $('#kanvas_kanvas_npwp_uuid').val(uuid);
                   $('#kanvas_kanvas_npwp_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#kanvas_kanvas_npwp_uuid').val();
                  $.get(BASE_URL + '/administrator/kanvas/delete_kanvas_npwp_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#kanvas_kanvas_npwp_uuid').val('');
                  $('#kanvas_kanvas_npwp_name').val('');
                }
              }
          }
      }); /*end kanvas_npwp galery*/
                     var params = {};
       params[csrf] = token;

       $('#kanvas_kanvas_foto_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/kanvas/upload_kanvas_foto_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + '/administrator/kanvas/delete_kanvas_foto_file',
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
          multiple : false,
          validation: {
              allowedExtensions: ["*"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#kanvas_kanvas_foto_galery').fineUploader('getUuid', id);
                   $('#kanvas_kanvas_foto_uuid').val(uuid);
                   $('#kanvas_kanvas_foto_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#kanvas_kanvas_foto_uuid').val();
                  $.get(BASE_URL + '/administrator/kanvas/delete_kanvas_foto_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#kanvas_kanvas_foto_uuid').val('');
                  $('#kanvas_kanvas_foto_name').val('');
                }
              }
          }
      }); /*end kanvas_foto galery*/
              
 
       

      
    
    
    }); /*end doc ready*/
</script>