

<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Kanvas        <small>Edit Kanvas</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/kanvas'); ?>">Kanvas</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Kanvas</h3>
                            <h5 class="widget-user-desc">Edit Kanvas</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/kanvas/edit_save/'.$this->uri->segment(4)), [
                            'name'    => 'form_kanvas', 
                            'class'   => 'form-horizontal form-step', 
                            'id'      => 'form_kanvas', 
                            'method'  => 'POST'
                            ]); ?>
                         
                                                <div class="form-group ">
                            <label for="kanvas_nama" class="col-sm-2 control-label">Nama 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_nama" id="kanvas_nama" placeholder="Nama" value="<?= set_value('kanvas_nama', $kanvas->kanvas_nama); ?>">
                                <small class="info help-block">
                                <b>Input Kanvas Nama</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_jenis_kelamin" class="col-sm-2 control-label">Jenis Kelamin 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="kanvas_jenis_kelamin" id="kanvas_jenis_kelamin" data-placeholder="Select Jenis Kelamin" >
                                    <option value=""></option>
                                    <option <?= $kanvas->kanvas_jenis_kelamin == "1" ? 'selected' :''; ?> value="1">Laki Laki</option>
                                    <option <?= $kanvas->kanvas_jenis_kelamin == "2" ? 'selected' :''; ?> value="2">Perempuan</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_tgl_lahir" class="col-sm-2 control-label">Tanggal Lahir 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datepicker" name="kanvas_tgl_lahir"  placeholder="Tanggal Lahir" id="kanvas_tgl_lahir" value="<?= set_value('kanvas_kanvas_tgl_lahir_name', $kanvas->kanvas_tgl_lahir); ?>">
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                       
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_nik" class="col-sm-2 control-label">NIK 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_nik" id="kanvas_nik" placeholder="NIK" value="<?= set_value('kanvas_nik', $kanvas->kanvas_nik); ?>" min="1" onkeypress="return event.charCode >= 48 && event.charCode <=57">
                                <small class="info help-block">
                                <b>Input Kanvas Nik</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_npwp" class="col-sm-2 control-label">NPWP 
                            </label>
                            <div class="col-sm-8">
                                <div id="kanvas_kanvas_npwp_galery"></div>
                                <input class="data_file data_file_uuid" name="kanvas_kanvas_npwp_uuid" id="kanvas_kanvas_npwp_uuid" type="hidden" value="<?= set_value('kanvas_kanvas_npwp_uuid'); ?>">
                                <input class="data_file" name="kanvas_kanvas_npwp_name" id="kanvas_kanvas_npwp_name" type="hidden" value="<?= set_value('kanvas_kanvas_npwp_name', $kanvas->kanvas_npwp); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                  
                                                <div class="form-group ">
                            <label for="kanvas_telp" class="col-sm-2 control-label">Telp 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_telp" id="kanvas_telp" placeholder="Telp" value="<?= set_value('kanvas_telp', $kanvas->kanvas_telp); ?>" min="1" onkeypress="return event.charCode >= 48 && event.charCode <=57">
                                <small class="info help-block">
                                <b>Input Kanvas Telp</b> Max Length : 15.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_email" class="col-sm-2 control-label">Email 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_email" id="kanvas_email" placeholder="Email" value="<?= set_value('kanvas_email', $kanvas->kanvas_email); ?>">
                                <small class="info help-block">
                                <b>Input Kanvas Email</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_password" class="col-sm-2 control-label">Password 
                            </label>
                            <div class="col-sm-6">
                              <div class="input-group col-md-8 input-password">
                              <input type="password" class="form-control password" name="kanvas_password" id="kanvas_password" placeholder="Password" value="">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-flat show-password"><i class="fa fa-eye eye"></i></button>
                                </span>
                              </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                 
                                                 <div class="form-group ">
                            <label for="kanvas_provinsi" class="col-sm-2 control-label">Provinsi 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen" onchange="pilihprovinsi()" name="kanvas_provinsi" id="kanvas_provinsi" data-placeholder="Select Provinsi" >
                                    <option value="">Pilih Provinsi</option>
                                    <?php foreach (db_get_all_data('reg_provinces') as $row): ?>
                                    <option <?=  $row->id ==  $kanvas->kanvas_provinsi ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->name; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Kanvas Provinsi</b> Max Length : 11.</small>
                            </div>
                        </div>


                                                 
                                                <div class="form-group ">
                            <label for="kanvas_kota" class="col-sm-2 control-label">Kota 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen" onchange="pilihkota()" name="kanvas_kota" id="kanvas_kota" data-placeholder="Select Kota" >
                                    <option value="">Pilih Kota</option>
                                </select>
                                <small class="info help-block">
                                <b>Input Kanvas Kota</b> Max Length : 11.</small>
                            </div>
                        </div>


                         <div class="form-group ">
                            <label for="kanvas_kec" class="col-sm-2 control-label">Kecamatan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen" onchange="pilihkecamatan()" name="kanvas_kec" id="kanvas_kec" data-placeholder="Select Kecamatan" >
                                    <option value="">Pilih Kecamatan</option>
                                </select>
                                <small class="info help-block">
                                <b>Input Kanvas Kecamatan</b> Max Length : 11.</small>
                            </div>
                        </div>


                                                 
                                                <div class="form-group ">
                            <label for="kanvas_kelurahan" class="col-sm-2 control-label">Kelurahan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen" name="kanvas_kelurahan" id="kanvas_kelurahan" data-placeholder="Select Kelurahan" >
                                    <option value="">Pilih Kelurahan</option>
                                </select>
                                <small class="info help-block">
                                <b>Input Kanvas Kelurahan</b> Max Length : 11.</small>
                            </div>
                        </div>


                                                 
                                                <div class="form-group ">
                            <label for="kanvas_rt" class="col-sm-2 control-label">RT 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_rt" id="kanvas_rt" placeholder="RT" value="<?= set_value('kanvas_rt', $kanvas->kanvas_rt); ?>">
                                <small class="info help-block">
                                <b>Input Kanvas Rt</b> Max Length : 11.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_rw" class="col-sm-2 control-label">RW 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_rw" id="kanvas_rw" placeholder="RW" value="<?= set_value('kanvas_rw', $kanvas->kanvas_rw); ?>">
                                <small class="info help-block">
                                <b>Input Kanvas Rw</b> Max Length : 11.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_alamat" class="col-sm-2 control-label">Alamat 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_alamat" id="kanvas_alamat" placeholder="Alamat" value="<?= set_value('kanvas_alamat', $kanvas->kanvas_alamat); ?>">
                                <small class="info help-block">
                                <b>Input Kanvas Alamat</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_cabang" class="col-sm-2 control-label">Cabang 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="kanvas_cabang" id="kanvas_cabang" data-placeholder="Select Cabang" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('cabang') as $row): ?>
                                    <option <?=  $row->cabang_id ==  $kanvas->kanvas_cabang ? 'selected' : ''; ?> value="<?= $row->cabang_id ?>"><?= $row->cabang_nama; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Kanvas Cabang</b> Max Length : 11.</small>
                            </div>
                        </div>


                                                 
                                                <div class="form-group ">
                            <label for="kanvas_foto" class="col-sm-2 control-label">Foto 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <div id="kanvas_kanvas_foto_galery"></div>
                                <input class="data_file data_file_uuid" name="kanvas_kanvas_foto_uuid" id="kanvas_kanvas_foto_uuid" type="hidden" value="<?= set_value('kanvas_kanvas_foto_uuid'); ?>">
                                <input class="data_file" name="kanvas_kanvas_foto_name" id="kanvas_kanvas_foto_name" type="hidden" value="<?= set_value('kanvas_kanvas_foto_name', $kanvas->kanvas_foto); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                  
                                                <div class="form-group ">
                            <label for="kanvas_status" class="col-sm-2 control-label">Status 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="kanvas_status" id="kanvas_status" data-placeholder="Select Status" >
                                    <option value=""></option>
                                    <option <?= $kanvas->kanvas_status == "1" ? 'selected' :''; ?> value="1">Iya</option>
                                    <option <?= $kanvas->kanvas_status == "2" ? 'selected' :''; ?> value="2">Tidak</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_longitude" class="col-sm-2 control-label">Longitude 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_longitude" id="kanvas_longitude" placeholder="Longitude" value="<?= set_value('kanvas_longitude', $kanvas->kanvas_longitude); ?>">
                                <small class="info help-block">
                                <b>Input Kanvas Longitude</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="kanvas_latitude" class="col-sm-2 control-label">Latitude 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="kanvas_latitude" id="kanvas_latitude" placeholder="Latitude" value="<?= set_value('kanvas_latitude', $kanvas->kanvas_latitude); ?>">
                                <small class="info help-block">
                                <b>Input Kanvas Latitude</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                
                                                 <div class="message"></div>
                                                <div class="row-fluid col-md-7 container-button-bottom">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                                                 <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    var firstload = 1;
	function pilihprovinsi(){
		var skota = '<?php echo $kanvas->kanvas_kota; ?>';
		//alert(skota);
		
		 $.ajax({
			type: "GET",
			data: {provinsi_id : $('#kanvas_provinsi option:selected').val(), kota_id: skota, first: firstload },
			url: BASE_URL+ "administrator/kanvas/getkota1/",
			success: function(data) {
				$('#kanvas_kota').html(data);
				if(firstload){
					pilihkota();
				}else{
					$('#kanvas_kec').html("<option value=''>Pilih Kecamatan</option>");
					$('#kanvas_kelurahan').html("<option value=''>Pilih Kelurahan</option>");
				}
				//alert(data);
			}
		});
	}
	function pilihkota(){
			var skecamatan = '<?php echo $kanvas->kanvas_kec; ?>';
			//alert(skecamatan);
			
			 $.ajax({
				type: "GET",
				data: {kota_id : $('#kanvas_kota option:selected').val(), kecamatan_id: skecamatan, first: firstload},
				url: BASE_URL+ "administrator/kanvas/getkecamatan1/",
				success: function(data) {
					//alert(firstload);
					$('#kanvas_kec').html(data);
					if(firstload){
						pilihkecamatan();
						firstload = 0;
					}else{
						$('#kanvas_kelurahan').html("<option value=''>Pilih Kelurahan</option>");
					}
					//alert(data);
				}
			});
	}
	function pilihkecamatan(){
			var skelurahan = '<?php echo $kanvas->kanvas_kelurahan; ?>';
			//alert(skelurahan);
			
			 $.ajax({
				type: "GET",
				data: {kecamatan_id : $('#kanvas_kec option:selected').val(), kelurahan_id: skelurahan, first: firstload},
				url: BASE_URL+ "administrator/kanvas/getkelurahan1/",
				success: function(data) {
					$('#kanvas_kelurahan').html(data);
					//alert(data);
				}
			});
	}
    $(document).ready(function(){
       
      pilihprovinsi();
       
      
             
      $('#btn_cancel').click(function(){
        swal({
            title: "Are you sure?",
            text: "the data that you have created will be in the exhaust!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/kanvas';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_kanvas = $('#form_kanvas');
        var data_post = form_kanvas.serializeArray();
        var save_type = $(this).attr('data-stype');
        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: form_kanvas.attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          $('form').find('.form-group').removeClass('has-error');
          $('form').find('.error-input').remove();
          $('.steps li').removeClass('error');
          if(res.success) {
            var id = $('#kanvas_image_galery').find('li').attr('qq-file-id');
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            $('.data_file_uuid').val('');
    
          } else {
            if (res.errors) {
               parseErrorField(res.errors);
            }
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
                     var params = {};
       params[csrf] = token;

       $('#kanvas_kanvas_npwp_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/kanvas/upload_kanvas_npwp_file',
              params : params
          },
          deleteFile: {
              enabled: true, // defaults to false
              endpoint: BASE_URL + '/administrator/kanvas/delete_kanvas_npwp_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/kanvas/get_kanvas_npwp_file/<?= $kanvas->kanvas_id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["*"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#kanvas_kanvas_npwp_galery').fineUploader('getUuid', id);
                   $('#kanvas_kanvas_npwp_uuid').val(uuid);
                   $('#kanvas_kanvas_npwp_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#kanvas_kanvas_npwp_uuid').val();
                  $.get(BASE_URL + '/administrator/kanvas/delete_kanvas_npwp_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#kanvas_kanvas_npwp_uuid').val('');
                  $('#kanvas_kanvas_npwp_name').val('');
                }
              }
          }
      }); /*end kanvas_npwp galey*/
                            var params = {};
       params[csrf] = token;

       $('#kanvas_kanvas_foto_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/kanvas/upload_kanvas_foto_file',
              params : params
          },
          deleteFile: {
              enabled: true, // defaults to false
              endpoint: BASE_URL + '/administrator/kanvas/delete_kanvas_foto_file'
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
           session : {
             endpoint: BASE_URL + 'administrator/kanvas/get_kanvas_foto_file/<?= $kanvas->kanvas_id; ?>',
             refreshOnRequest:true
           },
          multiple : false,
          validation: {
              allowedExtensions: ["*"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#kanvas_kanvas_foto_galery').fineUploader('getUuid', id);
                   $('#kanvas_kanvas_foto_uuid').val(uuid);
                   $('#kanvas_kanvas_foto_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#kanvas_kanvas_foto_uuid').val();
                  $.get(BASE_URL + '/administrator/kanvas/delete_kanvas_foto_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#kanvas_kanvas_foto_uuid').val('');
                  $('#kanvas_kanvas_foto_name').val('');
                }
              }
          }
      }); /*end kanvas_foto galey*/
              
       
       

      async function chain(){
      }
       
      chain();


    
    
    }); /*end doc ready*/
</script>