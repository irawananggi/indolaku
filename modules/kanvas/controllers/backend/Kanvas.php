<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Kanvas Controller
*| --------------------------------------------------------------------------
*| Kanvas site
*|
*/
class Kanvas extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_kanvas');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Kanvass
	*
	* @var $offset String
	*/
	function getkota($id)
    {
		$data = $this->model_kanvas->withquery("select * from reg_regencies where province_id = '".$id."' order by name asc","result");
		echo "<option value=''>Pilih Kota</option>";
		foreach($data as $datalist){
			echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			
		}
    }
	function getkecamatan($id)
    {
		$data = $this->model_kanvas->withquery("select * from reg_districts where regency_id  = '".$id."' order by name asc","result");
		echo "<option value=''>Pilih Kecamatan</option>";
		foreach($data as $datalist){
			echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			
		}
    }
	
	function getkelurahan($id)
    {
		$data = $this->model_kanvas->withquery("select * from reg_villages where district_id  = '".$id."' order by name asc","result");
		echo "<option value=''>Pilih Kelurahan</option>";
		foreach($data as $datalist){
			echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			
		}
    }
	function getkota1()
    {
		
		$data = $this->model_kanvas->withquery("select * from reg_regencies where province_id = '".$_GET['provinsi_id']."' order by name asc","result");
		echo "<option value=''>Pilih Kota</option>";
		foreach($data as $datalist){
			if($datalist->id == $_GET['kota_id'] && $_GET['first'] == 1){
				echo "<option value='".$datalist->id."' selected>".$datalist->name."</option>";
			}else{
				echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			}
			
		}
    }
	function getkecamatan1()
    {
		$data = $this->model_kanvas->withquery("select * from reg_districts where regency_id  = '".$_GET['kota_id']."' order by name asc","result");
		echo "<option value=''>Pilih Kecamatan</option>";
		foreach($data as $datalist){
			if($datalist->id == $_GET['kecamatan_id']  && $_GET['first'] == 1 ){
				echo "<option value='".$datalist->id."' selected>".$datalist->name."</option>";
			}else{
				echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			}
			
		}
    }
	
	function getkelurahan1()
    {
		$data = $this->model_kanvas->withquery("select * from reg_villages where district_id  = '".$_GET['kecamatan_id']."' order by name asc","result");
		echo "<option value=''>Pilih Kelurahan</option>";
		foreach($data as $datalist){
			
			if($datalist->id == $_GET['kelurahan_id']  && $_GET['first'] == 1){
				echo "<option value='".$datalist->id."' selected>".$datalist->name."</option>";
			}else{
				echo "<option value='".$datalist->id."'>".$datalist->name."</option>";
			}
		}
    }
	public function index($offset = 0)
	{
		$this->is_allowed('kanvas_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['kanvass'] = $this->model_kanvas->get($filter, $field, $this->limit_page, $offset);
		$this->data['kanvas_counts'] = $this->model_kanvas->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/kanvas/index/',
			'total_rows'   => $this->model_kanvas->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Kanvas List');
		$this->render('backend/standart/administrator/kanvas/kanvas_list', $this->data);
	}
	
	/**
	* Add new kanvass
	*
	*/
	public function add()
	{
		$this->is_allowed('kanvas_add');

		$this->template->title('Kanvas New');
		$this->render('backend/standart/administrator/kanvas/kanvas_add', $this->data);
	}

	/**
	* Add New Kanvass
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('kanvas_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('kanvas_nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kanvas_jenis_kelamin', 'Jenis Kelamin', 'trim|required');
		$this->form_validation->set_rules('kanvas_tgl_lahir', 'Tanggal Lahir', 'trim|required');
		$this->form_validation->set_rules('kanvas_nik', 'NIK', 'trim|required|max_length[255]');
		//$this->form_validation->set_rules('kanvas_kanvas_npwp_name', 'NPWP', 'trim|required');
		$this->form_validation->set_rules('kanvas_telp', 'Telp', 'trim|required|max_length[15]');
		$this->form_validation->set_rules('kanvas_email', 'Email', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kanvas_provinsi', 'Provinsi', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kanvas_kota', 'Kota/Kab', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kanvas_kec', 'Kecamatan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kanvas_kelurahan', 'Kelurahan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kanvas_rt', 'RT', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kanvas_rw', 'RW', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kanvas_alamat', 'Alamat', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kanvas_cabang', 'Cabang', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kanvas_kanvas_foto_name', 'Foto', 'trim|required');
		$this->form_validation->set_rules('kanvas_status', 'Status', 'trim|required');
		$this->form_validation->set_rules('kanvas_longitude', 'Longitude', 'trim|max_length[255]');
		$this->form_validation->set_rules('kanvas_latitude', 'Latitude', 'trim|max_length[255]');
		
		$kalimat = $this->input->post('kanvas_email');
        if(strpos($kalimat, '.') AND strpos($kalimat, '.')) {

			if ($this->form_validation->run()) {
				$kanvas_kanvas_npwp_uuid = $this->input->post('kanvas_kanvas_npwp_uuid');
				$kanvas_kanvas_npwp_name = $this->input->post('kanvas_kanvas_npwp_name');
				$kanvas_kanvas_foto_uuid = $this->input->post('kanvas_kanvas_foto_uuid');
				$kanvas_kanvas_foto_name = $this->input->post('kanvas_kanvas_foto_name');
			
				$save_data = [
					'kanvas_nama' => $this->input->post('kanvas_nama'),
					'kanvas_jenis_kelamin' => $this->input->post('kanvas_jenis_kelamin'),
					'kanvas_tgl_lahir' => $this->input->post('kanvas_tgl_lahir'),
					'kanvas_nik' => $this->input->post('kanvas_nik'),
					'kanvas_telp' => $this->input->post('kanvas_telp'),
					'kanvas_email' => $this->input->post('kanvas_email'),
					'kanvas_password' => md5($this->input->post('kanvas_password')),
					'kanvas_provinsi' => $this->input->post('kanvas_provinsi'),
					'kanvas_kota' => $this->input->post('kanvas_kota'),
					'kanvas_kec' => $this->input->post('kanvas_kec'),
					'kanvas_kelurahan' => $this->input->post('kanvas_kelurahan'),
					'kanvas_rt' => $this->input->post('kanvas_rt'),
					'kanvas_rw' => $this->input->post('kanvas_rw'),
					'kanvas_alamat' => $this->input->post('kanvas_alamat'),
					'kanvas_cabang' => $this->input->post('kanvas_cabang'),
					'kanvas_status' => $this->input->post('kanvas_status'),
					'kanvas_longitude' => $this->input->post('kanvas_longitude'),
					'kanvas_latitude' => $this->input->post('kanvas_latitude'),
					'kanvas_token' =>  md5($this->input->post('kanvas_email')."".$this->input->post('kanvas_password')."".time()),
				];

				if (!is_dir(FCPATH . '/uploads/kanvas/')) {
					mkdir(FCPATH . '/uploads/kanvas/');
				}

				if (!empty($kanvas_kanvas_npwp_name)) {
					$kanvas_kanvas_npwp_name_copy = date('YmdHis') . '-' . $kanvas_kanvas_npwp_name;

					rename(FCPATH . 'uploads/tmp/' . $kanvas_kanvas_npwp_uuid . '/' . $kanvas_kanvas_npwp_name, 
							FCPATH . 'uploads/kanvas/' . $kanvas_kanvas_npwp_name_copy);

					if (!is_file(FCPATH . '/uploads/kanvas/' . $kanvas_kanvas_npwp_name_copy)) {
						echo json_encode([
							'success' => false,
							'message' => 'Error uploading file'
							]);
						exit;
					}

					$save_data['kanvas_npwp'] = $kanvas_kanvas_npwp_name_copy;
				}
			
				if (!empty($kanvas_kanvas_foto_name)) {
					$kanvas_kanvas_foto_name_copy = date('YmdHis') . '-' . $kanvas_kanvas_foto_name;

					rename(FCPATH . 'uploads/tmp/' . $kanvas_kanvas_foto_uuid . '/' . $kanvas_kanvas_foto_name, 
							FCPATH . 'uploads/kanvas/' . $kanvas_kanvas_foto_name_copy);

					if (!is_file(FCPATH . '/uploads/kanvas/' . $kanvas_kanvas_foto_name_copy)) {
						echo json_encode([
							'success' => false,
							'message' => 'Error uploading file'
							]);
						exit;
					}

					$save_data['kanvas_foto'] = $kanvas_kanvas_foto_name_copy;
				}
			
				
				$save_kanvas = $this->model_kanvas->store($save_data);
	            

				if ($save_kanvas) {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = true;
						$this->data['id'] 	   = $save_kanvas;
						$this->data['message'] = cclang('success_save_data_stay', [
							anchor('administrator/kanvas/edit/' . $save_kanvas, 'Edit Kanvas'),
							anchor('administrator/kanvas', ' Go back to list')
						]);
					} else {
						set_message(
							cclang('success_save_data_redirect', [
							anchor('administrator/kanvas/edit/' . $save_kanvas, 'Edit Kanvas')
						]), 'success');

	            		$this->data['success'] = true;
						$this->data['redirect'] = base_url('administrator/kanvas');
					}
				} else {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = false;
						$this->data['message'] = cclang('data_not_change');
					} else {
	            		$this->data['success'] = false;
	            		$this->data['message'] = cclang('data_not_change');
						$this->data['redirect'] = base_url('administrator/kanvas');
					}
				}

			} else {
				$this->data['success'] = false;
				$this->data['message'] = 'Opss validation failed';
				$this->data['errors'] = $this->form_validation->error_array();
			}
		}else {
        	$this->data['success'] = false;
        	$this->data['message'] = 'Email tidak sesuai format';
        	$this->data['errors'] = $this->form_validation->error_array();
        }
		echo json_encode($this->data);
	}
	
		/**
	* Update view Kanvass
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('kanvas_update');

		$this->data['kanvas'] = $this->model_kanvas->find($id);

		$this->template->title('Kanvas Update');
		$this->render('backend/standart/administrator/kanvas/kanvas_update', $this->data);
	}

	/**
	* Update Kanvass
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('kanvas_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('kanvas_nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kanvas_jenis_kelamin', 'Jenis Kelamin', 'trim|required');
		$this->form_validation->set_rules('kanvas_tgl_lahir', 'Tanggal Lahir', 'trim|required');
		$this->form_validation->set_rules('kanvas_nik', 'NIK', 'trim|required|max_length[255]');
		//$this->form_validation->set_rules('kanvas_kanvas_npwp_name', 'NPWP', 'trim|required');
		$this->form_validation->set_rules('kanvas_telp', 'Telp', 'trim|required|max_length[15]');
		$this->form_validation->set_rules('kanvas_email', 'Email', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kanvas_provinsi', 'Provinsi', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kanvas_kota', 'Kota/Kab', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kanvas_kec', 'Kecamatan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kanvas_kelurahan', 'Kelurahan', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kanvas_rt', 'RT', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kanvas_rw', 'RW', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kanvas_alamat', 'Alamat', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('kanvas_cabang', 'Cabang', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('kanvas_kanvas_foto_name', 'Foto', 'trim|required');
		$this->form_validation->set_rules('kanvas_status', 'Status', 'trim|required');
		$this->form_validation->set_rules('kanvas_longitude', 'Longitude', 'trim|max_length[255]');
		$this->form_validation->set_rules('kanvas_latitude', 'Latitude', 'trim|max_length[255]');
		
		$kalimat = $this->input->post('kanvas_email');
        if(strpos($kalimat, '.') AND strpos($kalimat, '.')) {
			if ($this->form_validation->run()) {
				$kanvas_kanvas_npwp_uuid = $this->input->post('kanvas_kanvas_npwp_uuid');
				$kanvas_kanvas_npwp_name = $this->input->post('kanvas_kanvas_npwp_name');
				$kanvas_kanvas_foto_uuid = $this->input->post('kanvas_kanvas_foto_uuid');
				$kanvas_kanvas_foto_name = $this->input->post('kanvas_kanvas_foto_name');
			
				$save_data = [];

				if(empty($this->input->post('kanvas_password'))){
					
					$save_data = [
						'kanvas_nama' => $this->input->post('kanvas_nama'),
						'kanvas_jenis_kelamin' => $this->input->post('kanvas_jenis_kelamin'),
						'kanvas_tgl_lahir' => $this->input->post('kanvas_tgl_lahir'),
						'kanvas_nik' => $this->input->post('kanvas_nik'),
						'kanvas_telp' => $this->input->post('kanvas_telp'),
						'kanvas_email' => $this->input->post('kanvas_email'),
						'kanvas_provinsi' => $this->input->post('kanvas_provinsi'),
						'kanvas_kota' => $this->input->post('kanvas_kota'),
						'kanvas_kec' => $this->input->post('kanvas_kec'),
						'kanvas_kelurahan' => $this->input->post('kanvas_kelurahan'),
						'kanvas_rt' => $this->input->post('kanvas_rt'),
						'kanvas_rw' => $this->input->post('kanvas_rw'),
						'kanvas_alamat' => $this->input->post('kanvas_alamat'),
						'kanvas_cabang' => $this->input->post('kanvas_cabang'),
						'kanvas_status' => $this->input->post('kanvas_status'),
						'kanvas_longitude' => $this->input->post('kanvas_longitude'),
						'kanvas_latitude' => $this->input->post('kanvas_latitude'),
					];

				}else{
					$save_data = [
					'kanvas_nama' => $this->input->post('kanvas_nama'),
					'kanvas_jenis_kelamin' => $this->input->post('kanvas_jenis_kelamin'),
					'kanvas_tgl_lahir' => $this->input->post('kanvas_tgl_lahir'),
					'kanvas_nik' => $this->input->post('kanvas_nik'),
					'kanvas_telp' => $this->input->post('kanvas_telp'),
					'kanvas_email' => $this->input->post('kanvas_email'),
					'kanvas_password' => md5($this->input->post('kanvas_password')),
					'kanvas_provinsi' => $this->input->post('kanvas_provinsi'),
					'kanvas_kota' => $this->input->post('kanvas_kota'),
					'kanvas_kec' => $this->input->post('kanvas_kec'),
					'kanvas_kelurahan' => $this->input->post('kanvas_kelurahan'),
					'kanvas_rt' => $this->input->post('kanvas_rt'),
					'kanvas_rw' => $this->input->post('kanvas_rw'),
					'kanvas_alamat' => $this->input->post('kanvas_alamat'),
					'kanvas_cabang' => $this->input->post('kanvas_cabang'),
					'kanvas_status' => $this->input->post('kanvas_status'),
					'kanvas_longitude' => $this->input->post('kanvas_longitude'),
					'kanvas_latitude' => $this->input->post('kanvas_latitude'),
				];
				}
				if (!is_dir(FCPATH . '/uploads/kanvas/')) {
					mkdir(FCPATH . '/uploads/kanvas/');
				}

				if (!empty($kanvas_kanvas_npwp_uuid)) {
					$kanvas_kanvas_npwp_name_copy = date('YmdHis') . '-' . $kanvas_kanvas_npwp_name;

					rename(FCPATH . 'uploads/tmp/' . $kanvas_kanvas_npwp_uuid . '/' . $kanvas_kanvas_npwp_name, 
							FCPATH . 'uploads/kanvas/' . $kanvas_kanvas_npwp_name_copy);

					if (!is_file(FCPATH . '/uploads/kanvas/' . $kanvas_kanvas_npwp_name_copy)) {
						echo json_encode([
							'success' => false,
							'message' => 'Error uploading file'
							]);
						exit;
					}

					$save_data['kanvas_npwp'] = $kanvas_kanvas_npwp_name_copy;
				}
			
				if (!empty($kanvas_kanvas_foto_uuid)) {
					$kanvas_kanvas_foto_name_copy = date('YmdHis') . '-' . $kanvas_kanvas_foto_name;

					rename(FCPATH . 'uploads/tmp/' . $kanvas_kanvas_foto_uuid . '/' . $kanvas_kanvas_foto_name, 
							FCPATH . 'uploads/kanvas/' . $kanvas_kanvas_foto_name_copy);

					if (!is_file(FCPATH . '/uploads/kanvas/' . $kanvas_kanvas_foto_name_copy)) {
						echo json_encode([
							'success' => false,
							'message' => 'Error uploading file'
							]);
						exit;
					}

					$save_data['kanvas_foto'] = $kanvas_kanvas_foto_name_copy;
				}
			
				
				$save_kanvas = $this->model_kanvas->change($id, $save_data);

				if ($save_kanvas) {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = true;
						$this->data['id'] 	   = $id;
						$this->data['message'] = cclang('success_update_data_stay', [
							anchor('administrator/kanvas', ' Go back to list')
						]);
					} else {
						set_message(
							cclang('success_update_data_redirect', [
						]), 'success');

	            		$this->data['success'] = true;
						$this->data['redirect'] = base_url('administrator/kanvas');
					}
				} else {
					if ($this->input->post('save_type') == 'stay') {
						$this->data['success'] = false;
						$this->data['message'] = cclang('data_not_change');
					} else {
	            		$this->data['success'] = false;
	            		$this->data['message'] = cclang('data_not_change');
						$this->data['redirect'] = base_url('administrator/kanvas');
					}
				}
			} else {
				$this->data['success'] = false;
				$this->data['message'] = 'Opss validation failed';
				$this->data['errors'] = $this->form_validation->error_array();
			}
		}else {
        	$this->data['success'] = false;
        	$this->data['message'] = 'Email tidak sesuai format';
        	$this->data['errors'] = $this->form_validation->error_array();
        }
		echo json_encode($this->data);
	}
	
	/**
	* delete Kanvass
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('kanvas_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'kanvas'), 'success');
        } else {
            set_message(cclang('error_delete', 'kanvas'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Kanvass
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('kanvas_view');

		$this->data['kanvas'] = $this->model_kanvas->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Kanvas Detail');
		$this->render('backend/standart/administrator/kanvas/kanvas_view', $this->data);
	}
	
	/**
	* delete Kanvass
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$kanvas = $this->model_kanvas->find($id);

		if (!empty($kanvas->kanvas_npwp)) {
			$path = FCPATH . '/uploads/kanvas/' . $kanvas->kanvas_npwp;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		if (!empty($kanvas->kanvas_foto)) {
			$path = FCPATH . '/uploads/kanvas/' . $kanvas->kanvas_foto;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_kanvas->remove($id);
	}
	
	/**
	* Upload Image Kanvas	* 
	* @return JSON
	*/
	public function upload_kanvas_npwp_file()
	{
		if (!$this->is_allowed('kanvas_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'kanvas',
		]);
	}

	/**
	* Delete Image Kanvas	* 
	* @return JSON
	*/
	public function delete_kanvas_npwp_file($uuid)
	{
		if (!$this->is_allowed('kanvas_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'kanvas_npwp', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'kanvas',
            'primary_key'       => 'kanvas_id',
            'upload_path'       => 'uploads/kanvas/'
        ]);
	}

	/**
	* Get Image Kanvas	* 
	* @return JSON
	*/
	public function get_kanvas_npwp_file($id)
	{
		if (!$this->is_allowed('kanvas_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$kanvas = $this->model_kanvas->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'kanvas_npwp', 
            'table_name'        => 'kanvas',
            'primary_key'       => 'kanvas_id',
            'upload_path'       => 'uploads/kanvas/',
            'delete_endpoint'   => 'administrator/kanvas/delete_kanvas_npwp_file'
        ]);
	}
	
	/**
	* Upload Image Kanvas	* 
	* @return JSON
	*/
	public function upload_kanvas_foto_file()
	{
		if (!$this->is_allowed('kanvas_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'kanvas',
		]);
	}

	/**
	* Delete Image Kanvas	* 
	* @return JSON
	*/
	public function delete_kanvas_foto_file($uuid)
	{
		if (!$this->is_allowed('kanvas_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'kanvas_foto', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'kanvas',
            'primary_key'       => 'kanvas_id',
            'upload_path'       => 'uploads/kanvas/'
        ]);
	}

	/**
	* Get Image Kanvas	* 
	* @return JSON
	*/
	public function get_kanvas_foto_file($id)
	{
		if (!$this->is_allowed('kanvas_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$kanvas = $this->model_kanvas->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'kanvas_foto', 
            'table_name'        => 'kanvas',
            'primary_key'       => 'kanvas_id',
            'upload_path'       => 'uploads/kanvas/',
            'delete_endpoint'   => 'administrator/kanvas/delete_kanvas_foto_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('kanvas_export');

		$this->model_kanvas->export('kanvas', 'kanvas');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('kanvas_export');

		$this->model_kanvas->pdf('kanvas', 'kanvas');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('kanvas_export');

		$table = $title = 'kanvas';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_kanvas->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file kanvas.php */
/* Location: ./application/controllers/administrator/Kanvas.php */