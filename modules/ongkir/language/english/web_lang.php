<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['ongkir'] = 'Ongkir';
$lang['ongkir_id'] = 'Ongkir Id';
$lang['ongkir_per_meter'] = 'Jumlah Meter';
$lang['ongkir_harga'] = 'Harga';
$lang['ongkir_keterangan'] = 'Keterangan';
