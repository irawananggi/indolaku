<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Ongkir Controller
*| --------------------------------------------------------------------------
*| Ongkir site
*|
*/
class Ongkir extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_ongkir');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Ongkirs
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('ongkir_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['ongkirs'] = $this->model_ongkir->get($filter, $field, $this->limit_page, $offset);
		$this->data['ongkir_counts'] = $this->model_ongkir->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/ongkir/index/',
			'total_rows'   => $this->model_ongkir->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Ongkir List');
		$this->render('backend/standart/administrator/ongkir/ongkir_list', $this->data);
	}
	
	/**
	* Add new ongkirs
	*
	*/
	public function add()
	{
		$this->is_allowed('ongkir_add');

		$this->template->title('Ongkir New');
		$this->render('backend/standart/administrator/ongkir/ongkir_add', $this->data);
	}

	/**
	* Add New Ongkirs
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('ongkir_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('ongkir_per_meter', 'Jumlah Meter', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('ongkir_harga', 'Harga', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'ongkir_per_meter' => $this->input->post('ongkir_per_meter'),
				'ongkir_harga' => $this->input->post('ongkir_harga'),
				'ongkir_keterangan' => $this->input->post('ongkir_keterangan'),
			];

			
			$save_ongkir = $this->model_ongkir->store($save_data);
            

			if ($save_ongkir) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_ongkir;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/ongkir/edit/' . $save_ongkir, 'Edit Ongkir'),
						anchor('administrator/ongkir', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/ongkir/edit/' . $save_ongkir, 'Edit Ongkir')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ongkir');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ongkir');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Ongkirs
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('ongkir_update');

		$this->data['ongkir'] = $this->model_ongkir->find($id);

		$this->template->title('Ongkir Update');
		$this->render('backend/standart/administrator/ongkir/ongkir_update', $this->data);
	}

	/**
	* Update Ongkirs
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('ongkir_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('ongkir_per_meter', 'Jumlah Meter', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('ongkir_harga', 'Harga', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'ongkir_per_meter' => $this->input->post('ongkir_per_meter'),
				'ongkir_harga' => $this->input->post('ongkir_harga'),
				'ongkir_keterangan' => $this->input->post('ongkir_keterangan'),
			];

			
			$save_ongkir = $this->model_ongkir->change($id, $save_data);

			if ($save_ongkir) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/ongkir', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/ongkir');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/ongkir');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Ongkirs
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('ongkir_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'ongkir'), 'success');
        } else {
            set_message(cclang('error_delete', 'ongkir'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Ongkirs
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('ongkir_view');

		$this->data['ongkir'] = $this->model_ongkir->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Ongkir Detail');
		$this->render('backend/standart/administrator/ongkir/ongkir_view', $this->data);
	}
	
	/**
	* delete Ongkirs
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$ongkir = $this->model_ongkir->find($id);

		
		
		return $this->model_ongkir->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('ongkir_export');

		$this->model_ongkir->export('ongkir', 'ongkir');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('ongkir_export');

		$this->model_ongkir->pdf('ongkir', 'ongkir');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('ongkir_export');

		$table = $title = 'ongkir';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_ongkir->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file ongkir.php */
/* Location: ./application/controllers/administrator/Ongkir.php */