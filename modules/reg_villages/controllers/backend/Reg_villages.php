<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Reg Villages Controller
*| --------------------------------------------------------------------------
*| Reg Villages site
*|
*/
class Reg_villages extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_reg_villages');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Reg Villagess
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('reg_villages_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['reg_villagess'] = $this->model_reg_villages->get($filter, $field, $this->limit_page, $offset);
		$this->data['reg_villages_counts'] = $this->model_reg_villages->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/reg_villages/index/',
			'total_rows'   => $this->model_reg_villages->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Reg Villages List');
		$this->render('backend/standart/administrator/reg_villages/reg_villages_list', $this->data);
	}
	
	/**
	* Add new reg_villagess
	*
	*/
	public function add()
	{
		$this->is_allowed('reg_villages_add');

		$this->template->title('Reg Villages New');
		$this->render('backend/standart/administrator/reg_villages/reg_villages_add', $this->data);
	}

	/**
	* Add New Reg Villagess
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('reg_villages_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('district_id', 'District Id', 'trim|required|max_length[8]');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[255]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'district_id' => $this->input->post('district_id'),
				'name' => $this->input->post('name'),
			];

			
			$save_reg_villages = $this->model_reg_villages->store($save_data);
            

			if ($save_reg_villages) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_reg_villages;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/reg_villages/edit/' . $save_reg_villages, 'Edit Reg Villages'),
						anchor('administrator/reg_villages', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/reg_villages/edit/' . $save_reg_villages, 'Edit Reg Villages')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/reg_villages');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/reg_villages');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Reg Villagess
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('reg_villages_update');

		$this->data['reg_villages'] = $this->model_reg_villages->find($id);

		$this->template->title('Reg Villages Update');
		$this->render('backend/standart/administrator/reg_villages/reg_villages_update', $this->data);
	}

	/**
	* Update Reg Villagess
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('reg_villages_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('district_id', 'District Id', 'trim|required|max_length[8]');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[255]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'district_id' => $this->input->post('district_id'),
				'name' => $this->input->post('name'),
			];

			
			$save_reg_villages = $this->model_reg_villages->change($id, $save_data);

			if ($save_reg_villages) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/reg_villages', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/reg_villages');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/reg_villages');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Reg Villagess
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('reg_villages_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'reg_villages'), 'success');
        } else {
            set_message(cclang('error_delete', 'reg_villages'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Reg Villagess
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('reg_villages_view');

		$this->data['reg_villages'] = $this->model_reg_villages->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Reg Villages Detail');
		$this->render('backend/standart/administrator/reg_villages/reg_villages_view', $this->data);
	}
	
	/**
	* delete Reg Villagess
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$reg_villages = $this->model_reg_villages->find($id);

		
		
		return $this->model_reg_villages->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('reg_villages_export');

		$this->model_reg_villages->export('reg_villages', 'reg_villages');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('reg_villages_export');

		$this->model_reg_villages->pdf('reg_villages', 'reg_villages');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('reg_villages_export');

		$table = $title = 'reg_villages';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_reg_villages->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file reg_villages.php */
/* Location: ./application/controllers/administrator/Reg Villages.php */