<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Checkout extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {

        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   
	    if ($token != '') {
		  $umkm = $this->mymodel->getbywhere('umkm','umkm_token',$token,"row");
		  if (!empty($umkm)) {
			  $produkarr = explode(',',$this->input->post('produk'));
			  $qtyarr = explode(',',$this->input->post('qty'));
			  $hrgarr = explode(',',$this->input->post('harga'));
			  $sisastokcabang = [];
			  $idstokcabang = [];
			  $sisastokcanvas = [];
			  $idstokcanvas = [];
			  $cabangterdekat;
			  $kanvasterdekat;
			  $cabangterdekatarr = $this->mymodel->withquery("select *, ( 6371 * acos( cos( radians(".$umkm->umkm_latitude.") ) * cos( radians( c.cabang_latitude ) ) * 
	cos( radians( c.cabang_longitude ) - radians(".$umkm->umkm_longitude.") ) + sin( radians(".$umkm->umkm_latitude.") ) * 
	sin( radians( c.cabang_latitude ) ) ) ) as jarak from cabang c where c.cabang_status = 1 and cabang_pusat = 1
	ORDER BY jarak limit 10","result");
	
			  foreach($cabangterdekatarr as $cbt){
				  $ada = true;
				  for($i = 0; $i < count($produkarr); $i++){
					  $idproduk = $produkarr[$i];
					  $qty = $qtyarr[$i];
					  $cekstok = $this->mymodel->withquery("select * from produk_per_cabang where produk_per_cabang_produk_id = '".$idproduk."' and produk_per_cabang_stok >= '".$qty."' and produk_per_cabang_cabang_id = '".$cbt->cabang_id."'","row");
					  $sisastokcabang[] = $cekstok->produk_per_cabang_stok - $qty;
					  $idstokcabang[] = $cekstok->produk_per_cabang_id;
					  if(empty($cekstok)){
						  $ada = false;
					  }
				  }
				  if($ada){
					  $cabangterdekat =  $cbt;
					  break;
				  }
			  }


		
			  
			  if(empty($cabangterdekat)){
				  $status = "401";
				  $msg = array('status' => 0, 'message'=>'Stok Tidak ada', 'data' => new stdClass());
			  }else{
					  $transcabang = array('transaksi_umkm_pusat_id' => 0
					  , 'transaksi_umkm_id' => $umkm->umkm_id
					  , 'transaksi_cabang_id' => $cabangterdekat->cabang_id
					  , 'transaksi_note' => $this->input->post('note')
					  , 'transaksi_longitude' => $this->input->post('longitude')
					  , 'transaksi_latitude' => $this->input->post('latitude')
					  , 'transaksi_total' => $this->input->post('total')
					  , 'transaksi_status' => 1
					  , 'transaksi_ongkir' => $this->input->post('ongkir')
					  , 'transaksi_tanggal' => date('Y-m-d h:i:s')
					  , 'transaksi_alamat' => $this->input->post('alamat')
					  , 'transaksi_jenispembayaran' => $this->input->post('jenispembayaran'));
					  
					  
					 
					  $this->mymodel->insert("transaksi_umkm_pusat", $transcabang);
					  
					  $dtransto = $this->mymodel->withquery("select * from
					  transaksi_umkm_pusat tcc, cabang c, umkm cs, transaksi_status ts where 
					  tcc.transaksi_umkm_id = '".$umkm->umkm_id."' 
					  and cs.umkm_id = '".$umkm->umkm_id."' 
					  and tcc.transaksi_cabang_id = '".$cabangterdekat->cabang_id."' 
					  and c.cabang_id ='".$cabangterdekat->cabang_id."' 
					  and tcc.transaksi_status = ts.transaksi_status_id 
					  order by transaksi_umkm_pusat_id desc","row");
					  
					  for($i = 0; $i < count($produkarr); $i++){
						  $td = array("transaksi_detail_id"=>0,
						  "transaksi_detail_product_id" => $produkarr[$i],
						  "transaksi_detail_harga" => $hrgarr[$i],
						  "transaksi_detail_qty" => $qtyarr[$i],
						  "transaksi_detail_total" => ($hrgarr[$i] * $qtyarr[$i]),
						  "transaksi_jenis"=>4,
						  "transaksi_id"=>$dtransto->transaksi_umkm_pusat_id ,
						  "transaksi_customer_id"=>$umkm->umkm_id  );
						  $this->mymodel->insert("transaksi_detail",$td);
						   
						  $datastokcabangupdate = array("produk_per_cabang_stok" => $sisastokcabang[$i]);
						  $this->mymodel->update('produk_per_cabang',$datastokcabangupdate,'produk_per_cabang_id',$idstokcabang[$i]);
					  }
					  
					  if($this->input->post('jenispembayaran') == 1){
						  
						  $termin = $this->mymodel->withquery("select * from termin","result");
						  
						  foreach($termin as $t){
							  
							  $td = array("transaksi_umkm_termin_id "=>0,
							  "transaksi_umkm_termin_termin_id" => $t->termin_id,
							  "transaksi_umkm_termin_transaksi_id" =>$dtransto->transaksi_umkm_pusat_id );
							  $this->mymodel->insert("transaksi_umkm_termin",$td);
						  }
					  }
					  $status = "200";
					  $msg = array('status' => 1, 'message'=>'Transaksi Berhasil' ,'data' => $dtransto);
					  $this->mymodel->delete("keranjang_umkm","keranjang_umkm_umkm_id",$umkm->umkm_id);
			  }
				 
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
		  }
		  $this->response($msg,$status);
	   }else {
		 $status = "401";
		 $data = array();
		 $msg = array('status' => 0, 'message'=>'Token anda kosong', 'data' => new stdClass());
		 $this->response($msg,$status);
	   }     
      }
	  
  }
?>
