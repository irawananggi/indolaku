<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Updatekeranjang extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {

        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   
	    if ($token != '') {
		  $umkm = $this->mymodel->getbywhere('umkm','umkm_token',$token,"row");
		  if (!empty($umkm)) {
			  
			  
			  $cekcart = $this->mymodel->withquery("select * from keranjang_umkm where keranjang_umkm_umkm_id = '".$umkm->umkm_id."' and keranjang_umkm_produk = '".$this->post('produk_id')."'","row");
			  				  
				  $data = array( 
				  "keranjang_umkm_qty" => $this->post('qty'),
				  "keranjang_umkm_note" => $this->post('note'));
				  $this->mymodel->update('keranjang_umkm',$data,"keranjang_umkm_id",$cekcart->keranjang_umkm_id);
			  
			  $keranjang_umkm = $this->mymodel->withquery("select * from keranjang_umkm, produk where keranjang_umkm_umkm_id = '".$umkm->umkm_id."' and keranjang_umkm_produk = '".$this->post('produk_id')."' and produk_id = keranjang_umkm_produk;","result");
			  foreach ($keranjang_umkm as $key => $value) {
          $value->produk_foto = base_url('uploads/produk/'.$value->produk_foto);
        }
			  $status = "200";
			  $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $keranjang_umkm);
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
		  }
		  $this->response($msg,$status);
	   }else {
		 $status = "401";
		 $data = array();
		 $msg = array('status' => 0, 'message'=>'Token anda kosong');
		 $this->response($msg,$status);
	   }     
      }
	  
  }
?>
