<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
class Terlaris extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['x-token']))
        $token =  $headers['x-token'];

        if($token != '') {
          $umkm = $this->mymodel->getbywhere('umkm','umkm_token',$token,"row");

  		    if (!empty($umkm)) {
            $gettransaksi_umkm = $this->mymodel->withquery("select * from transaksi_umkm_pusat where transaksi_status = 6","result");

            if(count($gettransaksi_umkm) > 0){

            $idTransUmkm = [];
            foreach ($gettransaksi_umkm as $key1 => $value1) {
                  $idTransUmkm[] = $value1->transaksi_umkm_pusat_id;                
            }
            $idTransUmkm = implode(', ',$idTransUmkm);
            $where = 'where transaksi_jenis = 4 AND transaksi_id IN ('.$idTransUmkm.')';
            $gettransaksidetail = $this->mymodel->withquery(
              "
              SELECT p.produk_id, SUM(p.qty_produk), SUM(p.total_produk),p.transaksi_id, p.transaksi_jenis
                from 
                  (
                    select transaksi_detail_product_id as produk_id
                    , SUM(transaksi_detail_qty
                  ) as qty_produk
                    , COUNT(transaksi_detail_product_id) as total_produk
                    , transaksi_jenis, transaksi_id 
                      from transaksi_detail $where 
                      GROUP BY transaksi_jenis,transaksi_id, transaksi_detail_product_id) as p 
                GROUP BY p.produk_id ORDER BY p.total_produk DESC
                ","result"
                );
              $TransDetUmkm = [];
              foreach ($gettransaksidetail as $key2 => $value2) {
                  $TransDetUmkm[] = $value2->produk_id;
              }

              $TransDetUmkm = implode(', ',$TransDetUmkm);
              
              $terlaris = $this->mymodel->withquery("select * from produk where produk_id IN ($TransDetUmkm)","result");
                $terlarisId = [];
                foreach ($terlaris as $key2 => $value2) {
                  $terlarisId[]= $value2->produk_id;
                }
              $terlarisId = implode(', ',$terlarisId);
              $produk = $this->mymodel->withquery("
                  select *,
                  if(produk_id IN ($terlarisId), 1, 0) as terlaris
                   from 
                   produk where produk_id IN ($terlarisId)=1 ORDER BY terlaris DESC 
                   ","result");
                foreach ($produk as $key => $value) {
                  $value->produk_foto = base_url('uploads/produk/'.$value->produk_foto);
                }
              $status = "200";
              $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $produk);
            }else{
              $produk = $this->mymodel->withquery("
                select * from 
                 produk ","result");
              foreach ($produk as $key => $value) {
                $value->produk_foto = base_url('uploads/produk/'.$value->produk_foto);
                $value->terlaris = '0';
              }
              $status = "200";
              $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $produk);
            }
          }else {
              $status = "200";
              $msg = array('status' => 0, 'message'=>'Umkm Tidak Ditemukan ', 'data' => array());
          }
        $this->response($msg,$status);
      }else {
        $status = "200";
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong', 'data' => array());
        $this->response($msg,$status);
      }
    }

}