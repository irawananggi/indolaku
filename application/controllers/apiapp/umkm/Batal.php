<?php

header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Batal extends REST_Controller {

   
  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {
      
        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   

      if ($token != '') {
		  $umkm = $this->mymodel->getbywhere('umkm','umkm_token',$token,"row");
		  if (!empty($umkm)) {
			  
			    $databukti = array(
				   "transaksi_status" => 5,
				   "transaksi_alasanbatal" => $this->input->post('alasan'));
				   
				   
				   $this->mymodel->update('transaksi_umkm_pusat',$databukti,'transaksi_umkm_pusat_id ',
				   $this->input->post('id_transaksi'));
				   
				   /*$balikstok = $this->mymodel->withquery("select * from transaksi_detail where 
				   transaksi_jenis = '".$this->input->post('jenis')."' and
				   transaksi_id = '".$this->input->post('id_transaksi')."'","result");
				   

				   $getidcabang = $this->mymodel->withquery("select * from transaksi_umkm_pusat  where 
						   transaksi_umkm_pusat_id  = '".$this->input->post('id_transaksi')."'","row")->transaksi_cabang_id;
				   foreach($balikstok as $d){
						
						$produkpercabang = $this->mymodel->withquery("select * from produk_per_cabang  where 
						   produk_per_cabang_produk_id = '".$d->transaksi_detail_product_id."' and
						   produk_per_cabang_cabang_id = '".$getidcabang."'","row");
						$updatestok = array("produk_per_cabang_stok" => intval($produkpercabang->produk_per_cabang_stok) + intval($d->transaksi_detail_qty));
						$this->mymodel->update('produk_per_cabang',$updatestok,
							   'produk_per_cabang_id', $produkpercabang->produk_per_cabang_id );
				   }*/
				   $msg = array('status' => 1, 'message'=>'Transaksi Berhasil dibatalkan');
				
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
		  }
          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }

      $this->response($msg);
    }
}
?>
