<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');
define( 'API_ACCESS_KEY', 'AAAAWyu_Puo:APA91bEbUprYzMYA4yV_RVzdsdQozDOEG1KcG-R1IVBvYn13UraC0Weu20vE37c-e1BDME9P-u9ap-70OERDxg7gzrVSGmWq__RRAnefzz02z-WyA92veVFm_Uj6GrNr236MvldG7wIe' );

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Checkout extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {

        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   
	    if ($token != '') {
		  $umkm = $this->mymodel->getbywhere('umkm','umkm_token',$token,"row");
		  if (!empty($umkm)) {
		  	
		  	$lat = $this->input->post('latitude');
		  	$long = $this->input->post('longitude');
			  $produkarr = explode(',',$this->input->post('produk'));
			  $qtyarr = explode(',',$this->input->post('qty'));
			  $hrgarr = explode(',',$this->input->post('harga'));
				$notearr = explode(',',$this->input->post('note_keranjang'));

			  $sisastokcabang = [];
			  $idstokcabang = [];
			  $sisastokcanvas = [];
			  $idstokcanvas = [];
			  $cabangterdekat;
			  $kanvasterdekat;
			  $cabangterdekatarr = $this->mymodel->withquery("select *, ( 6371 * acos( cos( radians(".$lat.") ) * cos( radians( c.cabang_latitude ) ) * cos( radians( c.cabang_longitude ) - radians(".$long.") ) + sin( radians(".$lat.") ) * sin( radians( c.cabang_latitude ) ) ) ) as jarak from cabang c where c.cabang_status = 1 and c.cabang_pusat = 1  and c.cabang_fcm_id IS NOT NULL ORDER BY jarak DESC limit 10","result");
				
						  
					if(count($cabangterdekatarr) > 0){
						foreach($cabangterdekatarr as $cbt){
						  $cabangterdekat =  $cbt;
					  }

						for($i = 0; $i < count($produkarr); $i++){
						  $idproduk = $produkarr[$i];
						  $qty = $qtyarr[$i];
						  $cekstok = $this->mymodel->withquery("select * from produk_per_cabang where produk_per_cabang_produk_id = '".$idproduk."' and produk_per_cabang_stok >= '".$qty."' and produk_per_cabang_cabang_id = '".$cbt->cabang_id."'","row");
					  
						  $sisastokcabang[] = $cekstok->produk_per_cabang_stok/* - $qty*/;
						  $idstokcabang[] = $cekstok->produk_per_cabang_id;
					  }

					  //cek data cabang
						if(!empty($cabangterdekat)){
							$data_list_produk_cabang = $this->mymodel->withquery("select d.*, m.* from keranjang_umkm d join produk m on d.keranjang_umkm_produk = m.produk_id where d.keranjang_umkm_umkm_id = '".$umkm->umkm_id."'","result");
				  
						  $totProdukCabang = 0;
						  $stokProdukCabang = array();
						  foreach($data_list_produk_cabang as $key => $value){
						  	$cekstokprodukcabang = $this->mymodel->withquery("select * from produk_per_cabang where produk_per_cabang_produk_id = '".$value->keranjang_umkm_produk."'   and produk_per_cabang_cabang_id = '".$cabangterdekat->cabang_id."' ","row");
						  	$totalprodukcabang = $this->mymodel->withquery("select * from produk_per_cabang where produk_per_cabang_produk_id = '".$value->keranjang_umkm_produk."'   and produk_per_cabang_cabang_id = '".$cabangterdekat->cabang_id."' ","result");
						  	$value->produk_id = $value->keranjang_umkm_produk;
						  	$value->produk_foto = base_url('uploads/produk/'.$value->produk_foto);
						  	
						  	$value->stok = (($cekstokprodukcabang->produk_per_cabang_stok ==NULL)?'kosong':(($value->keranjang_umkm_qty > $cekstokprodukcabang->produk_per_cabang_stok)?''.$cekstokprodukcabang->produk_per_cabang_stok - $value->keranjang_umkm_qty.'':$cekstokprodukcabang->produk_per_cabang_stok));
						  	$totProdukCabang += count($totalprodukcabang);

								$stokProdukCabang[]= ($cekstokprodukcabang->produk_per_cabang_stok - $value->keranjang_umkm_qty);
						  }
						}
						$input = preg_quote('-', '~');
						$results_array_cabang = preg_grep('~' . $input . '~', $stokProdukCabang);
					  $no_tracking = 'TRS-'.date('dmYHi').''.$umkm->umkm_id;
					  if(empty($cabangterdekat)){
						  
						  $status = "200";
						  $msg = array('status' => 0, 'message'=>'Stok Tidak ada', 'data' => new stdClass());
					  }else{
					  	
					  	if($totProdukCabang < count($data_list_produk_cabang)){
								  
								$status = "200";
								$msg = array('status' => 0, 'message'=>'Barang Tidak tersedia','data'=>$data_list_produk_cabang);
						}elseif($results_array_cabang){
						  	
						  	$status = "200";
							$msg = array('status' => 0, 'message'=>'Stok Tidak tersedia','data'=>$data_list_produk_cabang);
							$this->response($msg,$status);
						}else{

							  $transcabang = array('transaksi_umkm_pusat_id' => 0
							  , 'transaksi_umkm_id' => $umkm->umkm_id
							  , 'transaksi_cabang_id' => $cabangterdekat->cabang_id
					  		, 'transaksi_no_tracking' => $no_tracking
							  , 'transaksi_note' => $this->input->post('note')
							  , 'transaksi_longitude' => $this->input->post('longitude')
							  , 'transaksi_latitude' => $this->input->post('latitude')
							  , 'transaksi_total' => $this->input->post('total')
							  , 'transaksi_status' => 7
							  , 'transaksi_ongkir' => $this->input->post('ongkir')
							  , 'transaksi_tanggal' => date('Y-m-d H:i:s')
							  , 'transaksi_alamat' => $this->input->post('alamat')
							  , 'transaksi_jenispembayaran' => $this->input->post('jenispembayaran'));
							  
							  $this->mymodel->insert("transaksi_umkm_pusat", $transcabang);
							  
							  $dtransto = $this->mymodel->withquery("select * from
							  transaksi_umkm_pusat tcc, cabang c, umkm cs, transaksi_status ts where 
							  tcc.transaksi_umkm_id = '".$umkm->umkm_id."' 
							  and cs.umkm_id = '".$umkm->umkm_id."' 
							  and tcc.transaksi_cabang_id = '".$cabangterdekat->cabang_id."' 
							  and c.cabang_id ='".$cabangterdekat->cabang_id."' 
							  and tcc.transaksi_status = ts.transaksi_status_id 
							  order by transaksi_umkm_pusat_id desc","row");
							  
							  for($i = 0; $i < count($produkarr); $i++){
								  $td = array("transaksi_detail_id"=>0,
								  "transaksi_detail_product_id" => $produkarr[$i],
								  "transaksi_detail_harga" => $hrgarr[$i],
								  "transaksi_detail_qty" => $qtyarr[$i],
								  "transaksi_detail_total" => ($hrgarr[$i] * $qtyarr[$i]),
								  "transaksi_jenis"=>4,
								  "transaksi_id"=>$dtransto->transaksi_umkm_pusat_id ,
								  "transaksi_customer_id"=>$umkm->umkm_id,
									"transaksi_detail_note"=>$notearr[$i]  );
								  $this->mymodel->insert("transaksi_detail",$td);
								   
								  $datastokcabangupdate = array("produk_per_cabang_stok" => $sisastokcabang[$i]);
								  $this->mymodel->update('produk_per_cabang',$datastokcabangupdate,'produk_per_cabang_id',$idstokcabang[$i]);
							  }
							  
							  if($this->input->post('jenispembayaran') == 1){
								  
								  $termin = $this->mymodel->withquery("select * from termin","result");
								  
								  foreach($termin as $t){
									  
									  $td = array("transaksi_umkm_termin_id "=>0,
									  "transaksi_umkm_termin_termin_id" => $t->termin_id,
									  "transaksi_umkm_termin_transaksi_id" =>$dtransto->transaksi_umkm_pusat_id );
									  $this->mymodel->insert("transaksi_umkm_termin",$td);
								  }
							  }

								//$data_jenis = $this->mymodel->getbywhere('transaksi_detail','transaksi_id',$dtransto->transaksi_umkm_pusat_id,'transaksi_jenis','4',"row");
								$data_jenis = $this->mymodel->withquery("select * from transaksi_detail where transaksi_id = '".$dtransto->transaksi_umkm_pusat_id."' AND transaksi_jenis=4","row");
                                $dtransto->transaksi_jenis = $data_jenis->transaksi_jenis;

								//send notif cabang
								$cekfcmcabang = $this->mymodel->getbywhere("cabang", "cabang_id", $cabangterdekat->cabang_id, "row");
								if (!empty($cekfcmcabang->cabang_fcm_id)) {
								$this->send_notif("Pesanan Baru!!!", "Transaksi anda " . $dtransto->transaksi_no_tracking . " ", $cekfcmcabang->cabang_fcm_id, array('title' => "Pesanan Baru!!!", 'message' => "Transaksi anda " .$dtransto->transaksi_no_tracking . " ", 'tipe' => 'detail_transaksi', 'content' => array("transaksi_customer_cabang_id " => $dtransto->transaksi_customer_cabang_id , "no_tracking" => $dtransto->transaksi_no_tracking)));
								}


                            
							  $this->mymodel->delete("keranjang_umkm","keranjang_umkm_umkm_id",$umkm->umkm_id);
							  $status = "200";
							  $msg = array('status' => 1, 'message'=>'Transaksi Berhasil' ,'data' => $dtransto);
							  $this->response($msg,$status);
							}
					  }
					  
				        $this->response($msg,$status);
					}else {
					  $status = "200";
					  $msg = array('status' => 0, 'message'=>'Cabang Tidak Ditemukan ', 'data' => new stdClass());
				  }
				  $this->response($msg,$status);
		  }else {
			  $status = "200";
			  $msg = array('status' => 0, 'message'=>'Umkm Tidak Ditemukan ', 'data' => new stdClass());
		  }
		  $this->response($msg,$status);
	   
	   }else {

			 $status = "200";
			 $data = array();
			 $msg = array('status' => 0, 'message'=>'Token anda kosong', 'data' => new stdClass());
			 $this->response($msg,$status);
	   }     
  }


	  public function send_notif($title, $desc, $id_fcm, $data)
    {
        $Msg = array(
            'body' => $desc,
            'title' => $title
        );

        $fcmFields = array(
	      'to' => $id_fcm,
	      'notification' => $Msg,
	      'data'=>$data
	    );
	    $headers = array(
	      'Authorization: key=' . API_ACCESS_KEY,
	      'Content-Type: application/json'
	    );
	    $ch = curl_init();
	    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
	    curl_setopt( $ch,CURLOPT_POST, true );
	    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
	    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
	    $result = curl_exec($ch );
	    curl_close( $ch );

	    $cek_respon = explode(',',$result);
	    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
	    
        //echo $result."\n\n";
    }
  }
?>
