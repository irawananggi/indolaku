<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
class Produkterlaris extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['x-token']))
        $token =  $headers['x-token'];

      if ($token != '') {
          $umkm = $this->mymodel->getbywhere('umkm','umkm_token',$token,"row");
          if (!empty($umkm)) {
        //$produk = $this->mymodel->getall('produk');

        $gettransaksi = $this->mymodel->withquery("select transaksi_status, transaksi_umkm_pusat_id from transaksi_umkm_pusat where transaksi_status = 6","result");

        foreach ($gettransaksi as $key => $value) {
          $gettransaksidetail = $this->mymodel->withquery("select transaksi_id, transaksi_jenis, transaksi_detail_product_id, COUNT(transaksi_detail_product_id) as total_produk from transaksi_detail where transaksi_jenis = 4 AND transaksi_id = '".$value->transaksi_umkm_pusat_id."' order by total_produk DESC","result");
          

          foreach ($gettransaksidetail as $key2 => $value2) {
            $produk = $this->mymodel->withquery("select * from produk where produk_id = '".$value2->transaksi_detail_product_id."' ","result");

              foreach ($produk as $key3 => $value3) {
                $value->produk_foto = base_url('uploads/produk/'.$value3->produk_foto);
              }

          }
        }
        $status = "200";
        $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $produk);
          }else {
              $status = "401";
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
          }
          $this->response($msg,$status);
      }else {
        $status = "401";
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg,$status);
      }
    }

}