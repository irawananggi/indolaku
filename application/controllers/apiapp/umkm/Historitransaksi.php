<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
class Historitransaksi extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['x-token']))
        $token =  $headers['x-token'];

      if ($token != '') {
		  $umkm = $this->mymodel->getbywhere('umkm','umkm_token',$token,"row");
            $url_bkttrs = base_url('upload/buktitrf/');
  		  if (!empty($umkm)) {
  			  $dtranscabang = $this->mymodel->withquery("select *,
            (SELECT CASE WHEN transaksi_bukti_trf IS NULL OR transaksi_bukti_trf = '' THEN '' ELSE CONCAT('".$url_bkttrs."', transaksi_bukti_trf) END) as transaksi_bukti_trf,
             0 as kurir, 0 as termin from transaksi_umkm_pusat tcc,
         			       cabang c, umkm cs, transaksi_status ts where 
  					   tcc.transaksi_umkm_id = '".$umkm->umkm_id."' and
  					   c.cabang_id =tcc.transaksi_cabang_id and 
  					   cs.umkm_id = tcc.transaksi_umkm_id and 
  					   tcc.transaksi_status = ts.transaksi_status_id ORDER BY tcc.transaksi_tanggal DESC","result");

  			  foreach($dtranscabang as $d){
  				  if($d->transaksi_kurir == ""){
  					 $d->kurir = new stdClass();
  				  }else{
  					 $d->kurir = $this->mymodel->withquery("select * from kurir where kurir_id = '".$d->transaksi_kurir."'", "row");
  				  }
            $url_bkt_trs = base_url('upload/buktitrf/');
  				  $d->termin = $this->mymodel->withquery("select *,(SELECT CASE WHEN transaksi_bukti_trf IS NULL OR transaksi_bukti_trf = '' THEN '' ELSE CONCAT('".$url_bkt_trs."', transaksi_bukti_trf) END) as transaksi_bukti_trf from transaksi_umkm_termin, termin where transaksi_umkm_termin_transaksi_id = '".$d->transaksi_umkm_pusat_id ."' and termin_id = transaksi_umkm_termin_termin_id", "result");

  			    //data produk dari detail transaksi
            $produk_transaksi = $this->mymodel->withquery("select transaksi_detail_product_id,transaksi_detail_qty from transaksi_detail where transaksi_id = '".$d->transaksi_umkm_pusat_id."' AND  transaksi_jenis=4","result");
            $d->produk = $produk_transaksi;
            foreach ($d->produk as $key2 => $value2) {
              $dataproduk = $this->mymodel->withquery("select * from produk where produk_id = '".$value2->transaksi_detail_product_id."'","row");
              $value2->produk_id = $dataproduk->produk_id;
              $value2->produk_nama = $dataproduk->produk_nama;
              $value2->produk_kategori = $dataproduk->produk_kategori;
              $value2->produk_foto = base_url('uploads/produk/'.$dataproduk->produk_foto);
              $value2->produk_harga = $dataproduk->produk_harga;
              $value2->produk_harga_promo = $dataproduk->produk_harga_promo;
              $value2->produk_total_view = $dataproduk->produk_total_view;
              $value2->qty = $value2->transaksi_detail_qty;
              unset($value2->transaksi_detail_product_id);
            }

            $cek_umkm_pusat = $this->mymodel->withquery("select * from transaksi_umkm_termin where transaksi_umkm_termin_transaksi_id = '".$d->transaksi_umkm_pusat_id."' and transaksi_lunas=0","result");
            if(count($cek_umkm_pusat) > 0){
              $d->termin_status = 'Belum Lunas';
            }else{                
              $d->termin_status = 'Sudah Lunas';
            } 
  			  }      
  			  $status = "200";
  			  $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' , 'data' => $dtranscabang);
        }else {
            $status = "401";
            $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => array());
        }
        $this->response($msg,$status);
      }else {
        $status = "401";
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong', 'data' =>array());
        $this->response($msg,$status);
      }
    }

}