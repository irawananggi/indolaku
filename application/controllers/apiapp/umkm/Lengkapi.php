<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Lengkapi extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {

        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   
	    if ($token != '') {
		  $umkm = $this->mymodel->getbywhere('umkm','umkm_token',$token,"row");
		  if (!empty($umkm)) {
		  	if($_FILES['img']['name'] == NULL){
                $status = 0;
                $data = array( "umkm_nama" => $this->post('nama'), 
                  /*"umkm_telp" => $this->post('nama'),
                  "umkm_foto" => $file_name,  */
                  "umkm_alamat" => $this->post('alamat'),
                  "umkm_longitude" => $this->post('longitude'),
                  "umkm_latitude" => $this->post('latitude'),
                  "umkm_alamatnote" => $this->post('catatan_alamat'),
                  "umkm_nik" => $this->post('nik'),
                  "umkm_npwp" => $this->post('npwp'),
                  "umkm_province" => $this->post('prov'),
                  "umkm_kota" => $this->post('kota'),
                  "umkm_kec" => $this->post('kec'),
                  "umkm_kel" => $this->post('kel'));
                $this->mymodel->update('umkm',$data,'umkm_token',$token);
                $umkm = $this->mymodel->getbywhere('umkm','umkm_token',$token,"row");
				  	$status = "200";
		        $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $umkm);
		  	}else{
		  		$uploaddir = './upload/avatar/';
                $img = explode('.', $_FILES['img']['name']);
                $extension = end($img);
                $file_name =  md5(date('Y-m-d H:i:s').$_FILES['img']['name']).".".$extension;
                $uploadfile = $uploaddir.$file_name;
                $status = 0;
                if (move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile)) {
                	$data = array( "umkm_nama" => $this->post('nama'), 
                	  /*"umkm_telp" => $this->post('nama'), */
                	  "umkm_foto" => $file_name, 
                	  "umkm_alamat" => $this->post('alamat'),
                	  "umkm_longitude" => $this->post('longitude'),
                	  "umkm_latitude" => $this->post('latitude'),
                	  "umkm_alamatnote" => $this->post('catatan_alamat'),
                	  "umkm_nik" => $this->post('nik'),
                	  "umkm_npwp" => $this->post('npwp'),
                	  "umkm_province" => $this->post('prov'),
                	  "umkm_kota" => $this->post('kota'),
                	  "umkm_kec" => $this->post('kec'),
                	  "umkm_kel" => $this->post('kel'));
                $this->mymodel->update('umkm',$data,'umkm_token',$token);
                $umkm = $this->mymodel->getbywhere('umkm','umkm_token',$token,"row");
                $status = "200";
                $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $umkm);
					}else{
						$status = "401";
						$msg = array('status' => 0, 'message'=>'Foto Tidak berhasil diupload', 'data' => new stdClass());
					}

		  	}
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
		  }
		  $this->response($msg,$status);
	   }else {
		 $status = "401";
		 $data = array();
		 $msg = array('status' => 0, 'message'=>'Token anda kosong');
		 $this->response($msg,$status);
	   }     
      }
	  
  }
?>
