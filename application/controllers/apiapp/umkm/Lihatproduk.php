<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Lihatproduk extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {

        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   
	    if ($token != '') {
		  $umkm = $this->mymodel->getbywhere('umkm','umkm_token',$token,"row");
		  if (!empty($umkm)) {
			  
			  $produkview = $this->mymodel->getbywhere('produk','produk_id',$this->post('produk_id'),"row");
			  $data = array( 
			  "produk_total_view" => $produkview->produk_total_view+1);
			  $this->mymodel->update('produk',$data,'produk_id ',$this->post('produk_id'));
        $produkview = $this->mymodel->getbywhere('produk','produk_id',$this->post('produk_id'),"row");
        $produkview->produk_foto = base_url('uploads/produk/'.$produkview->produk_foto);
			  $status = "200";
			  $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $produkview);
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
		  }
		  $this->response($msg,$status);
	   }else {
		 $status = "401";
		 $data = array();
		 $msg = array('status' => 0, 'message'=>'Token anda kosong');
		 $this->response($msg,$status);
	   }     
      }
	  
  }
?>
