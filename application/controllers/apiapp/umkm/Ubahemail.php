<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Ubahemail extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {

        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   
	    if ($token != '') {
		  $umkm = $this->mymodel->getbywhere('umkm','umkm_token',$token,"row");
		  if (!empty($umkm)) {
			  if($umkm->umkm_email == $this->input->post('email_lama')){
				  
				  $data = array( "umkm_email" => $this->input->post('email_baru'));
				  $this->mymodel->update('umkm',$data,'umkm_token',$token);
				  $umkm = $this->mymodel->getbywhere('umkm','umkm_token',$token,"row");
				  $status = "200";
				  $msg = array('status' => 1, 'message'=>'Berhasil Simpan Data' ,'data' => $umkm);
			  }else{
				  
				  $status = "200";
				  $msg = array('status' => 0, 'message'=>'Email Anda Salah' ,'data' => new stdClass());
			  }
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
		  }
		  $this->response($msg,$status);
	   }else {
		 $status = "401";
		 $data = array();
		 $msg = array('status' => 0, 'message'=>'Token anda kosong');
		 $this->response($msg,$status);
	   }     
   }

   
	  
  }
?>
