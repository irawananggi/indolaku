<?php

header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Updatelokasi extends REST_Controller {

   
  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {
      
        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   

      if ($token != '') {
          $umkm = $this->mymodel->getbywhere('umkm','umkm_token',$token,'row');

          if (!empty($umkm)) {
			    $data = array(
				   "umkm_longitude" => $this->post("umkm_longitude"),
				   "umkm_latitude" => $this->post("umkm_latitude"));
				   
				    $this->mymodel->update('umkm',$data,'umkm_token',$token);
					$umkm = $this->mymodel->getbywhere('umkm','umkm_token',$token,'row');
					   
				   $msg = array('status' => 1, 'message'=>'Lokasi berhasil diupdate', 'data'=>$umkm);
				
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
		  }
          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong', 'data' => new stdClass());
        $this->response($msg);
      }

      $this->response($msg);
    }
}
?>
