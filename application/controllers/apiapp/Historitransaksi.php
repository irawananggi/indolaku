<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
class Historitransaksi extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['x-token']))
        $token =  $headers['x-token'];

      if ($token != '') {
          $customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
          if (!empty($customer)) {
			  $histori = $this->mymodel->withquery("SELECT transaksi_id,transaksi_jenis, 0 as cabang, 0 as kanvas, 0 as kurir,transaksi_detail_product_id FROM transaksi_detail where transaksi_customer_id = '".$customer->customer_id."' GROUP BY transaksi_id,transaksi_jenis order by transaksi_detail_id  desc","result");

			  foreach($histori as $list){
				   $id = $list->transaksi_id;
				   if($list->transaksi_jenis == 1){
					   $dtranskanvas = $this->mymodel->withquery("select * from transaksi_customer_canvas tcc, kanvas c, customer cs, transaksi_status ts where 
					   tcc.transaksi_customer_canvas_id = '".$id."' and 
					   cs.customer_id = tcc.transaksi_customer_id and 
					   c.kanvas_id =tcc.transaksi_canvas_id and 
					   tcc.transaksi_status = ts.transaksi_status_id","row");
						  
					   $list->transaksi_tanggal = $dtranskanvas->transaksi_tanggal;
					   $list->kanvas = $dtranskanvas;
					   $list->cabang = new stdClass();
					   $list->kurir = new stdClass();
					   $detailtrans = $this->mymodel->withquery("select transaksi_detail_product_id,transaksi_detail_qty from transaksi_detail where transaksi_id = '".$id."' AND  transaksi_jenis=1","result");
					   $list->produk = $detailtrans;
					   foreach ($list->produk as $key2 => $value2) {
								$dataproduk = $this->mymodel->withquery("select * from produk where produk_id = '".$value2->transaksi_detail_product_id."'","row");
								$value2->produk_id = $dataproduk->produk_id;
								$value2->produk_nama = $dataproduk->produk_nama;
								$value2->produk_kategori = $dataproduk->produk_kategori;
								$value2->produk_foto = base_url('uploads/produk/'.$dataproduk->produk_foto);
								$value2->produk_harga = $dataproduk->produk_harga;
								$value2->produk_harga_promo = $dataproduk->produk_harga_promo;
								$value2->produk_total_view = $dataproduk->produk_total_view;
								$value2->qty = $value2->transaksi_detail_qty;
					   	unset($value2->transaksi_detail_qty);
					   	unset($value2->transaksi_detail_product_id);
					   }
					   //die();
					   unset($list->transaksi_detail_product_id);
				   }else{
					   
					   $dtranscabang = $this->mymodel->withquery("select * from transaksi_customer_cabang tcc, cabang c, customer cs, transaksi_status ts where 
					   tcc.transaksi_customer_cabang_id = '".$id."' and 
					   cs.customer_id = tcc.transaksi_customer_id and 
					   c.cabang_id =tcc.transaksi_cabang_id and 
					   tcc.transaksi_status = ts.transaksi_status_id","row");
						 
					   $list->transaksi_tanggal = $dtranscabang->transaksi_tanggal;
					   $list->cabang = $dtranscabang;
					   $list->kanvas = new stdClass();
					   $list->kurir = $this->mymodel->withquery("select * from kurir where kurir_id = '".$dtranscabang->transaksi_kurir."'","row");
					   $detailtrans = $this->mymodel->withquery("select transaksi_detail_product_id,transaksi_detail_qty from transaksi_detail where transaksi_id = '".$id."' AND  transaksi_jenis=2","result");
					   $list->produk = $detailtrans;
					   foreach ($list->produk as $key2 => $value2) {
								$dataproduk = $this->mymodel->withquery("select * from produk where produk_id = '".$value2->transaksi_detail_product_id."'","row");
								$value2->produk_id = $dataproduk->produk_id;
								$value2->produk_nama = $dataproduk->produk_nama;
								$value2->produk_kategori = $dataproduk->produk_kategori;
								$value2->produk_foto = base_url('uploads/produk/'.$dataproduk->produk_foto);
								$value2->produk_harga = $dataproduk->produk_harga;
								$value2->produk_harga_promo = $dataproduk->produk_harga_promo;
								$value2->produk_total_view = $dataproduk->produk_total_view;
								$value2->qty = $value2->transaksi_detail_qty;
					   	unset($value2->transaksi_detail_qty);
					   	unset($value2->transaksi_detail_product_id);
					   }
					   //die();
					   
					   unset($list->transaksi_detail_product_id);
				   }
				   $jenis = $list->transaksi_jenis;   
				   $list->transaksi_jenis = $this->mymodel->getbywhere('transaksi_jenis','transaksi_jenis_id',$jenis,"row");
				   
			  }
			  
			  $status = "200";
			  $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' , 'data' => $histori);
			  
           
          }else {
              $status = "401";
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => array());
          }

          $this->response($msg,$status);
      }else {
        $status = "401";
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong', 'data' => array());
        $this->response($msg,$status);
      }
    }

}