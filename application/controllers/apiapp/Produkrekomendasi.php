<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
class Produkrekomendasi extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['x-token']))
        $token =  $headers['x-token'];

      if ($token != '') {
          $customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
          if (!empty($customer)) {
			  $produk = $this->mymodel->withquery("SELECT * FROM produk ORDER BY RAND() limit 12","result");
          foreach ($produk as $key => $value) {
            $value->produk_foto = base_url('uploads/produk/'.$value->produk_foto);
          }
			  $status = "200";
			  $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $produk);
          }else {
              $status = "401";
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
          }
          $this->response($msg,$status);
      }else {
        $status = "401";
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg,$status);
      }
    }

}