<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
class Historitransaksi extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['x-token']))
        $token =  $headers['x-token'];

      if ($token != '') {
          $kurir = $this->mymodel->getbywhere('kurir','kurir_token',$token,'row');

          if (!empty($kurir)) {
            
             $histori = $this->mymodel->withquery("SELECT transaksi_id,transaksi_jenis, 0 as cabang, 0 as umkm, transaksi_detail_product_id FROM transaksi_detail,transaksi_umkm_pusat where transaksi_jenis IN (2, 4) GROUP BY transaksi_id,transaksi_jenis  order by transaksi_detail_id DESC","result");
            
            foreach($histori as $list){
                
              $id = $list->transaksi_id;
              
              if($list->transaksi_jenis == 2){
                $dtranscabang = $this->mymodel->withquery("select * from transaksi_customer_cabang tcc, cabang c, customer cs, transaksi_status ts where tcc.transaksi_customer_cabang_id = '".$id."' and  
                   tcc.transaksi_kurir = '".$kurir->kurir_id."' and c.cabang_id =tcc.transaksi_cabang_id and cs.customer_id = tcc.transaksi_customer_id and tcc.transaksi_status = ts.transaksi_status_id order  by transaksi_customer_cabang_id desc","row");



                $list->transaksi_tanggal = $dtranscabang->transaksi_tanggal;
                $list->cabang = $dtranscabang;
                $list->umkm = new stdClass();
                if(!empty($dtranscabang)){

                  $detailtrans = $this->mymodel->withquery("select transaksi_detail_product_id,transaksi_detail_qty from transaksi_detail where transaksi_id = '".$id."' AND  transaksi_jenis=2","result");
                  $list->produk = $detailtrans;
                  foreach ($list->produk as $key2 => $value2) {
                    $dataproduk = $this->mymodel->withquery("select * from produk where produk_id = '".$value2->transaksi_detail_product_id."'","row");
                    $value2->produk_id = $dataproduk->produk_id;
                    $value2->produk_nama = $dataproduk->produk_nama;
                    $value2->produk_kategori = $dataproduk->produk_kategori;
                    $value2->produk_foto = base_url('uploads/produk/'.$dataproduk->produk_foto);
                    $value2->produk_harga = $dataproduk->produk_harga;
                    $value2->produk_harga_promo = $dataproduk->produk_harga_promo;
                    $value2->produk_total_view = $dataproduk->produk_total_view;
                    $value2->qty = $value2->transaksi_detail_qty;
                    unset($value2->transaksi_detail_qty);
                    unset($value2->transaksi_detail_product_id);
                  }
                  $jenis = $list->transaksi_jenis;   
                  $list->transaksi_jenis = $this->mymodel->getbywhere('transaksi_jenis','transaksi_jenis_id',$jenis,"row");

                 //die();
                }else{
                  $list->transaksi_id = "";
                  $list->transaksi_jenis = new stdClass();
                  $list->cabang = new stdClass();
                  $list->umkm = new stdClass();
                  $list->produk =array();
                }
              }

              if($list->transaksi_jenis == 4){
              $dtransumkm = $this->mymodel->withquery("select * from transaksi_umkm_pusat tcc, cabang c, umkm cs, transaksi_status ts where tcc.transaksi_umkm_pusat_id = '".$id."' and tcc.transaksi_kurir = '".$kurir->kurir_id."' and c.cabang_id =tcc.transaksi_cabang_id and 
                 cs.umkm_id = tcc.transaksi_umkm_id and tcc.transaksi_status = ts.transaksi_status_id order by transaksi_umkm_pusat_id desc","row");
                if(!empty($dtransumkm)){
                  $list->transaksi_tanggal = $dtransumkm->transaksi_tanggal;
                  $list->umkm = $dtransumkm;
                  $list->cabang = new stdClass();
                  $detailtrans = $this->mymodel->withquery("select transaksi_detail_product_id,transaksi_detail_qty from transaksi_detail where transaksi_id = '".$id."' AND  transaksi_jenis=4","result");
                  $list->produk = $detailtrans;
                  foreach ($list->produk as $key2 => $value2) {
                      $dataproduk = $this->mymodel->withquery("select * from produk where produk_id = '".$value2->transaksi_detail_product_id."'","row");
                      $value2->produk_id = $dataproduk->produk_id;
                      $value2->produk_nama = $dataproduk->produk_nama;
                      $value2->produk_kategori = $dataproduk->produk_kategori;
                      $value2->produk_foto = base_url('uploads/produk/'.$dataproduk->produk_foto);
                      $value2->produk_harga = $dataproduk->produk_harga;
                      $value2->produk_harga_promo = $dataproduk->produk_harga_promo;
                      $value2->produk_total_view = $dataproduk->produk_total_view;
                      $value2->qty = $value2->transaksi_detail_qty;
                      unset($value2->transaksi_detail_qty);
                      unset($value2->transaksi_detail_product_id);
                  }
                  $jenis = $list->transaksi_jenis;   
                  $list->transaksi_jenis = $this->mymodel->getbywhere('transaksi_jenis','transaksi_jenis_id',$jenis,"row");
                }else{
                  $list->transaksi_id = "";
                  $list->transaksi_jenis = new stdClass();
                  $list->cabang = new stdClass();
                  $list->umkm = new stdClass();
                  $list->produk =array();
                }
              }


             unset($list->transaksi_detail_product_id);
           
           
           
          }
            $status = "200";
            $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' , 'data' => $histori);
        }else {
            $status = "200";
            $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
        }

        $this->response($msg,$status);
      }else {
        $status = "200";
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong', 'data' =>new stdClass());
        $this->response($msg,$status);
      }
    }

}