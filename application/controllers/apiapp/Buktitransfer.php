<?php

header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');
define( 'API_ACCESS_KEY', 'AAAAWyu_Puo:APA91bEbUprYzMYA4yV_RVzdsdQozDOEG1KcG-R1IVBvYn13UraC0Weu20vE37c-e1BDME9P-u9ap-70OERDxg7gzrVSGmWq__RRAnefzz02z-WyA92veVFm_Uj6GrNr236MvldG7wIe' );

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Buktitransfer extends REST_Controller {

   
  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {
      
        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   

      if ($token != '') {
		  $customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
		  if (!empty($customer)) {
			  
			    $uploaddir = './upload/buktitrf/';
				$img = explode('.', $_FILES['img']['name']);
				$extension = end($img);
				$file_name =  md5(date('Y-m-d H:i:s').$_FILES['img']['name']).".".$extension;
				$uploadfile = $uploaddir.$file_name;
				$status = 0;

				if (move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile)) {
					
				   $databukti = array("transaksi_bukti_trf" => $file_name,
				   "transaksi_an" => $this->input->post('an'),
				   "transaksi_norek" => $this->input->post('norek'),
				   "transaksi_jenisbank" => $this->input->post('jenisbank'),
				   "transaksi_totalbayar" => $this->input->post('totalbayar'),
				   "transaksi_status" => 8);
				    if($this->input->post('jenis') == 1){
					   $this->mymodel->update('transaksi_customer_canvas',$databukti,'transaksi_customer_canvas_id ',$this->input->post('id_transaksi'));
					    //ambil data canvas
							$cektransaksi = $this->mymodel->withquery("select * from transaksi_customer_canvas  where 
								   transaksi_customer_canvas_id  = '".$this->input->post('id_transaksi')."'","row");
							$cekfcmkanvas = $this->mymodel->getbywhere("kanvas", "kanvas_id", $cektransaksi->transaksi_kanvas_id, "row");

					   	if (!empty($cekfcmkanvas->kanvas_fcm_id)) {
							$this->send_notif("Customer Sudah upload Bukti Transfer", "Transaksi anda " . $cektransaksi->transaksi_no_tracking . " ", $cekfcmkanvas->kanvas_fcm_id, array('title' => "Pesanan dikirim!!!", 'message' => "Transaksi anda " .$cektransaksi->transaksi_no_tracking . " ", 'tipe' => 'detail_transaksi', 'content' => array("transaksi_canvas_cabang_id " => $cektransaksi->transaksi_canvas_cabang_id , "no_tracking" => $cektransaksi->transaksi_no_tracking)));
							}

				    }else{
					   $this->mymodel->update('transaksi_customer_cabang',$databukti,'transaksi_customer_cabang_id  ',$this->input->post('id_transaksi'));
					    //ambil data cabang
							$cektransaksi = $this->mymodel->withquery("select * from transaksi_customer_cabang  where 
								   transaksi_customer_cabang_id  = '".$this->input->post('id_transaksi')."'","row");
							$cekfcmcabang = $this->mymodel->getbywhere("cabang", "cabang_id", $cektransaksi->transaksi_cabang_id, "row");

					   	if (!empty($cekfcmcabang->cabang_fcm_id)) {
							$this->send_notif("Customer Sudah upload Bukti Transfer", "Transaksi anda " . $cektransaksi->transaksi_no_tracking . " ", $cekfcmcabang->cabang_fcm_id, array('title' => "Pesanan dikirim!!!", 'message' => "Transaksi anda " .$cektransaksi->transaksi_no_tracking . " ", 'tipe' => 'detail_transaksi', 'content' => array("transaksi_customer_cabang_id " => $cektransaksi->transaksi_customer_cabang_id , "no_tracking" => $cektransaksi->transaksi_no_tracking)));
							}
					}
				   $msg = array('status' => 1, 'message'=>'Bukti transfer Berhasil di update','link'=>base_url("/upload/buktitrf/".$file_name));
				}else{
				  $msg = array('status' => 0, 'message'=>'Bukti transfer Tidak Berhasil di update','link'=>'');
				}
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ','link'=>'');
		  }
          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong','link'=>'');
        $this->response($msg);
      }

      $this->response($msg);
    }
  public function send_notif($title, $desc, $id_fcm, $data)
    {
        $Msg = array(
            'body' => $desc,
            'title' => $title
        );

        $fcmFields = array(
	      'to' => $id_fcm,
	      'notification' => $Msg,
	      'data'=>$data
	    );
	    $headers = array(
	      'Authorization: key=' . API_ACCESS_KEY,
	      'Content-Type: application/json'
	    );
	    $ch = curl_init();
	    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
	    curl_setopt( $ch,CURLOPT_POST, true );
	    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
	    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
	    $result = curl_exec($ch );
	    curl_close( $ch );

	    $cek_respon = explode(',',$result);
	    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
	    
        //echo $result."\n\n";
    }
}
?>
