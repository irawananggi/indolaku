<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Hapusitemkeranjang extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {

        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   
	    if ($token != '') {
		  $customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
		  if (!empty($customer)) {

        $produk_id = $this->post('produk_id');			  
			  $cekdata = $this->mymodel->withquery("select * from keranjang where keranjang_customer='".$customer->customer_id."' and keranjang_produk='".$produk_id."'",'result');

			  if (!empty($cekdata)) {
			  	$this->mymodel->delete2('keranjang','keranjang_customer',$customer->customer_id,'keranjang_produk',$produk_id);			  	
				  $status = "200";
				  $msg = array('status' => 1, 'message'=>'Berhasil Hapus Data' , 'data' => new stdClass());
			  }else{
				  $status = "401";
				  $msg = array('status' => 0, 'message'=>'Data Tidak Ditemukan ', 'data' => new stdClass());
			  }
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
		  }
		  $this->response($msg,$status);
	   }else {
		 $status = "401";
		 $data = array();
		 $msg = array('status' => 0, 'message'=>'Token anda kosong');
		 $this->response($msg,$status);
	   }     
      }
	  
  }
?>
