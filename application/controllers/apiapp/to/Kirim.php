<?php

header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Kirim extends REST_Controller {

   
  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {
      
        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   

      if ($token != '') {
          $pelanggang = $this->mymodel->getbywhere('pelanggang','pelanggang_token',$token,"row");
          if (!empty($pelanggang)) {
			  
			    $databukti = array(
				   "transaksi_status" => 4);
				   
				    $this->mymodel->update('transaksi_to_pusat',$databukti,'transaksi_to_pusat_id',
					   $this->input->post('id_transaksi'));
					   
				   $msg = array('status' => 1, 'message'=>'Pesanan Dikirim');
				
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
		  }
          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }

      $this->response($msg);
    }
}
?>
