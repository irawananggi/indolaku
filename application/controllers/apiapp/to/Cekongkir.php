<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
class Cekongkir extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['x-token']))
        $token =  $headers['x-token'];
      if ($token != '') {
          $pelanggang = $this->mymodel->getbywhere('pelanggang','pelanggang_token',$token,"row");
          if (!empty($pelanggang)) {
    			  $totalmeter = $this->input->post('totalmeter');
    			  $ongkir = $this->mymodel->withquery("select * from ongkir where ongkir_id ='2'","row");
    			  
    			  $total = ($totalmeter / $ongkir->ongkir_per_meter) * $ongkir->ongkir_harga;
    			  $status = "200";
    			  $val = array("ongkir_per_meter" => $ongkir->ongkir_per_meter,
    						   "ongkir_harga" => $ongkir->ongkir_harga,
    						   "ongkir_total_meter" => $totalmeter,
    						   "ongkir_total_harga" => "".$total);
    			  $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $val);
    			  
                  $status = "200";
    			  $this->response($msg,$status);
          }else {
              $status = "401";
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
			  $this->response($msg,$status);
          }
      }else {
        $status = "401";
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg,$status);
      }
    }

}