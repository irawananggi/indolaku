<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');
define( 'API_ACCESS_KEY', 'AAAAWyu_Puo:APA91bEbUprYzMYA4yV_RVzdsdQozDOEG1KcG-R1IVBvYn13UraC0Weu20vE37c-e1BDME9P-u9ap-70OERDxg7gzrVSGmWq__RRAnefzz02z-WyA92veVFm_Uj6GrNr236MvldG7wIe' );

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Checkout extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {

        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   
	    if ($token != '') {
        	$pelanggang = $this->mymodel->getbywhere('pelanggang','pelanggang_token',$token,"row");
        	if (!empty($pelanggang)) {
				$lat = $this->input->post('latitude');
				$long = $this->input->post('longitude');
				$produkarr = explode(',',$this->input->post('produk'));
				$qtyarr = explode(',',$this->input->post('qty'));
				$hrgarr = explode(',',$this->input->post('harga'));
				$notearr = explode(',',$this->input->post('note_keranjang'));
				$sisastokcabang = [];
				$idstokcabang = [];
				$sisastokcanvas = [];
				$idstokcanvas = [];
				$cabangterdekat;
				$kanvasterdekat;
				$cabangterdekatarr = $this->mymodel->withquery("select *, ( 6371 * acos( cos( radians(".$lat.") ) * cos( radians( c.cabang_latitude ) ) * cos( radians( c.cabang_longitude ) - radians(".$long.") ) + sin( radians(".$lat.") ) * sin( radians( c.cabang_latitude ) ) ) ) as jarak from cabang c where c.cabang_status = 1 and c.cabang_pusat = 1 and c.cabang_fcm_id IS NOT NULL ORDER BY jarak DESC limit 10","result");

				foreach($cabangterdekatarr as $cbt){
				  $ada = true;
				  $cabangterdekat =  $cbt;
				}

				for($i = 0; $i < count($produkarr); $i++){
					$idproduk = $produkarr[$i];
					$qty = $qtyarr[$i];
					$cekstok = $this->mymodel->withquery("select * from produk_per_cabang where produk_per_cabang_produk_id = '".$idproduk."' and produk_per_cabang_stok >= '".$qty."' and produk_per_cabang_cabang_id = '".$cbt->cabang_id."'","row");
					$sisastokcabang[] = $cekstok->produk_per_cabang_stok/* - $qty*/;
					$idstokcabang[] = $cekstok->produk_per_cabang_id;
				}
				//cek data cabang
				if(!empty($cabangterdekat)){
					$data_list_produk_cabang = $this->mymodel->withquery("select d.*, m.* from keranjang_to d join produk m on d.keranjang_produk = m.produk_id where d.keranjang_to_to_id = '".$pelanggang->pelanggang_id."'","result");
					$totProdukCabang = 0;
					$stokProdukCabang = array();
					foreach($data_list_produk_cabang as $key => $value){
						$cekstokprodukcabang = $this->mymodel->withquery("select * from produk_per_cabang where produk_per_cabang_produk_id = '".$value->keranjang_produk."'   and produk_per_cabang_cabang_id = '".$cabangterdekat->cabang_id."' ","row");
						$totalprodukcabang = $this->mymodel->withquery("select * from produk_per_cabang where produk_per_cabang_produk_id = '".$value->keranjang_produk."'   and produk_per_cabang_cabang_id = '".$cabangterdekat->cabang_id."' ","result");
						$value->produk_id = $value->keranjang_produk;
						$value->produk_foto = base_url('uploads/produk/'.$value->produk_foto);
						
						$value->stok = (($cekstokprodukcabang->produk_per_cabang_stok ==NULL)?'kosong':(($value->keranjang_qty > $cekstokprodukcabang->produk_per_cabang_stok)?''.$cekstokprodukcabang->produk_per_cabang_stok - $value->keranjang_qty.'':$cekstokprodukcabang->produk_per_cabang_stok));
						$totProdukCabang += count($totalprodukcabang);

						$stokProdukCabang[]= ($cekstokprodukcabang->produk_per_cabang_stok - $value->keranjang_qty);
					}
				}
				$input = preg_quote('-', '~');
				$results_array_cabang = preg_grep('~' . $input . '~', $stokProdukCabang);
				$no_tracking = 'TRS-'.date('dmYHi').''.$customer->customer_id;

				//cek transaksi sebelumnya
				/*$cek_trans_old = $this->mymodel->withquery("select * from transaksi_to_pusat where transaksi_to_id='".$pelanggang->pelanggang_id."' AND transaksi_status != 6 AND transaksi_to_id='".$pelanggang->pelanggang_id."' AND transaksi_status != 10  AND transaksi_to_id='".$pelanggang->pelanggang_id."' AND transaksi_status != 9 AND transaksi_to_id='".$pelanggang->pelanggang_id."' AND transaksi_status != 5 ","result");*/
				$cek_trans_old = $this->mymodel->withquery("select * from transaksi_to_pusat where transaksi_to_id='".$pelanggang->pelanggang_id."' AND transaksi_status NOT IN(5, 6, 9, 10)","result");
				
				if(empty($cabangterdekat)){
				  $status = "200";
				  $msg = array('status' => 0, 'message'=>'Cabang tidak ditemukan', 'data' => new stdClass());
				}elseif(count($cek_trans_old) > 0){
				  $status = "200";
				  $msg = array('status' => 0, 'message'=>'Proses transaksi ditolak, silahkan selesaikan transaksi sebelumnya.', 'data' => new stdClass());
				}else{
				  	if($totProdukCabang < count($data_list_produk_cabang)){
					  $status = "200";
					  $msg = array('status' => 0, 'message'=>'Barang Tidak tersedia','data'=>$data_list_produk_cabang);
					}elseif($results_array_cabang){
					  	$status = "200";
						$msg = array('status' => 0, 'message'=>'Stok Tidak tersedia','data'=>$data_list_produk_cabang);
				  	}else{
						$jb = $this->input->post('jenispembayaran');
						$transcabang = array('transaksi_to_pusat_id' => 0
						, 'transaksi_to_id' => $pelanggang->pelanggang_id
						, 'transaksi_cabang_id' => $cabangterdekat->cabang_id
				        , 'transaksi_no_tracking' => $no_tracking
						, 'transaksi_note' => $this->input->post('note_keranjang')
						, 'transaksi_longitude' => $this->input->post('longitude')
						, 'transaksi_latitude' => $this->input->post('latitude')
						, 'transaksi_total' => $this->input->post('total')
						, 'transaksi_status' => $jb == 1 ? '7' : '7'
						, 'transaksi_ongkir' => $this->input->post('ongkir')
						, 'transaksi_tanggal' => date('Y-m-d H:i:s')
						, 'transaksi_alamat' => $this->input->post('transaksi_alamat')
						, 'transaksi_jenispembayaran' => $this->input->post('jenispembayaran'));



						$this->mymodel->insert("transaksi_to_pusat", $transcabang);

						$dtransto = $this->mymodel->withquery("select * from
						transaksi_to_pusat tcc, cabang c, pelanggang cs, transaksi_status ts where 
						tcc.transaksi_to_id = '".$pelanggang->pelanggang_id."' 
						and cs.pelanggang_id = '".$pelanggang->pelanggang_id."' 
						and tcc.transaksi_cabang_id = '".$cabangterdekat->cabang_id."' 
						and c.cabang_id ='".$cabangterdekat->cabang_id."' 
						and tcc.transaksi_status = ts.transaksi_status_id 
						order by transaksi_to_pusat_id desc","row");

						for($i = 0; $i < count($produkarr); $i++){
						$td = array("transaksi_detail_id"=>0,
						"transaksi_detail_product_id" => $produkarr[$i],
						"transaksi_detail_harga" => $hrgarr[$i],
						"transaksi_detail_qty" => $qtyarr[$i],
						"transaksi_detail_total" => ($hrgarr[$i] * $qtyarr[$i]),
						"transaksi_jenis"=>3,
						"transaksi_id"=>$dtransto->transaksi_to_pusat_id,
						"transaksi_customer_id"=>$pelanggang->pelanggang_id,
						"transaksi_detail_note"=>$notearr[$i]  );
						$this->mymodel->insert("transaksi_detail",$td);
						 
						$datastokcabangupdate = array("produk_per_cabang_stok" => $sisastokcabang[$i]);
						$this->mymodel->update('produk_per_cabang',$datastokcabangupdate,'produk_per_cabang_id',$idstokcabang[$i]);
						}
						$status = "200";
						$msg = array('status' => 1, 'message'=>'Transaksi Berhasil' ,'data' => $dtransto);
						$this->mymodel->delete("keranjang_to","keranjang_to_to_id",$pelanggang->pelanggang_id);

						//send notif cabang
							$cekfcmcabang = $this->mymodel->getbywhere("cabang", "cabang_id", $cabangterdekat->cabang_id, "row");
						if (!empty($cekfcmcabang->cabang_fcm_id)) {
							$this->send_notif("Pesanan Baru!!!", "Transaksi anda " . $dtranscabang->transaksi_no_tracking . " ", $cekfcmcabang->cabang_fcm_id, array('title' => "Pesanan Baru!!!", 'message' => "Transaksi anda " .$dtranscabang->transaksi_no_tracking . " ", 'tipe' => 'detail_transaksi', 'content' => array("transaksi_to_pusat_id " => $dtranscabang->transaksi_to_pusat_id , "no_tracking" => $dtranscabang->transaksi_no_tracking)));
						}
				 	}
				}					 
			}else {
			  $status = "200";
			  $msg = array('status' => 0, 'message'=>'Pelanggang Tidak Ditemukan ', 'data' => new stdClass());
			}
			$this->response($msg,$status);
		}else {
			$status = "200";
			$data = array();
			$msg = array('status' => 0, 'message'=>'Token anda kosong', 'data' => new stdClass());
			$this->response($msg,$status);
		}     
   }

	 public function send_notif($title, $desc, $id_fcm, $data)
    {
        $Msg = array(
            'body' => $desc,
            'title' => $title
        );

        $fcmFields = array(
	      'to' => $id_fcm,
	      'notification' => $Msg,
	      'data'=>$data
	    );
	    $headers = array(
	      'Authorization: key=' . API_ACCESS_KEY,
	      'Content-Type: application/json'
	    );
	    $ch = curl_init();
	    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
	    curl_setopt( $ch,CURLOPT_POST, true );
	    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
	    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
	    $result = curl_exec($ch );
	    curl_close( $ch );

	    $cek_respon = explode(',',$result);
	    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
	    
        //echo $result."\n\n";
    } 
  }
?>
