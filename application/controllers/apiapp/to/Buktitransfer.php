<?php

header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Buktitransfer extends REST_Controller {

   
  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {
      
        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   

      if ($token != '') {
          $pelanggang = $this->mymodel->getbywhere('pelanggang','pelanggang_token',$token,"row");
          if (!empty($pelanggang)) {
			  
			    $uploaddir = './upload/buktitrf/';
				$img = explode('.', $_FILES['img']['name']);
				$extension = end($img);
				$file_name =  md5(date('Y-m-d H:i:s').$_FILES['img']['name']).".".$extension;
				$uploadfile = $uploaddir.$file_name;
				$status = 0;

				if (move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile)) {
					
				   $databukti = array("transaksi_bukti_trf" => $file_name,
				   "transaksi_an" => $this->input->post('an'),
				   "transaksi_norek" => $this->input->post('norek'),
				   "transaksi_jenisbank" => $this->input->post('jenisbank'),
				   "transaksi_totalbayar" => $this->input->post('totalbayar'),
				   "transaksi_status" => 8);
				    $this->mymodel->update('transaksi_to_pusat',$databukti,'transaksi_to_pusat_id',$this->input->post('id_transaksi'));
					
				   $msg = array('status' => 1, 'message'=>'Bukti transfer Berhasil di update','link'=>base_url("/upload/buktitrf/".$file_name));
				}else{
				  $msg = array('status' => 0, 'message'=>'Bukti transfer Tidak Berhasil di update','link'=>'');
				}
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ','link'=>'');
		  }
          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong','link'=>'');
        $this->response($msg);
      }

      $this->response($msg);
    }
}
?>
