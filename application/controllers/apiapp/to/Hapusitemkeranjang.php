<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Hapusitemkeranjang extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {

        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   
	    if ($token != '') {
        $pelanggang = $this->mymodel->getbywhere('pelanggang','pelanggang_token',$token,"row");
		  if (!empty($pelanggang)) {

        $produk_id = $this->post('produk_id');			  
			  $cekdata = $this->mymodel->withquery("select * from keranjang_to where keranjang_to_to_id='".$pelanggang->pelanggang_id."' and keranjang_produk='".$produk_id."'",'row');

			  if (!empty($cekdata)) {
			  	$this->mymodel->delete2('keranjang_to','keranjang_to_to_id',$pelanggang->pelanggang_id,'keranjang_produk',$produk_id);			  	
				  $status = "200";
				  $msg = array('status' => 1, 'message'=>'Berhasil Hapus Data' , 'data' => new stdClass());
			  }else{
				  $status = "401";
				  $msg = array('status' => 0, 'message'=>'Data Tidak Ditemukan ', 'data' => new stdClass());
			  }
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
		  }
		  $this->response($msg,$status);
	   }else {
		 $status = "401";
		 $data = array();
		 $msg = array('status' => 0, 'message'=>'Token anda kosong');
		 $this->response($msg,$status);
	   }     
      }
	  
  }
?>
