<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
class Historitransaksi extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['x-token']))
        $token =  $headers['x-token'];

      if ($token != '') {
          $pelanggang = $this->mymodel->getbywhere('pelanggang','pelanggang_token',$token,"row");
          if (!empty($pelanggang)) {
            $url_bkttrs = base_url('upload/buktitrf/');
        $dtranscabang = $this->mymodel->withquery("select *,
        (SELECT CASE WHEN transaksi_bukti_trf IS NULL OR transaksi_bukti_trf = '' THEN '' ELSE CONCAT('".$url_bkttrs."', transaksi_bukti_trf) END) as transaksi_bukti_trf from transaksi_to_pusat tcc,
                   cabang c, pelanggang cs, transaksi_status ts where 
             tcc.transaksi_to_id = '".$pelanggang->pelanggang_id."' and
             c.cabang_id =tcc.transaksi_cabang_id and 
             cs.pelanggang_id = tcc.transaksi_to_id and 
             tcc.transaksi_status = ts.transaksi_status_id  ORDER BY tcc.transaksi_tanggal DESC","result");
          foreach($dtranscabang as $key => $value){
            $detailtrans = $this->mymodel->withquery("select  transaksi_detail_product_id,transaksi_detail_qty,transaksi_detail_note  from transaksi_detail where transaksi_id  ='".$value->transaksi_to_pusat_id ."' and  transaksi_jenis='3' ","result");
            $value->produk = $detailtrans;
               foreach ($value->produk as $key2 => $value2) {
                  $dataproduk = $this->mymodel->withquery("select * from produk where produk_id = '".$value2->transaksi_detail_product_id."'","row");
                  $value2->produk_id = $dataproduk->produk_id;
                  $value2->produk_nama = $dataproduk->produk_nama;
                  $value2->produk_kategori = $dataproduk->produk_kategori;
                  $value2->produk_foto = base_url('uploads/produk/'.$dataproduk->produk_foto);
                  $value2->produk_harga = $dataproduk->produk_harga;
                  $value2->produk_harga_promo = $dataproduk->produk_harga_promo;
                  $value2->produk_total_view = $dataproduk->produk_total_view;
                  $value2->qty = $value2->transaksi_detail_qty;
                  $value2->note = $value2->transaksi_detail_note;
                unset($value2->transaksi_detail_product_id);
               }
          }
        $status = "200";
        $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' , 'data' => $dtranscabang);
           
          }else {
              $status = "401";
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
          }

          $this->response($msg,$status);
      }else {
        $status = "401";
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong', 'data' =>new stdClass());
        $this->response($msg,$status);
      }
    }

}