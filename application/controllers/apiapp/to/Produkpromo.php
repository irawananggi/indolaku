<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
class Produkpromo extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['x-token']))
        $token =  $headers['x-token'];

      if ($token != '') {
          $pelanggang = $this->mymodel->getbywhere('pelanggang','pelanggang_token',$token,"row");
          if (!empty($pelanggang)) {
    			  $gettransaksi_To = $this->mymodel->withquery("select * from transaksi_to_pusat where transaksi_status = 6","result");
            if(count($gettransaksi_To) > 0){            
              $idTransTo = [];
              foreach ($gettransaksi_To as $key1 => $value1) {
                    $idTransTo[] = $value1->transaksi_to_pusat_id;                
              }
              $idTransTo = implode(', ',$idTransTo);
              $where = 'where transaksi_jenis = 3 AND transaksi_id IN ('.$idTransTo.')';
              $gettransaksidetail = $this->mymodel->withquery(
                "
                SELECT p.produk_id, SUM(p.qty_produk), SUM(p.total_produk),p.transaksi_id, p.transaksi_jenis
                  from 
                    (
                      select transaksi_detail_product_id as produk_id
                      , SUM(transaksi_detail_qty
                    ) as qty_produk
                      , COUNT(transaksi_detail_product_id) as total_produk
                      , transaksi_jenis, transaksi_id 
                        from transaksi_detail $where 
                        GROUP BY transaksi_jenis,transaksi_id, transaksi_detail_product_id) as p 
                  GROUP BY p.produk_id ORDER BY p.total_produk DESC
                  ","result"
                  );
                  
              $TransDetTo = [];
              foreach ($gettransaksidetail as $key2 => $value2) {
                  $TransDetTo[] = $value2->produk_id;
              }

              $TransDetTo = implode(', ',$TransDetTo);
              $terlaris = $this->mymodel->withquery("select * from produk where produk_id IN ($TransDetTo)","result");
                $terlarisId = [];
                foreach ($terlaris as $key2 => $value2) {
                  $terlarisId[]= $value2->produk_id;
                }
              $terlarisId = implode(', ',$terlarisId);
              
              $produk = $this->mymodel->withquery("
                select *,
                if(produk_id IN ($terlarisId), 1, 0) as terlaris
                 from 
                 produk where produk_harga > produk_harga_promo AND produk_harga_promo > 0 ORDER BY terlaris DESC 
                 ","result");
              foreach ($produk as $key => $value) {
                $value->produk_foto = base_url('uploads/produk/'.$value->produk_foto);
              }

              $status = "200";
              $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $produk);
            }else{
              $produk = $this->mymodel->withquery("
                select * from 
                 produk where produk_harga > produk_harga_promo AND produk_harga_promo > 0 ORDER BY terlaris DESC 
                 ","result");
              foreach ($produk as $key => $value) {
                $value->produk_foto = base_url('uploads/produk/'.$value->produk_foto);
                $value->terlaris = '0';
              }
              
            $status = "200";
            $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $produk);
            }

            $this->response($msg,$status);
          }else {
              $status = "200";
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
          }
          $this->response($msg,$status);
      }else {
        $status = "200";
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg,$status);
      }
    }

}