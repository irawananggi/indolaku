<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Kirimotp extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {

        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   
	    if ($token != '') {
		  $customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
		  if (!empty($customer)) {
			  $data = array( "customer_verified" => 1);
			  $this->mymodel->update('customer',$data,'customer_token',$token);
			  send_wa($customer->customer_telp,$customer->customer_otp);
			  $status = "200";
			  $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $customer);
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
		  }
		  $this->response($msg,$status);
	   }else {
		 $status = "401";
		 $data = array();
		 $msg = array('status' => 0, 'message'=>'Token anda kosong');
		 $this->response($msg,$status);
	   }     
   }

   
	  
  }
  function send_wa($no,$kode){
		  $userkey = '9b7320e4f791';
		  $passkey = '3a5a799ab7e28e90572bc181';
		  $telepon = $no;
		  $message = '*Indolaku* Kode OTP Anda : '.$kode;
		  $url = 'https://console.zenziva.net/wareguler/api/sendWA/';
		  $curlHandle = curl_init();
		  curl_setopt($curlHandle, CURLOPT_URL, $url);
		  curl_setopt($curlHandle, CURLOPT_HEADER, 0);
		  curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		  curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
		  curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
		  curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
		  curl_setopt($curlHandle, CURLOPT_POST, 1);
		  curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
			  'userkey' => $userkey,
			  'passkey' => $passkey,
			  'to' => $telepon,
			  'message' => $message
		  ));
		  $results = json_decode(curl_exec($curlHandle), true);
		  curl_close($curlHandle);
	}
?>
