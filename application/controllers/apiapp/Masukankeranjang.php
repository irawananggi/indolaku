<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Masukankeranjang extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {

        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   
	    if ($token != '') {
		  $customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
		  if (!empty($customer)) {
			  
			  
			  $cekcart = $this->mymodel->withquery("select * from keranjang where keranjang_customer = '".$customer->customer_id."' and keranjang_produk = '".$this->post('produk_id')."'","row");
			  
			  if(empty($cekcart)){
				  
				 
				  $data = array( "keranjang_id" => 0, 
				  "keranjang_customer" =>  $customer->customer_id, 
				  "keranjang_produk" => $this->post('produk_id'),
				  "keranjang_qty" => $this->post('qty'),
				  "keranjang_note" => $this->post('note'));
				  $this->mymodel->insert('keranjang',$data);
			  
			  }else{
				  
				  $data = array( 
				  "keranjang_qty" => ($cekcart->keranjang_qty + $this->post('qty')),
				  "keranjang_note" => $this->post('note'));
				  $this->mymodel->update('keranjang',$data,"keranjang_id",$cekcart->keranjang_id);
			  }
			  
			  $keranjang = $this->mymodel->withquery("select * from keranjang, produk where keranjang_customer = '".$customer->customer_id."' and produk_id = keranjang_produk;","result");
			  foreach ($keranjang as $key => $value) {
                 $value->produk_foto = base_url('uploads/produk/'.$value->produk_foto);
              }
			  $status = "200";
			  $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $keranjang);
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
		  }
		  $this->response($msg,$status);
	   }else {
		 $status = "401";
		 $data = array();
		 $msg = array('status' => 0, 'message'=>'Token anda kosong');
		 $this->response($msg,$status);
	   }     
      }
	  
  }
?>
