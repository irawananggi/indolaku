<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
class Produkbykategori extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['x-token']))
        $token =  $headers['x-token'];

      if ($token != '') {
          $customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
        if (!empty($customer)) {
          $id = $this->input->get('kategori_id');

          $gettransaksi_cabang = $this->mymodel->withquery("select * from transaksi_customer_cabang where transaksi_status = 6","result");


          $gettransaksi_canvas = $this->mymodel->withquery("select transaksi_status, transaksi_customer_canvas_id from transaksi_customer_canvas where transaksi_status = 6","result");
          
          if(count($gettransaksi_cabang) > 0 AND count($gettransaksi_canvas) > 0){

            $idTransCab = [];
            foreach ($gettransaksi_cabang as $key1 => $value1) {
                  $idTransCab[] = $value1->transaksi_customer_cabang_id;                
            }


            $idTransCan = [];
            foreach ($gettransaksi_canvas as $key2 => $value2) {
                  $idTransCan[] = $value2->transaksi_customer_canvas_id;                
            }
            $idTransCab = implode(', ',$idTransCab);
            $idTransCan = implode(', ',$idTransCan);
            if($idTransCab != NULL){
              $where2 = 'where transaksi_jenis = 2 AND transaksi_id IN ('.$idTransCab.')';
            }
            if($idTransCan != NULL){
              $where1 = 'where transaksi_jenis = 1 AND transaksi_id IN ('.$idTransCan.')';
            }
            /*if($idTransCab != NULL AND $idTransCan != NULL){
              $where = 'where transaksi_jenis = 2 AND transaksi_id IN('.$idTransCab.') OR transaksi_jenis = 1 AND transaksi_id IN('.$idTransCan.')';
            }*/

            $gettransaksidetailCan = $this->mymodel->withquery(
              "
                SELECT p.produk_id, SUM(p.qty_produk), SUM(p.total_produk),p.transaksi_id, p.transaksi_jenis
              from 
              (
                select transaksi_detail_product_id as produk_id
                , SUM(transaksi_detail_qty
              ) as qty_produk
                , COUNT(transaksi_detail_product_id) as total_produk
                , transaksi_jenis, transaksi_id 
                  from transaksi_detail $where1 
                      GROUP BY transaksi_jenis,transaksi_id, transaksi_detail_product_id) as p 
                      GROUP BY p.produk_id ORDER BY p.total_produk DESC
              ","result"
            );
            
            $TransDetCan = [];
            foreach ($gettransaksidetailCan as $key2 => $value2) {
                $TransDetCan[] = $value2->produk_id;
              
            }

            $gettransaksidetailCab = $this->mymodel->withquery(
              "
                SELECT p.produk_id, SUM(p.qty_produk), SUM(p.total_produk),p.transaksi_id, p.transaksi_jenis
              from 
              (
                select transaksi_detail_product_id as produk_id
                , SUM(transaksi_detail_qty
              ) as qty_produk
                , COUNT(transaksi_detail_product_id) as total_produk
                , transaksi_jenis, transaksi_id 
                  from transaksi_detail $where2 
                      GROUP BY transaksi_jenis,transaksi_id, transaksi_detail_product_id) as p 
                      GROUP BY p.produk_id ORDER BY p.total_produk DESC
              ","result"
            );

            $TransDetcab = [];
            foreach ($gettransaksidetailCab as $key3 => $value3) {
              
                $TransDetcab[] = $value3->produk_id;
            }
            $TransDetcab = implode(', ',$TransDetcab);
            $TransDetCan = implode(', ',$TransDetCan);
           
            $produk_Can = $this->mymodel->withquery("select * from produk_per_canvas where produk_per_canvas_produkid IN ($TransDetCan) GROUP BY produk_per_canvas_produkid","result");
            
            $produk_Can_id = [];
            foreach ($produk_Can as $key2 => $value2) {
              $produk_Can_id[] = $value2->produk_per_canvas_produkid;
            }
            $produk_Can_id = implode(', ',$produk_Can_id);
            
            
            $produk_Cab = $this->mymodel->withquery("select * from produk_per_cabang where produk_per_cabang_id IN ($TransDetcab) GROUP BY produk_per_cabang_produk_id","result");
            $produk_Cab_id = [];
            foreach ($produk_Cab as $key2 => $value2) {
              $produk_Cab_id[] = $value2->produk_per_cabang_produk_id;
            }
            $produk_Cab_id = implode(', ',$produk_Cab_id);


            if(!empty($produk_Cab_id) AND empty($produk_Can_id)){
              $terlaris = $this->mymodel->withquery("select * from produk where produk_id IN ($produk_Cab_id)","result");
            }elseif(empty($produk_Cab_id) AND !empty($produk_Can_id)){
              $terlaris = $this->mymodel->withquery("select * from produk where produk_id IN ($produk_Can_id)","result");

            }elseif(!empty($produk_Cab_id) AND !empty($produk_Can_id)){
              $terlaris = $this->mymodel->withquery("select * from produk where produk_id IN ($produk_Cab_id) OR  produk_id IN ($produk_Can_id)","result");
            }else{
              $terlaris = $this->mymodel->withquery("select * from produk where produk_id","result");
            }

            $terlarisId = [];
            foreach ($terlaris as $key2 => $value2) {
             $terlarisId[]= $value2->produk_id;
            }

           
            if(empty($terlarisId)){
              $produk = $this->mymodel->withquery("
                select * from 
                 produk WHERE produk_kategori='".$id."' ORDER BY produk_harga ASC 
                 ","result");
              foreach ($produk as $key => $value) {
                $value->produk_foto = base_url('uploads/produk/'.$value->produk_foto);
                $value->terlaris = '0';
              }
              $status = "200";
              $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $produk);
            }else{
              $produk = $this->mymodel->withquery("
                select *,
                if(produk_id IN ($terlarisId), 1, 0) as terlaris
                 from 
                 produk WHERE produk_kategori='".$id."' ORDER BY terlaris DESC 
                 ","result");
              foreach ($produk as $key => $value) {
                $value->produk_foto = base_url('uploads/produk/'.$value->produk_foto);
              }
              $status = "200";
              $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $produk);
            }
          }else{
            $produk = $this->mymodel->withquery("
              select * from 
               produk WHERE produk_kategori='".$id."' ORDER BY produk_harga ASC 
               ","result");
            foreach ($produk as $key => $value) {
              $value->produk_foto = base_url('uploads/produk/'.$value->produk_foto);
              $value->terlaris = '0';
            }
            $status = "200";
            $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $produk);
          }
          $this->response($msg,$status);

        }else {
            $status = "200";
            $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
        }
        $this->response($msg,$status);
      }else {
        $status = "200";
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg,$status);
      }
    }

}