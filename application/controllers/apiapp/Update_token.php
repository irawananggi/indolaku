<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Update_token extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_get() {

        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	    if ($token != '') {
	    	$customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
	    	if(!empty($customer)){	 
			  	$data = array( 
				  "customer_token" => md5($customer->telp."".time())
				);
				$this->mymodel->update('customer',$data,"customer_id",$customer->customer_id);
				$status = "200";
				$msg = array('status' => 1, 'message'=>'Berhasil update fcm' ,'data' => $data);
	    	}else{
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Cutomer Tidak Ditemukan', 'data' => new stdClass());

	    	}
		  
		  $this->response($msg,$status);
		}else {
			 $status = "401";
			 $data = array();
			 $msg = array('status' => 0, 'message'=>'Token anda kosong');
			 $this->response($msg,$status);
		}     
    }
	  
  }
?>
