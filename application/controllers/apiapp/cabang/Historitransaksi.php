<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
class Historitransaksi extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['x-token']))
        $token =  $headers['x-token'];

      if ($token != '') {
          $cabang = $this->mymodel->getbywhere('cabang','cabang_token',$token,'row');

          if (!empty($cabang)) {
			  
			  $alltranscabang = $this->mymodel->withquery("SELECT transaksi_id,transaksi_jenis, transaksi_detail_product_id,
			  0 as detailtrscust, 
			  0 as detailtrsto,
			  0 as detailtrsumkm,
			  0 as termin,
			  0 as cabang, 0 as kurir, 0 as pelanggang, 0 as umkm 
			  FROM transaksi_detail, transaksi_jenis GROUP BY transaksi_id,transaksi_jenis order 
			  by transaksi_detail_id desc", "result");
			  
			  $url_bkttrs = base_url('upload/buktitrf/');
			  foreach($alltranscabang as $list){

				  $id = $list->transaksi_id;
				  if($list->transaksi_jenis == 2){
				  	$dtranscabang = $this->mymodel->withquery("select *,

					  	(SELECT CASE WHEN transaksi_bukti_trf IS NULL OR transaksi_bukti_trf = '' THEN '' ELSE CONCAT('".$url_bkttrs."', transaksi_bukti_trf) END) as transaksi_bukti_trf 

					  	from transaksi_customer_cabang tcc,
							   cabang c, transaksi_status ts where 
							   tcc.transaksi_customer_cabang_id  = '".$list->transaksi_id."' and
							   c.cabang_id =tcc.transaksi_cabang_id and 
							   tcc.transaksi_status = ts.transaksi_status_id","row");

				  	if($cabang->cabang_id == $dtranscabang->transaksi_cabang_id){
					  
						  $list->transaksi_jenis = $this->mymodel->withquery("select * from transaksi_jenis where transaksi_jenis_id  ='".$list->transaksi_jenis."'","row");
						  /*foreach($dtranscabang as $key =>$val){
						  	$list->detailtrscust = $key;
						  }*/
						  $list->detailtrscust = $dtranscabang;
						  $list->detailtrsto =  new stdClass();
						  $list->detailtrsumkm =  new stdClass();
						  $list->cabang = $this->mymodel->withquery("select * from cabang where cabang_id  ='".$dtranscabang->transaksi_cabang_id."'","row");
						  if($dtranscabang->transaksi_kurir  == 0){
							 $list->kurir =  new stdClass();
						  }else{
							  $list->kurir = $this->mymodel->withquery("select * from kurir where kurir_id  ='".$dtranscabang->transaksi_kurir."'","row");
						  }
						  $list->pelanggang =  new stdClass();
						  $list->umkm =  new stdClass();
						  $list->termin =  [];
						  $detailtrans = $this->mymodel->withquery("select transaksi_detail_product_id,transaksi_detail_qty,transaksi_detail_note from transaksi_detail where transaksi_id = '".$id."' AND  transaksi_jenis=2","result");
					   $list->produk = $detailtrans;
					   foreach ($list->produk as $key2 => $value2) {
								$dataproduk = $this->mymodel->withquery("select * from produk where produk_id = '".$value2->transaksi_detail_product_id."'","row");
								$value2->produk_id = $dataproduk->produk_id;
								$value2->produk_nama = $dataproduk->produk_nama;
								$value2->produk_kategori = $dataproduk->produk_kategori;
								$value2->produk_foto = base_url('uploads/produk/'.$dataproduk->produk_foto);
								$value2->produk_harga = $dataproduk->produk_harga;
								$value2->produk_harga_promo = $dataproduk->produk_harga_promo;
								$value2->produk_total_view = $dataproduk->produk_total_view;
								$value2->qty = $value2->transaksi_detail_qty;
					   	unset($value2->transaksi_detail_product_id);
					   }

						 /* $list->produk =  $this->mymodel->withquery("select * from produk where produk_id = '".$list->transaksi_detail_product_id."'","result");
						   foreach ($list->produk as $key => $value) {
						   	// code...
						   	$value->produk_foto= base_url('uploads/produk/'.$value->produk_foto);
	            	$value->qty = $this->mymodel->withquery("select * from transaksi_detail where transaksi_id  ='".$list->transaksi_id."' and transaksi_detail_product_id = '".$value->produk_id."'","row")->transaksi_detail_qty;
						   }*/
					  	$list->data_pemesan = $this->mymodel->getbywhere("customer", "customer_id", $dtranscabang->transaksi_customer_id, "row");
				  	}else{
							$list->transaksi_id = '';
							$list->transaksi_jenis = new stdClass();
						  $list->detailtrscust = new stdClass();
						  $list->detailtrsto =  new stdClass();
						  $list->detailtrsumkm =  new stdClass();
						  $list->cabang = new stdClass();
							$list->kurir =  new stdClass();					 
						  $list->pelanggang =  new stdClass();
						  $list->umkm =  new stdClass();
						  $list->termin =  [];
					  	$list->data_pemesan =  new stdClass();
				  	}
					  
				  }else if($list->transaksi_jenis == 3){
					  
					  $dtransto = $this->mymodel->withquery("select *,

					  	(SELECT CASE WHEN transaksi_bukti_trf IS NULL OR transaksi_bukti_trf = '' THEN '' ELSE CONCAT('".$url_bkttrs."', transaksi_bukti_trf) END) as transaksi_bukti_trf  
					  	from transaksi_to_pusat tcc,
							   cabang c, transaksi_status ts where 
							   tcc.transaksi_to_pusat_id  = '".$list->transaksi_id."' and
							   c.cabang_id =tcc.transaksi_cabang_id and 
							   tcc.transaksi_status = ts.transaksi_status_id","row");
					  if($cabang->cabang_id == $dtransto->transaksi_cabang_id){
						  $list->transaksi_jenis = $this->mymodel->withquery("select * from transaksi_jenis where transaksi_jenis_id  ='".$list->transaksi_jenis."'","row");	   
						  $list->detailtrscust =new stdClass();
						  $list->detailtrsto =  $dtransto;
						  $list->detailtrsumkm =  new stdClass();
						  $list->kurir =  new stdClass();
						  $list->cabang = $this->mymodel->withquery("select * from cabang where cabang_id  ='".$dtransto->transaksi_cabang_id."'","row");
						  
						  $list->pelanggang =  $this->mymodel->withquery("select * from pelanggang where pelanggang_id  ='".$dtransto->transaksi_to_id."'","row");
						  
						  $list->umkm =  new stdClass();
						  $list->termin =  [];
						  
						  $detailtrans = $this->mymodel->withquery("select transaksi_detail_product_id,transaksi_detail_qty,transaksi_detail_note from transaksi_detail where transaksi_id = '".$id."' AND  transaksi_jenis=3","result");
					   $list->produk = $detailtrans;
					   foreach ($list->produk as $key2 => $value2) {
								$dataproduk = $this->mymodel->withquery("select * from produk where produk_id = '".$value2->transaksi_detail_product_id."'","row");
								$value2->produk_id = $dataproduk->produk_id;
								$value2->produk_nama = $dataproduk->produk_nama;
								$value2->produk_kategori = $dataproduk->produk_kategori;
								$value2->produk_foto = base_url('uploads/produk/'.$dataproduk->produk_foto);
								$value2->produk_harga = $dataproduk->produk_harga;
								$value2->produk_harga_promo = $dataproduk->produk_harga_promo;
								$value2->produk_total_view = $dataproduk->produk_total_view;
								$value2->qty = $value2->transaksi_detail_qty;
					   	unset($value2->transaksi_detail_product_id);
					   }
					   /*
						  $list->produk =  $this->mymodel->withquery("select * from produk where produk_id = '".$list->transaksi_detail_product_id."'","result");
						   foreach ($list->produk as $key => $value) {
						   	// code...
						   	$value->produk_foto= base_url('uploads/produk/'.$value->produk_foto);
						   }*/
					  	$list->data_pemesan =  $this->mymodel->withquery("select * from pelanggang where pelanggang_id  ='".$dtransto->transaksi_to_id."'","row");

					  }else{
              $list->transaksi_id = '';
              $list->transaksi_jenis = new stdClass();
              $list->detailtrscust = new stdClass();
              $list->detailtrsto =  new stdClass();
              $list->detailtrsumkm =  new stdClass();
              $list->cabang = new stdClass();
              $list->kurir =  new stdClass();					 
              $list->pelanggang =  new stdClass();
              $list->umkm =  new stdClass();
              $list->termin =  [];
              $list->produk =  [];
					  	$list->data_pemesan =  new stdClass();

					  }

				  }else if($list->transaksi_jenis == 4){
					  
					  $dtransumkm = $this->mymodel->withquery("select *,

					  	(SELECT CASE WHEN transaksi_bukti_trf IS NULL OR transaksi_bukti_trf = '' THEN '' ELSE CONCAT('".$url_bkttrs."', transaksi_bukti_trf) END) as transaksi_bukti_trf 
					  	from transaksi_umkm_pusat tcc,
							   cabang c, transaksi_status ts where 
							   tcc.transaksi_umkm_pusat_id   = '".$list->transaksi_id."' and
							   c.cabang_id =tcc.transaksi_cabang_id and 
							   tcc.transaksi_status = ts.transaksi_status_id","row");
					  
					  if($cabang->cabang_id == $dtransumkm->transaksi_cabang_id){
					  	
						  $list->transaksi_jenis = $this->mymodel->withquery("select * from transaksi_jenis where transaksi_jenis_id  ='".$list->transaksi_jenis."'","row");
						  $list->detailtrscust =new stdClass();
						  $list->detailtrsto =  new stdClass();
						  $list->detailtrsumkm =   $dtransumkm;
						  $list->cabang = $this->mymodel->withquery("select * from cabang where cabang_id  ='".$dtransumkm->transaksi_cabang_id."'","row");
						  
						   if($dtranscabang->transaksi_kurir  == 0){
							 $list->kurir =  new stdClass();
						  }else{
							  $list->kurir = $this->mymodel->withquery("select * from kurir where kurir_id  ='".$dtransumkm->transaksi_kurir."'","row");
						  }
						  $list->pelanggang =  new stdClass();
						  $list->umkm =  $this->mymodel->withquery("select * from umkm where umkm_id  ='".$dtransumkm->transaksi_umkm_id."'","row");
						  $url_bkt_trs = base_url('upload/buktitrf/');
						  $list->termin = $this->mymodel->withquery("select *,(SELECT CASE WHEN transaksi_bukti_trf IS NULL OR transaksi_bukti_trf = '' THEN '' ELSE CONCAT('".$url_bkt_trs."', transaksi_bukti_trf) END) as transaksi_bukti_trf from transaksi_umkm_termin, termin where transaksi_umkm_termin_transaksi_id = '".$dtransumkm->transaksi_umkm_pusat_id ."' and termin_id = transaksi_umkm_termin_termin_id", "result");

						  
						  $detailtrans = $this->mymodel->withquery("select transaksi_detail_product_id,transaksi_detail_qty,transaksi_detail_note from transaksi_detail where transaksi_id = '".$id."' AND  transaksi_jenis=4","result");
					   $list->produk = $detailtrans;
					   foreach ($list->produk as $key2 => $value2) {
								$dataproduk = $this->mymodel->withquery("select * from produk where produk_id = '".$value2->transaksi_detail_product_id."'","row");
								$value2->produk_id = $dataproduk->produk_id;
								$value2->produk_nama = $dataproduk->produk_nama;
								$value2->produk_kategori = $dataproduk->produk_kategori;
								$value2->produk_foto = base_url('uploads/produk/'.$dataproduk->produk_foto);
								$value2->produk_harga = $dataproduk->produk_harga;
								$value2->produk_harga_promo = $dataproduk->produk_harga_promo;
								$value2->produk_total_view = $dataproduk->produk_total_view;
								$value2->qty = $value2->transaksi_detail_qty;
					   	unset($value2->transaksi_detail_product_id);
					   }
							
						  /*$list->produk =  $this->mymodel->withquery("select * from produk where produk_id = '".$list->transaksi_detail_product_id."'","result");
						   foreach ($list->produk as $key => $value) {
						   	// code...
						   	$value->produk_foto= base_url('uploads/produk/'.$value->produk_foto);
						   }*/
					  	$list->data_pemesan =  $this->mymodel->withquery("select * from umkm where umkm_id  ='".$dtransumkm->transaksi_umkm_id."'","row");
					  }else{
						    $list->transaksi_id = '';
								$list->transaksi_jenis = new stdClass();
						    $list->detailtrscust = new stdClass();
                $list->detailtrsto =  new stdClass();
                $list->detailtrsumkm =  new stdClass();
                $list->cabang = new stdClass();
                $list->kurir =  new stdClass();					 
                $list->pelanggang =  new stdClass();
                $list->umkm =  new stdClass();
                $list->termin =  [];
                $list->produk =  [];	
					  		$list->data_pemesan =  new stdClass();				  	
					  }
			  
				  }else{
				  	
						$list->transaksi_id = '';
						$list->transaksi_jenis = new stdClass();
					  $list->detailtrscust = new stdClass();
					  $list->detailtrsto =  new stdClass();
					  $list->detailtrsumkm =  new stdClass();
					  $list->cabang = new stdClass();
						$list->kurir =  new stdClass();					 
					  $list->pelanggang =  new stdClass();
					  $list->umkm =  new stdClass();
					  $list->termin =  [];
					  $list->produk =  [];
					  $list->data_pemesan =  new stdClass();
				  }
				  unset($list->transaksi_detail_product_id);
			  }
			  
			 // foreach($dtranscabang as $list){
				//  $list->kurir = $this->mymodel->withquery("select * from kurir where kurir_id = '".$list->transaksi_kurir."'","row");
				  
			  //}
			  $status = "200";
			  $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' , 'data' => $alltranscabang);
			  
           
          }else {
              $status = "401";
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => array());
          }

          $this->response($msg,$status);
      }else {
        $status = "401";
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong', 'data' => array());
        $this->response($msg,$status);
      }
    }

}