<?php

header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Updatestatustermin extends REST_Controller {

   
  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {
      
        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   

      if ($token != '') {
          $cabang = $this->mymodel->getbywhere('cabang','cabang_token',$token,'row');

          if (!empty($cabang)) {
				$data = array(
					"transaksi_lunas" => 1
				);

				$this->mymodel->update('transaksi_umkm_termin',$data,'transaksi_umkm_termin_id',$this->post("transaksi_umkm_termin_id"));
				$cabang = $this->mymodel->getbywhere('transaksi_umkm_termin','transaksi_umkm_termin_id',$this->post("transaksi_umkm_termin_id"),'row');

				//cek data status lunas 0
				$cekData = $this->mymodel->withquery("select * from transaksi_umkm_termin where transaksi_umkm_termin_transaksi_id  ='".$this->post("id_transaksi")."' AND transaksi_lunas=0 ","result");
				if(count($cekData) == 0){
					$data_trans = array(
						"transaksi_status" => 6
					);

					$this->mymodel->update('transaksi_umkm_pusat',$data_trans,'transaksi_umkm_pusat_id',$this->post("id_transaksi"));

				}
				$msg = array('status' => 1, 'message'=>'Status berhasil diupdate', 'data'=>$cabang);
				
		  }else {
			  $status = "200";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
		  }
          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong', 'data' => new stdClass());
        $this->response($msg);
      }

      $this->response($msg);
    }
}
?>
