<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
define( 'API_ACCESS_KEY', 'AAAAxYQ88yo:APA91bH4_6wnS4fpy89nnXOa-9t4eukXbj3XyBNh1UH8si7zPgAtoJaHqkNG13dEtOvpLF7WmZaeHoNT31SUD5ug-yTiz_MER-VHOYOruB9_ujr8rqqb_SGAn0kpeP0b1uYhKBcB7vUH' );

class Reminder extends My_Controller {
    function __construct()
    {
       parent::__construct();
      $this->load->model('mymodel');
    }
    public function index(){
      //test cronjobs cpanel
      $data = array( 
        "status" => 1, 
          "created_at" =>date('Y-m-d H:i:s')
        );
        $simpan = $this->mymodel->insert('test_cronjobs',$data);

      $tanggal_skrg = date('Y-m-d H:i:s', time()-900);

      //data transaksi batal customer cabang
      $DataTransCabang = $this->mymodel->withquery("select * from transaksi_customer_cabang  where 
                 transaksi_tanggal  < '".$tanggal_skrg."' AND transaksi_status IN('1, 8')","result");
      
      foreach($DataTransCabang as $key => $value) {
        //update status setiap transaksi dengan status 1 dan 8
        $databukti = array(
         "transaksi_status" => 5,
         "transaksi_alasanbatal" => 'Transaksi dibatalkan sistem');

        $this->mymodel->update('transaksi_customer_cabang',$databukti,'transaksi_customer_cabang_id ',
         $value->transaksi_customer_cabang_id);

        /*$balikstok = $this->mymodel->withquery("select * from transaksi_detail where 
           transaksi_jenis = '2' and
           transaksi_id = '".$value->transaksi_customer_cabang_id."'","result");
        foreach($balikstok as $d){
          $produkpercabang = $this->mymodel->withquery("select * from produk_per_cabang  where 
             produk_per_cabang_produk_id = '".$d->transaksi_detail_product_id."' and
             produk_per_cabang_cabang_id = '".$value->transaksi_customer_cabang_id."'","row");
            
          $updatestok = array("produk_per_cabang_stok" => intval($produkpercabang->produk_per_cabang_stok) + intval($d->transaksi_detail_qty));
          
          $this->mymodel->update('produk_per_cabang',$updatestok,
               'produk_per_cabang_id', $produkpercabang->produk_per_cabang_id );
        }*/
          $cekfcmcustomer = $this->mymodel->getbywhere("customer", "customer_id", $value->transaksi_customer_id, "row");

          if (!empty($cekfcmcustomer->customer_fcm_id)) {
            $this->send_notif("Pesanan dibatalkan!!!", "Transaksi anda " . $value->transaksi_no_tracking . " ", $cekfcmcustomer->customer_fcm_id, array('title' => "Pesanan dibatalkan!!!", 'message' => "Transaksi anda " .$value->transaksi_no_tracking . " ", 'tipe' => 'detail_transaksi', 'content' => array("transaksi_customer_cabang_id " => $value->transaksi_customer_cabang_id , "no_tracking" => $value->transaksi_no_tracking)));
          }
          $cekfcmcabang = $this->mymodel->getbywhere("cabang", "cabang_id", $value->transaksi_cabang_id, "row");

          if (!empty($cekfcmcabang->cabang_fcm_id)) {
            $this->send_notif("Pesanan dibatalkan!!!", "Transaksi " . $value->transaksi_no_tracking . " ", $cekfcmcabang->cabang_fcm_id, array('title' => "Pesanan dibatalkan!!!", 'message' => "Transaksi " .$value->transaksi_no_tracking . " ", 'tipe' => 'detail_transaksi', 'content' => array("transaksi_customer_cabang_id " => $value->transaksi_customer_cabang_id , "no_tracking" => $value->transaksi_no_tracking)));
          }

       }

       //data transaksi batal customer kanvas
      $DataTransCanvas = $this->mymodel->withquery("select * from transaksi_customer_canvas  where 
                 transaksi_tanggal  < '".$tanggal_skrg."' AND transaksi_status IN('1, 8')","result");
      
      foreach($DataTransCanvas as $key => $value) {
        //update status setiap transaksi dengan status 1 dan 8
        $databukti = array(
         "transaksi_status" => 5,
         "transaksi_alasanbatal" => 'Transaksi dibatalkan sistem');

        $this->mymodel->update('transaksi_customer_canvas',$databukti,'transaksi_customer_canvas_id ',
         $value->transaksi_customer_canvas_id);

        /*$balikstokKan = $this->mymodel->withquery("select * from transaksi_detail where 
           transaksi_jenis = '1' and
           transaksi_id = '".$value->transaksi_customer_canvas_id."'","result");
        foreach($balikstokKan as $d){
          $produkpercanvas = $this->mymodel->withquery("select * from produk_per_canvas  where 
             produk_per_canvas_produkid = '".$d->transaksi_detail_product_id."' and
             produk_per_canvas_canvasid = '".$value->transaksi_customer_canvas_id."'","row");
            
          $updatestokkan = array("produk_per_canvas_stok" => intval($produkpercanvas->produk_per_canvas_stok) + intval($d->transaksi_detail_qty));
          
          $this->mymodel->update('produk_per_canvas',$updatestokkan,
               'produk_per_canvasid', $produkpercanvas->produk_per_canvasid );
        }*/

        $cekfcmcustomer = $this->mymodel->getbywhere("customer", "customer_id", $value->transaksi_customer_id, "row");

        if (!empty($cekfcmcustomer->customer_fcm_id)) {
          $this->send_notif("Pesanan dibatalkan!!!", "Transaksi anda " . $value->transaksi_no_tracking . " ", $cekfcmcustomer->customer_fcm_id, array('title' => "Pesanan dibatalkan!!!", 'message' => "Transaksi anda " .$value->transaksi_no_tracking . " ", 'tipe' => 'detail_transaksi', 'content' => array("transaksi_customer_canvas_id " => $value->transaksi_customer_canvas_id , "no_tracking" => $value->transaksi_no_tracking)));
        }
        $cekfcmkanvas = $this->mymodel->getbywhere("kanvas", "kanvas_id", $value->transaksi_canvas_id, "row");

        if (!empty($cekfcmkanvas->kanvas_fcm_id)) {
          $this->send_notif("Pesanan dibatalkan!!!", "Transaksi " . $value->transaksi_no_tracking . " ", $cekfcmkanvas->kanvas_fcm_id, array('title' => "Pesanan dibatalkan!!!", 'message' => "Transaksi " .$value->transaksi_no_tracking . " ", 'tipe' => 'detail_transaksi', 'content' => array("transaksi_customer_canvas_id " => $value->transaksi_customer_canvas_id , "no_tracking" => $value->transaksi_no_tracking)));
        }
      }
      
    }

  public function send_notif($title, $desc, $id_fcm, $data)
  {
      
      $Msg = array(
          'body' => $desc,
          'title' => $title
      );

      $fcmFields = array(
      'to' => $id_fcm,
      'notification' => $Msg,
      'data'=>$data
    );
    $headers = array(
      'Authorization: key=' . API_ACCESS_KEY,
      'Content-Type: application/json'
    );
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
    $result = curl_exec($ch );
    curl_close( $ch );

    $cek_respon = explode(',',$result);
    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
    
      //echo $result."\n\n";
  }

}
?>
