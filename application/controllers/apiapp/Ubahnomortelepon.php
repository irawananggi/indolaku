<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Ubahnomortelepon extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {

        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   
	    if ($token != '') {
		  $customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
		  if (!empty($customer)) {
			  if($customer->customer_telp == $this->input->post('telp_lama')){
				  
				  $data = array( "customer_telp" => $this->input->post('telp_baru'));
				  $this->mymodel->update('customer',$data,'customer_token',$token);
				  $customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
				  $status = "200";
				  $msg = array('status' => 1, 'message'=>'Berhasil Simpan Data' ,'data' => $customer);
			  }else{
				  
				  $status = "200";
				  $msg = array('status' => 0, 'message'=>'Nomor Telepon Anda Salah' ,'data' => new stdClass());
			  }
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
		  }
		  $this->response($msg,$status);
	   }else {
		 $status = "401";
		 $data = array();
		 $msg = array('status' => 0, 'message'=>'Token anda kosong');
		 $this->response($msg,$status);
	   }     
   }

   
	  
  }
  function send_wa($no,$kode){
		  $userkey = 'dc85ab108a7c';
		  $passkey = '5s3qrrnyg5';
		  $telepon = $no;
		  $message = '*Indolaku* Kode OTP Anda : '.$kode;
		  $url = 'https://gsm.zenziva.net/api/sendWA/';
		  $curlHandle = curl_init();
		  curl_setopt($curlHandle, CURLOPT_URL, $url);
		  curl_setopt($curlHandle, CURLOPT_HEADER, 0);
		  curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		  curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
		  curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
		  curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
		  curl_setopt($curlHandle, CURLOPT_POST, 1);
		  curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
			  'userkey' => $userkey,
			  'passkey' => $passkey,
			  'nohp' => $telepon,
			  'pesan' => $message
		  ));
		  $results = json_decode(curl_exec($curlHandle), true);
		  curl_close($curlHandle);
	}
?>
