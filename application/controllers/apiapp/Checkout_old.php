<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Checkout extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {

        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   
	    if ($token != '') {
		  $customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
		  /*cek($customer);
		  die();*/
		  if (!empty($customer)) {
			  $produkarr = explode(',',$this->input->post('produk'));
			  $qtyarr = explode(',',$this->input->post('qty'));
			  $hrgarr = explode(',',$this->input->post('harga'));
			  $sisastokcabang = [];
			  $idstokcabang = [];
			  $sisastokcanvas = [];
			  $idstokcanvas = [];
			  $cabangterdekat;
			  $kanvasterdekat;
			  $cabangterdekatarr = $this->mymodel->withquery("select *, ( 6371 * acos( cos( radians(".$customer->customer_latitude.") ) * cos( radians( c.cabang_latitude ) ) * 
	cos( radians( c.cabang_longitude ) - radians(".$customer->customer_longitude.") ) + sin( radians(".$customer->customer_latitude.") ) * 
	sin( radians( c.cabang_latitude ) ) ) ) as jarak from cabang c where c.cabang_status = 1
	ORDER BY jarak limit 10","result");
	
			  foreach($cabangterdekatarr as $cbt){
				  $ada = true;
				  for($i = 0; $i < count($produkarr); $i++){
					  $idproduk = $produkarr[$i];
					  $qty = $qtyarr[$i];
					  $cekstok = $this->mymodel->withquery("select * from produk_per_cabang where produk_per_cabang_produk_id = '".$idproduk."' and produk_per_cabang_stok >= '".$qty."' and produk_per_cabang_cabang_id = '".$cbt->cabang_id."'","row");
					  $sisastokcabang[] = $cekstok->produk_per_cabang_stok - $qty;
					  $idstokcabang[] = $cekstok->produk_per_cabang_id;
					  if(empty($cekstok)){
						  $ada = false;
					  }
				  }
				  if($ada){
					  $cabangterdekat =  $cbt;
					  break;
				  }
			  }


			$kanvasterdekatarr = $this->mymodel->withquery("select *, 6371 * acos( cos( radians(".$customer->customer_latitude.") ) * cos( radians( c.kanvas_latitude ) ) * 
	cos( radians( c.kanvas_longitude ) - radians(".$customer->customer_longitude.") ) + sin( radians(".$customer->customer_latitude.") ) * 
	sin( radians( c.kanvas_latitude ) ) )  AS jarak from kanvas c  where c.kanvas_status = 1
				ORDER BY jarak limit 10","result");
	
			  foreach($kanvasterdekatarr as $knvs){
				  $ada = true;
				  for($i = 0; $i < count($produkarr); $i++){
					  $idproduk = $produkarr[$i];
					  $qty = $qtyarr[$i];
					  $cekstok = $this->mymodel->withquery("select * from produk_per_canvas where produk_per_canvas_produkid = '".$idproduk."' and produk_per_canvas_stok >= '".$qty."' and produk_per_canvas_canvasid = '".$knvs->kanvas_id."'","row");
					  $sisastokcanvas[] = $cekstok->produk_per_canvas_stok - $qty; 
					  $idstokcanvas[] = $cekstok->produk_per_canvasid;
					   if(empty($cekstok)){
						  $ada = false;
					  }
				  }
				  if($ada){
					  $kanvasterdekat =  $knvs;
					  break;
				  }
			  }
			  
			  
			  if(empty($kanvasterdekat) && empty($cabangterdekat)){
				  $status = "200";
				  $msg = array('status' => 0, 'message'=>'Stok Tidak ada', 'data' => new stdClass());
			  }else{
				  $transcabang = array('transaksi_customer_cabang_id' => 0
				  , 'transaksi_customer_id' => $customer->customer_id
				  , 'transaksi_cabang_id' => $cabangterdekat->cabang_id
				  , 'transaksi_note' => $customer->customer_alamat_note
				  , 'transaksi_kurir' => 0
				  , 'transaksi_longitude' =>$customer->customer_longitude
				  , 'transaksi_latitude' =>$customer->customer_latitude
				  , 'transaksi_alamat' =>$customer->customer_alamat
				  , 'transaksi_total' => $this->input->post('total')
				  , 'transaksi_status' => 1
				  , 'transaksi_ongkir' => $this->input->post('ongkir')
				  , 'transaksi_tanggal' => date('Y-m-d h:i:s')
				  , 'transaksi_catatan' => $this->input->post('catatan'));
				  
				  
				  $transkanvas = array('transaksi_customer_canvas_id' => 0
				  , 'transaksi_customer_id' => $customer->customer_id
				  , 'transaksi_canvas_id' => $kanvasterdekat->kanvas_id
				  , 'transaksi_note' => $customer->customer_alamat_note
				  , 'transaksi_longitude' =>$customer->customer_longitude
				  , 'transaksi_latitude' =>$customer->customer_latitude
				  , 'transaksi_alamat' =>$customer->customer_alamat
				  , 'transaksi_total' => $this->input->post('total')
				  , 'transaksi_status' => 1
				  , 'transaksi_ongkir' => $this->input->post('ongkir')
				  , 'transaksi_tanggal' => date('Y-m-d h:i:s')
				  , 'transaksi_catatan' => $this->input->post('catatan'));
				  if(!empty($kanvasterdekat) && empty($cabangterdekat)){
					  $this->mymodel->insert("transaksi_customer_canvas", $transkanvas);
					  $dtranskanvas = $this->mymodel->withquery("select * from transaksi_customer_canvas tcc, kanvas c, customer cs, transaksi_status ts where tcc.transaksi_customer_id = '".$customer->customer_id."' and cs.customer_id = '".$customer->customer_id."' and tcc.transaksi_canvas_id = '".$kanvasterdekat->kanvas_id."' and c.kanvas_id ='".$kanvasterdekat->kanvas_id."' and tcc.transaksi_status = ts.transaksi_status_id order by transaksi_customer_canvas_id desc","row");
						  
					  for($i = 0; $i < count($produkarr); $i++){
						  $td = array("transaksi_detail_id"=>0,
						  "transaksi_detail_product_id" => $produkarr[$i],
						  "transaksi_detail_harga" => $hrgarr[$i],
						  "transaksi_detail_qty" => $qtyarr[$i],
						  "transaksi_detail_total" => ($hrgarr[$i] * $qtyarr[$i]),
						  "transaksi_jenis"=>1,
						  "transaksi_id"=>$dtranskanvas->transaksi_customer_canvas_id,
						  "transaksi_customer_id"=>$customer->customer_id);
						  $this->mymodel->insert("transaksi_detail",$td);
						  
						  $datastokcanvasupdate = array("produk_per_canvas_stok" => $sisastokcanvas[$i]);
						  $this->mymodel->update('produk_per_canvas',$datastokcanvasupdate,'produk_per_canvasid',$idstokcanvas[$i]);
					  }
					  $status = "200";
					  $msg = array('status' => 1, 'message'=>'Transaksi Berhasil' ,'data' => $dtranskanvas);
				  }else if(empty($kanvasterdekat) && !empty($cabangterdekat)){
					  $this->mymodel->insert("transaksi_customer_cabang", $transcabang);
					  $dtranscabang = $this->mymodel->withquery("select * from transaksi_customer_cabang tcc, cabang c, customer cs, transaksi_status ts where tcc.transaksi_customer_id = '".$customer->customer_id."' and cs.customer_id = '".$customer->customer_id."' and tcc.transaksi_cabang_id = '".$cabangterdekat->cabang_id."' and c.cabang_id ='".$cabangterdekat->cabang_id."' and tcc.transaksi_status = ts.transaksi_status_id order by  transaksi_customer_cabang_id desc","row");
					  for($i = 0; $i < count($produkarr); $i++){
						  $td = array("transaksi_detail_id"=>0,
						  "transaksi_detail_product_id" => $produkarr[$i],
						  "transaksi_detail_harga" => $hrgarr[$i],
						  "transaksi_detail_qty" => $qtyarr[$i],
						  "transaksi_detail_total" => ($hrgarr[$i] * $qtyarr[$i]),
						  "transaksi_jenis"=>2,
						  "transaksi_id"=>$dtranscabang->transaksi_customer_cabang_id,
						  "transaksi_customer_id"=>$customer->customer_id  );
						  $this->mymodel->insert("transaksi_detail",$td);
						   
						  $datastokcabangupdate = array("produk_per_cabang_stok" => $sisastokcabang[$i]);
						  $this->mymodel->update('produk_per_cabang',$datastokcabangupdate,'produk_per_cabang_id',$idstokcabang[$i]);
					  }
					  $status = "200";
					  $msg = array('status' => 1, 'message'=>'Transaksi Berhasil' ,'data' => $dtranscabang);
				  }else{
					  if($cabangterdekat->jarak >= $kanvasterdekat->jarak){
						  $this->mymodel->insert("transaksi_customer_canvas", $transkanvas);
						  $dtranskanvas = $this->mymodel->withquery("select * from transaksi_customer_canvas tcc, kanvas c, customer cs, transaksi_status ts where tcc.transaksi_customer_id = '".$customer->customer_id."' and cs.customer_id = '".$customer->customer_id."' and tcc.transaksi_canvas_id = '".$kanvasterdekat->kanvas_id."' and c.kanvas_id ='".$kanvasterdekat->kanvas_id."' and tcc.transaksi_status = ts.transaksi_status_id order by transaksi_customer_canvas_id desc","row");
						  for($i = 0; $i < count($produkarr); $i++){
							  $td = array("transaksi_detail_id"=>0,
							  "transaksi_detail_product_id" => $produkarr[$i],
							  "transaksi_detail_harga" => $hrgarr[$i],
							  "transaksi_detail_qty" => $qtyarr[$i],
							  "transaksi_detail_total" => ($hrgarr[$i] * $qtyarr[$i]),
							  "transaksi_jenis"=>1,
							  "transaksi_id"=>$dtranskanvas->transaksi_customer_canvas_id,
						      "transaksi_customer_id"=>$customer->customer_id );
							  $this->mymodel->insert("transaksi_detail",$td);
							  
							  $datastokcanvasupdate = array("produk_per_canvas_stok" => $sisastokcanvas[$i]);
						      $this->mymodel->update('produk_per_canvas',$datastokcanvasupdate,'produk_per_canvasid',$idstokcanvas[$i]);
						  }
						  $status = "200";
					      $msg = array('status' => 1, 'message'=>'Transaksi Berhasil' ,'data' => $dtranskanvas);
					  }else{
						  
						  $this->mymodel->insert("transaksi_customer_cabang", $transcabang);
						  
						  $dtranscabang = $this->mymodel->withquery("select * from transaksi_customer_cabang tcc, cabang c, customer cs, transaksi_status ts where tcc.transaksi_customer_id = '".$customer->customer_id."' and cs.customer_id = '".$customer->customer_id."' and tcc.transaksi_cabang_id = '".$cabangterdekat->cabang_id."' and c.cabang_id ='".$cabangterdekat->cabang_id."' and tcc.transaksi_status = ts.transaksi_status_id order by  transaksi_customer_cabang_id desc","row");
						  for($i = 0; $i < count($produkarr); $i++){
							  $td = array("transaksi_detail_id"=>0,
							  "transaksi_detail_product_id" => $produkarr[$i],
							  "transaksi_detail_harga" => $hrgarr[$i],
							  "transaksi_detail_qty" => $qtyarr[$i],
							  "transaksi_detail_total" => ($hrgarr[$i] * $qtyarr[$i]),
							  "transaksi_jenis"=>2,
							  "transaksi_id"=>$dtranscabang->transaksi_customer_cabang_id,
						      "transaksi_customer_id"=>$customer->customer_id  );
							  $this->mymodel->insert("transaksi_detail",$td);
						      $datastokcabangupdate = array("produk_per_cabang_stok" => $sisastokcabang[$i]);
							  $this->mymodel->update('produk_per_cabang',$datastokcabangupdate,'produk_per_cabang_id',$idstokcabang[$i]);
							  
						  }
						  
						  $status = "200";
						  $msg = array('status' => 1, 'message'=>'Transaksi Berhasil' ,'data' => $dtranscabang);
					  }
					   $this->mymodel->delete("keranjang","keranjang_customer",$customer->customer_id);
				  }
			  }
				 
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
		  }
		  $this->response($msg,$status);
	   }else {
		 $status = "401";
		 $data = array();
		 $msg = array('status' => 0, 'message'=>'Token anda kosong');
		 $this->response($msg,$status);
	   }     
      }
	  
  }
?>
