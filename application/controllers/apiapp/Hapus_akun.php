<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
class Hapus_akun extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['token']))
        $token =  $headers['token'];

      if ($token != '') {
          $data = $this->mymodel->getbywhere('customer','token',$token,"row");
          if (isset($data)) {
            $customer_id = $this->input->post('customer_id');
            $data = $this->mymodel->withquery("select * from customer WHERE customer_id='$customer_id'","result");
            if(count($data)){
              $data = array(
                "is_active" =>2
              );

              $this->mymodel->update('customer',$data,'customer_id',$customer_id);
              $msg = array('status' => 1, 'message'=>'Berhasil Update data' ,'data'=>array());

            }else {
              $msg = array('status' => 0, 'message'=>'Data tidak bisa dihapus' ,'data'=>array());
            }
          }else {
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
          }

          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg);
      }
    }
}