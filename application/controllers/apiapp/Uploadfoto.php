<?php

header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Uploadfoto extends REST_Controller {

   
  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {
      
        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   

      if ($token != '') {
		  $customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
		  if (!empty($customer)) {
			  
			    $uploaddir = './upload/avatar/';
				$img = explode('.', $_FILES['img']['name']);
				$extension = end($img);
				$file_name =  md5(date('Y-m-d H:i:s').$_FILES['img']['name']).".".$extension;
				$uploadfile = $uploaddir.$file_name;
				$status = 0;

				if (move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile)) {
					
				   $data = array("customer_foto" => $file_name);
				      $this->mymodel->update('customer',$data,'customer_token',$token);
					
				   $msg = array('status' => 1, 'message'=>'Foto Berhasil di update','link'=>base_url("/upload/avatar/".$file_name));
				}else{
				  $msg = array('status' => 0, 'message'=>'Foto Tidak Berhasil di update','link'=>'');
				}
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ','link'=>'');
		  }
          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong','link'=>'');
        $this->response($msg);
      }

      $this->response($msg);
    }
}
?>
