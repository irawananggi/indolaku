<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');
define( 'API_ACCESS_KEY', 'AAAAWyu_Puo:APA91bEbUprYzMYA4yV_RVzdsdQozDOEG1KcG-R1IVBvYn13UraC0Weu20vE37c-e1BDME9P-u9ap-70OERDxg7gzrVSGmWq__RRAnefzz02z-WyA92veVFm_Uj6GrNr236MvldG7wIe' );
//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Pesan extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {

        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   
	    if ($token != '') {
		  $customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
		  if (!empty($customer)) {
		  	$lat = $this->input->post('latitude');
		  	$long = $this->input->post('longitude');
			  $produkarr = explode(',',$this->input->post('produk'));
			  $qtyarr = explode(',',$this->input->post('qty'));
			  $hrgarr = explode(',',$this->input->post('harga'));
			  $listproduk = [];
			  //$liststokproduk = [];
			  $sisastokcabang = [];
			  $idstokcabang = [];
			  $sisastokcanvas = [];
			  $idstokcanvas = [];
			  $cabangterdekat;
			  $kanvasterdekat;
			  $cabangterdekatarr = $this->mymodel->withquery("select *, ( 6371 * acos( cos( radians(".$lat.") ) * cos( radians( c.cabang_latitude ) ) * 
	cos( radians( c.cabang_longitude ) - radians(".$long.") ) + sin( radians(".$lat.") ) * 
	sin( radians( c.cabang_latitude ) ) ) ) as jarak from cabang c where c.cabang_status = 1
	ORDER BY jarak DESC limit 10","result");
			  foreach($cabangterdekatarr as $cbt){
				  $ada = true;
				  for($i = 0; $i < count($produkarr); $i++){
					  $idproduk = $produkarr[$i];
					  $qty = $qtyarr[$i];
					  $cekstok = $this->mymodel->withquery("select * from produk_per_cabang where produk_per_cabang_produk_id = '".$idproduk."' and produk_per_cabang_stok >= '".$qty."' and produk_per_cabang_cabang_id = '".$cbt->cabang_id."'","row");
				  
					  $sisastokcabang[] = $cekstok->produk_per_cabang_stok/* - $qty*/;
					  $idstokcabang[] = $cekstok->produk_per_cabang_id;
/*
					  if(empty($cekstok)){
						  $ada = false;
					  }*/


				  }
				  /*if($ada){
						  $cabangterdekat =  $cbt;
					  }*/
			    $cabangterdekat =  $cbt;
			  }

			$kanvasterdekatarr = $this->mymodel->withquery("select *, 6371 * acos( cos( radians(".$lat.") ) * cos( radians( c.kanvas_latitude ) ) * 
	cos( radians( c.kanvas_longitude ) - radians(".$long.") ) + sin( radians(".$lat.") ) * 
	sin( radians( c.kanvas_latitude ) ) )  AS jarak from kanvas c  where c.kanvas_status = 1
				ORDER BY jarak DESC limit 10","result");
	
			  foreach($kanvasterdekatarr as $knvs){
				  $ada = true;
				  for($i = 0; $i < count($produkarr); $i++){
					  $idproduk = $produkarr[$i];
					  $qty = $qtyarr[$i];
					  $cekstok = $this->mymodel->withquery("select * from produk_per_canvas where produk_per_canvas_produkid = '".$idproduk."' and produk_per_canvas_stok >= '".$qty."' and produk_per_canvas_canvasid = '".$knvs->kanvas_id."'","row");
					  $sisastokcanvas[] = $cekstok->produk_per_canvas_stok/* - $qty*/; 
					  $idstokcanvas[] = $cekstok->produk_per_canvasid;
					   /*if(empty($cekstok)){
						  $ada = false;
					  }*/
				  }
				  /*if($ada){
					  $kanvasterdekat =  $knvs;
					  break;
				  }*/
				  $kanvasterdekat =  $knvs;
			  }
			  
				//cek data cabang
				if(!empty($cabangterdekat)){

				  $data_list_produk_cabang = $this->mymodel->withquery("select * from keranjang, produk where keranjang_customer = '".$customer->customer_id."' and produk_id = keranjang_produk","result");
				  $totProdukCabang = 0;
				  $stokProdukCabang = array();
				  foreach($data_list_produk_cabang as $key => $value){
				  	$cekstokprodukcabang = $this->mymodel->withquery("select * from produk_per_cabang where produk_per_cabang_produk_id = '".$value->keranjang_produk."'   and produk_per_cabang_cabang_id = '".$cabangterdekat->cabang_id."' ","row");
				  	$totalprodukcabang = $this->mymodel->withquery("select * from produk_per_cabang where produk_per_cabang_produk_id = '".$value->keranjang_produk."'   and produk_per_cabang_cabang_id = '".$cabangterdekat->cabang_id."' ","result");
				  	$value->produk_id = $value->keranjang_produk;
				  	$value->produk_foto = base_url('uploads/produk/'.$value->produk_foto);
				  	
				  	$value->stok = (($cekstokprodukcabang->produk_per_cabang_stok ==NULL)?'kosong':(($value->keranjang_qty > $cekstokprodukcabang->produk_per_cabang_stok)?''.$cekstokprodukcabang->produk_per_cabang_stok - $value->keranjang_qty.'':$cekstokprodukcabang->produk_per_cabang_stok));
				  	$totProdukCabang += count($totalprodukcabang);

						$stokProdukCabang[]= ($cekstokprodukcabang->produk_per_cabang_stok - $value->keranjang_qty);
				  }
				}

			  //cek data canvas
				if(!empty($kanvasterdekat)){
					
					$data_list_produk_canvas = $this->mymodel->withquery("select * from keranjang, produk where keranjang_customer = '".$customer->customer_id."' and produk_id = keranjang_produk","result");
					$totProdukCanvas = 0;
					$stokProdukCanvas = array();
					foreach($data_list_produk_canvas as $key => $value){
						$cekstokprodukcanvas = $this->mymodel->withquery("select * from produk_per_canvas where produk_per_canvas_produkid = '".$value->keranjang_produk."'   and produk_per_canvas_canvasid = '".$kanvasterdekat->kanvas_id."' ","row");
						$totalprodukcanvas = $this->mymodel->withquery("select * from produk_per_canvas where produk_per_canvas_produkid = '".$value->keranjang_produk."'   and produk_per_canvas_canvasid = '".$kanvasterdekat->kanvas_id."' ","result");

						$value->produk_id = $value->keranjang_produk;
				  	    $value->produk_foto = base_url('uploads/produk/'.$value->produk_foto);
						$value->stok = (($cekstokprodukcanvas->produk_per_canvas_stok ==NULL)?'kosong':(($value->keranjang_qty > $cekstokprodukcanvas->produk_per_canvas_stok)?''.$cekstokprodukcanvas->produk_per_canvas_stok - $value->keranjang_qty.'' : $cekstokprodukcanvas->produk_per_canvas_stok));
						$totProdukCanvas += count($totalprodukcanvas);
						$stokProdukCanvas[]= ($cekstokprodukcanvas->produk_per_canvas_stok - $value->keranjang_qty);

					}
					
				}
				
			$input = preg_quote('-', '~');

			$results_array_cabang = preg_grep('~' . $input . '~', $stokProdukCabang);
			$results_array_kanvas = preg_grep('~' . $input . '~', $stokProdukCanvas);
		
		
			$data_cabang_null =
				array(
					"transaksi_customer_cabang_id"=> "",
	        "transaksi_customer_id"=> "",
	        "transaksi_cabang_id"=> "",
	        "transaksi_note"=> "",
	        "transaksi_kurir"=> "",
	        "transaksi_longitude"=> "",
	        "transaksi_latitude"=> "",
	        "transaksi_alamat"=> "",
	        "transaksi_total"=> "",
	        "transaksi_status"=> "",
	        "transaksi_ongkir"=> "",
	        "transaksi_tanggal"=> "",
	        "transaksi_bukti_trf"=> "",
	        "transaksi_an"=> "",
	        "transaksi_norek"=> "",
	        "transaksi_jenisbank"=> "",
	        "transaksi_totalbayar"=> "",
	        "transaksi_alasanbatal"=> "",
	        "transaksi_catatan"=> "",
	        "cabang_id"=> "",
	        "cabang_nama"=> "",
	        "cabang_username"=> "",
	        "cabang_password"=> "",
	        "cabang_alamat"=> "",
	        "cabang_telp"=> "",
	        "cabang_email"=> "",
	        "cabang_status"=> "",
	        "cabang_pusat"=> "",
	        "cabang_pj"=> "",
	        "cabang_pj_telp"=> "",
	        "cabang_npwp"=> "",
	        "cabang_longitude"=> "",
	        "cabang_latitude"=> "",
	        "cabang_token"=> "",
	        "customer_id"=> "",
	        "customer_nama"=> "",
	        "customer_telp"=> "",
	        "customer_latitude"=> "",
	        "customer_longitude"=> "",
	        "customer_token"=> "",
	        "customer_otp"=> "",
	        "customer_verified"=> "",
	        "customer_nik"=> "",
	        "customer_npwp"=> "",
	        "customer_alamat"=> "",
	        "customer_alamat_note"=> "",
	        "customer_survey"=> "",
	        "customer_foto"=> "",
	        "transaksi_status_id"=> "",
	        "transaksi_status_keterangan"=> "",
	        "transaksi_jenis"=> ""
	      );
			$data_kanvas_null =
				array(
					"transaksi_customer_canvas_id"=> "",
	        "transaksi_customer_id"=> "",
	        "transaksi_canvas_id"=> "",
	        "transaksi_note"=> "",
	        "transaksi_total"=> "",
	        "transaksi_longitude"=> "",
	        "transaksi_latitude"=> "",
	        "transaksi_status"=> "",
	        "transaksi_ongkir"=> "",
	        "transaksi_tanggal"=> "",
	        "transaksi_alamat"=> "",
	        "transaksi_bukti_trf"=> "",
	        "transaksi_an"=> "",
	        "transaksi_norek"=> "",
	        "transaksi_jenisbank"=> "",
	        "transaksi_totalbayar"=> "",
	        "transaksi_alasanbatal"=> "",
	        "transaksi_catatan"=> "",
	        "kanvas_id"=> "",
	        "kanvas_nama"=> "",
	        "kanvas_jenis_kelamin"=> "",
	        "kanvas_tgl_lahir"=> "",
	        "kanvas_nik"=> "",
	        "kanvas_npwp"=> "",
	        "kanvas_telp"=> "",
	        "kanvas_email"=> "",
	        "kanvas_password"=> "",
	        "kanvas_provinsi"=> "",
	        "kanvas_kota"=> "",
	        "kanvas_kec"=> "",
	        "kanvas_kelurahan"=> "",
	        "kanvas_rt"=> "",
	        "kanvas_rw"=> "",
	        "kanvas_alamat"=> "",
	        "kanvas_cabang"=> "",
	        "kanvas_foto"=> "",
	        "kanvas_status"=> "",
	        "kanvas_longitude"=> "",
	        "kanvas_latitude"=> "",
	        "kanvas_token"=> "",
	        "customer_id"=> "",
	        "customer_nama"=> "",
	        "customer_telp"=> "",
	        "customer_latitude"=> "",
	        "customer_longitude"=> "",
	        "customer_token"=> "",
	        "customer_otp"=> "",
	        "customer_verified"=> "",
	        "customer_nik"=> "",
	        "customer_npwp"=> "",
	        "customer_alamat"=> "",
	        "customer_alamat_note"=> "",
	        "customer_survey"=> "",
	        "customer_foto"=> "",
	        "transaksi_status_id"=> "",
	        "transaksi_status_keterangan"=> "",
	        "transaksi_jenis"=> ""
		      );
			$no_tracking = 'TRS-'.date('dmYHi').''.$customer->customer_id;
			  if(empty($kanvasterdekat) && empty($cabangterdekat)){
				  $status = "200";
				  $msg = array('status' => 0, 'message'=>'Stok Tidak ada', 'data' =>array());
			  }else{
				  $transcabang = array('transaksi_customer_cabang_id' => 0
				  , 'transaksi_customer_id' => $customer->customer_id
				  , 'transaksi_cabang_id' => $cabangterdekat->cabang_id
				  , 'transaksi_no_tracking' => $no_tracking
				  , 'transaksi_note' => $this->input->post('note')
				  , 'transaksi_kurir' => 0
				  , 'transaksi_longitude' =>$long
				  , 'transaksi_latitude' =>$lat
				  , 'transaksi_alamat' =>$this->input->post('alamat')
				  , 'transaksi_total' => $this->input->post('total')
				  , 'transaksi_status' => 7
				  , 'transaksi_ongkir' => $this->input->post('ongkir')
				  , 'transaksi_tanggal' => date('Y-m-d H:i:s')
				  , 'transaksi_catatan' => $this->input->post('catatan'));
				  
				  
				  $transkanvas = array('transaksi_customer_canvas_id' => 0
				  , 'transaksi_customer_id' => $customer->customer_id
				  , 'transaksi_canvas_id' => $kanvasterdekat->kanvas_id
				  , 'transaksi_no_tracking' => $no_tracking
				  , 'transaksi_note' => $this->input->post('note')
				  , 'transaksi_longitude' =>$long
				  , 'transaksi_latitude' =>$lat
				  , 'transaksi_alamat' =>$this->input->post('alamat')
				  , 'transaksi_total' => $this->input->post('total')
				  , 'transaksi_status' => 7
				  , 'transaksi_ongkir' => $this->input->post('ongkir')
				  , 'transaksi_tanggal' => date('Y-m-d H:i:s')
				  , 'transaksi_catatan' => $this->input->post('catatan'));
				  if(!empty($kanvasterdekat) && empty($cabangterdekat)){
					  	
						  if($totProdukCanvas < count($data_list_produk_canvas)){
						  	$status = "200";
							  $msg = array('status' => 0, 'message'=>'Barang Tidak tersedia','data'=>$data_list_produk_canvas);
						  }elseif($results_array_kanvas){
						  	$status = "200";
							  $msg = array('status' => 0, 'message'=>'Stok Tidak tersedia','data'=>$data_list_produk_canvas);
						  }else{
							  $this->mymodel->insert("transaksi_customer_canvas", $transkanvas);
							  $dtranskanvas = $this->mymodel->withquery("select * from transaksi_customer_canvas tcc, kanvas c, customer cs, transaksi_status ts where tcc.transaksi_customer_id = '".$customer->customer_id."' and cs.customer_id = '".$customer->customer_id."' and tcc.transaksi_canvas_id = '".$kanvasterdekat->kanvas_id."' and c.kanvas_id ='".$kanvasterdekat->kanvas_id."' and tcc.transaksi_status = ts.transaksi_status_id order by transaksi_customer_canvas_id desc","row");
								  
							  for($i = 0; $i < count($produkarr); $i++){
								  $td = array("transaksi_detail_id"=>0,
								  "transaksi_detail_product_id" => $produkarr[$i],
								  "transaksi_detail_harga" => $hrgarr[$i],
								  "transaksi_detail_qty" => $qtyarr[$i],
								  "transaksi_detail_total" => ($hrgarr[$i] * $qtyarr[$i]),
								  "transaksi_jenis"=>1,
								  "transaksi_id"=>$dtranskanvas->transaksi_customer_canvas_id,
								  "transaksi_customer_id"=>$customer->customer_id);
								  $this->mymodel->insert("transaksi_detail",$td);
								  
								  $datastokcanvasupdate = array("produk_per_canvas_stok" => $sisastokcanvas[$i]);
								  $this->mymodel->update('produk_per_canvas',$datastokcanvasupdate,'produk_per_canvasid',$idstokcanvas[$i]);
							  }
		  					$data_jenis = $this->mymodel->getbywhere('transaksi_detail','transaksi_id',$dtranskanvas->transaksi_customer_canvas_id,"row");
		  					$dtranskanvas->transaksi_jenis = $data_jenis->transaksi_jenis;

							  $status = "200";
							  //$msg = array('status' => 1, 'message'=>'Transaksi Berhasil' ,'data' => $dtranskanvas);
							  $msg = array('status' => 1, 'message'=>'Transaksi Berhasil' ,'data_cabang' => $data_cabang_null,'data_kanvas'=>$dtranskanvas);

							  //send notif kanvas
								$cekfcmkanvas = $this->mymodel->getbywhere("kanvas", "kanvas_id", $kanvasterdekat->kanvas_id, "row");
								if (!empty($cekfcmkanvas->kanvas_fcm_id)) {
								$this->send_notif("Pesanan Baru!!!", "Transaksi anda " . $dtranskanvas->transaksi_no_tracking . " ", $cekfcmkanvas->kanvas_fcm_id, array('title' => "Pesanan Baru!!!", 'message' => "Transaksi anda " .$dtranskanvas->transaksi_no_tracking . " ", 'tipe' => 'detail_transaksi', 'content' => array("transaksi_customer_canvas_id " => $dtranskanvas->transaksi_customer_canvas_id , "no_tracking" => $dtranskanvas->transaksi_no_tracking)));
								}

							}
				  }else if(empty($kanvasterdekat) && !empty($cabangterdekat)){
					  if($totProdukCabang < count($data_list_produk_cabang)){
						  $status = "200";
						  $msg = array('status' => 0, 'message'=>'Barang Tidak tersedia','data'=>$data_list_produk_cabang);
					  }elseif($results_array_cabang){
					  	$status = "200";
						  $msg = array('status' => 0, 'message'=>'Stok Tidak tersedia','data'=>$data_list_produk_cabang);
					  }else{
						  	$this->mymodel->insert("transaksi_customer_cabang", $transcabang);
						  	$dtranscabang = $this->mymodel->withquery("select * from transaksi_customer_cabang tcc, cabang c, customer cs, transaksi_status ts where tcc.transaksi_customer_id = '".$customer->customer_id."' and cs.customer_id = '".$customer->customer_id."' and tcc.transaksi_cabang_id = '".$cabangterdekat->cabang_id."' and c.cabang_id ='".$cabangterdekat->cabang_id."' and tcc.transaksi_status = ts.transaksi_status_id order by  transaksi_customer_cabang_id desc","row");
							  for($i = 0; $i < count($produkarr); $i++){
								  $td = array("transaksi_detail_id"=>0,
								  "transaksi_detail_product_id" => $produkarr[$i],
								  "transaksi_detail_harga" => $hrgarr[$i],
								  "transaksi_detail_qty" => $qtyarr[$i],
								  "transaksi_detail_total" => ($hrgarr[$i] * $qtyarr[$i]),
								  "transaksi_jenis"=>2,
								  "transaksi_id"=>$dtranscabang->transaksi_customer_cabang_id,
								  "transaksi_customer_id"=>$customer->customer_id  );
								  $this->mymodel->insert("transaksi_detail",$td);
								   
								  $datastokcabangupdate = array("produk_per_cabang_stok" => $sisastokcabang[$i]);
								  $this->mymodel->update('produk_per_cabang',$datastokcabangupdate,'produk_per_cabang_id',$idstokcabang[$i]);
							  }

	  					$data_jenis = $this->mymodel->getbywhere('transaksi_detail','transaksi_id',$dtranscabang->transaksi_customer_cabang_id,"row");
	  					$dtranscabang->transaksi_jenis = $data_jenis->transaksi_jenis;
						  $status = "200";
						  //$msg = array('status' => 1, 'message'=>'Transaksi Berhasil' ,'data' => $dtranscabang);
							$msg = array('status' => 1, 'message'=>'Transaksi Berhasil' ,'data_cabang' => $dtranscabang,'data_kanvas'=>$data_kanvas_null);

							//send notif cabang
							$cekfcmcabang = $this->mymodel->getbywhere("cabang", "cabang_id", $cabangterdekat->cabang_id, "row");

							if (!empty($cekfcmcabang->cabang_fcm_id)) {
							$this->send_notif("Pesanan Baru!!!", "Transaksi anda " . $dtranscabang->transaksi_no_tracking . " ", $cekfcmcabang->cabang_fcm_id, array('title' => "Pesanan Baru!!!", 'message' => "Transaksi anda " .$dtranscabang->transaksi_no_tracking . " ", 'tipe' => 'detail_transaksi', 'content' => array("transaksi_customer_cabang_id " => $dtranscabang->transaksi_customer_cabang_id , "no_tracking" => $dtranscabang->transaksi_no_tracking)));
							}
						}
				  }else{
					  if($cabangterdekat->jarak >= $kanvasterdekat->jarak){
						  if($totProdukCanvas < count($data_list_produk_canvas)){
						  	$status = "200";
							  $msg = array('status' => 0, 'message'=>'Barang Tidak tersedia','data'=>$data_list_produk_canvas);
						  }else if($results_array_kanvas){
						  	$status = "200";
							  $msg = array('status' => 0, 'message'=>'Stok Tidak tersedia','data'=>$data_list_produk_canvas);
						  }else{
							  $this->mymodel->insert("transaksi_customer_canvas", $transkanvas);
							  $dtranskanvas = $this->mymodel->withquery("select * from transaksi_customer_canvas tcc, kanvas c, customer cs, transaksi_status ts where tcc.transaksi_customer_id = '".$customer->customer_id."' and cs.customer_id = '".$customer->customer_id."' and tcc.transaksi_canvas_id = '".$kanvasterdekat->kanvas_id."' and c.kanvas_id ='".$kanvasterdekat->kanvas_id."' and tcc.transaksi_status = ts.transaksi_status_id order by transaksi_customer_canvas_id desc","row");
							  
							  
                            for($i = 0; $i < count($produkarr); $i++){
								  $td = array("transaksi_detail_id"=>0,
								  "transaksi_detail_product_id" => $produkarr[$i],
								  "transaksi_detail_harga" => $hrgarr[$i],
								  "transaksi_detail_qty" => $qtyarr[$i],
								  "transaksi_detail_total" => ($hrgarr[$i] * $qtyarr[$i]),
								  "transaksi_jenis"=>1,
								  "transaksi_id"=>$dtranskanvas->transaksi_customer_canvas_id,
							      "transaksi_customer_id"=>$customer->customer_id );
								  $this->mymodel->insert("transaksi_detail",$td);
								  
								  $datastokcanvasupdate = array("produk_per_canvas_stok" => $sisastokcanvas[$i]);
							      $this->mymodel->update('produk_per_canvas',$datastokcanvasupdate,'produk_per_canvasid',$idstokcanvas[$i]);
							 }

	  					$data_jenis = $this->mymodel->getbywhere('transaksi_detail','transaksi_id',$dtranskanvas->transaksi_customer_canvas_id,"row");
	  					$dtranskanvas->transaksi_jenis = $data_jenis->transaksi_jenis;
						  $status = "200";
				      //$msg = array('status' => 1, 'message'=>'Transaksi Berhasil' ,'data' => $dtranskanvas);
						  
						  $msg = array('status' => 1, 'message'=>'Transaksi Berhasil' ,'data_cabang' => $data_cabang_null,'data_kanvas'=>$dtranskanvas);
						  //send notif
							$cekfcmkanvas = $this->mymodel->getbywhere("kanvas", "kanvas_id", $kanvasterdekat->kanvas_id, "row");
							if (!empty($cekfcmkanvas->kanvas_fcm_id)) {
							$this->send_notif("Pesanan Baru!!!", "Transaksi anda " . $dtranskanvas->transaksi_no_tracking . " ", $cekfcmkanvas->kanvas_fcm_id, array('title' => "Pesanan Baru!!!", 'message' => "Transaksi anda " .$dtranskanvas->transaksi_no_tracking . " ", 'tipe' => 'detail_transaksi', 'content' => array("transaksi_customer_canvas_id " => $dtranskanvas->transaksi_customer_canvas_id , "no_tracking" => $dtranskanvas->transaksi_no_tracking)));
							}
						  }
					  }else{
						  if($totProdukCabang < count($data_list_produk_cabang)){

							  $status = "200";
							  $msg = array('status' => 0, 'message'=>'Barang Tidak tersedia','data'=>$data_list_produk_cabang);
						  }elseif($results_array_cabang){
						  	$status = "200";
							  $msg = array('status' => 0, 'message'=>'Stok Tidak tersedia','data'=>$data_list_produk_cabang);
						  }else{

							  $this->mymodel->insert("transaksi_customer_cabang", $transcabang);
							  
							  $dtranscabang = $this->mymodel->withquery("select * from transaksi_customer_cabang tcc, cabang c, customer cs, transaksi_status ts where tcc.transaksi_customer_id = '".$customer->customer_id."' and cs.customer_id = '".$customer->customer_id."' and tcc.transaksi_cabang_id = '".$cabangterdekat->cabang_id."' and c.cabang_id ='".$cabangterdekat->cabang_id."' and tcc.transaksi_status = ts.transaksi_status_id order by  transaksi_customer_cabang_id desc","row");
							  for($i = 0; $i < count($produkarr); $i++){
								  $td = array("transaksi_detail_id"=>0,
								  "transaksi_detail_product_id" => $produkarr[$i],
								  "transaksi_detail_harga" => $hrgarr[$i],
								  "transaksi_detail_qty" => $qtyarr[$i],
								  "transaksi_detail_total" => ($hrgarr[$i] * $qtyarr[$i]),
								  "transaksi_jenis"=>2,
								  "transaksi_id"=>$dtranscabang->transaksi_customer_cabang_id,
							      "transaksi_customer_id"=>$customer->customer_id  );
								  $this->mymodel->insert("transaksi_detail",$td);
							      $datastokcabangupdate = array("produk_per_cabang_stok" => $sisastokcabang[$i]);
								  $this->mymodel->update('produk_per_cabang',$datastokcabangupdate,'produk_per_cabang_id',$idstokcabang[$i]);
								  
							  }
	  							
									$status = "200";
									  //$msg = array('status' => 1, 'message'=>'Transaksi Berhasil' ,'data' => $dtranscabang);
									  
									$msg = array('status' => 1, 'message'=>'Transaksi Berhasils' ,'data_cabang' => $dtranscabang,'data_kanvas'=>$data_kanvas_null);
								$data_jenis = $this->mymodel->getbywhere('transaksi_detail','transaksi_id',$dtranscabang->transaksi_customer_cabang_id,"row");
			  					$dtranscabang->transaksi_jenis = $data_jenis->transaksi_jenis;

								//send notif cabang
								$cekfcmcabang = $this->mymodel->getbywhere("cabang", "cabang_id", $cabangterdekat->cabang_id, "row");
								
								if (!empty($cekfcmcabang->cabang_fcm_id)) {
								$this->send_notif("Pesanan Baru!!!", "Transaksi anda " . $dtranscabang->transaksi_no_tracking . " ", $cekfcmcabang->cabang_fcm_id, array('title' => "Pesanan Baru!!!", 'message' => "Transaksi anda " .$dtranscabang->transaksi_no_tracking . " ", 'tipe' => 'detail_transaksi', 'content' => array("transaksi_customer_cabang_id " => $dtranscabang->transaksi_customer_cabang_id , "no_tracking" => $dtranscabang->transaksi_no_tracking)));
								}
	
						  }
					  }
					   $this->mymodel->delete("keranjangs","keranjang_customer",$customer->customer_id);
				  }
			  }
				 
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
		  }
		  $this->response($msg,$status);
	   }else {
		 $status = "401";
		 $data = array();
		 $msg = array('status' => 0, 'message'=>'Token anda kosong');
		 $this->response($msg,$status);
	   }     
      }
	  
	public function send_notif($title, $desc, $id_fcm, $data)
    {
        
        $Msg = array(
            'body' => $desc,
            'title' => $title
        );

        $fcmFields = array(
	      'to' => $id_fcm,
	      'notification' => $Msg,
	      'data'=>$data
	    );
	    $headers = array(
	      'Authorization: key=' . API_ACCESS_KEY,
	      'Content-Type: application/json'
	    );
	    $ch = curl_init();
	    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
	    curl_setopt( $ch,CURLOPT_POST, true );
	    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
	    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
	    $result = curl_exec($ch );
	    curl_close( $ch );

	    $cek_respon = explode(',',$result);
	    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
	    
        //echo $result."\n\n";
    }
  }
?>
