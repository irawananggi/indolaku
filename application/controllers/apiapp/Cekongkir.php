<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
class Cekongkir extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_post()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['x-token']))
        $token =  $headers['x-token'];
      if ($token != '') {
          $customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
          if (!empty($customer)) {
            $lat = $this->input->post('latitude');
            $long = $this->input->post('longitude');
            $cabangterdekatarr = $this->mymodel->withquery("select *, ( 6371 * acos( cos( radians(".$lat.") ) * cos( radians( c.cabang_latitude ) ) * 
  cos( radians( c.cabang_longitude ) - radians(".$long.") ) + sin( radians(".$lat.") ) * 
  sin( radians( c.cabang_latitude ) ) ) ) as jarak from cabang c where c.cabang_status = 1 and c.cabang_fcm_id IS NOT NULL 
  ORDER BY jarak DESC limit 10","result");
            foreach($cabangterdekatarr as $cbt){
              $cabangterdekat =  $cbt;
            }
            $kanvasterdekatarr = $this->mymodel->withquery("select *, 6371 * acos( cos( radians(".$lat.") ) * cos( radians( c.kanvas_latitude ) ) * 
  cos( radians( c.kanvas_longitude ) - radians(".$long.") ) + sin( radians(".$lat.") ) * 
  sin( radians( c.kanvas_latitude ) ) )  AS jarak from kanvas c  where c.kanvas_status = 1
        ORDER BY jarak DESC limit 10","result");
            foreach($kanvasterdekatarr as $knvs){
              $kanvasterdekat =  $knvs;
            }


        //$totalmeter = $this->input->post('totalmeter');
      $ongkir = $this->mymodel->withquery("select * from ongkir where ongkir_id ='1'","row");
      
      if(!empty($kanvasterdekat) && empty($cabangterdekat)){
        $jarak = 0;
        $ongkir_meter = 0;
        $ongkir = 0;
      }else if(empty($kanvasterdekat) && !empty($cabangterdekat)){
        $jarak = round($cabangterdekat->jarak,1);
        $ongkir_meter = $ongkir->ongkir_per_meter;
        $ongkir = $ongkir->ongkir_harga;
      }else{
        if($cabangterdekat->jarak >= $kanvasterdekat->jarak){
          $jarak = round($kanvasterdekat->jarak,1);
          $ongkir_meter = $ongkir->ongkir_per_meter;
          $ongkir = $ongkir->ongkir_harga;
        }else{
          $jarak = 0;
          $ongkir_meter = 0;
          $ongkir = 0;
        }
      }
        $total = round(($jarak) * $ongkir->ongkir_harga);
        $status = "200";
        $val = array("ongkir_per_meter" => "".$ongkir_meter,
               "ongkir_harga" => "".$ongkir,
               "ongkir_total_meter" => "".$jarak,
               "ongkir_total_harga" => "".$total);
        $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' ,'data' => $val);
        
              $status = "200";
        $this->response($msg,$status);
          }else {
              $status = "401";
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
        $this->response($msg,$status);
          }
      }else {
        $status = "401";
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg,$status);
      }
    }

}