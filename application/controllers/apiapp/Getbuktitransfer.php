<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
class Getbuktitransfer extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['x-token']))
        $token =  $headers['x-token'];

      if ($token != '') {
          $customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
          if (!empty($customer)) {			     

            if($this->input->post('jenis') == 1){

              $getData = $this->mymodel->getbywhere('transaksi_customer_canvas','transaksi_customer_canvas_id',$this->input->post('id_transaksi'),"row");
              if(empty($getData)){
                $databukti = new stdClass();
                $msg = array('status' => 0, 'message'=>'Data tidak ditemukan !!!', 'data' => $databukti);
              }else{
                $databukti = array(
                  "transaksi_bukti_trf" => base_url("/upload/buktitrf/".$getData->transaksi_bukti_trf),
                  "transaksi_an" => $getData->transaksi_an,
                  "transaksi_norek" => $getData->transaksi_norek,
                  "transaksi_jenisbank" => $getData->transaksi_jenisbank,
                  "transaksi_totalbayar" => $getData->transaksi_totalbayar
                );
                $msg = array('status' => 1, 'message'=>'Berhasil ambil data ', 'data' => $databukti);
              }
            }else{

              $getData = $this->mymodel->getbywhere('transaksi_customer_cabang','transaksi_customer_cabang_id',$this->input->post('id_transaksi'),"row");
                if(empty($getData)){
                  $databukti = new stdClass();
                  $msg = array('status' => 0, 'message'=>'Data tidak ditemukan !!! ', 'data' => $databukti);

                }else{
                  $databukti = array(
                    "transaksi_bukti_trf" => base_url("/upload/buktitrf/".$getData->transaksi_bukti_trf),
                    "transaksi_an" => $getData->transaksi_an,
                    "transaksi_norek" => $getData->transaksi_norek,
                    "transaksi_jenisbank" => $getData->transaksi_jenisbank,
                    "transaksi_totalbayar" => $getData->transaksi_totalbayar
                  );
                  $msg = array('status' => 1, 'message'=>'Berhasil ambil data ', 'data' => $databukti);
                }
            }
          }else {
              $status = "401";
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
          }
          $this->response($msg,$status);
      }else {
        $status = "401";
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong');
        $this->response($msg,$status);
      }
    }

}