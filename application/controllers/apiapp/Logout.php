<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Logout extends REST_Controller {

  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {

        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   
	    if ($token != '') {
	    	$id =  $this->post('id');
	    	if($id == 1){
	    		$customer = $this->mymodel->getbywhere('customer','customer_token',$token,"row");
				  if (!empty($customer)) {
					   
						  $data = array( 
							  "customer_fcm_id" => NULL,
							  "is_login" => 0,
							);
						  $this->mymodel->update('customer',$data,"customer_id",$customer->customer_id);
					  
					  
					  $status = "200";
					  $msg = array('status' => 1, 'message'=>'Berhasil update data' ,'data' => array());
				  }else {
					  $status = "200";
					  $msg = array('status' => 0, 'message'=>'Data Tidak Ditemukan ', 'data' => new stdClass());
				  }
	    	}
	    	if($id == 2){
	    		$cabang = $this->mymodel->getbywhere('cabang','cabang_token',$token,"row");
				  if (!empty($cabang)) {
					   
					   
						  $data = array( 
							  "cabang_fcm_id" => NULL,
							  "is_login" => 0,
							);
						  $this->mymodel->update('cabang',$data,"cabang_id",$cabang->cabang_id);
					  
					  
					  $status = "200";
					  $msg = array('status' => 1, 'message'=>'Berhasil update data' ,'data' => array());
				  }else {
					  $status = "200";
					  $msg = array('status' => 0, 'message'=>'Data Tidak Ditemukan ', 'data' => new stdClass());
				  }
	    	}
	    	if($id == 3){
	    		$pelanggang = $this->mymodel->getbywhere('pelanggang','pelanggang_token',$token,"row");

				  if (!empty($cabang)) {
					   
						  $data = array( 
							  "pelanggang_fcm_id" => NULL,
							  "is_login" => 0,
							);
						  $this->mymodel->update('pelanggang',$data,"pelanggang_id",$pelanggang->pelanggang_id);
					  
					  
					  $status = "200";
					  $msg = array('status' => 1, 'message'=>'Berhasil update data' ,'data' => array());
				  }else {
					  $status = "200";
					  $msg = array('status' => 0, 'message'=>'Data Tidak Ditemukan ', 'data' => new stdClass());
				  }

	    	}
	    	if($id == 4){
	    		$umkm = $this->mymodel->getbywhere('umkm','umkm_token',$token,"row");
				  if (!empty($umkm)) {
					   
						  $data = array( 
							  "umkm_fcm_id" => NULL,
							  "is_login" => 0,
							);
						  $this->mymodel->update('umkm',$data,"umkm_id",$umkm->umkm_id);
					  
					  
					  $status = "200";
					  $msg = array('status' => 1, 'message'=>'Berhasil update data' ,'data' => array());
				  }else {
					  $status = "200";
					  $msg = array('status' => 0, 'message'=>'Data Tidak Ditemukan ', 'data' => new stdClass());
				  }

	    	}
	    	if($id == 5){
	    		$kanvas = $this->mymodel->getbywhere('kanvas','kanvas_token',$token,"row");
				  if (!empty($kanvas)) {
					   
						  $data = array( 
							  "kanvas_fcm_id" => NULL,
							  "is_login" => 0,
							);
						  $this->mymodel->update('kanvas',$data,"kanvas_id",$kanvas->kanvas_id);
					  
					  
					  $status = "200";
					  $msg = array('status' => 1, 'message'=>'Berhasil update data' ,'data' => array());
				  }else {
					  $status = "200";
					  $msg = array('status' => 0, 'message'=>'Data Tidak Ditemukan ', 'data' => new stdClass());
				  }

	    	}
	    	
	    	if($id == 6){
	    		$kurir = $this->mymodel->getbywhere('kurir','kurir_token',$token,"row");
	    		
				  if (!empty($kurir)) {
					   
						  $data = array( 
							  "kanvas_fcm_id" => NULL,
							  "is_login" => 0,
							);
					   
						  $this->mymodel->update('kurir',$data,"kurir_id",$kurir->kurir_id);
					  
					  
					  $status = "200";
					  $msg = array('status' => 1, 'message'=>'Berhasil update data' ,'data' => array());
				  }else {
					  $status = "200";
					  $msg = array('status' => 0, 'message'=>'Data Tidak Ditemukan ', 'data' => new stdClass());
				  }

	    	}
		  
		  $this->response($msg,$status);
	   }else {
			 $status = "200";
			 $data = array();
			 $msg = array('status' => 0, 'message'=>'Token anda kosong');
			 $this->response($msg,$status);
		   }     
    }
	  
  }
?>
