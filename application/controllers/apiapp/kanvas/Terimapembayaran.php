<?php

header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');
define( 'API_ACCESS_KEY', 'AAAAWyu_Puo:APA91bEbUprYzMYA4yV_RVzdsdQozDOEG1KcG-R1IVBvYn13UraC0Weu20vE37c-e1BDME9P-u9ap-70OERDxg7gzrVSGmWq__RRAnefzz02z-WyA92veVFm_Uj6GrNr236MvldG7wIe' );

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Terimapembayaran extends REST_Controller {

   
  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {
      
        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   

      	if ($token != '') {
          $kanvas = $this->mymodel->getbywhere('kanvas','kanvas_token',$token,'row');

          	if (!empty($kanvas)) {
			  
			    $databukti = array(
				   "transaksi_status" => 2);
				   
			  	if($this->input->post('jenis_transaksi') == "1"){
					$this->mymodel->update('transaksi_customer_canvas',$databukti,'transaksi_customer_canvas_id',$this->input->post('id_transaksi'));

				    //cek detail transaksi
					$cektransaksi = $this->mymodel->getbywhere("transaksi_customer_canvas", "transaksi_customer_canvas_id", $this->input->post('id_transaksi'), "row");
					$cektransaksiDetail = $this->mymodel->withquery("select * from transaksi_detail where transaksi_jenis=".$this->input->post('jenis_transaksi')." AND transaksi_id=".$this->input->post('id_transaksi')." ","result");
					$produkarr = [];
					$sisastokcanvas = [];
		  			$idstokcanvas = [];
		  			$totProdukcanvas = 0;
					foreach ($cektransaksiDetail as $key => $value) {
						$cekstok = $this->mymodel->withquery("select * from produk_per_canvas where produk_per_canvas_produkid = '".$value->transaksi_detail_product_id."' and produk_per_canvas_canvasid = '".$cektransaksi->transaksi_canvas_id."'","row");

						$totalprodukcanvas = $this->mymodel->withquery("select * from produk_per_canvas where produk_per_canvas_produkid = '".$value->transaksi_detail_product_id."'   and produk_per_canvas_canvasid = '".$cektransaksi->transaksi_canvas_id."' ","result");


						$totProdukcanvas += count($totalprodukcanvas);

						$sisastokcanvas[] = $cekstok->produk_per_canvas_stok - $value->transaksi_detail_qty;

				  		$idstokcanvas[] = $cekstok->produk_per_canvasid;
				  		$produkarr[] = $value->transaksi_detail_product_id;

					}

					$input = preg_quote('-', '~');

					$results_array_canvas = preg_grep('~' . $input . '~', $sisastokcanvas);
					
					if($totProdukcanvas < count($data_list_produk_canvas)){
					  	$status = "200";
						  $msg = array('status' => 0, 'message'=>'Barang Tidak tersedia','data'=>array());
					}elseif($results_array_canvas){
					  	$status = "200";
						  $msg = array('status' => 0, 'message'=>'Stok Tidak tersedia','data'=>array());
					}else{
						
						//mengurangi stok
						for($i = 0; $i < count($produkarr); $i++){
							$datastokcanvasupdate = array("produk_per_canvas_stok" => $sisastokcanvas[$i]);
							$this->mymodel->update('produk_per_canvas',$datastokcanvasupdate,'produk_per_canvasid',$idstokcanvas[$i]);
						}
				    	//cek data customer
						

					    //cek data customer
						$cektransaksi = $this->mymodel->getbywhere("transaksi_customer_canvas", "transaksi_customer_canvas_id", $this->input->post('id_transaksi'), "row");
						$cekfcmcustomer = $this->mymodel->getbywhere("customer", "customer_id", $cektransaksi->transaksi_customer_id, "row");

						if (!empty($cekfcmcustomer->customer_fcm_id)) {
							$this->send_notif("Pembayaran diterima!!!", "Transaksi anda " . $cektransaksi->transaksi_no_tracking . " ", $cekfcmcustomer->customer_fcm_id, array('title' => "Pembayaran diterima!!!", 'message' => "Transaksi anda " .$cektransaksi->transaksi_no_tracking . " ", 'tipe' => 'detail_transaksi', 'content' => array("transaksi_customer_canvas_id " => $cektransaksi->transaksi_customer_canvas_id , "no_tracking" => $cektransaksi->transaksi_no_tracking)));
						}

						$msg = array('status' => 1, 'message'=>'Pembayaran Berhasil Diterima');
					}

					$this->response($msg);
				}
				
		  	}else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ');
		  	}
          	$this->response($msg);
      	}else {
        	$data = array();
        	$msg = array('status' => 0, 'message'=>'Token anda kosong');
        	$this->response($msg);
      	}
     	$this->response($msg);
    }

    public function send_notif($title, $desc, $id_fcm, $data)
    {
        $Msg = array(
            'body' => $desc,
            'title' => $title
        );

        $fcmFields = array(
	      'to' => $id_fcm,
	      'notification' => $Msg,
	      'data'=>$data
	    );
	    $headers = array(
	      'Authorization: key=' . API_ACCESS_KEY,
	      'Content-Type: application/json'
	    );
	    $ch = curl_init();
	    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
	    curl_setopt( $ch,CURLOPT_POST, true );
	    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
	    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
	    $result = curl_exec($ch );
	    curl_close( $ch );

	    $cek_respon = explode(',',$result);
	    $berhasil = substr($cek_respon[1],strpos($cek_respon[1],':')+1);
	    
        //echo $result."\n\n";
    }
}
?>
