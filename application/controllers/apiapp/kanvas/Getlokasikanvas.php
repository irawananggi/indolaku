<?php

header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Getlokasikanvas extends REST_Controller {

   
  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_get() {
      
        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   

      if ($token != '') {
      		$token_kanvas = $this->get("token_kanvas"); 
      		
          	$getdata = $this->mymodel->getbywhere('kanvas','kanvas_token',$token_kanvas,'row');
          	$data = array(
						 "kanvas_latitude" => $getdata->kanvas_latitude,
						 "kanvas_longitude" => $getdata->kanvas_longitude
						);

				   $msg = array('status' => 1, 'message'=>'Lokasi berhasil ambil', 'data'=>$data);
				
          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong', 'data' => new stdClass());
        $this->response($msg);
      }

      $this->response($msg);
    }
}
?>
