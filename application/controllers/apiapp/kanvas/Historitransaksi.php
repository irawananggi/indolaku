<?php
header("Access-Control-Allow-Origin: *");
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');
class Historitransaksi extends REST_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
      $status = "";
      $token = "";
      $headers=array();
      foreach (getallheaders() as $name => $value) {
          $headers[$name] = $value;
      }
      if(isset($headers['x-token']))
        $token =  $headers['x-token'];

      if ($token != '') {
        $kanvas = $this->mymodel->getbywhere('kanvas','kanvas_token',$token,'row');
        
        if (!empty($kanvas)) {
          $url_bkttrs = base_url('upload/buktitrf/');
        $dtranskanvas = $this->mymodel->withquery("select *,(SELECT CASE WHEN transaksi_bukti_trf IS NULL OR transaksi_bukti_trf = '' THEN '' ELSE CONCAT('".$url_bkttrs."', transaksi_bukti_trf) END) as transaksi_bukti_trf,  0 as kurir from transaksi_customer_canvas tcc,
                   kanvas c, customer cs, transaksi_status ts where 
             tcc.transaksi_canvas_id = '".$kanvas->kanvas_id."' and
             c.kanvas_id = tcc.transaksi_canvas_id and 
             cs.customer_id = tcc.transaksi_customer_id and 
             tcc.transaksi_status = ts.transaksi_status_id order by tcc.transaksi_tanggal DESC","result");
        foreach ($dtranskanvas as $key => $value) {
              $detailtrans = $this->mymodel->withquery("select transaksi_detail_product_id,transaksi_detail_qty from transaksi_detail where transaksi_id = '".$value->transaksi_customer_canvas_id."'  AND transaksi_jenis='1' ","result");
               $value->produk = $detailtrans;
               foreach ($value->produk as $key2 => $value2) {
                  $dataproduk = $this->mymodel->withquery("select * from produk where produk_id = '".$value2->transaksi_detail_product_id."'","row");
                  $value2->produk_id = $dataproduk->produk_id;
                  $value2->produk_nama = $dataproduk->produk_nama;
                  $value2->produk_kategori = $dataproduk->produk_kategori;
                  $value2->produk_foto = base_url('uploads/produk/'.$dataproduk->produk_foto);
                  $value2->produk_harga = $dataproduk->produk_harga;
                  $value2->produk_harga_promo = $dataproduk->produk_harga_promo;
                  $value2->produk_total_view = $dataproduk->produk_total_view;
                  $value2->qty = $value2->transaksi_detail_qty;
                unset($value2->transaksi_detail_product_id);
               }


/*
            $cek_produk_detail = $this->mymodel->withquery("select * from transaksi_detail where transaksi_id  ='".$value->transaksi_customer_canvas_id ."' and  transaksi_jenis='1' ","result");
            foreach ($cek_produk_detail as $key2 => $value2) {
              $value->produk =  $this->mymodel->withquery("select * from produk where produk_id = '".$value2->transaksi_detail_product_id."'","result");

              foreach ($value->produk as $key3 => $value3) {
                // code...
                $value3->produk_foto= base_url('uploads/produk/'.$value3->produk_foto);
                $value3->qty = $this->mymodel->withquery("select * from transaksi_detail where transaksi_id  ='".$value->transaksi_customer_canvas_id."' and transaksi_detail_product_id = '".$value3->produk_id."' and  transaksi_jenis='1' ","row")->transaksi_detail_qty;
              }
            }*/
        }
        $status = "200";
        $msg = array('status' => 1, 'message'=>'Berhasil Ambil Data' , 'data' => $dtranskanvas);
        
           
          }else {
              $status = "401";
              $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => array());
          }

          $this->response($msg,$status);
      }else {
        $status = "401";
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong', 'data' => array());
        $this->response($msg,$status);
      }
    }

}