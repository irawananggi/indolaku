<?php

header('Access-Control-Allow-Origin: *');
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . 'libraries/REST_Controller.php';
ob_start();
class Updatelokasi extends REST_Controller {

   
  function __construct($config = 'rest') {
      parent::__construct($config);
  }

  function index_post() {
      
        $status = "";
		$token = "";
		$headers=array();
		foreach (getallheaders() as $name => $value) {
			$headers[$name] = $value;
		}
		if(isset($headers['x-token']))
		   $token =  $headers['x-token'];
	   

      if ($token != '') {
          $kanvas = $this->mymodel->getbywhere('kanvas','kanvas_token',$token,'row');

          if (!empty($kanvas)) {
			    $data = array(
				   "kanvas_longitude" => $this->post("kanvas_longitude"),
				   "kanvas_latitude" => $this->post("kanvas_latitude"));
				   
				    $this->mymodel->update('kanvas',$data,'kanvas_token',$token);
					$kanvas = $this->mymodel->getbywhere('kanvas','kanvas_token',$token,'row');
					   
				   $msg = array('status' => 1, 'message'=>'Lokasi berhasil diupdate', 'data'=>$kanvas);
				
		  }else {
			  $status = "401";
			  $msg = array('status' => 0, 'message'=>'Token Tidak Ditemukan ', 'data' => new stdClass());
		  }
          $this->response($msg);
      }else {
        $data = array();
        $msg = array('status' => 0, 'message'=>'Token anda kosong', 'data' => new stdClass());
        $this->response($msg);
      }

      $this->response($msg);
    }
}
?>
