<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Invoice</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <style>
        @page {
            size: A5
        }

        @page {
            margin: 0
        }

        body {
            margin: 0
        }

        .sheet {
            margin: 0;
            position: relative;
            box-sizing: border-box;
            page-break-after: always;
        }

        /** Paper sizes **/
        body.A3 .sheet {
            width: 297mm;
            height: 419mm
        }

        body.A3.landscape .sheet {
            width: 420mm;
            height: 296mm
        }

        body.A4 .sheet {
            width: 210mm;
            height: 296mm
        }

        body.A4.landscape .sheet {
            width: 297mm;
            height: 209mm
        }

        body.A5 .sheet {
            width: 148mm;
            height: 209mm
        }

        body.A5.landscape .sheet {
            width: 210mm;
            height: 147mm
        }

        body.letter .sheet {
            width: 216mm;
            height: 279mm
        }

        body.letter.landscape .sheet {
            width: 280mm;
            height: 215mm
        }

        body.legal .sheet {
            width: 216mm;
            height: 356mm
        }

        body.legal.landscape .sheet {
            width: 357mm;
            height: 215mm
        }

        /** Padding area **/
        .sheet.padding-10mm {
            padding: 10mm
        }

        .sheet.padding-15mm {
            padding: 15mm
        }

        .sheet.padding-20mm {
            padding: 20mm
        }

        .sheet.padding-25mm {
            padding: 25mm
        }

        .container {
            background: #fff;
            padding: 10px 15px;
            margin-bottom: 10px;
        }
            .sheet {
                background: #f8f9fa;
                font-family: 'Poppins', sans-serif;
            }

            body.A3.landscape {
                width: 420mm
            }

            body.A3,
            body.A4.landscape {
                width: 297mm
            }

            body.A4,
            body.A5.landscape {
                width: 210mm
            }

            body.A5 {
                width: 148mm
            }

            body.letter,
            body.legal {
                width: 216mm
            }

            body.letter.landscape {
                width: 280mm
            }

            body.legal.landscape {
                width: 357mm
            }

            .text-center {
                text-align: center;
            }

            .text-right {
                text-align: right;
            }

            .table {
                width: 100%;
            }

            .table td {
                padding: 6px 0;
            }

            .table tfoot {
                border-top: 1px dashed #000;
            }

            .table tfoot td {
                padding-top: 10px;
                border-top: 1px dashed #000;
            }

            .m-0 {
                margin: 0;
            }

            .grey {
                color: #969595;
            }
    </style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->

<body class="A5">

    <!-- Each sheet element should have the class "sheet" -->
    <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
    <div class="A5 sheet padding-10mm">

        <!-- Write HTML just like a web page -->
        <div>
            <div class="text-center container" style="padding-top: 20px;padding-bottom:20px">
                <p class="m-0"><strong>Indolaku</strong></p>
                <p class="m-0"><?php echo $cabang->alamat;?></p>
                <p class="m-0">Telp. <?php echo $cabang->telp;?></p>
            </div>
            <div class="container">
                <p><strong>Tanggal Order</strong></p>
                <p class="grey"><?php echo $datatrans->transaksi_tanggal;?></p>
            </div>
            <div class="container">
                <p><strong>Ringkasan Belanja</strong></p>
                <table class="table">
                    <tbody>
                        <?php
                            $no=1;
                            foreach($detailtrans as $key => $value){
                                $data_produk = $this->mymodel->withquery("select * from produk where produk_id=$value->transaksi_detail_product_id","row");
                            ?>

                        <tr>
                            <td class="grey"><?php echo $data_produk->produk_nama;?></td>
                            <td class="grey"><?php echo $value->transaksi_detail_harga;?></td>
                            <td class="grey"><?php echo $value->transaksi_detail_qty;?></td>
                            <td class="text-right"><?php echo "Rp " . number_format($value->transaksi_detail_total,0,',','.');?></td>
                        </tr>

                        <?php $no++; }
                        ?>
                        <tr>
                            <td colspan="3" class="grey">Sub Total</td>
                            <td class="text-right"><?php echo "Rp " . number_format(($datatrans->transaksi_total- @$datatrans->transaksi_ongkir),0,',','.');?></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="grey">Ongkos Kirim</td>
                            <td class="text-right"><?php echo "Rp " . number_format(@$datatrans->transaksi_ongkir,0,',','.');?></td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3">Total</td>
                            <td class="text-right"><?php echo "Rp " . number_format($datatrans->transaksi_total,0,',','.');?></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="text-center" style="margin-top: 40px;">
                <img src="https://backend.indolaku.com/uploads/logo.jpg" width="70" alt="">
                <p>
                    Terimakasih atas kunjungan Anda. <br>
                    Barang yang sudah dibeli tidak bisa ditukar atau dikembalikan
                </p>
            </div>
        </div>

    </div>

</body>

</html>