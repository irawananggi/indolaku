<html>
<head>
    <title>Cetak PDF</title>
    <link href="https://getbootstrap.com/docs/5.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        table.content
{
    width: 1000px;
    border-collapse: collapse;
    padding: 0px;
    margin: 0px;
    font-family: Arial;
    font-size: 11px;
    line-height: 1.4;
}
        table.content2
{
    width: 100%;
    border-collapse: collapse;
    padding: 0px;
    margin: 0px;
    font-family: Arial;
    font-size: 11px;
    line-height: 1.4;
}

.content th
{
    /*border: solid 1px #ffffff;*/
    padding: 0px;
    text-align: left;
}
.content td
{
    /*border: solid 1px #ffffff;*/
    padding: 0px;
    text-align: left;
}


.content2 th
{
    /*border: solid 1px #cccccc;*/
    padding: 5px;
    text-align: center;
}
.content2 td
{
    /*border: solid 1px #cccccc;*/
    padding: 5px;
    
    text-align: left;
}

.content2 .row
{
    /*border-top: solid 1px #cccccc;*/
    padding: 5px;
}
    </style>
</head>
<body>
<h1 style="text-align: center;font-size: 12px;font-weight: bold;">INDOLAKU</h1>
    <h3 style="text-align: center;font-size: 8px;padding:0px;margin:0px"><?php echo $cabang->alamat;?></h3>
<br>
<br>
<br>

<h1 style="text-align: left;font-size: 11px;font-weight: bold;">Tanggal Order</h1>
<h3 style="text-align: left;font-size: 8px;padding:0px;margin:0px"><?php echo $datatrans->transaksi_tanggal;?> </h3>
<br>
<br>
<br>

<h1 style="text-align: left;font-size: 11px;font-weight: bold;">Ringkasan Belanja</h1>
<table class="content2" >
<!-- <tr>
    <th>Nama</th>
    <th>Harga</th>
    <th>Qty</th>
    <th>Sub Total</th>
    
</tr> -->
<?php
    $no=1;
    foreach($detailtrans as $key => $value){ 
        /*$tot = $this->mymodel->withquery("select * from transaksi where id_iuran=$value->id_iuran AND kode_transaksi=2 ORDER BY tgl_bayar DESC","row");

        $date=date_create($value->periode); */
        $data_produk = $this->mymodel->withquery("select * from produk where produk_id=$value->transaksi_detail_product_id","row");
    ?>
    <tr>
        <td style="width: 150px;"><?php echo $data_produk->produk_nama;?></td>
        <td style="width: 80px;"><?php echo $value->transaksi_detail_harga;?></td>
        <td style="width: 80px;"><?php echo $value->transaksi_detail_qty;?></td>
        <td style="text-align:right;width: 80px;"><?php echo "Rp " . number_format($value->transaksi_detail_total,0,',','.');?></td>
    </tr>

<?php $no++; }
?>

    <tr>
        <td style="width: 150px;" >Sub Total</td>
        <td style="width: 80px;"></td>
        <td style="width: 80px;"></td>
        <td style="text-align:right;width: 80px;"><?php echo "Rp " . number_format(($datatrans->transaksi_total- @$datatrans->transaksi_ongkir),0,',','.');?></td>
    </tr>
    <tr>
        <td style="width: 150px;" >Ongkos Kirim</td>
        <td style="width: 80px;"></td>
        <td style="width: 80px;"></td>
        <td style="text-align:right;width: 80px;"><?php echo "Rp " . number_format(@$datatrans->transaksi_ongkir,0,',','.');?></td>
    </tr>
    <tr>
        <td style="width: 150px;" >Total</td>
        <td style="width: 80px;"></td>
        <td style="width: 80px;"></td>
        <td style="text-align:right;width: 80px;border-top:2px dotted brown;"><?php echo "Rp " . number_format($datatrans->transaksi_total,0,',','.');?></td>
    </tr>
</table>
<br>
<br>
<br>

<h1 style="text-align: center;font-size: 12px;font-weight: bold;"><img src="https://backend.indolaku.com/uploads/logo.jpg" style="width:80px;height: auto;" /></h1>
    <h3 style="text-align: center;font-size: 8px;padding:0px;margin:0px">Terimakasih atas kunjungan anda</h3>
    <h3 style="text-align: center;font-size: 8px;padding:0px;margin:0px">Barang yang sudah dibeli tidak bisa di tukar atau</h3>
    <h3 style="text-align: center;font-size: 8px;padding:0px;margin:0px">dikembalikan</h3>


</body>
</html>